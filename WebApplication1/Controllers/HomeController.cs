using System.Web.Mvc;

namespace WebApplication1.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			return View();
		}

		public ActionResult Menu()
		{
			if (Request.Cookies["ASCruiseSafeUser"] != null)
			{
				return View();
			}
			return RedirectToAction("index", "home");
		}

		public HomeController()
			: base()
		{
		}
	}
}
