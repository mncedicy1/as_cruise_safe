using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
	public class OrganisationController : Controller
	{
		private as_cruise_safeEntities db = new as_cruise_safeEntities();

		public ActionResult Index()
		{
			return (ActionResult)(object)View();
		}

        public ActionResult Regions()
        {
            if (Request.Cookies["ASCruiseSafeUser"] != null)
            {
                return (ActionResult)(object)View();
            }
            return (ActionResult)(object)RedirectToAction("index", "home");
        }

        public ActionResult Department()
		{
			if (Request.Cookies["ASCruiseSafeUser"] != null)
			{
				return (ActionResult)(object)View();
			}
			return (ActionResult)(object)RedirectToAction("index", "home");
		}

		public ActionResult Team()
		{
			if (Request.Cookies["ASCruiseSafeUser"] != null)
			{
				return (ActionResult)(object)View();
			}
			return (ActionResult)(object)RedirectToAction("index", "home");
		}

		public ActionResult Suppliers()
		{
			if (Request.Cookies["ASCruiseSafeUser"] != null)
			{
				return (ActionResult)(object)View();
			}
			return (ActionResult)(object)RedirectToAction("index", "home");
		}

		[HttpGet]
		public JsonResult getUsers(int clientid)
		{
			IEnumerable<cruise_safe_user> users = db.Database.SqlQuery<cruise_safe_user>("select * from cruise_safe_user where user_client_id = '" + clientid + "'", Array.Empty<object>());
			return Json((object)users, (JsonRequestBehavior)0);
		}

		[HttpGet]
		public JsonResult getUserRoles(int clientid)
		{
			IEnumerable<cruise_safe_role> roles = db.Database.SqlQuery<cruise_safe_role>("select * from cruise_safe_role where role_client_id = '" + clientid + "' and role_status != 'deleted'", Array.Empty<object>());
			return Json((object)new { roles }, (JsonRequestBehavior)0);
		}

		[HttpPost]
		public JsonResult activate(int userId, int clientid, string fullname, int system_user_id)
		{
			try
			{
				cruise_safe_user fetchRow = ((IQueryable<cruise_safe_user>)db.cruise_safe_user).Where((cruise_safe_user u) => u.user_id == system_user_id).FirstOrDefault();
				if (fetchRow != null)
				{
					fetchRow.user_system_status = "Active";
					fetchRow.user_saved_by = userId;
					((DbContext)db).SaveChanges();
					HomeController userHistory = new HomeController();
				}
				string successMessage = "User " + fullname + " is now active";
				return Json((object)new
				{
					message = successMessage
				});
			}
			catch (Exception ex)
			{
				string errorMessage = "Connection Error occured " + ex.Message;
				return Json((object)new
				{
					message = errorMessage
				});
			}
		}

		[HttpPost]
		public JsonResult deactivate(int userId, int clientid, string fullname, int system_user_id)
		{
			try
			{
				cruise_safe_user fetchRow = ((IQueryable<cruise_safe_user>)db.cruise_safe_user).Where((cruise_safe_user u) => u.user_id == system_user_id).FirstOrDefault();
				if (fetchRow != null)
				{
					fetchRow.user_system_status = "InActive";
					fetchRow.user_saved_by = userId;
					((DbContext)db).SaveChanges();
					HomeController userHistory = new HomeController();
				}
				string successMessage = "User " + fullname + " is now inactive";
				return Json((object)new
				{
					message = successMessage
				});
			}
			catch (Exception ex)
			{
				string errorMessage = "Connection Error occured " + ex.Message;
				return Json((object)new
				{
					message = errorMessage
				});
			}
		}

		public OrganisationController()
			: base()
		{
		}
	}
}
