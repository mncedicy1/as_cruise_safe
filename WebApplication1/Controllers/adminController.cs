using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ASencryption;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
	public class adminController : Controller
	{
		public class aa_fines_response
		{
			public string person_id { get; set; }

			public string fullname { get; set; }

			public string message { get; set; }

			public string result { get; set; }
		}

		public class system_user_person
		{
			public cruise_safe_user system_user { get; set; }

			public string results { get; set; }
		}

		private as_cruise_safeEntities db = new as_cruise_safeEntities();

		public static HttpCookie myCookie;

		public ActionResult Index()
		{
			return View();
		}

		[HttpGet]
		public JsonResult login(cruise_safe_user userr)
		{
			system_user_person user_person = new system_user_person();
			try
			{
				string username = userr.user_email;
				string password = userr.user_password;
				string device_id = userr.user_device_id;
				string today = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
				password = Encryption.encrypt(password);
				List<cruise_safe_user> user = ((IQueryable<cruise_safe_user>)db.cruise_safe_user).Where((cruise_safe_user x) => x.user_email == username && x.user_password == password).ToList();
				if (user.Count > 0)
				{
					user.First().user_session_counrty = userr.user_session_counrty;
					user.First().user_session_province = userr.user_session_province;
					user.First().user_session_city = userr.user_session_city;
					user.First().user_session_counrty_code = userr.user_session_counrty_code;
					user.First().user_session_province_code = userr.user_session_province_code;
					user.First().user_session_continent_code = userr.user_session_continent_code;
					user.First().user_session_continent = userr.user_session_continent;
					user.First().user_session_latitude = userr.user_session_latitude;
					user.First().user_session_longitude = userr.user_session_longitude;
					user.First().user_device_id = userr.user_device_id;
					user.First().user_last_login_date = DateTime.Now;
					user.First().user_session_ip = userr.user_session_ip;
					user.First().user_session_local_ip = userr.user_session_local_ip;
					user.First().user_session_id = string.Concat("SESSION" + user.First().user_id, DateTime.Now.ToBinary().ToString());
					((DbContext)db).SaveChanges();
					user_person.system_user = user.First();
					if (user.First().user_system_status == "InActive")
					{
						user_person.results = "Your account is not active, please contact your administrator.";
					}
					else
					{
						user_person.results = "success";
						createSession(user_person);
					}
				}
				else
				{
					int i = db.Database.ExecuteSqlCommand("update cruise_safe_user set user_login_attemps = (user_login_attemps+1) where user_email ='" + username + "'", Array.Empty<object>());
					user_person.results = "Incorrect login details";
				}
				return Json((object)user_person, (JsonRequestBehavior)0);
			}
			catch (Exception ex)
			{
				user_person.results = ex.Message;
				return Json((object)user_person, (JsonRequestBehavior)0);
			}
		}

		public string createSession(system_user_person u)
		{
			myCookie = new HttpCookie("ASCruiseSafeUser");
			myCookie.Values["clientid"] = u.system_user.user_client_id.ToString();
			myCookie.Values["userId"] = u.system_user.user_id.ToString();
			myCookie.Values["fullname"] = u.system_user.user_firstname + " " + u.system_user.user_surname;
			myCookie.Values["userRole"] = u.system_user.user_role_id.ToString();
			myCookie.Values["username"] = u.system_user.user_email;
			myCookie.Values["device_id"] = u.system_user.user_device_id;
			myCookie.Values["person_id"] = string.Concat(u.system_user.user_id);
			myCookie.Values["user_session_counrty"] = u.system_user.user_session_counrty;
			myCookie.Values["user_session_province"] = u.system_user.user_session_province;
			myCookie.Values["user_session_city"] = u.system_user.user_session_city;
			myCookie.Values["user_session_counrty_code"] = u.system_user.user_session_counrty_code;
			myCookie.Values["user_session_province_code"] = u.system_user.user_session_province_code;
			myCookie.Values["user_session_province_code"] = u.system_user.user_session_province_code;
			myCookie.Values["user_session_continent_code"] = u.system_user.user_session_continent_code;
			myCookie.Values["user_session_continent"] = u.system_user.user_session_continent;
			myCookie.Values["user_session_ip"] = u.system_user.user_session_ip;
			myCookie.Values["user_session_id"] = u.system_user.user_session_id;
			myCookie.Values["user_session_latitude"] = u.system_user.user_session_latitude;
			myCookie.Values["user_session_longitude"] = u.system_user.user_session_longitude;
            myCookie.Values["region_id"] = u.system_user.user_region_id.Value.ToString();
            myCookie.Values["region_name"] = u.system_user.user_region_name;
            myCookie.Expires = DateTime.Now.AddHours(24.0);
			Response.Cookies.Add(myCookie);
			Request.Cookies["ASCruiseSafeUser"].Expires = DateTime.Now.AddHours(24.0);
			return "Success";
		}

		[HttpGet]
		public JsonResult killSession(int userId)
		{
			string today = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
			int i = db.Database.ExecuteSqlCommand(("update cruise_safe_user set user_last_logout_date ='" + today + "' where user_id =" + userId) ?? "", Array.Empty<object>());
			HttpCookie currentUserCookie = Request.Cookies["ASCruiseSafeUser"];
			Response.Cookies.Remove("ASCruiseSafeUser");
			currentUserCookie.Expires = DateTime.Now.AddDays(-10.0);
			currentUserCookie.Value = null;
			Response.SetCookie(currentUserCookie);
			return Json((object)"logged out", (JsonRequestBehavior)0);
		}

		[HttpGet]
		public JsonResult chechSession(cruise_safe_user userr)
		{
			try
			{
				string username = userr.user_email;
				string user_session_id = userr.user_session_id;
				List<cruise_safe_user> user = ((IQueryable<cruise_safe_user>)db.cruise_safe_user).Where((cruise_safe_user x) => x.user_email == username && x.user_session_id == user_session_id).ToList();
				if (user.Count > 0)
				{
					return Json((object)true, (JsonRequestBehavior)0);
				}
				HttpCookie currentUserCookie = Request.Cookies["ASCruiseSafeUser"];
				Response.Cookies.Remove("ASCruiseSafeUser");
				currentUserCookie.Expires = DateTime.Now.AddDays(-10.0);
				currentUserCookie.Value = null;
				Response.SetCookie(currentUserCookie);
				return Json((object)false, (JsonRequestBehavior)0);
			}
			catch (Exception ex)
			{
				return Json((object)ex.Message, (JsonRequestBehavior)0);
			}
		}

		[HttpGet]
		public JsonResult getMyPrivs(string role)
		{
			int roleid = Convert.ToInt32(role);
			IQueryable<long?> privs = from x in (IQueryable<cruise_safe_role_right>)db.cruise_safe_role_right
				where x.role_id == (long?)(long)roleid
				select x.right_id;
			return Json((object)privs, (JsonRequestBehavior)0);
		}

		[HttpGet]
		public JsonResult getPrivileges()
		{
			List<cruise_safe_right> data = ((IQueryable<cruise_safe_right>)db.cruise_safe_right).Select((cruise_safe_right s) => s).ToList();
			return Json((object)data, (JsonRequestBehavior)0);
		}

		[HttpPost]
		public JsonResult addPrivilege(int userId, int clientid, string userType, string clientType, string privilege_name, string description)
		{
			try
			{
				if (((IQueryable<cruise_safe_right>)db.cruise_safe_right).Where((cruise_safe_right u) => u.right_name == privilege_name).ToList().Exists((cruise_safe_right u) => u.right_name == privilege_name))
				{
					string errorMessage2 = "Privilege (" + privilege_name + ") already registered";
					return Json((object)new
					{
						ErrorMessage = errorMessage2
					});
				}
				cruise_safe_right privilege = new cruise_safe_right();
				privilege.right_source = "as-connect";
				privilege.right_name = privilege_name;
				privilege.right_description = description;
				db.cruise_safe_right.Add(privilege);
				((DbContext)db).SaveChanges();
				HomeController userHistory = new HomeController();
				string successMessage = "Added successfully";
				return Json((object)new
				{
					SuccessMessage = successMessage
				});
			}
			catch (Exception ex)
			{
				string errorMessage = "Connection Error occured " + ex.Message;
				return Json((object)new
				{
					ErrorMessage = errorMessage
				});
			}
		}

		[HttpPost]
		public JsonResult updatePrivilege(int userId, int clientid, string userType, string clientType, int privilege_id, string privilege_name, string description)
		{
			try
			{
				cruise_safe_right fetchRow = ((IQueryable<cruise_safe_right>)db.cruise_safe_right).Where((cruise_safe_right u) => u.right_id == (long)privilege_id).FirstOrDefault();
				if (fetchRow != null)
				{
					fetchRow.right_name = privilege_name;
					fetchRow.right_description = description;
					((DbContext)db).SaveChanges();
					HomeController userHistory2 = new HomeController();
					string successMessage = "Updated successfully";
					return Json((object)new
					{
						SuccessMessage = successMessage
					});
				}
				string errorMessage2 = "Failed to update";
				HomeController userHistory = new HomeController();
				return Json((object)new
				{
					ErrorMessage = errorMessage2
				});
			}
			catch (Exception ex)
			{
				string errorMessage = "Connection Error occured " + ex.Message;
				return Json((object)new
				{
					ErrorMessage = errorMessage
				});
			}
		}

		[HttpPost]
		public JsonResult removePrivilege(int userId, int clientid, string userType, string clientType, int privilege_id, string privilege_name)
		{
			try
			{
				cruise_safe_right fetchRow = ((IQueryable<cruise_safe_right>)db.cruise_safe_right).Where((cruise_safe_right u) => u.right_id == (long)privilege_id).FirstOrDefault();
				if (fetchRow != null)
				{
					db.cruise_safe_right.Remove(fetchRow);
					((DbContext)db).SaveChanges();
					int noOfRowInserted = db.Database.ExecuteSqlCommand("delete from cruise_safe_right where right_id = '" + privilege_id + "'", Array.Empty<object>());
					HomeController userHistory2 = new HomeController();
					string successMessage = "Removed successfully";
					return Json((object)new
					{
						SuccessMessage = successMessage
					});
				}
				string errorMessage2 = "Failed to update";
				HomeController userHistory = new HomeController();
				return Json((object)new
				{
					ErrorMessage = errorMessage2
				});
			}
			catch (Exception ex)
			{
				string errorMessage = "Connection Error occured " + ex.Message;
				return Json((object)new
				{
					ErrorMessage = errorMessage
				});
			}
		}

		[HttpGet]
		public JsonResult getRoles(int clientid)
		{
			IEnumerable<cruise_safe_role> roles = db.Database.SqlQuery<cruise_safe_role>("select * from cruise_safe_role where role_client_id = '" + clientid + "' and role_status != 'deleted'", Array.Empty<object>());
			IQueryable<cruise_safe_right> privs = ((IQueryable<cruise_safe_right>)db.cruise_safe_right).Select((cruise_safe_right y) => y);
			IQueryable<cruise_safe_role_right> privs_roles = ((IQueryable<cruise_safe_role_right>)db.cruise_safe_role_right).Select((cruise_safe_role_right xy) => xy);
			return Json((object)new { roles, privs, privs_roles }, (JsonRequestBehavior)0);
		}

		[HttpPost]
		public JsonResult addRole(int userId, int clientid, string role_name, string desc, string privis)
		{
			try
			{
				if (((IQueryable<cruise_safe_role>)db.cruise_safe_role).Where((cruise_safe_role u) => u.role_name == role_name && u.role_client_id == (int?)clientid).ToList().Exists((cruise_safe_role u) => u.role_name == role_name))
				{
					string errorMessage2 = "Role (" + role_name + ") already registered";
					return Json((object)new
					{
						ErrorMessage = errorMessage2
					});
				}
				cruise_safe_role role = new cruise_safe_role();
				role.role_name = role_name;
				role.role_description = desc;
				role.role_client_id = clientid;
				role.role_last_update = DateTime.Now;
				role.role_status = "active";
				role.role_timestamp = DateTime.Now;
				role.role_updated_by = userId;
				db.cruise_safe_role.Add(role);
				((DbContext)db).SaveChanges();
				List<cruise_safe_role> data = db.Database.SqlQuery<cruise_safe_role>("select * from cruise_safe_role order by role_id desc", Array.Empty<object>()).ToList();
				string roleId = data.First().role_id.ToString();
				int noOf = 0;
				string[] arr = privis.Substring(0, privis.Length - 1).Split('@');
				string[] array = arr;
				foreach (string item in array)
				{
					noOf = db.Database.ExecuteSqlCommand("insert into cruise_safe_role_right(role_id,right_id)  values(" + roleId + "," + item + ")", Array.Empty<object>());
				}
				string successMessage = "Added successfully";
				return Json((object)new
				{
					SuccessMessage = successMessage
				});
			}
			catch (Exception ex)
			{
				string errorMessage = "Connection Error occured " + ex.Message;
				return Json((object)new
				{
					ErrorMessage = errorMessage
				});
			}
		}

		[HttpPost]
		public JsonResult updateRole(int userId, int clientid, int role_id, string role_name, string desc, string privis)
		{
			try
			{
				cruise_safe_role fetchRow = ((IQueryable<cruise_safe_role>)db.cruise_safe_role).Where((cruise_safe_role u) => u.role_id == role_id).FirstOrDefault();
				if (fetchRow != null)
				{
					if (fetchRow.role_type == "default" && fetchRow.role_name != role_name)
					{
						return Json((object)new
						{
							ErrorMessage = "This record is locked! You are not allowed to change name!!"
						});
					}
					fetchRow.role_name = role_name;
					fetchRow.role_description = desc;
					fetchRow.role_last_update = DateTime.Now;
					fetchRow.role_updated_by = userId;
					((DbContext)db).SaveChanges();
					int noOf = db.Database.ExecuteSqlCommand("delete from cruise_safe_role_right where role_id = '" + role_id + "'", Array.Empty<object>());
					int noOfInsert = 0;
					string[] arr = privis.Substring(0, privis.Length - 1).Split('@');
					string[] array = arr;
					foreach (string item in array)
					{
						noOfInsert += db.Database.ExecuteSqlCommand("insert into cruise_safe_role_right(role_id,right_id) values(" + role_id + "," + item + ")", Array.Empty<object>());
					}
					string successMessage = "Updated successfully";
					return Json((object)new
					{
						SuccessMessage = successMessage
					});
				}
				string errorMessage2 = "Failed to update";
				return Json((object)new
				{
					ErrorMessage = errorMessage2
				});
			}
			catch (Exception ex)
			{
				string errorMessage = "Connection Error occured " + ex.Message;
				return Json((object)new
				{
					ErrorMessage = errorMessage
				});
			}
		}

		[HttpPost]
		public JsonResult removeRole(int userId, int clientid, int role_id, string role_name)
		{
			try
			{
				cruise_safe_role fetchRow = ((IQueryable<cruise_safe_role>)db.cruise_safe_role).Where((cruise_safe_role u) => u.role_id == role_id).FirstOrDefault();
				if (fetchRow != null)
				{
					if (fetchRow.role_type == "default")
					{
						return Json((object)new
						{
							ErrorMessage = "This record is locked! You are not allowed to remove it!!"
						});
					}
					fetchRow.role_status = "deleted";
					fetchRow.role_last_update = DateTime.Now;
					fetchRow.role_updated_by = userId;
					((DbContext)db).SaveChanges();
					string successMessage = "Removed successfully";
					return Json((object)new
					{
						SuccessMessage = successMessage
					});
				}
				string errorMessage2 = "Failed to update";
				HomeController userHistory = new HomeController();
				return Json((object)new
				{
					ErrorMessage = errorMessage2
				});
			}
			catch (Exception ex)
			{
				string errorMessage = "Connection Error occured " + ex.Message;
				return Json((object)new
				{
					ErrorMessage = errorMessage
				});
			}
		}

		[HttpGet]
		public JsonResult getUsers(int clientid)
		{
			IEnumerable<cruise_safe_user> users = db.Database.SqlQuery<cruise_safe_user>("select * from cruise_safe_user where user_client_id = '" + clientid + "'", Array.Empty<object>());
			return Json((object)users, (JsonRequestBehavior)0);
		}

		[HttpGet]
		public JsonResult getUserRoles(int clientid)
		{
			IEnumerable<cruise_safe_role> roles = db.Database.SqlQuery<cruise_safe_role>("select * from cruise_safe_role where role_client_id = '" + clientid + "' and role_status != 'deleted'", Array.Empty<object>());
			return Json((object)new { roles }, (JsonRequestBehavior)0);
		}





		[HttpPost]
		public JsonResult activate(int userId, int clientid, string fullname, int system_user_id)
		{
			try
			{
				cruise_safe_user fetchRow = ((IQueryable<cruise_safe_user>)db.cruise_safe_user).Where((cruise_safe_user u) => u.user_id == system_user_id).FirstOrDefault();
				if (fetchRow != null)
				{
					fetchRow.user_system_status = "Active";
					fetchRow.user_saved_by = userId;
					((DbContext)db).SaveChanges();
					HomeController userHistory = new HomeController();
				}
				string successMessage = "User " + fullname + " is now active";
				return Json((object)new
				{
					message = successMessage
				});
			}
			catch (Exception ex)
			{
				string errorMessage = "Connection Error occured " + ex.Message;
				return Json((object)new
				{
					message = errorMessage
				});
			}
		}

		[HttpPost]
		public JsonResult deactivate(int userId, int clientid, string fullname, int system_user_id)
		{
			try
			{
				cruise_safe_user fetchRow = ((IQueryable<cruise_safe_user>)db.cruise_safe_user).Where((cruise_safe_user u) => u.user_id == system_user_id).FirstOrDefault();
				if (fetchRow != null)
				{
					fetchRow.user_system_status = "InActive";
                    fetchRow.user_session_ip = "";
                    fetchRow.user_session_local_ip = "";
                    fetchRow.user_session_id = "";
                    fetchRow.user_saved_by = userId;
					((DbContext)db).SaveChanges();
					HomeController userHistory = new HomeController();
				}
				string successMessage = "User " + fullname + " is now inactive";
				return Json((object)new
				{
					message = successMessage
				});
			}
			catch (Exception ex)
			{
				string errorMessage = "Connection Error occured " + ex.Message;
				return Json((object)new
				{
					message = errorMessage
				});
			}
		}










        [HttpPost]
        public JsonResult activateDept(int userId, int clientid,string fullname, int department_id)
        {
            try
            {
                cruise_safe_organo_department fetchRow = ((IQueryable<cruise_safe_organo_department>)db.cruise_safe_organo_department).Where((cruise_safe_organo_department u) => u.department_id == department_id).FirstOrDefault();
                if (fetchRow != null)
                {
                    fetchRow.department_status = "active";
                    fetchRow.department_updated_by = userId;
                    ((DbContext)db).SaveChanges();
                    HomeController userHistory = new HomeController();
                }
                string successMessage = "Department " + fullname + " is now active";
                return Json((object)new
                {
                    message = successMessage
                });
            }
            catch (Exception ex)
            {
                string errorMessage = "Connection Error occured " + ex.Message;
                return Json((object)new
                {
                    message = errorMessage
                });
            }
        }



        [HttpPost]
        public JsonResult deactivateDept(int userId, int clientid, string fullname, int department_id)
        {
            try
            {
                cruise_safe_organo_department fetchRow = ((IQueryable<cruise_safe_organo_department>)db.cruise_safe_organo_department).Where((cruise_safe_organo_department u) => u.department_id == department_id).FirstOrDefault();
                if (fetchRow != null)
                {
                    fetchRow.department_status = "inactive";
                    fetchRow.department_updated_by = userId;
                    ((DbContext)db).SaveChanges();
                    HomeController userHistory = new HomeController();
                }
                string successMessage = "Department " + fullname + " is now inactive";
                return Json((object)new
                {
                    message = successMessage
                });
            }
            catch (Exception ex)
            {
                string errorMessage = "Connection Error occured " + ex.Message;
                return Json((object)new
                {
                    message = errorMessage
                });
            }
        }









        [HttpPost]
        public JsonResult activateTeam(int userId, int clientid, string fullname, int team_id)
        {
            try
            {
                cruise_safe_organo_team fetchRow = ((IQueryable<cruise_safe_organo_team>)db.cruise_safe_organo_team).Where((cruise_safe_organo_team u) => u.team_id == team_id).FirstOrDefault();
                if (fetchRow != null)
                {
                    fetchRow.team_status = "active";
                    fetchRow.team_updated_by = userId;
                    ((DbContext)db).SaveChanges();
                    HomeController userHistory = new HomeController();
                }
                string successMessage = "Team " + fullname + " is now active";
                return Json((object)new
                {
                    message = successMessage
                });
            }
            catch (Exception ex)
            {
                string errorMessage = "Connection Error occured " + ex.Message;
                return Json((object)new
                {
                    message = errorMessage
                });
            }
        }



        [HttpPost]
        public JsonResult deactivateTeam(int userId, int clientid, string fullname, int team_id)
        {
            try
            {
                cruise_safe_organo_team fetchRow = ((IQueryable<cruise_safe_organo_team>)db.cruise_safe_organo_team).Where((cruise_safe_organo_team u) => u.team_id == team_id).FirstOrDefault();
                if (fetchRow != null)
                {
                    fetchRow.team_status = "inactive";
                    fetchRow.team_updated_by = userId;
                    ((DbContext)db).SaveChanges();
                    HomeController userHistory = new HomeController();
                }
                string successMessage = "Team " + fullname + " is now inactive";
                return Json((object)new
                {
                    message = successMessage
                });
            }
            catch (Exception ex)
            {
                string errorMessage = "Connection Error occured " + ex.Message;
                return Json((object)new
                {
                    message = errorMessage
                });
            }
        }














        [HttpPost]
        public JsonResult activateSupp(int userId, int clientid, string fullname, int supplier_id)
        {
            try
            {
                cruise_safe_organo_supplier fetchRow = ((IQueryable<cruise_safe_organo_supplier>)db.cruise_safe_organo_supplier).Where((cruise_safe_organo_supplier u) => u.supplier_id == supplier_id).FirstOrDefault();
                if (fetchRow != null)
                {
                    fetchRow.supplier_status = "active";
                    fetchRow.supplier_updated_by = userId;
                    ((DbContext)db).SaveChanges();
                    HomeController userHistory = new HomeController();
                }
                string successMessage = "Supplier " + fullname + " is now active";
                return Json((object)new
                {
                    message = successMessage
                });
            }
            catch (Exception ex)
            {
                string errorMessage = "Connection Error occured " + ex.Message;
                return Json((object)new
                {
                    message = errorMessage
                });
            }
        }



        [HttpPost]
        public JsonResult deactivateSupp(int userId, int clientid, string fullname, int supplier_id)
        {
            try
            {
                cruise_safe_organo_supplier fetchRow = ((IQueryable<cruise_safe_organo_supplier>)db.cruise_safe_organo_supplier).Where((cruise_safe_organo_supplier u) => u.supplier_id == supplier_id).FirstOrDefault();
                if (fetchRow != null)
                {
                    fetchRow.supplier_status = "inactive";
                    fetchRow.supplier_updated_by = userId;
                    ((DbContext)db).SaveChanges();
                    HomeController userHistory = new HomeController();
                }
                string successMessage = "Supplier " + fullname + " is now inactive";
                return Json((object)new
                {
                    message = successMessage
                });
            }
            catch (Exception ex)
            {
                string errorMessage = "Connection Error occured " + ex.Message;
                return Json((object)new
                {
                    message = errorMessage
                });
            }
        }












        public adminController()
			: base()
		{
		}
	}
}
