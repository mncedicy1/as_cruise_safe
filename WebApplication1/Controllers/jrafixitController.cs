using System.Web.Mvc;

namespace WebApplication1.Controllers
{
	public class jrafixitController : Controller
	{
		public ActionResult home()
		{
			if (Request.Cookies["ASCruiseSafeUser"] != null)
			{
				return View();
			}
			return RedirectToAction("index", "home");
		}

		public ActionResult privileges()
		{
			if (Request.Cookies["ASCruiseSafeUser"] != null)
			{
				return View();
			}
			return RedirectToAction("index", "home");
		}

		public ActionResult modules()
		{
			if (Request.Cookies["ASCruiseSafeUser"] != null)
			{
				return View();
			}
			return RedirectToAction("index", "home");
		}

		public ActionResult roles()
		{
			if (Request.Cookies["ASCruiseSafeUser"] != null)
			{
				return View();
			}
			return RedirectToAction("index", "home");
		}

		public ActionResult users()
		{
			if (Request.Cookies["ASCruiseSafeUser"] != null)
			{
				return View();
			}
			return RedirectToAction("index", "home");
		}

		public ActionResult profile()
		{
			if (Request.Cookies["ASCruiseSafeUser"] != null)
			{
				return View();
			}
			return RedirectToAction("index", "home");
		}

		public ActionResult Trace()
		{
			if (Request.Cookies["ASCruiseSafeUser"] != null)
			{
				return View();
			}
			return RedirectToAction("index", "home");
		}

		public ActionResult Screening()
		{
			if (Request.Cookies["ASCruiseSafeUser"] != null)
			{
				return View();
			}
			return RedirectToAction("index", "home");
		}

		public ActionResult Comments()
		{
			if (Request.Cookies["ASCruiseSafeUser"] != null)
			{
				return View();
			}
			return RedirectToAction("index", "home");
		}

		public ActionResult Settings()
		{
			if (Request.Cookies["ASCruiseSafeUser"] != null)
			{
				return View();
			}
			return RedirectToAction("index", "home");
		}


        public ActionResult Dashboards()
        {
            if (Request.Cookies["ASCruiseSafeUser"] != null)
            {
                return View();
            }
            return RedirectToAction("index", "home");
        }
        public ActionResult SupportDashboards()
        {
            if (Request.Cookies["ASCruiseSafeUser"] != null)
            {
                return View();
            }
            return RedirectToAction("index", "home");
        }

        public ActionResult DataReports()
        {
            if (Request.Cookies["ASCruiseSafeUser"] != null)
            {
                return View();
            }
            return RedirectToAction("index", "home");
        }

        public ActionResult TrafficLights()
		{
			if (Request.Cookies["ASCruiseSafeUser"] != null)
			{
				return View();
			}
			return RedirectToAction("index", "home");
		}

		public ActionResult ReportedIssues()
		{
			if (Request.Cookies["ASCruiseSafeUser"] != null)
			{
				return View();
			}
			return RedirectToAction("index", "home");
		}

		public ActionResult FAQs()
		{
			if (Request.Cookies["ASCruiseSafeUser"] != null)
			{
				return View();
			}
			return RedirectToAction("index", "home");
		}


        public ActionResult VIPs()
        {
            if (Request.Cookies["ASCruiseSafeUser"] != null)
            {
                return View();
            }
            return RedirectToAction("index", "home");
        }

        public ActionResult Categories()
		{
			if (Request.Cookies["ASCruiseSafeUser"] != null)
			{
				return View();
			}
			return RedirectToAction("index", "home");
		}

		public ActionResult Reporting()
		{
			if (Request.Cookies["ASCruiseSafeUser"] != null)
			{
				return View();
			}
			return RedirectToAction("index", "home");
		}

		public ActionResult Inspections()
		{
			if (Request.Cookies["ASCruiseSafeUser"] != null)
			{
				return View();
			}
			return RedirectToAction("index", "home");
		}

		public ActionResult Fixings()
		{
			if (Request.Cookies["ASCruiseSafeUser"] != null)
			{
				return View();
			}
			return RedirectToAction("index", "home");
		}

		public ActionResult Confirmations()
		{
			if (Request.Cookies["ASCruiseSafeUser"] != null)
			{
				return View();
			}
			return RedirectToAction("index", "home");
		}

		public jrafixitController()
			: base()
		{
		}
	}
}
