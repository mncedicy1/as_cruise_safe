//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebApplication1.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class cruise_safe_reporting
    {
        public long reporting_id { get; set; }
        public string reporting_type { get; set; }
        public string reporting_category { get; set; }
        public string reporting_sub_category { get; set; }
        public string reporting_title { get; set; }
        public string reporting_body { get; set; }
        public Nullable<System.DateTime> reporting_timestamp { get; set; }
        public string reporting_status { get; set; }
        public Nullable<long> reporting_registered_id { get; set; }
        public Nullable<int> reporting_client_id { get; set; }
        public string reporting_as_anonymous { get; set; }
        public string reporting_person_location { get; set; }
        public string reporting_incident_location_address { get; set; }
        public string reporting_incident_location_latitude { get; set; }
        public string reporting_incident_location_longitude { get; set; }
        public string reporting_incident_location_postal_code { get; set; }
        public string reporting_incident_location_town { get; set; }
        public string reporting_incident_location_province { get; set; }
        public string reporting_incident_location_type { get; set; }
        public string reporting_incident_location_metropolitan { get; set; }
        public Nullable<System.DateTime> reporting_last_update { get; set; }
        public string reporting_person_name { get; set; }
        public Nullable<System.DateTime> reporting_delete_date { get; set; }
        public Nullable<int> reporting_deleted_by { get; set; }
        public string reporting_delete_comment { get; set; }
        public Nullable<System.DateTime> reporting_viewed_date { get; set; }
        public Nullable<int> reporting_viewed_by { get; set; }
        public string reporting_viewer_comment { get; set; }
        public Nullable<System.DateTime> reporting_resolved_date { get; set; }
        public Nullable<int> reporting_resolved_by { get; set; }
        public string reporting_resolver_comment { get; set; }
        public Nullable<System.DateTime> reporting_comfirmed_date { get; set; }
        public Nullable<int> reporting_comfirmed_by { get; set; }
        public string reporting_comfirmer_comment { get; set; }
        public string reporting_reference { get; set; }
        public string reporting_device_name { get; set; }
        public string reporting_device_type { get; set; }
        public string reporting_device_id { get; set; }
        public string reporting_incident_location_street { get; set; }
        public string reporting_incident_location_suburb { get; set; }
        public string reporting_incident_location_country { get; set; }
        public Nullable<int> reporting_year { get; set; }
        public Nullable<int> reporting_month { get; set; }
        public Nullable<int> reporting_day { get; set; }
        public string reporting_incident_location_end_latitude { get; set; }
        public string reporting_incident_location_end_longitude { get; set; }
        public string reporting_app_status { get; set; }
        public Nullable<System.DateTime> reporting_app_status_date { get; set; }
        public string reporting_other { get; set; }
        public string reporting_verified_comment { get; set; }
        public Nullable<int> reporting_verified_by { get; set; }
        public Nullable<System.DateTime> reporting_verified_date { get; set; }
        public Nullable<long> reporting_ticket_id { get; set; }
        public Nullable<long> reporting_app_record_id { get; set; }
        public string reporting_ticket_reference { get; set; }
        public Nullable<long> reporting_system_reporting_id { get; set; }
        public Nullable<long> reporting_category_id { get; set; }
        public Nullable<long> reporting_category_sub_id { get; set; }
        public Nullable<int> reporting_vip_level { get; set; }
        public string reporting_region_name { get; set; }
        public string reporting_id_last_02 { get; set; }
        public string reporting_id_last_35 { get; set; }
        public string reporting_ticket_reference_last_02 { get; set; }
        public string reporting_ticket_reference_last_35 { get; set; }
        public string reporting_ticket_id_last_02 { get; set; }
        public string reporting_ticket_id_last_35 { get; set; }
        public string reporting_registered_id_last_02 { get; set; }
        public string reporting_registered_id_last_35 { get; set; }
    }
}
