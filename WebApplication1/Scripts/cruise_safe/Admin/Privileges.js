﻿

$(document).ready(function () {




    var privilege;
    var id;


    //------------------------------ Get all Privilege -------------------------------------//












    function getPrivilege() {
        $('#myLoader').modal('show');

        $.getJSON('/admin/getPrivileges', function (data) {
            privilege = data;
            //console.log('privilege', privilege);

            if (data) {

                var str = "";
                for (i = 0; i < data.length; i++) {

                    str += '<tr><td>' + (i + 1) + '</td>';
                    str += '<td>' + data[i]['right_id'] + '</td>';
                    str += '<td>' + data[i]['right_name'] + '</td>';
                    str += '<td>' + data[i]['right_description'] + '</td>';
                    str += '<td>       <button id="b' + i + '" style="width:100px" type="button" class="btn btn-warning btn-flat btn-xs btnView">View</button>        <button id="b' + i + '" style="width:100px" type="button" class="btn btn-danger bg-red btn-flat btn-xs btnRemove"> Remove</button>    </td>';

                }
                $('#tblPrivileges').html(str);
            }

                $('#myLoader').modal('hide');








            //------------ view -------------------//


            $('.btnView').click(function () {
                $('#myLoader').modal('show');

                id = this.id.substring(1);

                $('#btnAddPrivilege').hide();
                $('#btnUpdatePrivilege').show();

                //--------------- Populate Fields ------------------//

                $('#txtPrivilegeName').val(privilege[id]['right_name']);
                $('#txtDescription').val(privilege[id]['right_description']);

                //--------------- Populate Fields ------------------//

                    $('#myLoader').modal('hide');

                $('.adminPrivilege-modal-lg').modal('show');
            });

            //------------ view -------------------//





            //------------ Remove -------------------//

            $('.btnRemove').click(function () {
                $('#myLoader').modal('show');
                id = this.id.substring(1);


                $('#btnConfirm').show();
                $('#btnConfirmActive').hide();


                $('#confirmModal').modal('show');
                $('#lblConfirmMessage').html("Are you sure, you want to remove this Privilege:- " + privilege[id]['right_name']);

                    $('#myLoader').modal('hide');
            });

            //------------ Remove -------------------//



        });
    }

    getPrivilege();


    //----------------------------- Search Privilege ----------------------------------------//

    $(".search-bar").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#tblPrivileges tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });


    //----------------------------- Search Privilege ----------------------------------------//






    //------------------------------ Get all Privilege -------------------------------------//












    //------------------------ Add new Privilege ---------------------------//


    $('#btnAddNew').click(function () {
        $('.cleanField').val('');

        $('#btnAddPrivilege').show();
        $('#btnUpdatePrivilege').hide();

        $('.adminPrivilege-modal-lg').modal('show');

    });



    $('#btnAddPrivilege').on('click', function () {
        if (vaidateFields('requiredPrivilege'))
            return;

        $('.myLoader').modal('show');

        var details = {
            userId: $('#lblUserID').text(),
            clientid: $('#lblClientID').text(),
            userType: $('#lblUserType').text(),
            clientType: $('#lblClientType').text(),
            privilege_name: $('#txtPrivilegeName').val(),
            description: $('#txtDescription').val()
        };

        $.post('/admin/addPrivilege', details, function (data) {
            if (data.ErrorMessage) {
                    $('#myLoader').modal('hide');
                showErrorMessage(data.ErrorMessage);
            }
            if (data.SuccessMessage) {
                getPrivilege();
                    $('#myLoader').modal('hide');
                showSuccessMessageClose(data.SuccessMessage);
                $('.adminPrivilege-modal-lg').modal('hide');
            }
        });
    });




    //------------------------ Add new Privilege ---------------------------//









    //------------------------ Update Privilege ---------------------------//


    $('#btnUpdatePrivilege').on('click', function () {
        if (vaidateFields('requiredPrivilege'))
            return;

        $('.myLoader').modal('show');

        var details = {
            userId: $('#lblUserID').text(),
            clientid: $('#lblClientID').text(),
            userType: $('#lblUserType').text(),
            clientType: $('#lblClientType').text(),
            privilege_id: privilege[id]['right_id'],
            privilege_name: $('#txtPrivilegeName').val(),
            description: $('#txtDescription').val()
        };

        $.post('/admin/updatePrivilege', details, function (data) {
            if (data.ErrorMessage) {
                    $('#myLoader').modal('hide');
                showErrorMessage(data.ErrorMessage);
            }
            if (data.SuccessMessage) {
                getPrivilege();
                    $('#myLoader').modal('hide');
                showSuccessMessageClose(data.SuccessMessage);
                $('.adminPrivilege-modal-lg').modal('hide');
            }
        });
    });


    //------------------------ Update Privilege ---------------------------//








    //------------------------ Remove Privilege starts ----------------------------------//


    $('#btnConfirm').on('click', function () {
        $('.myLoader').modal('show');

        var details = {
            userId: $('#lblUserID').text(),
            clientid: $('#lblClientID').text(),
            userType: $('#lblUserType').text(),
            clientType: $('#lblClientType').text(),
            privilege_id: privilege[id]['right_id'],
            privilege_name: privilege[id]['right_name']
        };

        $.post('/admin/removePrivilege', details, function (data) {
            //console.log(data);
            if (data.ErrorMessage) {
                    $('#myLoader').modal('hide');
                showErrorMessage(data.ErrorMessage);
                $('#confirmModal').modal('hide');
            }
            if (data.SuccessMessage) {
                getPrivilege();
                    $('#myLoader').modal('hide');
                showSuccessMessageClose(data.SuccessMessage);
                $('#confirmModal').modal('hide');
            }
        });
    });


    //------------------------ Remove Privilege starts ----------------------------------//











    /***************************************************************
            Validations
***************************************************************/


    function vaidateFields(clas) {
        $('.' + clas).css('border-color', 'silver');
        var input;
        $('.' + clas).map(function () {
            if (!$(this).val() || $(this).css('border-color') == 'red') {
                $(this).css('border-color', 'red');
                $(this).focus();
                input = $(this);
            }
        }).get();

        if (input) {
            input.focus();
            return true;
        }
        else
            return false;
    }

    function validateEmail(inputText) {
        var d = false;
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(inputText)) {
            d = true;
        }

        if (d == false) {
            var msg = "Please enter valid email address.";
            checkEmptyFields(msg);
        }
        return d;
    }

    function SuccessMessage(msg) {
        $('#myPleaseWait').modal('show');
        $('#txtModalLoaderAlertMessage').hide;
        $('#txtModalLoaderSuccessMessage').show();
        $('#txtModalLoaderSuccessMessage b').html(msg);

        setTimeout(function () {
            $('#myPleaseWait').hide();
            $('#txtModalLoaderSuccessMessage').hide();
            $('#myPleaseWait').modal('hide');
        }, 3000);
    }

    function checkEmptyFields(msg) {

        $('#myPleaseWait').modal('show');
        $('#txtModalLoaderAlertMessage').show();
        $('#txtModalLoaderAlertMessage b').text(msg);

        setTimeout(function () {
            $('#myPleaseWait').hide();
            $('#myPleaseWait').modal('hide');
            $('#txtModalLoaderAlertMessage').hide();
        }, 3000);
    }



    /***************************************************************
                        Validations Ends
    ***************************************************************/



    //Removing  Messages CoOperation Services

    $(document).on('click', function () {
        if ($('#txtModalLoaderAlertMessage').is(':visible')) {
            $('#txtModalLoaderAlertMessage').hide();
        }
        if ($('#txtModalLoaderSuccessMessage').is(':visible')) {
            $('#txtModalLoaderSuccessMessage').hide();
        }

    });








});