﻿function checkPrev(myPrivs) {

    //-------------------- Check Prev ---------------------------//

    if ($('#lblUserType').text() != 'business') {
        if (myPrivs.indexOf(1001) >= 0) {

        } else {
            location.href = '/admin/home';
        }
    }

    //-------------------- Check Prev ---------------------------//
}


$(document).ready(function () {



    var allPrivs;

    var Roles;
    var roleid;
    var roleNamerolename;
    var id;

   // var how_is_connection = checkNetworkConnection();



    $('.actionAdministrator').trigger('click');
    $('.btnRoleManagement').find('a').addClass('activeMenu');




    //----------------------------------------- Populate privilege checkboxex --------------------------------------//

    getSelectPrivilages();
    function getSelectPrivilages() {
        //if (how_is_connection == "offline") {
        //    showConnectionMessage("Looks like you are offline. Please check your internet connection.");
        //    return;
        //}

        $('.myLoader').modal('show');

        $.getJSON('/admin/getPrivileges', function (data) {
            //console.log(data);
            $('#first').html('');
            $('#second').html('');
            $('#first1').html('');
            $('#second1').html('');

            if (data) {
                var rem = data.length % 2;
                for (i = 0; i < data.length; i += 2) {

                    $('#first').append(privilageList(data[i]['right_id'], data[i]['right_name'], data[i]['right_description']));
                    $('#first1').append(privilageList1(data[i]['right_id'], data[i]['right_name'], data[i]['right_description']));
                    if (i < data.length - 1) {

                        $('#second').append(privilageList(data[i + 1]['right_id'], data[i + 1]['right_name'], data[i + 1]['right_description']));
                        $('#second1').append(privilageList1(data[i + 1]['right_id'], data[i + 1]['right_name'], data[i + 1]['right_description']));
                    }
                }
            }
            else {
                showErrorMessage("no privilages");
            }
            $('.funkyradio').tooltip();
                $('#myLoader').modal('hide');
        });
    }

   

    function privilageList(id, name, desc) {

        var str = '<div class="checkbox checkbox-primary" title="' + desc + '">' +
            '<input id="c' + id + '" type="checkbox" class="lstPriv"><label for="c' +
            id + '">' + name + '</label></div>';

        return str;
    }

    function privilageList1(id, name, desc) {
        var str = '<div class="checkbox checkbox-primary" title="' + desc + '">' +
            '<input id="e' + id + '" type="checkbox" class="lstPriv1"><label for="e' +
            id + '">' + name + '</label></div>';

        return str;
    }


    //----------------------------------------- Populate privilege checkboxex --------------------------------------//
















    //------------------------------ Get all Roles -------------------------------------//



    function getRoles() {
        //if (how_is_connection == "offline") {
        //    showConnectionMessage("Looks like you are offline. Please check your internet connection.");
        //    return;
        //}

        $('#myLoader').modal('show');

        var details = {
            clientid: $('#lblClientID').text(),
          //  userId: $('#lblUserID').text(),
        };
        $.getJSON('/admin/getRoles',details, function (data) {
            Roles = data;
            console.log('Roles', Roles);

            var total = data['privs'].length;
            allPrivs = Array();
            $('#tblRoles').html('');
            if (data['roles']) {

                for (i = 0; i < data['roles'].length; i++) {
                    var myPriv = 0;
                    var title = "";
                    var selPrivs = Array();
                    for (c = 0; c < data['privs_roles'].length; c++) {
                        if (data['privs_roles'][c]['role_id'] == data['roles'][i]['role_id']) {
                            myPriv++;
                            selPrivs.push(data['privs_roles'][c]['right_id']);
                            title += getPrivById(data['privs_roles'][c]['right_id'], data) + ', ';
                        }
                    }
                    allPrivs.push(selPrivs);

                    var txt = '<tr><td>' + (i + 1) + '</td><td>' + data['roles'][i]['role_id'] + '</td>' +
                        '<td>' + data['roles'][i]['role_name'] + '</td>' +
                        '<td>' + data['roles'][i]['role_category'] + '</td>' +
                        '<td>' + data['roles'][i]['role_type'] + '</td>' +
                        '<td>' + data['roles'][i]['role_description'] + '</td>' + addPrivPro(total, myPriv) + RolesAction(data['roles'][i]['role_id'], data['roles'][i]['role_name'], data['roles'][i]['role_description'], i, data['roles'][i]['landing_page'], data['roles'][i]['role_type']);
                    $('#tblRoles').append(txt);
                }

            }





                $('#myLoader').modal('hide');





            //------------ view -------------------//


            $('.editRole').click(function () {
                $('#myLoader').modal('show');

               // $('#all').trigger('click');

                $('#btnAddRole').hide();
                $('#btnUpdateRole').show();

                //--------------- Populate Fields ------------------//

                $('#txtRoleName').removeAttr('disabled', 'disabled').css('color','#495057');
                $('#txtDescription').removeAttr('disabled', 'disabled').css('color', '#495057');

                if (this.id.split('###')[5] == 'default') {
                    $('#txtRoleName').attr('disabled', 'disabled').css('color', 'white');;
                  //  $('#txtDescription').attr('disabled', 'disabled').css('color', 'white');;
                }

                $('#txtRoleName').val(this.id.split('###')[1]);
                $('#txtDescription').val(this.id.split('###')[2]);
                $('#cboLandingPage').val(this.id.split('###')[4]);

                //--------------- Populate Fields ------------------//



                id = this.id.split('###')[0];
                roleid = id;
                rolename = this.id.split('###')[1];
                var index = this.id.split('###')[3];
                //$('input:checkbox.lstPriv').map(function () {
                //    $(this).prop("checked", false);

                //    if (allPrivs[index].indexOf(parseInt(id)) >= 0) {
                //        $(this).prop("checked", true);
                //    }
                //}).get();

                $('.lstPriv').prop("checked", false);
                for (r = 0; r < allPrivs[index].length; r++) {
                    $('#c' + allPrivs[index][r]).trigger('click');
                }

                    $('#myLoader').modal('hide');

                $('.adminRoles-modal-lg').modal('show');
            });

            //------------ view -------------------//





            //------------ Remove -------------------//

            $('.deleteRole').click(function () {
                $('#myLoader').modal('show');
                id = this.id.split('###')[0];
                roleid = id;
                rolename = this.id.split('###')[1];

                $('#btnConfirm').show();
                $('#btnConfirmActive').hide();


                $('#confirmModal').modal('show');
                $('#lblConfirmMessage').html("Are you sure, you want to remove this Roles:- " + this.id.split('###')[1]);

                    $('#myLoader').modal('hide');
            });

            //------------ Remove -------------------//



        });
    }

    getRoles();




    function RolesAction(id, pname, pdesc, i, landing_page,type) {
        var disableDefaultRoles = '';
        if (type == "default" || pname == "Live Chat Support Agent" || pname == "Tickets Support Agent") {
            disableDefaultRoles = 'disabled="disabled"';
        }

        var iidd = id + '###' + pname + '###' + pdesc + '###' + i + '###' + landing_page + '###' + type;
        var str = '<td style=""><button    id="' + iidd + '" style="width:100px" type="button" class="btn btn-warning btn-flat btn-xs editRole">View</button> ' +
            ' <button   id="' + iidd + '"  style="width:100px" type="button" ' + disableDefaultRoles +' class="btn btn-danger btn-flat btn-xs deleteRole">Remove</button></td>';
        return str;
    }


    function getPrivById(id, data) {
        var name = '';
        return id;
    }

    function addPrivPro(tot, my) {
        var percent = Math.round(my * 100 / tot);
        var colors = ['progress-bar-danger', 'progress-bar-warning', 'progress-bar-aqua', 'progress-bar progress-bar-success'];

        var str = '<td title="' + my + '/' + tot + '"><div class="progress-group"><div class="progress progress-xs progress-striped activee sm">' +
                  '<div class="progress-bar ' + colors[Math.floor(percent / 30)] + '" style="width: ' + percent + '%"></div></div></div></td>';

        return str;
    }


    //----------------------------- Search Roles ----------------------------------------//

    $(".search-bar").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#tblRoles tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
    //----------------------------- Search Roles ----------------------------------------//











    //------------------------------ Get all Roles -------------------------------------//












    //------------------------ Add new Roles ---------------------------//


    $('.clsShowRightsByCategory').click(function () {
        $('.clsShowRightsByCategory').removeClass('btn-dark').addClass('btn-success').addClass('asbtn');
        $(this).removeClass('btn-success').removeClass('asbtn').addClass('btn-dark');

        $('.funkyradio').hide();
        var current_category = this.id;
        if (current_category != 'all')
            $('.' + current_category).show();
        else
            $('.funkyradio').show();
    });





    $('#btnAddNew').click(function () {
        $('.cleanField').val('');
        $('.lstPriv').prop("checked", false);

       //$('#all').trigger('click');

        $('#btnAddRole').show();
        $('#btnUpdateRole').hide();

        $('.adminRoles-modal-lg').modal('show');

    });



    $(document).on("keydown", ".requiredRole", function (evt) {
        var firstChar = $(this).val()
        if (evt.keyCode == 32 && firstChar == "") {
            return false;
        }
    });


    $('#btnAddRole').on('click', function () {
        //if (how_is_connection == "offline") {
        //    showConnectionMessage("Looks like you are offline. Please check your internet connection.");
        //    return;
        //}

        if (vaidateFields('requiredRole'))
            return;


        var str = "";
        $('input:checkbox.lstPriv').filter(':checked').map(function () {
            str += this.id.toString().substring(1) + '@';
            $(this).removeAttr('checked');
        }).get();


        if (str == "")
        {
            showErrorMessage('Please select privileges');
            return;
        }

        $('.myLoader').modal('show');
        var details = {
            userId: $('#lblUserID').text(),
            clientid: $('#lblClientID').text(),
            role_name: $('#txtRoleName').val(),
            desc: $('#txtDescription').val(),
            privis: str
        };

        console.log(details);
        
        $.post('/admin/addRole', details, function (data) {
            if (data.ErrorMessage) {
                    $('#myLoader').modal('hide');

                showErrorMessage(data.ErrorMessage);
            }
            if (data.SuccessMessage) {
                getRoles();
                    $('#myLoader').modal('hide');

                showSuccessMessageClose(data.SuccessMessage);
             
                //SuccessMessage(data.SuccessMessage);
                $('.adminRoles-modal-lg').modal('hide');
            }
        });
    });




    //------------------------ Add new Roles ---------------------------//









    //------------------------ Update Roles ---------------------------//


    $('#btnUpdateRole').on('click', function () {
        //if (how_is_connection == "offline") {
        //    showConnectionMessage("Looks like you are offline. Please check your internet connection.");
        //    return;
        //}

        if (vaidateFields('requiredRole'))
            return;

        var str = "";
        $('input:checkbox.lstPriv').filter(':checked').map(function () {
            str += this.id.toString().substring(1) + '@';
            $(this).removeAttr('checked');
        }).get();


        if (str == "") {
            showErrorMessage('Please select privileges');
            return;
        }


        $('.myLoader').modal('show');

        var details = {
            userId: $('#lblUserID').text(),
            clientid: $('#lblClientID').text(),
            role_id: roleid,
            role_name: $('#txtRoleName').val(),
            desc: $('#txtDescription').val(),
            privis: str
        };
        //console.log(details);
        $.post('/admin/updateRole', details, function (data) {
            if (data.ErrorMessage) {
                    $('#myLoader').modal('hide');
                showErrorMessage(data.ErrorMessage);
            }
            if (data.SuccessMessage) {
                getRoles();
                    $('#myLoader').modal('hide');

                showSuccessMessageClose(data.SuccessMessage);
               
                //SuccessMessage(data.SuccessMessage);
                $('.adminRoles-modal-lg').modal('hide');
            }
        });
    });


    //------------------------ Update Roles ---------------------------//








    //------------------------ Remove Roles starts ----------------------------------//


    $('#btnConfirm').on('click', function () {
        //if (how_is_connection == "offline") {
        //    showConnectionMessage("Looks like you are offline. Please check your internet connection.");
        //    return;
        //}

        $('.myLoader').modal('show');

        var details = {
            userId: $('#lblUserID').text(),
            clientid: $('#lblClientID').text(),
            role_id: roleid,
            Roles_name: rolename
        };

        $.post('/admin/removeRole', details, function (data) {
            //console.log(data);
            if (data.ErrorMessage) {
                    $('#myLoader').modal('hide');
                showErrorMessage(data.ErrorMessage);
                $('#confirmModal').modal('hide');
            }
            if (data.SuccessMessage) {
                getRoles();
                    $('#myLoader').modal('hide');

                showSuccessMessageClose(data.SuccessMessage);
             
                //SuccessMessage(data.SuccessMessage);
                $('#confirmModal').modal('hide');
            }
        });
    });


    //------------------------ Remove Roles starts ----------------------------------//











    /***************************************************************
            Validations
***************************************************************/


    function vaidateFields(clas) {
        $('.' + clas).css('border-color', 'silver');
        var input;
        $('.' + clas).map(function () {
            if (!$(this).val() || $(this).css('border-color') == 'red') {
                $(this).css('border-color', 'red');
                $(this).focus();
                input = $(this);
            }
        }).get();

        if (input) {
            input.focus();
            return true;
        }
        else
            return false;
    }

    function validateEmail(inputText) {
        var d = false;
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(inputText)) {
            d = true;
        }

        if (d == false) {
            var msg = "Please enter valid email address.";
            checkEmptyFields(msg);
        }
        return d;
    }

    function SuccessMessage(msg) {
        $('#myPleaseWait').modal('show');
        $('#txtModalLoaderAlertMessage').hide;
        $('#txtModalLoaderSuccessMessage').show();
        $('#txtModalLoaderSuccessMessage b').html(msg);

        setTimeout(function () {
            $('#myPleaseWait').hide();
            $('#txtModalLoaderSuccessMessage').hide();
            $('#myPleaseWait').modal('hide');
        }, 3000);
    }

    function checkEmptyFields(msg) {

        $('#myPleaseWait').modal('show');
        $('#txtModalLoaderAlertMessage').show();
        $('#txtModalLoaderAlertMessage b').text(msg);

        setTimeout(function () {
            $('#myPleaseWait').hide();
            $('#myPleaseWait').modal('hide');
            $('#txtModalLoaderAlertMessage').hide();
        }, 3000);
    }



    /***************************************************************
                        Validations Ends
    ***************************************************************/



    //Removing  Messages CoOperation Services

    $(document).on('click', function () {
        if ($('#txtModalLoaderAlertMessage').is(':visible')) {
            $('#txtModalLoaderAlertMessage').hide();
        }
        if ($('#txtModalLoaderSuccessMessage').is(':visible')) {
            $('#txtModalLoaderSuccessMessage').hide();
        }

    });








});