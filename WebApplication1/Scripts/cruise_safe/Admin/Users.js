﻿function checkPrev(myPrivs) {

    //-------------------- Check Prev ---------------------------//

    if ($('#lblUserType').text() != 'business') {
        if (myPrivs.indexOf(1002) >= 0) {

        } else {
            location.href = '/Home/menu';
        }
    }

    //-------------------- Check Prev ---------------------------//
}

$(document).ready(function () {


    var http = httpsapi + "/api/admin/";
    var https = httpsapi + "/api/";
   // http = httpsapi_local + '/api/admin/';

    var how_is_connection = '';//checkNetworkConnection();




    $('.actionAdministrator').trigger('click');
    $('.btnUserManagement').find('a').addClass('activeMenu');





    //------------------------------------------- Search Person --------------------------------------------------------------//



    var member = Array();

    var id;
    var start_from = 0;
    var last_row = false;
    var isSending = false;
    var fullname = "";
    var person_id;
    var system_user_id;


    //------------------------------ Get all Users -------------------------------------//


    function getMembers() {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        if (isSending)
            return;

        isSending = true;
        $('#myLoader').modal('show');

        if (start_from == 0)
            $('#tblMembers').html('');


        var details = {
            clientid: $('#lblClientID').text(),
            //  userId: $('#lblUserID').text(),
        };
        //console.log(details);
        $.getJSON('/admin/getUsers', details, function (data) {
            console.log('getMembers', data);
            displayMembers(data);
            if ((start_from + data.length) < (start_from + details.limit)) {
                last_row = true;
            }
            start_from += data.length;

                $('#myLoader').modal('hide');
        });
    }


   


 


    function displayMembers(data) {

        $('#spResult').hide();
        if (data.length > 0) {

            var str = "";
            for (i = 0; i < data.length; i++) {
                member.push(data[i]);

                str += '<tr class="btnChooseMember" style="cursor:pointer;" id="b' + i + '"><td>' + (i + 1) + '</td>';
                str += '<td>' + data[i]['user_title'] + ' ' + data[i]['user_firstname'] + ' ' + data[i]['user_surname'] + '</td>';
                str += '<td>' + data[i]['user_email'] + '</td>';
                str += '<td></td>';
                str += '<td>' + data[i]['user_gender'] + '</td>';
            }
            isSending = false;
            $('#tblMembers').append(str);
        }
        else {
            isSending = false;
        }


        if (start_from == 0 && data.length < 1) {
            $('#spResult').show().html('No Result Found');
            isSending = false;
        }



        //------------ view -------------------//


        $('.btnChooseMember').click(function () {
            var idMember = this.id.substring(1);
            $(this).addClass('activeTableRow');

            person_id = member[idMember]['person']['person_id'];
            system_user_id = member[idMember]['person']['system_user_id'];


            $('#cboTitle').val(member[idMember]['person']['person_title']);
            $('#txtFirstName').val(member[idMember]['person']['person_first_name']);
            $('#txtLastName').val(member[idMember]['person']['person_last_name']);
            $('#cboRegions').val(member[idMember]['person']['user_region_id']);
            $('#txtEmailAddress').val(member[idMember]['person']['contact_email_address']);
            $('#txtCellNumber').val(member[idMember]['person']['contact_cellphone']);

            $('#listMember').modal('hide');
        });

        //------------ view -------------------//

       

    }





    //----------------------------- Search Member ----------------------------------------//


    $('#btnSearchFilter').click(function () {
        if ($('#txtSearch').val() == "" && $('#cboSearchWith').val() == "") {
            showErrorMessage('Search value cannot be empty.');
            return;
        }
        member = Array();
        start_from = 0;
      //  getMembers();
    });


    $('#btnRefresh').click(function () {
        $('#cboSearchWith').val('person_id_number');
        $('#txtSearch').val('');
        member = Array();
        start_from = 0;
     //   getMembers();
    });

    //----------------------------- Search Member ----------------------------------------//






    //------------------------------------------- Search Person --------------------------------------------------------------//

































































    //------------------------------------------------------------------ Get all Users ----------------------------------------------------------------------------------------//





    var scrollHeight = 0;
    var Users = Array();

    var roles = Array();
    var regions = Array();
    var start_fromm = 0;
    var user_id = null;
    var isSendingg = false;


    function getUsers() {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        if (isSendingg)
            return;

        isSendingg = true;
        $('#myLoader').modal('show');

        if (start_fromm == 0)
            $('#tblUsers').html('');

        var details = {
            clientid: $('#lblClientID').text(),
            //  userId: $('#lblUserID').text(),
        };
        //console.log(details);
        $.getJSON('/admin/getUsers', details, function (data) {
            console.log('getUsers', data);
            displayUsers(data);

                $('#myLoader').modal('hide');
        });
    }






    //$(window).scroll(function () {
    //    if (!last_roww) {
    //        if ($(window).scrollTop() + $(window).height() > $(document).height() - 10) {
    //            if ((scrollHeight - 30) < $(document).height()) {
    //                scrollHeight = $(document).height();
    //                getUsers();
    //            }
    //        }
    //    }
    //});




    function displayUsers(data) {

        $('#zeroStatsUsers').hide();
        if (data.length > 0) {

            Users = Array();
            var str = "";
            for (i = 0; i < data.length; i++) {
               Users.push(data[i]);


                var userStatus = "";
                var statusButton = "";

                if (data[i]['user_system_status'] == 'Active') {
                    userStatus = "label-success badge badge-success";
                    statusButton = '<button id="v' + i + '"  style="width:90px" type="button" class="btn btn-danger removeUser btn-flat btn-xs"> Deactivate</button>';
                }
                else {
                    userStatus = "label-danger badge badge-danger";
                    statusButton = '<button id="t' + i + '"  style="width:90px" type="button" class="btn btn-success activateUser btn-flat btn-xs"> Activate</button>';
                }

                str += '<tr class="btnChooseMember" style="cursor:pointer;" id="b' + i + '"><td>' + (i + 1) + '</td>';
                str += '<td>' + data[i]['user_title'] + ' ' + data[i]['user_firstname'] + ' ' + data[i]['user_surname'] + '</td>';
                str += '<td>' + data[i]['user_email'] + '</td>';
                str += '<td>' + data[i]['user_cellphone'] + '</td>';
                str += '<td>' + getRole(data[i]['user_role_id']).role_name + '</td>';
                str += '<td><span class="' + userStatus + '">' + data[i]['user_system_status'] + '</span></td>';
                str += '<td>       <button id="b' + i + '" style="width:90px" type="button" class="btn btn-warning btn-flat btn-xs btnView">View</button>  ' + statusButton + ' </td>';
            }
            isSendingg = false;
            $('#tblUsers').html(str);
        }


        if (start_fromm == 0 && data.length == 0) {
            $('#zeroStatsUsers').show();
        }








        //------------ view -------------------//


        $('.btnView').click(function () {

            id = this.id.substring(1);
            $('#btnAddUser').html('Save Changes');

            //--------------- Populate Fields ------------------//
            $('.clsPassword').hide();
            $('.clsPasswords').removeAttr('disabled').removeClass('requiredUser');

            user_id = Users[id]['user_id'];

            $('#txtPassword').attr('disabled', 'disabled');
            $('#txtConfirmPassword').attr('disabled', 'disabled');
            $('#cboTitle').val(Users[id]['user_title']);
            $('#txtFirstName').val(Users[id]['user_firstname']);
            $('#txtLastName').val(Users[id]['user_surname']);
            $('#cboRegions').val(Users[id]['user_region_id']);
            $('#txtEmailAddress').val(Users[id]['user_email']);
            $('#txtCellNumber').val(Users[id]['user_cellphone']);
            $('#cboRole').val(Users[id]['user_role_id']);
            $('#txtIDNumber').val(Users[id]['user_id_number']);

            //--------------- Populate Fields ------------------//

            $('.adminUsers-modal-lg').modal('show');
        });

        //------------ view -------------------//

       






        //------------ activate -------------------//

        $('.activateUser').click(function () {
            id = this.id.substring(1);

            $('#btnConfirm').hide();
            $('#btnConfirmActive').show();


            $('#confirmModal').modal('show');
            $('#lblConfirmMessage').html("Are you sure, you want to activate " + Users[id]['user_firstname'] + ' ' + Users[id]['user_surname']);
        });

        //------------ activate -------------------//






        //------------ deactivate  -------------------//

        $('.removeUser').click(function () {
            id = this.id.substring(1);

            $('#btnConfirm').show();
            $('#btnConfirmActive').hide();


            $('#confirmModal').modal('show');
            $('#lblConfirmMessage').html("Are you sure, you want to remove " + Users[id]['user_firstname'] + ' ' + Users[id]['user_surname']);
        });

        //------------ deactivate   -------------------//





    }



    function getRole(id) {
        console.log(roles, id);
        var role = { role_id: '', role_name: '', role_description: '' };
        for (var i = 0; i < roles.length; i++) {
            if (roles[i].role_id == id)
                role = roles[i];
        }
        return role;
    }



    //------------------------------------------- Populate Dropdowns --------------------------------------------------------------//


    function getRoles() {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        roles = Array();

        var details = {
            clientid: $('#lblClientID').text(),
            //  userId: $('#lblUserID').text(),
        };
        $.getJSON('/admin/getUserRoles', details, function (data) {
            //console.log('Roles', data);
            if (data) {
                roles = data['roles'];
                $('#cboRole').html('<option value="">-- Choose Role --</option>');
                for (var i = 0; i < data['roles'].length; i++) {
                    var htmlcontent = '<option value="' + data['roles'][i].role_id + '">' + data['roles'][i].role_name + '</option>';
                    $('#cboRole').append(htmlcontent);
                }
            }
            getUsers();
        });

    }

    getRoles();



    //------------------------------------------- Populate Dropdowns --------------------------------------------------------------//



    function getRegions() {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        roles = Array();

        var details = {
            region_client_id: $('#lblClientID').text(),
            //  userId: $('#lblUserID').text(),
        };
        $.getJSON(http + 'getRegions', details, function (data) {
            console.log('getRegions', data);
            if (data) {
                regions = data;
                $('#cboRegions').html('<option value="">-- Choose Region --</option>');
                for (var i = 0; i < data.length; i++) {
                    var htmlcontent = '<option value="' + data[i].OBJECTID + '">' + data[i].REGIONNAME + '</option>';
                    $('#cboRegions').append(htmlcontent);
                }
            }
          
        });

    }

    getRegions();






    //----------------------------- Search Users ----------------------------------------//

    $(".search-bar").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#tblUsers tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

    //----------------------------- Search Users ----------------------------------------//






    //------------------------------------------------------------------ Get all Users ----------------------------------------------------------------------------------------//












    //------------------------ Add new Users ---------------------------//







    $('#btnAddNew').click(function () {
        user_id = null;
        $('.clsPassword').show();
        $('.clsPasswords').removeAttr('disabled').addClass('requiredUser');
        $('#btnAddUser').html('Save');
        $('.cleanUserField').val('');
        $('.adminUsers-modal-lg').modal('show');

    });











    $('#btnAddUser').on('click', function () {
        saveUser(user_id);
    });




    function saveUser(user_id) {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        if (vaidateFields('requiredUser'))
            return;
        if (user_id == null) {
            if ($('#txtPassword').val() != $('#txtConfirmPassword').val()) {
                showConnectionMessage("Passwords do not match");
                return;
            }
        }


        $('.myLoader').modal('show');

        var details = {
            user_id: user_id,
            user_client_id: $('#lblClientID').text(),
            user_id_number: $('#txtIDNumber').val(),
            user_title: $('#cboTitle').val(),
            user_firstname: $('#txtFirstName').val(),
            user_surname: $('#txtLastName').val(),
            user_region_id: $('#cboRegions').val(),
            user_cellphone: $('#txtCellNumber').val(),
            user_email: $('#txtEmailAddress').val(),
            user_role_id: $('#cboRole').val(),
            user_password: $('#txtPassword').val(),
            user_saved_by: $('#lblUserID').text(),
            user_updated_by: $('#lblUserID').text()

        };
        console.log(details);
        $.post(http + 'addUser', details, function (data) {
            console.log(data);
            if (data.indexOf('success') >= 0) {
                showSuccessMessage(data);
                getUsers();
                $('#myLoader').modal('hide');
      
                $('.adminUsers-modal-lg').modal('hide');
            }
            else {

       
                    $('#myLoader').modal('hide');
               
                showErrorMessage(data);
            }
        });
    }




    //------------------------ Add new Users ---------------------------//
















    //---------------------- activate user --------------------------//


    $('#btnConfirmActive').on('click', function () {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        $('#confirmModal').modal('hide');
        $('.myLoader').modal('show');

        var details = {
            userId: $('#lblUserID').text(),
            clientid: $('#lblClientID').text(),
            system_user_id: Users[id]['user_id'],
            fullname: Users[id]['user_firstname'] + ' ' + Users[id]['user_surname']
        };

        $.post('/admin/activate', details, function (data) {
          
            if (data.message.indexOf('Error')<0) {
                getUsers();
                    $('#myLoader').modal('hide');
              

                showSuccessMessageClose(data.message);
                    $('#successModal').modal('hide');
           
            }
            else {
                    $('#myLoader').modal('hide');
                showErrorMessage(data.message);
            }
        });
    });


    //---------------------- activate user ends --------------------------//








    //---------------------- deactivate user --------------------------//


    $('#btnConfirm').on('click', function () {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        $('#confirmModal').modal('hide');
        $('.myLoader').modal('show');

        var details = {
            userId: $('#lblUserID').text(),
            clientid: $('#lblClientID').text(),
            system_user_id: Users[id]['user_id'],
            fullname: Users[id]['user_firstname'] + ' ' + Users[id]['user_surname']
        };
        //console.log(details);
        //return;
        $.post('/admin/deactivate', details, function (data) {
            if (data.message.indexOf('Error') < 0) {
                getUsers();
                    $('#myLoader').modal('hide');

                showSuccessMessageClose(data.message);
                    $('#successModal').modal('hide');
            }
            else {
                    $('#myLoader').modal('hide');
                showErrorMessage(data.message);
            }
        });
    });


    //---------------------- deactivate user ends --------------------------//






























    //  ---------------------------------------------------------------- Manage Roles ---------------------------------------------------------------//

    var allPrivs;

    var Roles;
    var roleid;
    var rolename;









    //----------------------------------------- Populate privilege checkboxex --------------------------------------//

    getSelectPrivilages();

    function getSelectPrivilages() {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        //$('.myLoader').modal('show');

        $.getJSON('/as_smart_connects/getPrivileges', function (data) {

            $('#first').html('');
            $('#second').html('');
            $('#first1').html('');
            $('#second1').html('');

            if (data) {
                var rem = data.length % 2;
                for (i = 0; i < data.length; i += 2) {
                    var category = data[i]['category'];

                    if (data[i]['category'] == 'Live Chat Support') {
                        category = 'Live_Chat_Support';
                    }
                    if (data[i]['category'] == 'Support Tickets') {
                        category = 'Support_Tickets';
                    }


                    $('#first').append(privilageList(data[i]['right_id'], data[i]['right_name'], data[i]['right_description'], category));
                    $('#first1').append(privilageList1(data[i]['right_id'], data[i]['right_name'], data[i]['right_description'], category));
                    if (i < data.length - 1) {

                        var category2 = data[i + 1]['category'];
                        if (data[i + 1]['category'] == 'Live Chat Support') {
                            category2 = 'Live_Chat_Support';
                        }
                        if (data[i + 1]['category'] == 'Support Tickets') {
                            category2 = 'Support_Tickets';
                        }

                        $('#second').append(privilageList(data[i + 1]['right_id'], data[i + 1]['right_name'], data[i + 1]['right_description'], category2));
                        $('#second1').append(privilageList1(data[i + 1]['right_id'], data[i + 1]['right_name'], data[i + 1]['right_description'], category2));

                    }
                }
            }
            else {
                alert("no privilage");

            }
            $('.funkyradio').tooltip();
            //setTimeout(function () {
            //    $('#myLoader').modal('hide');
            //}, 1000);
        });
    }


    function privilageList(id, name, desc, category) {
        var str = '<div class="funkyradio ' + category + '" title="' + desc + '"><div class="funkyradio-default">' +
            '<input type="checkbox" class="lstPriv" name="checkbox" id="c' + id + '">' +
            '<label for="c' + id + '" style="margin-top:0px">' + name + '</label>' +
            '</div></div>';
        return str;
    }

    function privilageList1(id, name, desc, category) {
        var str = '<div class="funkyradio ' + category + '" title="' + desc + '"><div class="funkyradio-default">' +
            '<input type="checkbox" class="lstPriv1" name="checkbox" id="e' + id + '">' +
            '<label for="e' + id + '" style="margin-top:0px">' + name + '</label>' +
            '</div></div>';
        return str;
    }



    //----------------------------------------- Populate privilege checkboxex --------------------------------------//








    //------------------------ Add new Roles ---------------------------//

    $('.clsShowRightsByCategory').click(function () {
        $('.clsShowRightsByCategory').removeClass('btn-dark').addClass('btn-success').addClass('asbtn');
        $(this).removeClass('btn-success').removeClass('asbtn').addClass('btn-dark');

        $('.funkyradio').hide();
        var current_category = this.id;
        if (current_category != 'all')
            $('.' + current_category).show();
        else
            $('.funkyradio').show();
    });



    $('#btnAddRole').on('click', function () {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        if (vaidateFields('requiredRole'))
            return;


        var str = "";
        $('input:checkbox.lstPriv').filter(':checked').map(function () {
            str += this.id.toString().substring(1) + '@';
            $(this).removeAttr('checked');
        }).get();


        if (str == "") {
            showErrorMessage('Please select privileges');
            return;
        }

        $('.myLoader').modal('show');

        var details = {
            userId: $('#lblUserID').text(),
            clientid: $('#lblCompanyID').text(),
            userType: $('#lblUserType').text(),
            role_name: $('#txtRoleName').val(),
            desc: $('#txtDescription').val(),
            privis: str,
            session_token: $('#lblUserToken').text(),
        };

        //console.log(details);

        $.post('/as_smart_connects/addRoleUser', details, function (data) {
            if (data.ErrorMessage) {
                    $('#myLoader').modal('hide');
                    showErrorMessage(data.ErrorMessage);
            }
            if (data.SuccessMessage) {
                $('#cboRole').append('<option selected="selected" value="' + data.roleId + '">' + data.roleName +'</option>');

                    $('#myLoader').modal('hide');

                showSuccessMessageClose(data.SuccessMessage);
                

                //SuccessMessage(data.SuccessMessage);
                $('.adminRoles-modal-lg').modal('hide');
            }
        });
    });




    //------------------------ Add new Roles ---------------------------//






























    /***************************************************************
                        Validations
    ***************************************************************/

    function validateIdNumberAsMunicipality(thiss, genderr, dob, agee, minAge, maxAge) {

        var idNumber = thiss.val();

        // assume everything is correct and if it later turns out not to be, just set this to false
        var correct = '';

        // SA ID Number have to be 13 digits, so check the length
        if (idNumber.length != 13 || !isNumber(idNumber)) {
            correct = 'ID number : ' + idNumber + ' does not appear to be authentic - <b class="text-red">Input not a valid number!</b>';
        }

        // get first 6 digits as a valid date
        var tempDate = new Date(idNumber.substring(0, 2), idNumber.substring(2, 4) - 1, idNumber.substring(4, 6));

        var id_date = tempDate.getDate();
        var id_month = tempDate.getMonth();
        var id_year = tempDate.getFullYear();

        var month = id_month + 1;
        if (month < 10)
            month = '0' + month;
        var day = id_date;
        if (day < 10)
            day = '0' + day;
        var fullDates = id_year + "-" + month + "-" + day;

        var fullDate = id_year + "-" + (id_month + 1) + "-" + id_date;

        if (!((tempDate.getYear() == idNumber.substring(0, 2)) && (id_month == idNumber.substring(2, 4) - 1) && (id_date == idNumber.substring(4, 6))) && correct == '') {
            correct = 'ID number : <b class="text-yellow">' + idNumber + '</b> does not appear to be authentic - <b class="text-red">Date part not valid!</b>';
        }

        // get the gender
        var genderCode = idNumber.substring(6, 10);
        var gender = parseInt(genderCode) < 5000 ? "Female" : "Male";

        // get country ID for citzenship
        var citzenship = parseInt(idNumber.substring(10, 11)) == 0 ? "Yes" : "No";

        // apply Luhn formula for check-digits
        var tempTotal = 0;
        var checkSum = 0;
        var multiplier = 1;
        for (var i = 0; i < 13; ++i) {
            tempTotal = parseInt(idNumber.charAt(i)) * multiplier;
            if (tempTotal > 9) {
                tempTotal = parseInt(tempTotal.toString().charAt(0)) + parseInt(tempTotal.toString().charAt(1));
            }
            checkSum = checkSum + tempTotal;
            multiplier = (multiplier % 2 == 0) ? 1 : 2;
        }
        if ((checkSum % 10) != 0 && correct == '') {
            correct = 'ID number : <b class="text-yellow">' + idNumber + '</b> does not appear to be authentic - <b class="text-red">Check digit is not valid!</b>';
        };

        if (citzenship == "No" && correct == '') {
            correct = 'ID number : <b class="text-yellow">' + idNumber + '</b> does not appear to be authentic - <b class="text-red">Only south african Id number allowed!</b>';
        };

        var age = calculateAge(tempDate);
        if ((age < minAge || age > maxAge) && correct == '') {
            ////console.log(age);
            minAge = minAge == 0.5 ? "6 Months" : minAge;
            correct = 'ID number : <b class="text-yellow">' + idNumber + '</b> does not appear to be authentic - <b class="text-red">The age must be between ' + minAge + ' and ' + maxAge + ' Years!</b>';
        };

        // if no error found, hide the error message
        if (correct == '') {
            thiss.css('border-color', 'green');

            if (genderr)
                genderr.val(gender);
            if (agee)
                agee.val(Math.abs(age));
            if (dob)
                dob.val(fullDates);

            return true;
        }
        // otherwise, show the error
        else {
            checkEmptyFields(correct);
            thiss.css('border-color', 'red');
            if (dob)
                dob.val('').removeAttr('disabled');
            if (genderr)
                genderr.val('');
            if (agee)
                agee.val('');
            thiss.val('').focus();

            return false;
        }

    }


    function vaidateFields(clas) {
        $('.' + clas).css('border-color', 'silver');
        var input;
        $('.' + clas).map(function () {
            if (!$(this).val() || $(this).css('border-color') == 'red') {
                $(this).css('border-color', 'red');
                $(this).focus();
                input = $(this);
            }
        }).get();

        if (input) {
            input.focus();
            return true;
        }
        else
            return false;
    }

    function validateEmail(sEmail, msg) {
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(sEmail)) {
            return true;
        }
        else {
            //var msg = "Please enter valid email address.";
            checkEmptyFields(msg);
            return false;
        }
    }

    function SuccessMessage(msg) {
        $('#myPleaseWait').modal('show');
        $('#txtModalLoaderAlertMessage').hide;
        $('#txtModalLoaderSuccessMessage').show();
        $('#txtModalLoaderSuccessMessage b').html(msg);

        setTimeout(function () {
            $('#myPleaseWait').hide();
            $('#txtModalLoaderSuccessMessage').hide();
            $('#myPleaseWait').modal('hide');
        }, 3000);
    }

    function checkEmptyFields(msg) {

        $('#myPleaseWait').modal('show');
        $('#txtModalLoaderAlertMessage').show();
        $('#txtModalLoaderAlertMessage b').text(msg);

        setTimeout(function () {
            $('#myPleaseWait').hide();
            $('#myPleaseWait').modal('hide');
            $('#txtModalLoaderAlertMessage').hide();
        }, 3000);
    }



    /***************************************************************
                        Validations Ends
    ***************************************************************/



    //Removing  Messages CoOperation Services

    $(document).on('click', function () {
        if ($('#txtModalLoaderAlertMessage').is(':visible')) {
            $('#txtModalLoaderAlertMessage').hide();
        }
        if ($('#txtModalLoaderSuccessMessage').is(':visible')) {
            $('#txtModalLoaderSuccessMessage').hide();
        }

    });






});