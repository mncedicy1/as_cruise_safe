﻿$(document).ready(function () {

    var http = httpsapi + "/api/admin/";


    var how_is_connection = checkNetworkConnection();


    //-------------------------------------------------- Activate Account ---------------------------------------------//




    var uri = window.location.toString();
    function getValuParameter(uri) {
        return uri.substring(uri.indexOf("=") + 1);
    }

    

    activateAccount();

    function activateAccount() {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }


        $('#myLoader').modal('show');

        var value = getValuParameter(uri);
        var details = {
            encryptedEmail: value
        };

        $.getJSON(http + 'accountActivation', details, function (data) {
            if (data.response_type == 'success') {
                $('#spAlertMessage').show('slow');
                $('#spActivationMessage').html('<b>' + data.description + '</b>');
                $('#myLoader').modal('hide');
            }
            else {
                var htmlError = "";
                if (data.description == 'Your activation link has expired.') {
                    htmlError = " <a class='resendMyActivationLink' href='#'>Send me my activation link again</a>";
                }

                showErrorMessage(data.description + htmlError);
            }

            setTimeout(function () {
                $('#myLoader').modal('hide');
            }, 1000);
        });
    }







    $('#btnSignIn').click(function () {
        location.href = '/Home/index';
    });





    //-------------------------------------------------- Activate Account ---------------------------------------------//










    //--------------------------------- Resend My Activation Link ---------------------------------------//



    $(document).on('click', '.resendMyActivationLink', function () {
        $('#donee').trigger('click');
        resendActivationLink();
    });

    function resendActivationLink() {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        $('#myLoader').modal('show');
        var value = getValuParameter(uri);
        var details = {
            username: value,
            sent_from: 'account_activation'
        };

        $.getJSON(http + 'resend_my_activation_link', details, function (data) {
            if (data.response_type == 'success') {
                showSuccessMessage(data.description);
                $('#myLoader').modal('hide');
            }
            else {
                showErrorMessage(data.description);
            }

            setTimeout(function () {
                $('#myLoader').modal('hide');
            }, 1000);
        });
    }



    //--------------------------------- Resend My Activation Link ---------------------------------------//


















    //--------------------------------- Change Backgrounds ---------------------------------------//


    function changeBackgroundTheme() {
        var theme = Math.floor(Math.random() * 25);
        var image = 'url(../Content/Images/background_images/' + theme + '.jpg)'
        $('.accountbg').css('background-image', image);
    }

    //changeBackgroundTheme();



//--------------------------------- Change Backgrounds ---------------------------------------//
















});