﻿
$(document).ready(function () {



    var http = httpsapi + "/api/admin/";
    var https = httpsapi + "/api/";
    //var http = httpsapi_local + '/api/admin/';


    var how_is_connection = checkNetworkConnection();


    //------------------------------------------- Search Person --------------------------------------------------------------//



    var member = Array();

    var id;
    var start_from = 0;
    var last_row = false;
    var isSending = false;
    var fullname = "";
    var person_id;
    var system_user_id;


    //------------------------------ Get all Users -------------------------------------//

    var load_first = 0;

    function getMembers() {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        if (isSending)
            return;

        isSending = true;

        //if (load_first == 1)
        //    $('#myLoader').modal('show');

        if (start_from == 0)
            $('#tblMembers').html('');

        var details = {
            company_id: $('#lblCompanyID').text(),
            start_from: start_from,
            limit: 10,
            searchWith: $('#cboSearchWith').val(),
            searchValue: $('#txtSearch').val(),
            session_token: $('#lblUserToken').text(),
            system_user_id: $('#lblUserID').text(),
        };
        //console.log(details);
        $.getJSON(http + 'searchEmployees', details, function (data) {
            //console.log('getMembers', data);
            displayMembers(data);
            if ((start_from + data.length) < (start_from + details.limit)) {
                last_row = true;
            }
            start_from += data.length;

            if (load_first == 1) {
                setTimeout(function () {
                    $('#myLoader').modal('hide');
                }, 1000);
            }

            load_first = 1;

        });
    }

    getMembers();


    function displayMembers(data) {

        $('#spResult').hide();
        if (data.length > 0) {

            for (i = 0; i < data.length; i++) {
                member.push(data[i]);
                var str = "";

                str += '<tr class="btnChooseMember" style="cursor:pointer;" id="b' + i + '"><td>' + (i + 1) + '</td>';
                str += '<td>' + data[i]['person']['person_first_name'] + ' ' + data[i]['person']['person_last_name'] + '</td>';
                str += '<td>' + data[i]['person']['contact_email_address'] + '</td>';
                str += '<td>' + removeNull(data[i]['person']['contact_cellphone']) + '</td>';
                str += '<td>' + removeNull(data[i]['person']['person_gender']) + '</td>';

                if (data[i]['person']['person_id'] != $('#lblPersonId').text()) {
                    $('#tblMembers').append(str);
                }
            }
            isSending = false;
        }
        else {
            isSending = false;
        }


        if (start_from == 0 && data.length < 1) {
            $('#spResult').show().html('No Result Found');
            isSending = false;
        }



        //------------ view -------------------//


        $('.btnChooseMember').click(function () {
            var idMember = this.id.substring(1);
            //$(this).addClass('activeTableRow');


            if ($('#spActionEvent').html() == "Assign") {
                $('#spAssignToAction').show();

                $('#spAssignTo').html(member[idMember]['person']['person_id']);
                $('#textAssignFromName').html($('#lblFullName').text());
                $('#textAssignToName').html(member[idMember]['person']['person_first_name'] + ' ' + member[idMember]['person']['person_last_name']);
                $('#textAssignToEmail').html(member[idMember]['person']['contact_email_address']);
            }
            else if ($('#spActionEvent').html() == "Assign Update") {
                $('#spUpdateAssignToAction').show();

                $('#spUpdateAssignTo').html(member[idMember]['person']['person_id']);
                $('#textUpdateAssignFromName').html($('#lblFullName').text());
                $('#textUpdateAssignToName').html(member[idMember]['person']['person_first_name'] + ' ' + member[idMember]['person']['person_last_name']);
                $('#textUpdateAssignToEmail').html(member[idMember]['person']['contact_email_address']);
            }
            else if ($('#spActionEvent').html() == "Assign MSG") {
                $('#spAssignToAction_msg').show();

                $('#spAssignTo_msg').html(member[idMember]['person']['person_id']);
                $('#textAssignFromName_msg').html($('#lblFullName').text());
                $('#textAssignToName_msg').html(member[idMember]['person']['person_first_name'] + ' ' + member[idMember]['person']['person_last_name']);
                $('#textAssignToEmail_msg').html(member[idMember]['person']['contact_email_address']);
            }




            else if ($('#spActionEvent').html() == "Assign Automation") {
                $('.clsActivespAssignToAction').show();

                $('.clsActivespAssignTo').html(member[idMember]['person']['person_id']);
                $('#textAssignFromName').html($('#lblFullName').text());
                $('.clsActivetextAssignToName').html(member[idMember]['person']['person_first_name'] + ' ' + member[idMember]['person']['person_last_name']);
                $('#textAssignToEmail').html(member[idMember]['person']['contact_email_address']);
            }
            else if ($('#spActionEvent').html() == "Assign Automation MSG") {
                $('.clsActivespAssignToAction_msg').show();

                $('.clsActivespAssignTo_msg').html(member[idMember]['person']['person_id']);
                $('#textAssignFromName_msg').html($('#lblFullName').text());
                $('.clsActivetextAssignToName_msg').html(member[idMember]['person']['person_first_name'] + ' ' + member[idMember]['person']['person_last_name']);
                $('#textAssignToEmail_msg').html(member[idMember]['person']['contact_email_address']);
            }





            else if ($('#spActionEvent').html() == "Transfer") {
                $('#spTransferTo').html(member[idMember]['person']['person_id']);

                $('#textTransferFromName').html($('#lblFullName').text());
                $('#textTransferFromEmail').html($('#lblUsername').text());

                $('#textTransferToName').html(member[idMember]['person']['person_first_name'] + ' ' + member[idMember]['person']['person_last_name']);
                $('#textTransferToEmail').html(member[idMember]['person']['contact_email_address']);

                $('#modalTransferChat').modal('show');
            }



            $('#listMember').modal('hide');

        });

        //------------ view -------------------//



    }





    //----------------------------- Search Member ----------------------------------------//


    $('#btnSearchFilter').click(function () {
        if ($('#txtSearch').val() == "" && $('#cboSearchWith').val() == "") {
            showErrorMessage('Search value cannot be empty.');
            return;
        }
        member = Array();
        start_from = 0;
        $('#myLoader').modal('show');
        getMembers();
    });


    $('#btnRefresh').click(function () {
        $('#cboSearchWith').val('person_id_number');
        $('#txtSearch').val('');
        member = Array();
        start_from = 0;
        getMembers();
    });
    //----------------------------- Search Member ----------------------------------------//






    //------------------------------------------- Search Person --------------------------------------------------------------//







});