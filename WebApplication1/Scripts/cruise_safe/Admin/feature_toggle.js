﻿

$(document).ready(function () {



    var http = httpsapi + "/api/organisation/";
    //var http = httpsapi_local + '/api/organisation/';





    $('.actionAdministrator').trigger('click');
    $('.clsFeaturetoggle').find('a').addClass('activeMenu');



    var features;
    var id;
    var feature_status;



    function getFeatures() {
        $('#myLoader').modal('show');

        var details = {
            financial_year_company_id: $('#lblCompanyID').text()
        };
        $.getJSON(http + 'getFeatures', details, function (data) {
            //console.log('getFeatures',data);
            features = data;

            $('#zeroStats').hide();
            $('#listFeatures').html('');
            if (data.length > 0) {
                str = "";
                for (i = 0; i < data.length; i++) {

                    var featureStatus = 'label-success';
                    if (data[i]['status'] == 'off') {
                        featureStatus = 'label-danger';
                    }

                    str += '<tr><td>' + (i + 1) + '</td>';
                    str += '<td>' + data[i]['feature'] + '</td>';
                    str += '<td><span style="width:100px; cursor:pointer;" title="Switch feature on and off"  id="r' + i + '" class="badge btnActivateFeature ' + featureStatus + '">' + data[i]['status'] +'</span></td>';
                    str += '<td>  <button id="r' + i + '" style="width:100px; background-color:#05c8d2;" type="button" class="btn btn-primary btn-flat btn-xs btnView"> <i class="fas fa-eye"></i> View </button>  <button id="r' + i + '" style="width:100px" type="button" class="btn btn-default hidden btn-flat btn-xs btnRemove hidden"> <i class="fas fa-times"></i> Remove</button>   </td></tr>';
                }

                $('#listFeatures').html(str);
            }
            else {
                $('#zeroStats').show().html('No Result Found');
            }


            $('#myLoader').modal('hide');



            //------------ View -------------------//

            $('.btnView').click(function () {
                id = this.id.substring(1);

                $('#txtFeature').val(features[id]['feature']);
                $('#txtDescription').val(features[id]['description']);

                $('#btnAddFeature').addClass('hidden');
                $('#btnUpdateFeature').removeClass('hidden');

                $('.adminFeature-modal-lg').modal('show');
            });

            //------------ View -------------------//





            //------------ Active -------------------//

            $('.btnActivateFeature').click(function () {

                id = this.id.substring(1);
                var featureActivation = '';

                if (features[id]['status'] == 'off') {
                    feature_status = 'on';
                    featureActivation = 'activate';
                }
                else if (features[id]['status'] == 'on') {
                    feature_status = 'off';
                    featureActivation = 'switch off';
                }

                $('#btnConfirmActive').hide();
                $('#btnConfirm').show().text('I am sure. Activate Feature.');

                $('#confirmModal').modal('show');
                $('#lblConfirmMessage').html("Are you sure, you want to " + featureActivation + " this feature: " + features[id]['feature']);

            });

            //------------ Active -------------------//



        });
    }


    getFeatures();















    //------------------------------------------ Add Feature Toggle ------------------------------------------------------//



    $('#btnAddNew').click(function () {

        $('.adminFeature-modal-lg').modal('show');

        //--------------- Clear Fields ------------------//

        $('.cleanField').val("");

        //--------------- Clear Fields ------------------//

        $('#btnAddFeature').removeClass('hidden');
        $('#btnUpdateFeature').addClass('hidden');

    });

    $('#btnAddFeature').on('click', function () {
        if (vaidateFields('requiredFeature')) {
            return;
        }
        $('#myLoader').modal('show');

        var details = {
            feature: $('#txtFeature').val(),
            description: $('#txtDescription').val()
        };
        $.post(http + 'save_feature', details, function (data) {
            if (data.response_type == 'success') {
                showSuccessMessage(data.description);
                getFeatures();
                $('.adminFeature-modal-lg').modal('hide');

                setTimeout(function () {
                    $('#successModal').modal('hide');
                }, 1000);
            }
            else {
                showErrorMessage(data.description);
            }

            $('#myLoader').modal('hide');
        });
    });




    //------------------------------------------ Add Feature Toggle ------------------------------------------------------//

















    //------------------------------------------ Update Feature Toggle ------------------------------------------------------//




    $('#btnUpdateFeature').on('click', function () {
        if (vaidateFields('requiredFeature')) {
            return;
        }
        $('#myLoader').modal('show');

        var details = {
            eeze_feature_id: features[id]['eeze_feature_id'],
            feature: $('#txtFeature').val(),
            description: $('#txtDescription').val()
        };
        $.post(http + 'update_feature', details, function (data) {
            if (data.response_type == 'success') {
                showSuccessMessage(data.description);
                getFeatures();
                $('.adminFeature-modal-lg').modal('hide');

                setTimeout(function () {
                    $('#successModal').modal('hide');
                }, 1000);
            }
            else {
                showErrorMessage(data.description);
            }

            $('#myLoader').modal('hide');
        });
    });




    //------------------------------------------ Update Feature Toggle ------------------------------------------------------//















    //----------------------------------------- Remove -------------------------------------------//


    $('#btnConfirm').on('click', function () {
        $('#myLoader').modal('show');
        $('#confirmModal').modal('hide');

        var details = {
            eeze_feature_id: features[id]['eeze_feature_id'],
            status: feature_status
        };

        $.post(http + 'activate_feature', details, function (data) {

            if (data.response_type == 'success') {
                showSuccessMessage(data.description);
                getFeatures();
                $('#confirmModal').modal('hide');

                setTimeout(function () {
                    $('#successModal').modal('hide');
                }, 1000);
            }
            else {
                showErrorMessage(data.description);
            }

            $('#myLoader').modal('hide');

        });
    });



    //----------------------------------------- Remove -------------------------------------------//















});