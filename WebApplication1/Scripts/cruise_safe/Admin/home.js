﻿
$(document).ready(function () {


   // var http = httpsapi + "/api/admin/";
    var https = httpsapi + "/api/";
    var http = httpsapi_local + '/api/admin/';


















    checkLoggedInUser();
    function checkLoggedInUser() {
        $('.dvBusinessOwnerView').hide();
        if ($('#lblUserType').text() == 'business') {
            $('.dvBusinessOwnerView').show();
        }
    }




    //------------------------------------------- Populate Dropdowns --------------------------------------------------------------//
    var how_is_connection = checkNetworkConnection();

    function getRoles() {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        var details = {
            clientid: $('#lblCompanyID').text(),
            session_token: $('#lblUserToken').text(),
            system_user_id: $('#lblUserID').text()
        };
        $.getJSON('/as_smart_connects/getUserRoles', details, function (data) {
            //console.log('Roles', data);
            if (data) {
                $('#cboRole').html('<option value="">-- Choose Role --</option>');
                for (var i = 0; i < data['roles'].length; i++) {
                    var htmlcontent = '<option value="' + data['roles'][i].role_id + '">' + data['roles'][i].role_name + '</option>';
                    $('#cboRole').append(htmlcontent);
                }
            }
        });
    }
    getRoles();


    //------------------------------------------- Populate Dropdowns --------------------------------------------------------------//










    //----------------------------------------- Populate system views checkboxex --------------------------------------//

    getSelectSystemViews();
    function getSelectSystemViews() {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        //$('.myLoader').modal('show');

        $.getJSON('/as_smart_connects/getPrivileges', function (data) {
            //console.log(data);
            $('#first').html('');
            $('#second').html('');
            $('#first1').html('');
            $('#second1').html('');

            if (data) {
                var rem = data.length % 2;
                for (i = 0; i < data.length; i += 2) {
                    var category = data[i]['category'];

                    if (data[i]['category'] == 'Live Chat Support') {
                        category = 'Live_Chat_Support';
                    }
                    if (data[i]['category'] == 'Support Tickets') {
                        category = 'Support_Tickets';
                    }

                    $('#first').append(homeViewList(data[i]['right_id'], data[i]['right_name'], data[i]['right_description'], category));
                    $('#first1').append(homeViewList1(data[i]['right_id'], data[i]['right_name'], data[i]['right_description'], category));
                    if (i < data.length - 1) {
                        var category2 = data[i + 1]['category'];
                        if (data[i + 1]['category'] == 'Live Chat Support') {
                            category2 = 'Live_Chat_Support';
                        }
                        if (data[i + 1]['category'] == 'Support Tickets') {
                            category2 = 'Support_Tickets';
                        }

                        $('#second').append(homeViewList(data[i + 1]['right_id'], data[i + 1]['right_name'], data[i + 1]['right_description'], category2));
                        $('#second1').append(homeViewList1(data[i + 1]['right_id'], data[i + 1]['right_name'], data[i + 1]['right_description'], category2));
                    }
                }
            }
            else {
                showErrorMessage("no privilages");

            }
            $('.funkyradio').tooltip();

            //setTimeout(function () {
            //    $('#myLoader').modal('hide');
            //}, 1000);
        });
    }


    function homeViewList(id, name, desc, category) {
        var str = '<div class="funkyradio ' + category + '" title="' + desc + '"><div class="funkyradio-default">' +
            '<input type="checkbox" class="lstPriv listViewPage" data-category="' + category + '" data-page_id="' + id +'"  name="checkbox" id="c' + id + '">' +
            '<label for="c' + id + '" style="margin-top:0px">' + name + '</label>' +
            '</div></div>';
        return str;
    }

    function homeViewList1(id, name, desc, category) {
        var str = '<div class="funkyradio ' + category + '" title="' + desc + '"><div class="funkyradio-default">' +
            '<input type="checkbox" class="lstPriv1 listViewPage" data-category="' + category + '" data-page_id="' + id +'" name="checkbox" id="e' + id + '">' +
            '<label for="e' + id + '" style="margin-top:0px">' + name + '</label>' +
            '</div></div>';
        return str;
    }


    //----------------------------------------- Populate system views checkboxex --------------------------------------//














    var my_home_view;

    function getMyHomeView() {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }


        $('#myLoader').modal('show');

        var role_or_user = '';
        var user_or_role_id;
        if ($('#lblUserType').text() == 'business') {
            role_or_user = 'user';
            user_or_role_id = $('#lblUserID').text();
        }
        else {
            role_or_user = 'role';
            user_or_role_id = $('#lblUserRole').text();
        }

        var details = {
            session_token: $('#lblUserToken').text(),
            system_user_id: $('#lblUserID').text(),
            company_id: $('#lblCompanyID').text(),
            home_view_for_role_or_user: role_or_user,
            user_or_role_id: user_or_role_id
        };
        $.getJSON(http + 'getMyHomeView', details, function (data) {
            //console.log('getMyHomeView', data);
            my_home_view = data;




            if (data.length > 0) {
                $('.cls_my_home_view').hide();


                var distinct_category_arr = Array();
                for (var i = 0; i < data.length; i++) {
                    if (distinct_category_arr.indexOf(data[i]['view_category']) < 0) {
                        distinct_category_arr.push(data[i]['view_category']);
                    }
                }
                //console.log(distinct_category_arr);


                for (var ii = 0; ii < distinct_category_arr.length; ii++) {
                    //console.log(distinct_category_arr[ii]);
                    if (distinct_category_arr[ii] == 'Live Chat Support') {
                        $('.Live_Chat_Support').show();
                    }
                    else if (distinct_category_arr[ii] == 'Support Tickets') {
                        $('.Support_Tickets').show();
                    }
                    else {
                        $('.' + distinct_category_arr[ii]).show();
                    }
                    
                }


                for (var iii = 0; iii < data.length; iii++) {
                    //console.log(data[iii]['assigned_view_page_id']);
                    $('.cls_home_view_' + data[iii]['assigned_view_page_id']).show();
                }

            }
            else {
                $('.cls_my_home_view').show();
            }




            setTimeout(function () {
                $('#myLoader').modal('hide');
            }, 1000);

        });
    }

    getMyHomeView();













    $('.clsShowRightsByCategory').click(function () {
        $('.clsShowRightsByCategory').removeClass('btn-dark').addClass('btn-success').addClass('asbtn');
        $(this).removeClass('btn-success').removeClass('asbtn').addClass('btn-dark');

        $('.funkyradio').hide();
        var current_category = this.id;
        if (current_category != 'all')
            $('.' + current_category).show();
        else
            $('.funkyradio').show();
    });










    var textUserHomeView = '';
    var home_view_for_role_or_user = '';

    $('#btnCustomizeMyHomeView').click(function () {
        $('#dvCustomizeForRole').hide();

        $('.cleanField').val('');
        $('.lstPriv').prop("checked", false);
        $('#all').trigger('click');

        home_view_for_role_or_user = 'user';
        textUserHomeView = 'Select what you would like to have on your home view';
        $('#textUserHomeView').html(textUserHomeView);


        for (var i = 0; i < my_home_view.length; i++) {
            $('#c' + my_home_view[i]['assigned_view_page_id']).trigger('click');
        }


        $('.adminHomeView-modal-lg').modal('show');
    });






    $('#btnCustomizeRolesHomeView').click(function () {
        $('#dvCustomizeForRole').show();

        $('.cleanField').val('');
        $('.lstPriv').prop("checked", false);
        $('#all').trigger('click');

        home_view_for_role_or_user = 'role';
        textUserHomeView = 'Select what you would like your employees to have on their home view';
        $('#textUserHomeView').html(textUserHomeView);

        $('.adminHomeView-modal-lg').modal('show');
    });














    $('#cboRole').change(function () {
        $('.lstPriv').prop("checked", false);
        $('#all').trigger('click');
        how_is_connection = checkNetworkConnection();
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        if ($(this).val() != '') {
            getHomeViewForRoles($(this).val());
        }
    });




    function getHomeViewForRoles(user_or_role_id) {
        $('#myLoader').modal('show');

        var details = {
            session_token: $('#lblUserToken').text(),
            system_user_id: $('#lblUserID').text(),
            company_id: $('#lblCompanyID').text(),
            home_view_for_role_or_user: 'role',
            user_or_role_id: user_or_role_id
        };

        $.getJSON(http + 'getHomeViewForRoles', details, function (data) {

            if (data.length > 0) {
                for (var i = 0; i < data.length; i++) {
                    $('#c' + data[i]['assigned_view_page_id']).trigger('click');
                }
            }

            setTimeout(function () {
                $('#myLoader').modal('hide');
            }, 1000);

        });
    }

















    //------------------------ save home view ---------------------------//


    $('#btnSaveHomeView').on('click', function () {
        how_is_connection = checkNetworkConnection();
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        var user_or_role_id;
        if (home_view_for_role_or_user == 'user') {
            user_or_role_id = $('#lblUserID').text();
        }


        if (home_view_for_role_or_user == 'role') {
            if ($('#cboRole').val() == '') {
                showErrorMessage('Please select a role');
                return;
            }
            else {
                user_or_role_id = $('#cboRole').val();
            }
        }



        var view_pages_arr = Array();
        $('input:checkbox.listViewPage').filter(':checked').map(function () {
            var details = {
                session_token: $('#lblUserToken').text(),
                home_view_for_role_or_user: home_view_for_role_or_user,
                user_or_role_id: user_or_role_id,
                view_category: $(this).attr('data-category'),
                assigned_view_page_id: $(this).attr('data-page_id')
            };

            view_pages_arr.push(details);
            $(this).removeAttr('checked');
        }).get();


        if (view_pages_arr.length == 0) {
            showErrorMessage('You have not selected anything. ' + textUserHomeView);
            return;
        }

        $('.myLoader').modal('show');

        var details = {
            session_token: $('#lblUserToken').text(),
            system_user_id: $('#lblUserID').text(),
            user_or_role_id: user_or_role_id,
            home_view_for_role_or_user: home_view_for_role_or_user,
            company_id: $('#lblCompanyID').text(),
            home_view: view_pages_arr
        };

        //console.log(details);
        //return;
        $.post(http + 'save_work_view', details, function (data) {

            if (data.response_type == 'success') {

                if (home_view_for_role_or_user == 'user') {
                    getMyHomeView();
                }

                $('.adminHomeView-modal-lg').modal('hide');
                showSuccessMessage(data.description);
            }
            else {
                showErrorMessage(data.description);
            }
        });
    });



    //------------------------ save home view ---------------------------//





});






