﻿function checkPrev(myPrivs) {

    //-------------------- Check Prev ---------------------------//

    if ($('#lblUserType').text() != 'business') {
        if (myPrivs.indexOf(1002) >= 0) {

        } else {
            //   location.href = '/as_smart_connects/home';
        }
    }

    //-------------------- Check Prev ---------------------------//
}

$(document).ready(function () {


    var http = httpsapi + "/api/reporting/";
    var https = httpsapi + "/api/";
    //http = httpsapi_local + '/api/reporting/';


    var dataCategory = Array();

    var users = Array();
    var Teams = Array();
    var Suppliers = Array();
    var roles = Array();

    $('.actionAdministrator').trigger('click');
    $('.btnUserManagement').find('a').addClass('activeMenu');

    var chart_colors4 = [
        am4core.color("#ff0202"),
        am4core.color("#ff3102"),
    am4core.color("#ff6b02"),

    am4core.color("#ffa602"),
    am4core.color("#ffd402"),
    am4core.color("#fbff02"),
    am4core.color("#b5ff02")
    ];

    var chart_colors5 = [
        am5.color("#ff0202"),
        am5.color("#ff3102"),
    am5.color("#ff6b02"),
    am5.color("#ffa602"),
    am5.color("#ffd402"),
    am5.color("#fbff02"),
    am5.color("#b5ff02")
    ];


    var chart_colors5_mix = [
        am5.color("#ffd402"), 
       am5.color("#ff3102"),
        am5.color("#fbff02"),    
          am5.color("#ffa602"),
         am5.color("#ff0202"),
        am5.color("#ff6b02"),
        am5.color("#b5ff02")
    ];



    var destination;
    var source;
    var travelMode = 'DRIVING';
    var latMap = '';
    var lngMap = '';
    var town = '';
    var seletedTop1 = '';
    var seletedTop = '0';

    var allData = Array();
    var Categories = Array();
    var arr = Array();


    var arrCount = Array();
    var arrToday = Array();

    var subSeries, series;

    function createDataMix(type, type1) {
        var category = Array();
        var dataArray = Array();
        var series = Array();

        for (var i = 0; i < allData.length; i++) {
            var column = allData[i]['reporting_ticket'][type];
            var indx = category.indexOf(column);

            if (allData[i]['reporting_ticket']['ticket_category_id'] == Categories[seletedTop].category.category_id || seletedTop == '0') {

                if (indx < 0) {
                    category.push(column);
                    var obj = { category: column, value: 0, subData: Array() };
                    var arr = sortDesc(createDataWhereXY(type1, type, column));
                    for (var c = 0; c < arr.length; c++) {
                        var obj1 = {};
                        obj1[arr[c].column] = arr[c].value;
                        obj.value += arr[c].value;
                        obj.subData.push({ category: arr[c].column, value: arr[c].value })
                        obj = { ...obj, ...obj1 };
                        if (series.indexOf(arr[c].column) < 0)
                            series.push(arr[c].column);
                        
                    }
                    dataArray.push(obj);
                }
            }
           
        }
        console.log({ data: dataArray, series: series });
        return { data: dataArray, series: series };
    }



    function createDataWhere(type,whereType,whereValue) {
        var category = Array();
        var dataArray = Array();

        for (var i = 0; i < allData.length; i++) {

            if (allData[i]['reporting_ticket'][whereType] == Categories[whereValue].category.category_id || whereValue=='0') {
                var column = allData[i]['reporting_ticket'][type];
                var indx = category.indexOf(column);
                if (indx < 0) {
                    category.push(column);
                    dataArray.push({ column: column, value: 1 });
                }
                else {
                    dataArray[indx].value++;
                }
            }
        }
        return sortDesc(dataArray);
    }




    function createDataWhereXY(type, whereType, whereValue) {
        var category = Array();
        var dataArray = Array();

        for (var i = 0; i < allData.length; i++) {

            if (allData[i]['reporting_ticket'][whereType] == whereValue) {
                var column = allData[i]['reporting_ticket'][type];
                var indx = category.indexOf(column);
                if (indx < 0) {
                    category.push(column);
                    dataArray.push({ column: column, value: 1 });
                }
                else {
                    dataArray[indx].value++;
                }
            }
        }
        return sortDesc(dataArray);
    }



    function createData(type) {
        var category = Array();
        var dataArray = Array();

        for (var i = 0; i < allData.length; i++) {
            var column = allData[i]['reporting_ticket'][type];
            var indx = category.indexOf(column);
            if (indx < 0) {
                category.push(column);
                dataArray.push({ column: column, value: 1 });
            }
            else{
                dataArray[indx].value++;
            }
        }
        return sortDesc(dataArray);
    }



    function sortDesc(arr) {
        arr.sort(function (a, b) {
            return b.value - a.value;
        });
        return arr;
    }



    function getReporting() {


        $('#myLoader').modal('show');


        var details = {
            reporting_client_id: $('#lblClientID').text()
        };

        $.getJSON(http + 'getReportingAllTickets', details, function (data) {
            console.log('getReportingAllTickets', data);
            allData = data;


            loadData(true);

            loadMap();

          
            setTimeout(function () {
                $('#myLoader').modal('hide');
            }, 1000);

        });
    }

    getUsers(true);
    getCategories();




    function getCategories() {
        var details = {
            client_id: $('#lblClientID').text()
        };
        $.getJSON(http + 'getCategories', details, function (data) {
            console.log('getCategories', data);
            Categories = data;
            loadCategory();
            getReporting();
        });
    }




    function loadCategory() {
        var str = '';

        for (var r = -1; r < Categories.length; r++) {
            var name = r < 0 ? 'All' : Categories[r].category.category_name;
            var id = r < 0 ? 0 : Categories[r].category.category_id;
            var isActive = r < 0 ? 'active' : '';
            arr.push(id); arrCount.push(0); arrToday.push(0);
            str += '<div class="col-sm-6 col-lg-4"><div class="panel text-center btnWidget ' + isActive + '" id="cat' + id + '">' +
                '<div class="panel-heading" style="padding-bottom: 0px;"><h5 class="panel-title text-cornsilk font-light">' + name + '</h5>' +
                '</div><div class="panel-body p-t-0" style="padding:10px;"><h3 class="m-t-0 m-b-0" >' +
                '<i class="fas fa-chart-line text-primary m-r-10"></i> <b class="text-white">0</b></h3>' +
                '<small style="font-size:12px" class="text-cornsilk m-b-0 m-t-0"><b style="color:orange">0</b>  Older Then 5 Days</small></div></div></div>';
        }
        $('#listCategory').html(str);
    }






    function updateSeenStatus(reporting) {
        $.post(http + 'updateViewStatus', reporting, function (data) {
            console.log(data);
            if (data.indexOf('success') < 0) {
                showErrorMessage(data);
            }
        });
    }

    function getStatus(status, date) {
        date = date / 24;

        if (status == 'resolved' || status == 'completed')
            return '<span class="badge badge-success" style="width:105px" data-toggle="tooltip" data-placement="top" title="Completed">' + status + '</span>';
        else if (date < 5)
            return '<span class="badge badge-warning" style="width:105px" data-toggle="tooltip" data-placement="top" title="Was updated more than Five days ago, Please update urgently">' + status + '</span>';
        else
            return '<span class="badge badge-danger" style="width:105px" data-toggle="tooltip" data-placement="top" title="Was updated more than ' + date + ' day(s) ago, Please update">' + status + '</span>';
    }


    function getStatus1(status, date, status1) {
        date = date / 24;
        console.log(status, date, status1, status == status1);

        if (status == 'resolved' || status == 'completed' || status == status1)
            return '<span class="badge badge-success" style="width:105px" data-toggle="tooltip" data-placement="top" title="Completed">' + status + '</span>';
        else if (date < 5)
            return '<span class="badge badge-warning" style="width:105px" data-toggle="tooltip" data-placement="top" title="Was updated more than Five days ago, Please update urgently">' + status + '</span>';
        else
            return '<span class="badge badge-danger" style="width:105px" data-toggle="tooltip" data-placement="top" title="Was updated more than ' + date + ' day(s) ago, Please update">' + status + '</span>';
    }



    function getImage(reporting) {
        var str = '';
        for (var r = 0; r < reporting.length; r++) {
            for (var ii = 0; ii < reporting[r].image.length; ii++) {
                str += '<a data-magnify="gallery" data-src="" data-caption="" data-group="a" href="' + reporting[r].image[ii].image_location + '">' +
                    '<i class="fas fa-image" style="color:green; margin-right:5px"></i></a>';
            }
        }
        return str;
    }


    function getAssigned(data) {
        var str = '';
        if (data.ticket_other == 'acknowledged')
            str = getName(data.ticket_created_by);
        else if (data.ticket_other == 'inspection' || data.ticket_other == 'inspected')
            str = getName(data.ticket_inspector_id);
        else if (data.ticket_other == 'fixing' || data.ticket_other == 'fixed')
            str = getName(data.ticket_fixer_id);
        else if (data.ticket_other == 'completed' || data.ticket_other == 'resolved')
            str = getName(data.ticket_action_by);

        return str;
    }


    
    var imgPosition = 0;
    var selectedId;
    var selectedReportId = 0;
    function loadData(loadTop) {

        if (loadTop) {
            for (i = 0; i < allData.length; i++) {

                var indx = arr.indexOf(allData[i]['reporting_ticket']['ticket_category_id']);
                arrCount[indx]++;
                arrCount[0]++;
                var date = getHours(allData[i]['reporting_ticket']['ticket_created_date']);
                arrToday[indx] += (date > (24 * 5)) ? 1 : 0;
                arrToday[0] += (date > (24 * 5)) ? 1 : 0;

            }
            for (var c = 0; c < arr.length; c++) {
                $('#cat' + arr[c]).find('h3').find('b').html(arrCount[c]);
                $('#cat' + arr[c]).find('small').find('b').html(arrToday[c]);
            }

            loadXYChart(createDataMix('ticket_region_name', 'ticket_category'), 'chartdiv3');
            dataCategory = createDataMix('ticket_category', 'ticket_sub_category');

        }



        if (allData) {

           

            loadPieChart(createDataWhere('ticket_category', 'ticket_category_id', seletedTop), 'chartdiv');
            loadPieChart(createDataWhere('ticket_region_name', 'ticket_category_id', seletedTop), 'chartdiv1');



        


            series.data.setAll(dataCategory.data);
            subSeries.data.setAll(dataCategory.data[0].subData);


            var str = "";
            for (var w = 0; w < allData.length; w++) {
                if (allData[w]['reporting_ticket']['ticket_category'] == seletedTop
                    || (seletedTop == "Other" && (allData[w]['reporting_ticket']['ticket_category'] != "Pothole" && allData[w]['reporting_ticket']['ticket_category'] != "Broken Traffic Light"))) {

                    var date = getHours(allData[w]['reporting_ticket']['ticket_last_update']);

                    str += '<tr><td>' + (w + 1) + '</td>';
                    str += '<td>' + allData[w]['reporting_ticket']['ticket_reference'] + '</td>';
                    str += '<td>' + allData[w]['reporting_ticket']['ticket_town'] + '</td>';
                    str += '<td>' + allData[w]['reporting_ticket']['ticket_title'] + '</td>';
                    str += '<td>' + getAssigned(allData[w]['reporting_ticket']) + '</td>';
                    str += '<td>' + getStatus(allData[w]['reporting_ticket']['ticket_other'], date) + '</td>';
                    str += '<td>' + getDatee(allData[w]['reporting_ticket']['ticket_last_update']) + '</td>';
                    str += '<td>' + allData[w]['reporting_ticket']['ticket_issues'] + '</td>';
                    str += '<td><button id="b' + w + '" style="width:100px" ' +
                        'type="button" class="btn btn-primary btn-flat btn-xs btnView pull-right">View</button></td>';
                }
            }
            $('#tblReports').html(str);
        }



        $('.btnView').click(function () {
            selectedId = parseInt(this.id.substring(1));
            if (allData.length > 1)
                $('.isMany').show();
            else
                $('.isMany').hide();

            console.log(allData.length.length, selectedId);

            loadSelected(selectedId);
        });
    }



    function loadSelected(selectedId) {

        //if (allData[selectedId]['reporting_ticket'].reporting_status == "new") {
        //    allData[selectedId]['reporting_ticket'].reporting_status = "acknowledged";
        //    updateSeenStatus(allData[selectedId]['reporting_ticket']);
        //}
        $('.divHeaders').hide().slideDown('slow');


        if (allData[selectedId]['reporting_ticket'].reporting_status == "resolve")
            $('.btnResolve').hide();
        else
            $('.btnResolve').show();


        $('#viewModal').modal('show');
        $('.footer').hide('slow');
        latMap = allData[selectedId]['reporting_ticket'].ticket_latitude;
        lngMap = allData[selectedId]['reporting_ticket'].ticket_longitude;
        source = allData[selectedId]['reporting_ticket']['ticket_town'] + ' ' + allData[selectedId]['reporting_ticket']['ticket_province'];
        town = source + '';
        destination = { lat: parseFloat(latMap), lng: parseFloat(lngMap) };

        loadMap();


        $('.clsReferenceMain').html(allData[selectedId]['reporting_ticket']['ticket_reference']);
        $('.clsProvince').html(allData[selectedId]['reporting_ticket']['ticket_metropolitan'] + ' - ' + allData[selectedId]['reporting_ticket']['ticket_province']);
        $('.clsReference').html(allData[selectedId]['reporting_ticket']['ticket_reference']);
        $('.clsName').html(allData[selectedId]['reporting_ticket']['ticket_person_name']);
        $('.clsLocationType').html(allData[selectedId]['reporting_ticket']['ticket_location_type']);
        $('.clsType').html(allData[selectedId]['reporting_ticket']['ticket_title']);
        $('.clsCategory').html(allData[selectedId]['reporting_ticket']['ticket_category']);
        $('.clsMessage').val(allData[selectedId]['reporting_ticket']['ticket_body']);
        $('.clsDate').html(getDateTime(allData[selectedId]['reporting_ticket']['ticket_created_date']));
        $('.clsAddress').html(allData[selectedId]['reporting_ticket']['ticket_address']);
        var date = getHours(allData[selectedId]['reporting_ticket']['ticket_timestamp']);
        $('.clsStatus').html(getStatus(allData[selectedId]['reporting_ticket']['ticket_other']), date);
        $('.clsCoordinates').html(allData[selectedId]['reporting_ticket']['ticket_longitude'] + ',' + allData[selectedId]['reporting_ticket']['ticket_latitude']);
        var issues = allData[selectedId]['reporting_ticket']['ticket_issues'];
        $('.clsIssues').html(issues + " " + (issues == 1 ? 'Issue' : 'Issues'));
        $('.clsAcknowledgedBy').html(getName(allData[selectedId]['reporting_ticket']['ticket_created_by']));
        $('.clsAcknowledgeDate').html(getDateTime(allData[selectedId]['reporting_ticket']['ticket_timestamp']));


        $('.clsclsInspect').html('.');
        $('.divInspection').hide();
        if (allData[selectedId]['reporting_ticket']['ticket_inspector_id']) {
            $('.divInspection').show();
            $('.clsInspector').html(getName(allData[selectedId]['reporting_ticket']['ticket_inspector_id']));
            $('.clsInspectorType').html(allData[selectedId]['reporting_ticket']['ticket_inspector_type']);
            var date = getHours(allData[selectedId]['reporting_ticket']['ticket_inspector_assigned_date']);
            $('.clsInspectionStatus').html(getStatus(allData[selectedId]['reporting_ticket']['ticket_inspection_status'], date));
            $('.clsInspectorAssignedBy').html(getName(allData[selectedId]['reporting_ticket']['ticket_inspector_assigned_by']));
            $('.clsInspectorAssignDate').html(getDateTime(allData[selectedId]['reporting_ticket']['ticket_inspector_assigned_date']));
            var comment = allData[selectedId]['reporting_ticket']['ticket_inspector_assigned_comment'];
            $('.clsInspectionComment').html(comment ? comment : '.');
            $('.divInspectionFeedback').hide();
            if (allData[selectedId]['reporting_ticket']['ticket_inspector_feedback_date']) {
                $('.divInspectionFeedback').show();
                $('.clsInspectionFeedbackBy').html(getName(allData[selectedId]['reporting_ticket']['ticket_inspector_feedback_by']));
                $('.clsInspectionFeedbackStatus').html(getStatus1(allData[selectedId]['reporting_ticket']['ticket_inspector_feedback_status'], date, 'inspected'));
                $('.clsInspectionFeedbackDate').html(getDateTime(allData[selectedId]['reporting_ticket']['ticket_inspector_feedback_date']));
            }
        }


        $('.clsclsFix').html('.');
        $('.divFixing').hide();
        if (allData[selectedId]['reporting_ticket']['ticket_fixer_id']) {
            $('.divFixing').show();
            $('.clsFixer').html(getName(allData[selectedId]['reporting_ticket']['ticket_fixer_id']));
            $('.clsFixerType').html(allData[selectedId]['reporting_ticket']['ticket_fixer_type']);
            var date = getHours(allData[selectedId]['reporting_ticket']['ticket_fixer_assigned_date']);
            $('.clsFixingStatus').html(getStatus(allData[selectedId]['reporting_ticket']['ticket_inspection_status'], date));
            $('.clsFixerAssignedBy').html(getName(allData[selectedId]['reporting_ticket']['ticket_fixer_assigned_by']));
            $('.clsFixerAssignDate').html(getDateTime(allData[selectedId]['reporting_ticket']['ticket_fixer_assigned_date']));
            var comment = allData[selectedId]['reporting_ticket']['ticket_fixer_assigned_comment'];
            $('.clsFixingComment').html(comment ? comment : '.');
            $('.divFixingFeedback').hide();
            if (allData[selectedId]['reporting_ticket']['ticket_fixer_feedback_date']) {
                $('.divFixingFeedback').show();
                $('.clsFixingFeedbackBy').html(getName(allData[selectedId]['reporting_ticket']['ticket_fixer_feedback_by']));
                $('.clsFixingFeedbackStatus').html(getStatus1(allData[selectedId]['reporting_ticket']['ticket_fixer_feedback_status'], date, 'fixed'));
                $('.clsFixingFeedbackDate').html(getDateTime(allData[selectedId]['reporting_ticket']['ticket_fixer_feedback_date']));
            }
        }



        var str = '';
        for (var ii = 0; ii < allData[selectedId]['report_reportings']['image'].length; ii++) {
            str += '<a  style="margin-right:10px" data-magnify="gallery" data-src="" data-caption="" data-group="a" href="' + allData[selectedId]['report_reportings']['image'][ii].image_location + '">' +
                '<img src="' + allData[selectedId]['report_reportings']['image'][ii].image_location + '" height="95" width="95"></i></a>';
        }
        $('.clsImages').html(str);

        var strr = '';
        for (var a = 0; a < allData[selectedId]['report_reportings']['voice'].length; a++) {
            strr += '<audio controls id="myAudio"><source src="' + allData[selectedId]['report_reportings']['voice'][a].voice_location + '" type="audio/mpeg"></audio>';
        }
        $('.clsVoices').html(strr);
        $('#viewMapModal').modal('hide');

        var type = allData[selectedId]['reporting_ticket']['reporting_title'];
        if (allData[selectedId]['reporting_ticket']['ticket_body'] || allData[selectedId]['report_reportings']['image'].length > 0
            || allData[selectedId]['report_reportings']['voice'].length > 0)
            $('#divMsgImg').show('slow');
        else
            $('#divMsgImg').hide('slow');

    }


    var action = '';
    var action1 = '';
    var action2 = '';

    $('.btnActionType').click(function () {
        action1 = '';
        $('#cboReason').val('');
        $('.cboType').val('');
        $('.cboTypee').hide();
        $('.btnActionTypeDiv').hide();
        $('.' + this.id).show('slow');
        action = $(this).attr('data-type');
        if (this.id != 'btnCloseTicket')
            $('#clsComment').val(getMessages());
        else
            $('#clsComment').val('');
    });

    $('.cboType').change(function () {
        action2 = '';
        $('.btnAdd').html(' Select ');
        $('.cboTypee').hide();
        $('#' + $(this).val()).show('slow');
        action1 = $(this).val();
    });


    $('#cboReason').change(function () {
        action2 = '';
        action1 = $(this).val();
    });


    $('#btnSubmit').click(function () {
        if (action) {
            if (action1) {
                if (action == 'Close' || action2) {
                    allData[selectedId]['reporting_ticket']['ticket_action'] = action;
                    allData[selectedId]['reporting_ticket']['ticket_action1'] = action1;
                    allData[selectedId]['reporting_ticket']['ticket_action2'] = action2;
                    allData[selectedId]['reporting_ticket']['ticket_action3'] = $('#clsComment').val();
                    allData[selectedId]['reporting_ticket']['ticket_action_by'] = $('#lblUserID').text();
                    updateTicket();
                }
                else {
                    if (action1 == 'Employee')
                        showWarningMessageClose("Select an employee");
                    else if (action1 == 'Team')
                        showWarningMessageClose("Select a team");
                    else if (action1 == 'Supplier')
                        showWarningMessageClose("Select a supplier");
                }
            }
            else {
                if (action == 'Close')
                    showWarningMessageClose("Select a reason");
                else if (action == 'Inspect')
                    showWarningMessageClose("Select an inspector");
                else if (action == 'Fix')
                    showWarningMessageClose("Select a fixer");
            }
        }
        else
            showWarningMessageClose("Select an action");
    });

    function updateTicket() {

        $('#myLoader').modal('show');
        $.post(http + 'updateTicket', allData[selectedId]['reporting_ticket'], function (data) {
            console.log(data);
            if (data.indexOf('success') >= 0) {
                showSuccessMessage('Success');
                $('.acionModal').modal('hide');
                $('#viewModal').modal('hide');
                $('#viewMapModal').modal('hide');
                $('.footer').show('slow');
                if (document.getElementById("myAudio")) {
                    document.getElementById("myAudio").pause();
                    $('#myAudio').remove();
                }
                allData.splice(selectedId, 1);
                loadData(false);
                $('#myLoader').modal('hide');
            } else {
                showErrorMessageClose(data);
            }
        });
    }








    $('#btnEmployee').click(function () {
        listUsersAll();
    });
    function listUsersAll() {
        var str = '';
        $('#btnAddNewUser').html('New User');
        $(".clsTitle").html('Select Employee');
        for (var m = 0; m < users.length; m++) {
            var name = users[m]['user_title'] + ' ' + users[m]['user_firstname'] + ' ' + users[m]['user_surname'];
            str += '<div class="col-sm-6 col-lg-6 memberr"><div class="panel text-center btnWidget3 userList" data-id="' + users[m]['user_id'] + '"  data-name="' + name + '">' +
                '<div class="panel-body p-t-10" style="padding: 20px;"><h4 class="m-t-0 m-b-15">' +
                '<i class="fas fa-user text-primary m-r-10"></i><b>' + name + '</b></h4>' +
                '<p class="text-muted m-b-0 m-t-20"> ' + users[m].user_email + '</p></div></div></div>';
        }
        $('#listAllDiv').html(str);
        listAll('btnEmployee');


    }
    function getUsers(loadMore) {

        var details = {
            clientid: $('#lblClientID').text()
        };

        $.getJSON('/admin/getUsers', details, function (data) {
            users = data;
            if (loadMore)
                getTeams();
            else
                listUsersAll();
        });
    }
    function getName(user_id) {
        var name = '';
        for (var l = 0; l < users.length; l++) {
            if (users[l]['user_id'] == user_id)
                name = users[l]['user_firstname'] + ' ' + users[l]['user_surname'];
        }
        return name;
    }




    $('#btnTeam').click(function () {
        listTeamsAll();
    });
    function getTeams() {

        var details = {
            client_id: $('#lblClientID').text()
        };
        console.log(details);
        $.getJSON(https + 'Organisation/get_teams', details, function (data) {
            console.log('getTeams', data);
            Teams = data;
            getSuppliers();
        });
    }
    function listTeamsAll() {
        var str = '';
        $('#btnAddNewUser').html('Team Management');
        $(".clsTitle").html('Select Team');
        for (var m = 0; m < Teams.length; m++) {
            str += '<div class="col-sm-6 col-lg-6 memberr"><div class="panel text-center btnWidget3 userList"  data-id="' +
                Teams[m]['cruise_safe_organo_team']['team_id'] + '" data-name="' + Teams[m]['cruise_safe_organo_team']['team_name'] + '">' +
                '<div class="panel-body p-t-10" style="padding: 20px;"><h4 class="m-t-0 m-b-15">' +
                '<i class="fas fa-users text-primary m-r-10"></i><b>' + Teams[m]['cruise_safe_organo_team']['team_name'] + '</b></h4>' +
                '<p class="text-muted m-b-0 m-t-20"> Team Leader: ' + getName(Teams[m]['cruise_safe_organo_team'].team_leader) + '</p></div></div></div>';
        }
        $('#listAllDiv').html(str);
        listAll('btnTeam');


    }




    $('#btnSupplier').click(function () {
        listSuppliersAll();
    });
    function getSuppliers() {
        var details = {
            client_id: $('#lblClientID').text()
        };

        $.getJSON(https + 'Organisation/get_suppliers', details, function (data) {
            console.log('getSuppliers', data);
            Suppliers = data;

            getRoles();
        });
    }
    function listSuppliersAll() {
        var str = '';
        $('#btnAddNewUser').html('Supplier Management');
        $(".clsTitle").html('Select Supplier');
        for (var m = 0; m < Suppliers.length; m++) {
            str += '<div class="col-sm-6 col-lg-6 memberr"><div class="panel text-center btnWidget3 userList" data-id="' +
                Suppliers[m]['cruise_safe_organo_supplier']['supplier_id'] + '" data-name="' + Suppliers[m]['cruise_safe_organo_supplier']['supplier_company_name'] + '">' +
                '<div class="panel-body p-t-10" style="padding: 20px;"><h4 class="m-t-0 m-b-15">' +
                '<i class="fas fa-users text-primary m-r-10"></i><b>' + Suppliers[m]['cruise_safe_organo_supplier']['supplier_company_name'] + '</b></h4>' +
                '<p class="text-muted m-b-0 m-t-20"> Contact Person: ' + getName(Suppliers[m]['cruise_safe_organo_supplier']['supplier_contact_person']) + '</p></div></div></div>';
        }
        $('#listAllDiv').html(str);
        listAll('btnSupplier');
    }




    function listAll(btn) {
        $("#txtSearch").val('');
        $('.btnPickOne').hide();
        $('.usersmodallg').modal('show');

        $("#txtSearch").on("keyup", function () {
            var value = $(this).val().toLowerCase();
            $("#listAllDiv div.memberr").filter(function () {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
            });
            $('#rows').html($("#listAllDiv div.memberr:visible").length);
        });


        $('#btnPickOne').on('click', function () {
            action2 = $(this).attr('data-id');
            $('#' + btn).attr('data-id', $(this).attr('data-id'));
            $('#' + btn).html($(this).attr('data-name')).css('border-color', 'rgba(42, 50, 60, 0.2)');

            $('.usersmodallg').modal('hide');
        });

        $(document).on('click', ".btnWidget3", function () {
            $(document).find(".btnWidget3").removeClass('active');
            $(this).addClass('active');
            $('#btnPickOne').show('slow').html('Select ' + $(this).attr('data-name')).attr('data-id', $(this).attr('data-id'));;
            $('#btnPickOne').attr('data-name', $(this).attr('data-name'));
        });


    }


    function getRoles() {

        roles = Array();

        var details = {
            clientid: $('#lblClientID').text(),
            //  userId: $('#lblUserID').text(),
        };
        $.getJSON('/admin/getUserRoles', details, function (data) {
            console.log('Roles', data);
            if (data) {
                roles = data['roles'];
                $('#cboRole').html('<option value="">-- Choose Role --</option>');
                for (var i = 0; i < data['roles'].length; i++) {
                    if (data['roles'][i].role_category == 'supplier') {
                        var htmlcontent = '<option value="' + data['roles'][i].role_id + '">' + data['roles'][i].role_name + '</option>';
                        $('#cboRole').append(htmlcontent);
                    }
                }
            }
        });

    }


    $('#btnAddUser').on('click', function () {
        saveUser();
    });

    $('#btnAddNewUser').click(function () {
        if ($('#btnAddNewUser').text() == 'Team Management')
            location.href = '/Organisation/Team';
        else if ($('#btnAddNewUser').text() == 'Supplier Management')
            location.href = '/Organisation/Suppliers';
        else {
            $('.clsPassword').show();
            $('.clsPasswords').removeAttr('disabled').addClass('requiredUser');
            $('#btnAddUser').html('Save');
            $('.cleanUsertField').val('');
            $('.adminUsers-modal-lg').modal('show');
        }
    });


    function saveUser() {

        if (vaidateFields('requiredUser'))
            return;

        if ($('#txtPassword').val() != $('#txtConfirmPassword').val()) {
            showConnectionMessage("Passwords do not match");
            return;
        }


        $('.myLoader').modal('show');

        var details = {
            user_id: null,
            user_client_id: $('#lblClientID').text(),
            user_id_number: $('#txtIDNumber').val(),
            user_title: $('#cboTitle').val(),
            user_firstname: $('#txtFirstName').val(),
            user_surname: $('#txtLastName').val(),
            user_gender: $('#cboGender').val(),
            user_cellphone: $('#txtCellNumber').val(),
            user_email: $('#txtEmailAddress').val(),
            user_role_id: $('#cboRole').val(),
            user_password: $('#txtPassword').val(),
            user_saved_by: $('#lblUserID').text(),
            user_updated_by: $('#lblUserID').text()

        };
        console.log(details);
        $.post(https + 'admin/addUser', details, function (data) {
            console.log(data);
            if (data.indexOf('success') >= 0) {
                showSuccessMessageClose(data);
                getUsers(false);
                $('#myLoader').modal('hide');

                $('.adminUsers-modal-lg').modal('hide');
            }
            else {


                $('#myLoader').modal('hide');

                showErrorMessage(data);
            }
        });
    }















    function getMessages() {
        var str = '';
        for (var a = 0; a < allData[selectedId]['report_reportings'].length; a++) {
            var report = allData[selectedId]['report_reportings'][a]['report_itt'];
            if (report.reporting_title == 'User Reporting' && report.reporting_body) {
                str += report.reporting_person_name + ' : ' + report.reporting_body + '\r\n';
            }
            console.log(report.reporting_title, report.reporting_body);
        }
        return str;
    }



    $('.btnAction').click(function () {
        $('.cboType').val('');
        $('.cboTypee').hide();
        $('.btnActionTypeDiv').hide();
        action = '';
        $('.acionModal').modal('show');
    });


    $('#btnPrev').click(function () {
        if (selectedId > 0)
            selectedId--;
        else
            selectedId = allData.length - 1;
        loadSelected(selectedId);
    });
    $('#btnNext').click(function () {
        if (selectedId < (allData.length - 1))
            selectedId++;
        else
            selectedId = 0;
        loadSelected(selectedId);
    });


    $('.clsRight').click(function () {
        imgPosition++;
        if (imgPosition >= allData[selectedId]['image'].length)
            imgPosition = 0;

        $('.clsImage').attr('src', allData[selectedId]['image'][imgPosition].image_location);
    });


    $('.clsLeft').click(function () {
        imgPosition--;
        if (imgPosition < 0)
            imgPosition = allData[selectedId]['image'].length - 1;

        $('.clsImage').attr('src', allData[selectedId]['image'][imgPosition].image_location);
    });


    $('.btnClose').click(function () {
        $('#viewModal').modal('hide');
        $('.footer').show('slow');
        if (document.getElementById("myAudio"))
            document.getElementById("myAudio").pause();
        $('#myAudio').remove();
    });


    $('.btnCloseMap').click(function () {
        $('#viewMapModal').modal('hide');
        $('.footer').show('slow');
    });


    $('.btnResolve').click(function () {
        if (allData[selectedId]['report_itt'].reporting_status == "acknowledged") {
            allData[selectedId]['report_itt'].reporting_status = "resolved";
            updateResolveStatus(allData[selectedId]['report_itt']);
        }

    });

    function updateResolveStatus(reporting) {
        $.post(http + 'updateResolveStatus', reporting, function (data) {
            console.log(data);
            if (data.indexOf('success') >= 0) {
                showSuccessMessage('Success');
                $('#viewModal').modal('hide');
                $('#viewMapModal').modal('hide');
                $('.footer').show('slow');
                if (document.getElementById("myAudio")) {
                    document.getElementById("myAudio").pause();
                    $('#myAudio').remove();
                }
            } else {
                showErrorMessageClose(data);
            }
        });
    }





    $(document).on('click', ".btnWidget", function () {
        $(document).find(".btnWidget").removeClass('active');
        $(this).addClass('active');

        if (!$(this).hasClass('btnActionType')) {
            seletedTop = this.id.replace('cat', '');
            loadData(false);
            $('.selectedTitle').html($('#cat' + seletedTop).find('h5').html());
            loadMap1();
            loadMap4();
        }
    });

 





    am5.ready(function () {

        // Create root element
        // https://www.amcharts.com/docs/v5/getting-started/#Root_element
        var root = am5.Root.new("chartdivPie");


 

        // Set themes
        // https://www.amcharts.com/docs/v5/concepts/themes/
        root.setThemes([am5themes_Animated.new(root)]);




        var container = root.container.children.push(
            am5.Container.new(root, {
                width: am5.p100,
                height: am5.p100,
                layout: root.horizontalLayout
            })
        );

        // Create main chart
        // https://www.amcharts.com/docs/v5/charts/percent-charts/pie-chart/
        var chart = container.children.push(
            am5percent.PieChart.new(root, {
                tooltip: am5.Tooltip.new(root, {})
            })
        );

       
        // Create series
        // https://www.amcharts.com/docs/v5/charts/percent-charts/pie-chart/#Series
        series = chart.series.push(
            am5percent.PieSeries.new(root, {
                valueField: "value",
                categoryField: "category",
                alignLabels: true
            })
        );

        console.log('console', root.interfaceColors);

        series.get("colors").set("colors", chart_colors5_mix);


        series.labels.template.set("visible", false);
        series.labels.template.setAll({
            textType: "circular",
            radius: 4,
            fontSize: 13,
            fill: "orange"
        });
        series.ticks.template.set("visible", false);
        series.slices.template.set("toggleKey", "none");

        // add events
        series.slices.template.events.on("click", function (e) {
            selectSlice(e.target);
        });


        // Adding gradients
        series.slices.template.set("strokeOpacity", 1);
        series.slices.template.set("fillGradient", am5.RadialGradient.new(root, {
            stops: [{
                brighten: 1.5,opacity:0.8
            }, {
                    brighten: 1, opacity: 0.85
            }, {
                    brighten: 0.5, opacity: 0.9
            }, {
                    brighten: 0, opacity: 0.95
            }, {
                    brighten: -0.1, opacity: 1
            }]
        }));

        // Create sub chart
        // https://www.amcharts.com/docs/v5/charts/percent-charts/pie-chart/
        var subChart = container.children.push(
            am5percent.PieChart.new(root, {
                radius: am5.percent(50),
                tooltip: am5.Tooltip.new(root, {})
            })
        );

        // Create sub series
        // https://www.amcharts.com/docs/v5/charts/percent-charts/pie-chart/#Series
        subSeries = subChart.series.push(
            am5percent.PieSeries.new(root, {
                valueField: "value",
                categoryField: "category",
                alignLabels: true
            })
        );

    

        subSeries.get("colors").set("colors", chart_colors5_mix);
     //   subSeries.data.setAll([]);
        subSeries.ticks.template.setAll({
            location: 1
        });
        subSeries.slices.template.set("toggleKey", "none");
        subSeries.labels.template.setAll({
            textType: "circular",
            radius: 4,
            fontSize: 13,
            fill: "white"
        });

        // Adding gradients
        subSeries.slices.template.set("strokeOpacity", 1);
        subSeries.slices.template.set("fillGradient", am5.RadialGradient.new(root, {
            stops: [{
                brighten: 1.5, opacity: 0.8
            }, {
                brighten: 1, opacity: 0.85
            }, {
                brighten: 0.5, opacity: 0.9
            }, {
                brighten: 0, opacity: 0.95
            }, {
                brighten: -0.1, opacity: 1
            }]
        }));

        var selectedSlice;

        series.on("startAngle", function () {
            updateLines();
        });

        container.events.on("boundschanged", function () {
            root.events.on("frameended", function () {
                updateLines();
            })
        })

        function updateLines() {
            if (selectedSlice) {
                var startAngle = selectedSlice.get("startAngle");
                var arc = selectedSlice.get("arc");
                var radius = selectedSlice.get("radius");

                var x00 = radius * am5.math.cos(startAngle);
                var y00 = radius * am5.math.sin(startAngle);

                var x10 = radius * am5.math.cos(startAngle + arc);
                var y10 = radius * am5.math.sin(startAngle + arc);

                var subRadius = subSeries.slices.getIndex(0).get("radius");
                var x01 = 0;
                var y01 = -subRadius;

                var x11 = 0;
                var y11 = subRadius;

                var point00 = series.toGlobal({ x: x00, y: y00 });
                var point10 = series.toGlobal({ x: x10, y: y10 });

                var point01 = subSeries.toGlobal({ x: x01, y: y01 });
                var point11 = subSeries.toGlobal({ x: x11, y: y11 });

                line0.set("points", [point00, point01]);
                line1.set("points", [point10, point11]);
            }
        }

        // lines
        var line0 = container.children.push(
            am5.Line.new(root, {
                position: "absolute",
                stroke: "orange",
                strokeDasharray: [2, 2]
            })
        );
        var line1 = container.children.push(
            am5.Line.new(root, {
                position: "absolute",
                stroke: "orange",
                strokeDasharray: [2, 2]
            })
        );

        // Set data
        // https://www.amcharts.com/docs/v5/charts/percent-charts/pie-chart/#Setting_data
      //  series.data.setAll([]);

        function selectSlice(slice) {
            selectedSlice = slice;
            var dataItem = slice.dataItem;
            var dataContext = dataItem.dataContext;

            if (dataContext) {
                var i = 0;
                subSeries.data.each(function (dataObject) {
                    subSeries.data.setIndex(i, dataContext.subData[i]);
                    i++;
                });
            }

            var middleAngle = slice.get("startAngle") + slice.get("arc") / 2;
            var firstAngle = series.dataItems[0].get("slice").get("startAngle");

            series.animate({
                key: "startAngle",
                to: firstAngle - middleAngle,
                duration: 1000,
                easing: am5.ease.out(am5.ease.cubic)
            });
            series.animate({
                key: "endAngle",
                to: firstAngle - middleAngle + 360,
                duration: 1000,
                easing: am5.ease.out(am5.ease.cubic)
            });
        }

        container.appear(1000, 10);

        series.events.on("datavalidated", function () {
            selectSlice(series.slices.getIndex(0));
        });

    }); // end am5.ready()













    /***************************************************************
                        Map Starts
    ***************************************************************/

    var selector2 = 'all';

    var mapData;
    var features = Array();
    var markers = Array();
    var heatmapData = Array();
    var heatmap;

    var map = new google.maps.Map(document.getElementById('dvMap1'), {
        zoom: 10,
        center: new google.maps.LatLng(-26.2566038, 27.9848897),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false,
        options: {
            gestureHandling: 'greedy'
        }
    });



    // Add a style-selector control to the map.
    var styleControl = document.getElementById('style-selector-control');
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(styleControl);

    var styleSelector = document.getElementById('style-selector');
    map.setOptions({ styles: styles[styleSelector.value] });
    map.setTilt(45);
    styleSelector.addEventListener('change', function () {
        if (styleSelector.value == 'satellite') {
            map.setMapTypeId(google.maps.MapTypeId.HYBRID);
            map.setOptions({ styles: null });
        }
        else {
            map.setMapTypeId(google.maps.MapTypeId.ROADMAP);
            map.setOptions({ styles: styles[styleSelector.value] });
        }
    });



    $.getJSON('/Content/files/geo.json', function (data) {
        mapData = data;
        features = map.data.addGeoJson(mapData);
    });

    map.data.setStyle({
        icon: '//example.com/path/to/image.png',
        fillColor: 'lime',
        strokeWeight: 1
    });

    var styleSelector1 = document.getElementById('style-selector1');
    styleSelector1.addEventListener('change', function () {
        for (var i = 0; i < features.length; i++)
            map.data.remove(features[i]);
        heatmap.setMap(null);
        for (var i = 0; i < markers.length; i++)
            markers[i].setMap(null);

        if (styleSelector1.value == 'All') {
            features = map.data.addGeoJson(mapData);
            heatmap.setMap(map);
            for (var i = 0; i < markers.length; i++)
                markers[i].setMap(map);
        }
        else if (styleSelector1.value == 'Reports') {
            for (var i = 0; i < markers.length; i++)
                markers[i].setMap(map);
        }
        else if (styleSelector1.value == 'Heat') {
            heatmap.setMap(map);
        }
        else if (styleSelector1.value == 'Shape') {
            features = map.data.addGeoJson(mapData);
        }
    });





    var styleSelector2 = document.getElementById('style-selector2');
    styleSelector2.addEventListener('change', function () {
        selector2 = styleSelector2.value;
        if (heatmap)
            heatmap.setMap(null);
        loadMap();
    });





    map.data.addListener('click', function (event) {
        var bounds = new google.maps.LatLngBounds();
        event.feature.h.h[0].h.forEach(function (coord, index) {
            bounds.extend(coord);
        });
        //  map.setZoom(11);

        map.setCenter(bounds.getCenter());
        map.fitBounds(bounds);
    
    });
    google.maps.event.addListenerOnce(map, 'bounds_changed', function () {
        map.setZoom(map.getZoom() - 1);
    });


    map.data.addListener('mouseover', function (event) {
        map.data.overrideStyle(event.feature, { fillColor: 'red' });
    });
    map.data.addListener('mouseout', function (event) {
        map.data.overrideStyle(event.feature, { fillColor: 'lime' });
    });

    map.data.addListener('mouseover', mouseOverDataItem);
    map.data.addListener('mouseout', mouseOutOfDataItem);

    function mouseOverDataItem(mouseEvent) {
        const titleText = mouseEvent.feature.j.REGIONNAME;
        if (titleText) {
            map.getDiv().setAttribute('title', titleText);
        }
    }

    function mouseOutOfDataItem(mouseEvent) {
        map.getDiv().removeAttribute('title');
    }





    function loadMap() {

        clearMarkers();
        heatmapData = Array();


        var latlngbounds = new google.maps.LatLngBounds();
        for (var i = 0; i < allData.length; i++) {

            if (selector2 == 'all' || selector2 == allData[i]['reporting_ticket'].ticket_status) {

                var latLng = new google.maps.LatLng(parseFloat(allData[i]['reporting_ticket'].ticket_latitude)
                    , parseFloat(allData[i]['reporting_ticket'].ticket_longitude));


                var date = getHours(allData[i]['reporting_ticket']['ticket_created_date']);

                heatmapData.push({ location: latLng, weight: 0.2 })

                var div = document.createElement('div');
                div.className = (allData[i]['reporting_ticket']['ticket_status'] == 'new') ? 'ripple-redblue' :
                    (allData[i]['reporting_ticket']['ticket_status'] == 'acknowledged') ? 'ripple-red2' :
                        (allData[i]['reporting_ticket']['ticket_status'] == 'processing') ? 'ripple-redpep' :
                            (allData[i]['reporting_ticket']['ticket_status'] == 'paused') ? 'ripple-red1' :
                                (allData[i]['reporting_ticket']['ticket_status'] == 'out of bounds') ? 'ripple-red' :
                                    (allData[i]['reporting_ticket']['ticket_status'] == 'resolved') ? 'ripple-red3' : 'ripple-redsky';


                var marker = new RichMarker({
                    position: latLng,
                    map: map,
                    id: i,
                    flat: true,
                    content: div,
                    draggable: false,
                    animation: google.maps.Animation.DROP,
                    title: allData[i]['reporting_ticket'].ticket_category + ' - ' + allData[i]['reporting_ticket'].ticket_sub_category
                });
                markers.push(marker);

                (function (marker) {
                    google.maps.event.addListener(marker, "click", function (e) {
                        if (map.getZoom() != 18) {
                            map.setZoom(18);
                            map.setCenter(marker.getPosition());
                        } else {
                            loadSelected(marker.id);
                        }
                    });

                    google.maps.event.addListener(marker, "dblclick", function (e) {
                        loadSelected(marker.id);
                    });

                })(marker);
                latlngbounds.extend(marker.position);
            }
        }






        heatmap = new google.maps.visualization.HeatmapLayer({
            data: heatmapData
        });

        heatmap.setMap(map);
        heatmap.set("radius", heatmap.get("radius") ? null : 50);

        const trafficLayer = new google.maps.TrafficLayer();

        trafficLayer.setMap(map);

        //if (allData.length > 0)
        //    map.setCenter(latlngbounds.getCenter());
        //else
        //    map.setCenter(new google.maps.LatLng(-25.7307893, 28.007999));
        //map.setZoom(10);


    }































    function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    }

    function clearMarkers() {
        setMapOnAll(null);
    }

    function showMarkers() {
        setMapOnAll(map);
    }

    function deleteMarkers() {
        clearMarkers();
        markers = [];
    }

















    function vaidateFields(clas) {
        $('.' + clas).css('border-color', 'silver');
        var input;
        $('.' + clas).map(function () {
            if (!$(this).val() || $(this).css('border-color') == 'red') {
                $(this).css('border-color', 'red');
                $(this).focus();
                input = $(this);
            }
        }).get();

        if (input) {
            input.focus();
            return true;
        }
        else
            return false;
    }

    function validateEmail(sEmail, msg) {
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(sEmail)) {
            return true;
        }
        else {
            //var msg = "Please enter valid email address.";
            checkEmptyFields(msg);
            return false;
        }
    }

    function SuccessMessage(msg) {
        $('#myPleaseWait').modal('show');
        $('#txtModalLoaderAlertMessage').hide;
        $('#txtModalLoaderSuccessMessage').show();
        $('#txtModalLoaderSuccessMessage b').html(msg);

        setTimeout(function () {
            $('#myPleaseWait').hide();
            $('#txtModalLoaderSuccessMessage').hide();
            $('#myPleaseWait').modal('hide');
        }, 3000);
    }

    function checkEmptyFields(msg) {

        $('#myPleaseWait').modal('show');
        $('#txtModalLoaderAlertMessage').show();
        $('#txtModalLoaderAlertMessage b').text(msg);

        setTimeout(function () {
            $('#myPleaseWait').hide();
            $('#myPleaseWait').modal('hide');
            $('#txtModalLoaderAlertMessage').hide();
        }, 3000);
    }



    /***************************************************************
                        Validations Ends
    ***************************************************************/



    //Removing  Messages CoOperation Services

    $(document).on('click', function () {
        if ($('#txtModalLoaderAlertMessage').is(':visible')) {
            $('#txtModalLoaderAlertMessage').hide();
        }
        if ($('#txtModalLoaderSuccessMessage').is(':visible')) {
            $('#txtModalLoaderSuccessMessage').hide();
        }

    });































        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end

        // Create chart instance
       






      



    function loadPieChart(data, chartdiv) {
        var chart = am4core.create(chartdiv, am4charts.PieChart);
        console.log('data', data);
        chart.data = data;
        chart.colors.list = chart_colors4;

        // Add and configure Series
        var pieSeries = chart.series.push(new am4charts.PieSeries());
        pieSeries.dataFields.value = "value";
        pieSeries.dataFields.category = "column";
        pieSeries.innerRadius = am4core.percent(50);
        pieSeries.ticks.template.disabled = true;
        pieSeries.labels.template.disabled = true;
        pieSeries.colors.list = chart_colors4;

        var rgm = new am4core.RadialGradientModifier();
        rgm.brightnesses.push(-0.8, -0.8, -0.5, 0, - 0.5);
        pieSeries.slices.template.fillModifier = rgm;
        pieSeries.slices.template.strokeModifier = rgm;
        pieSeries.slices.template.strokeOpacity = 0.4;
        pieSeries.slices.template.strokeWidth = 0;

        chart.legend = new am4charts.Legend();
        chart.legend.position = "right";
        chart.legend.labels.template.maxWidth = 150;
        chart.legend.labels.template.truncate = true;
        chart.legend.valueLabels.template.fill = am4core.color("orange"); 
        chart.legend.labels.template.fill = am4core.color("white");
    }





    function loadXYChart(data, chartdiv) {

        var chart = am4core.create(chartdiv, am4charts.XYChart)
       
        chart.data = data.data;

        chart.colors.list = chart_colors4;

        // Create axes
        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "category";
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.labels.template.rotation = -45;
        categoryAxis.renderer.grid.template.disabled = false;
        categoryAxis.renderer.labels.template.fill = am4core.color("white");
        categoryAxis.renderer.minGridDistance = 20;


        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.renderer.inside = true;
        valueAxis.renderer.labels.template.disabled = true;
        valueAxis.min = 0;
        valueAxis.renderer.grid.template.strokeOpacity = 0.3;
        valueAxis.renderer.grid.template.stroke = am4core.color("orange");
        valueAxis.renderer.labels.template.fill = am4core.color("white");

        // Create series
        function createSeries(field, name) {

            // Set up series
            var series = chart.series.push(new am4charts.ColumnSeries());
            series.name = name;
            series.dataFields.valueY = field;
            series.dataFields.categoryX = "category";
            series.sequencedInterpolation = true;

            // Make it stacked
            series.stacked = true;

            // Configure columns
            series.columns.template.width = am4core.percent(60);
            series.columns.template.tooltipText = "[bold]{name}[/]\n[font-size:14px]{categoryX}: {valueY}";

            // Add label
            var labelBullet = series.bullets.push(new am4charts.LabelBullet());
            labelBullet.label.text = "{valueY}";
            labelBullet.locationY = 0.5;
            labelBullet.label.hideOversized = true;

            return series;
        }

        for (var i = 0; i < data.series.length; i++) {
            createSeries(data.series[i], capEachWord(data.series[i]));
        }

        // Legend
        //chart.legend = new am4charts.Legend();
        //chart.legend.valueLabels.template.fill = am4core.color("orange");
        //chart.legend.labels.template.fill = am4core.color("orange");

    }











});



















