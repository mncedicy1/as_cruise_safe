﻿

$(document).ready(function () {


    var http = httpsapi + "/api/organisation/";
    //var http = httpsapi_local + '/api/organisation/';




    var how_is_connection = checkNetworkConnection();



    var organogram;
    var structure;
    var id;




    //-------------------------------------------------- Load my organogram -------------------------------------------------------//



    function getMyOrganogram() {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        //$('#myLoader').modal('show');

        var details = {
            company_id: $('#lblCompanyID').text(),
            session_token: $('#lblUserToken').text(),
            system_user_id: $('#lblUserID').text(),
        };
        $.getJSON(http + 'getMyOrganogram', details, function (data) {
            //console.log('getMyOrganogram', data);

            organogram = '';
            structure = '';
            parent_divisionId = '';

            organogram = data;

            $('.noOrganogram').hide();

            if (data.length > 0) {

                for (var i = 0; i < data.length; i++) {
                    loadChild(i, data[i]['division'].division_name, data[i]['organogram'].division, data[i]['organogram'].parent_organo_division, data[i]);
                }

                loadOrganogram(structure);
                //loadOrganogramTabular(organogram);
            }
            else {
                $('.noOrganogram').show();
            }


            //setTimeout(function () {
            //    $('#myLoader').modal('hide');
            //}, 1000);

        });
    }

    getMyOrganogram();


    function loadChild(index, division_name, division_id, parent_division_id, division_obj) {

        var division_leader = '';
        if (division_obj.employee)
            division_leader = division_obj.employee[0].person_first_name + ' ' + division_obj.employee[0].person_last_name;

        var child = {
            index: index,
            name: division_name,
            title: division_leader,
            division_id: division_id,
            parent_division_id: parent_division_id,
            children: [],
            division_obj: division_obj
        }

        if (!structure || !parent_division_id) {
            structure = child;
        }
        else {
            if (structure.division_id == parent_division_id) {
                structure.children.push(child);
            }
            else {
                searchChild(index, structure, division_name, division_id, parent_division_id, division_obj, child);
            }
        }
    }



    function searchChild(index, currentStructure, division_name, division_id, parent_division_id, division_obj, child) {
        for (var i = 0; i < currentStructure.children.length; i++) {
            if (currentStructure.children[i].division_id == parent_division_id) {
                currentStructure.children[i].children.push(child);
                return;
            }
            else {
                searchChild(i, currentStructure.children[i], division_name, division_id, parent_division_id, division_obj, child);
            }
        }
    }




    function loadOrganogram(data) {
        //console.log(data);
        $.getScript("/Content/as_smart_connects/jquery.orgchart.min.js", function () {
            $('#chart-container').html('');
            var oc = $('#chart-container').orgchart({
                'data': data,
                'nodeContent': 'title',
                'pan': true,
                'zoom': true,
            });

        });
    }






    //-------------------------------------------------- Load my organogram ends -------------------------------------------------------//













    //---------------------------------------------------- Action -------------------------------------------------------------------------//



    $(document).on('click', '.node', function (event) {
        division_id = $(this).find('.clsCurrentDivision').attr('id');
        id = division_id.substring(1);

        if ($('#spActionEvent').html() == "Assign") {
            $('#spAssignToAction').show();
            $('#spAssignTo').html(organogram[id]['division']['division_id']);

            $('#textAssignFromName').html($('#lblFullName').text());
            $('#textAssignToName').html(organogram[id]['division']['division_name']);
        }
        else if ($('#spActionEvent').html() == "Assign Update") {
            $('#spUpdateAssignToAction').show();
            $('#spUpdateAssignTo').html(organogram[id]['division']['division_id']);

            $('#textUpdateAssignFromName').html($('#lblFullName').text());
            $('#textUpdateAssignToName').html(organogram[id]['division']['division_name']);
        }
        else if ($('#spActionEvent').html() == "Assign MSG") {
            $('#spAssignToAction_msg').show();
            $('#spAssignTo_msg').html(organogram[id]['division']['division_id']);

            $('#textAssignFromName_msg').html($('#lblFullName').text());
            $('#textAssignToName_msg').html(organogram[id]['division']['division_name']);
        }



        else if ($('#spActionEvent').html() == "Assign Automation") {
            $('.clsActivespAssignToAction').show();
            $('.clsActivespAssignTo').html(organogram[id]['division']['division_id']);

            $('.textAssignFromName').html($('#lblFullName').text());
            $('.clsActivetextAssignToName').html(organogram[id]['division']['division_name']);
        }
        else if ($('#spActionEvent').html() == "Assign Automation MSG") {
            $('.clsActivespAssignToAction_msg').show();
            $('.clsActivespAssignTo_msg').html(organogram[id]['division']['division_id']);

            $('#textAssignFromName_msg').html($('#lblFullName').text());
            $('.clsActivetextAssignToName_msg').html(organogram[id]['division']['division_name']);
        }



        else if ($('#spActionEvent').html() == "Transfer") {

            $('#spTransferTo').html(organogram[id]['division']['division_id']);

            $('#textTransferFromName').html($('#lblFullName').text());
            $('#textTransferFromEmail').html($('#lblUsername').text());

            $('#textTransferToName').html(organogram[id]['division']['division_name']);
            $('#textTransferToEmail').html('');

            $('#modalTransferChat').modal('show');
        }


        $('#listOrganizationStructure').modal('hide');

    });


    //---------------------------------------------------- Action -------------------------------------------------------------------------//








    



});