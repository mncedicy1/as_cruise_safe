﻿$(document).ready(function () {

    var http = httpsapi + "/api/admin/";


    var how_is_connection = checkNetworkConnection();


    //-------------------------------------------------- Activate Account ---------------------------------------------//


    $(document).on("keydown", ".emailRequired", function (evt) {
        var firstChar = $(this).val()
        if (evt.keyCode == 32 && firstChar == "") {
            return false;
        }
    });


    $('#btnRecoverPassword').on('click', function (evt) {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        if (vaidateFields('emailRequired'))
            return;

        if (!validateEmail($('#txtEmailAddress').val(), "Email is not valid")) {
            return;
        }

        $('#myLoader').modal('show');

        var details = {
            email: $('#txtEmailAddress').val()
        };

        $.getJSON(http + 'passwordRecovery', details, function (data) {

            if (data.response_type == 'success') {
                showSuccessMessage(data.description);

                $('#txtEmailAddress').val('');

                setTimeout(function () {
                    $('#myLoader').modal('hide');
                }, 1000);
            }
            else {
                setTimeout(function () {
                    $('#myLoader').modal('hide');
                }, 1000);

                showErrorMessage(data.description);
            }
        });

        evt.preventDefault();
    });





    //-------------------------------------------------- Activate Account ---------------------------------------------//






















    //--------------------------------- Change Backgrounds ---------------------------------------//


    function changeBackgroundTheme() {
        var theme = Math.floor(Math.random() * 25);
        var image = 'url(../Content/Images/background_images/' + theme + '.jpg)'
        $('.accountbg').css('background-image', image);
    }

    //changeBackgroundTheme();



//--------------------------------- Change Backgrounds ---------------------------------------//




















    /***************************************************************
            Validations
***************************************************************/

    function validateEmail(inputText) {
        var d = false;
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(inputText)) {
            d = true;
        }

        if (d == false) {
            var msg = "Please enter valid email address.";
            showErrorMessage(msg);
        }
        return d;
    }



    /***************************************************************
                        Validations Ends
    ***************************************************************/






});