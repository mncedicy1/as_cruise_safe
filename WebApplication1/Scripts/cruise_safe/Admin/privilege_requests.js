﻿function checkPrev(myPrivs) {

    //-------------------- Check Prev ---------------------------//

    if ($('#lblUserType').text() != 'business' && $('#lblUserRoleName').text() != "Administrator") {
        location.href = '/as_smart_connects/home';
    }

    //-------------------- Check Prev ---------------------------//
}



$(document).ready(function () {

    var http = httpsapi + "/api/admin/";
    var https = httpsapi + "/api/";
    //var http = httpsapi_local + '/api/admin/';




    var how_is_connection = checkNetworkConnection();




    $('.actionAdministrator').trigger('click');
    $('.btnPrivilegeRequests').find('a').addClass('activeMenu');




    var requests;
    var request_status = 'active';
    var action_status = '';
    var id;



    function getPrivilegeRequests() {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        $('#myLoader').modal('show');

        var details = {
            company_id: $('#lblCompanyID').text(),
            session_token: $('#lblUserToken').text(),
            system_user_id: $('#lblUserID').text(),
        };
        $.getJSON(http + 'getPrivilegeRequests', details, function (data) {
            //console.log('getPrivilegeRequests', data);
            requests = data;

            showPrivilegeRequestsList(data);
        });
    }

    getPrivilegeRequests();



    function showPrivilegeRequestsList(data) {
        $('#zeroStatsRequests').hide();
        $('#tblPrivilegeRequests').html('');
        if (data.length > 0) {

            str = "";
            for (i = 0; i < data.length; i++) {

                var requestStatus = "label-warning badge badge-warning";
                if (data[i]['request']['request_status'] == "active") {
                    requestStatus = "label-success badge badge-success";
                }
                else if (data[i]['request']['request_status'] == "declined") {
                    requestStatus = "label-danger badge badge-danger";
                }

                str += '<tr><td>' + (i + 1) + '</td>';
                str += '<td>' + data[i]['request']['person_full_name'] + '</td>';
                str += '<td>' + data[i]['role_name'] + '</td>';
                str += '<td>' + data[i]['right_name'] + '</td>';

                str += '<td><span class="label ' + requestStatus + '">' + data[i]['request']['request_status'] + '</span></td>';

                str += '<td style="width:393px;">' + data[i]['request']['message'] + '</td>';
                str += '<td>  <button id="r' + i + '" style="width:100px;" type="button" class="btn btn-success btn-flat btn-xs btnApprove"> <i class="fas fa-check"></i> Approve </button>     <button id="r' + i + '" style="width:100px;" type="button" class="btn btn-danger btn-flat btn-xs btnDecline"> <i class="fas fa-times"></i> Decline </button>  </td ></tr> ';
            }

            $('#tblPrivilegeRequests').html(str);
        }
        else {
            $('#zeroStatsRequests').show().html('No Result Found');
        }


        setTimeout(function () {
            $('#myLoader').modal('hide');
        }, 1000);




        //------------ Approve -------------------//

        $('.btnApprove').click(function () {
            id = this.id.substring(1);
            action_status = 'Approve';

            $('#btnConfirm').show();
            $('#btnConfirmActive').hide();

            $('#confirmModal').modal('show');
            $('#lblConfirmMessage').html("Are you sure, you want to approve this request from " + requests[id]['request']['person_full_name'] + "<br/><span style='color:red;'>Please note that by approving this request you will be adding the privilege to access the <strong style='color:black'>" + requests[id]['right_name'] + "</strong> to everyone who has the role of <strong style='color:black'>" + requests[id]['role_name'] +"</strong></span>");

        });

        //------------ Approve -------------------//




        //------------ Decline -------------------//

        $('.btnDecline').click(function () {
            id = this.id.substring(1);
            action_status = 'Decline';

            $('#btnConfirm').show();
            $('#btnConfirmActive').hide();

            $('#confirmModal').modal('show');
            $('#lblConfirmMessage').html("Are you sure, you want to decline this request from " + requests[id]['request']['person_full_name']);

        });

        //------------ Decline -------------------//




    }













    //----------------------------------------- Action -------------------------------------------//


    $('#btnConfirm').on('click', function () {
        $('#confirmModal').modal('hide');

        if (action_status == 'Approve') {
            approveRequest();
        }
        else if (action_status == 'Decline') {
            setTimeout(function () {
                $('#myLoader').modal('hide');
            }, 1000);

            $('#declineReasonModal').modal('show');
        }

    });




    $('#btnSubmitDeclineConfirmation').on('click', function () {
        if (vaidateFields('requiredReason')) {
            return;
        }

        declinedRequest()
    });


    function approveRequest() {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        $('#myLoader').modal('show');

        var details = {
            privilege_request_id: requests[id]['request']['privilege_request_id'],
            system_user_id: $('#lblUserID').text(),
            session_token: $('#lblUserToken').text(),
        };
        //console.log(details);
        $.getJSON(http + 'aprove_privilege_requests', details, function (data) {

            if (data.response_type == 'success') {
                showSuccessMessage(data.description);
                getPrivilegeRequests();

                setTimeout(function () {
                    $('#successModal').modal('hide');
                }, 1000);
            }
            else {
                showErrorMessage(data.description);
            }

            setTimeout(function () {
                $('#myLoader').modal('hide');
            }, 1000);
        });
    }






    function declinedRequest() {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        $('#myLoader').modal('show');

        var details = {
            privilege_request_id: requests[id]['request']['privilege_request_id'],
            system_user_id: $('#lblUserID').text(),
            session_token: $('#lblUserToken').text(),
            decline_reason: $('#txtDeclineReason').val()
        };
        //console.log(details);
        $.getJSON(http + 'declined_privilege_requests', details, function (data) {

            if (data.response_type == 'success') {
                showSuccessMessage(data.description);
                getPrivilegeRequests();

                setTimeout(function () {
                    $('#successModal').modal('hide');
                }, 1000);
            }
            else {
                showErrorMessage(data.description);
            }

            setTimeout(function () {
                $('#myLoader').modal('hide');
            }, 1000);
        });
    }


    //----------------------------------------- Action -------------------------------------------//















});