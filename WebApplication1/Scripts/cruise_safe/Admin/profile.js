﻿

$(document).ready(function () {










    var http = httpsapi + "/api/organisation/";
    //var http = httpsapi_local + '/api/organisation/';





    var how_is_connection = checkNetworkConnection();







    //----------------------------------------------- Choose View -------------------------------------------------//

    $('.clsTabProfile').click(function () {
        $('.clsSelectedView').removeClass('active');
        $(this).addClass('active');
        var currentTab = this.id;

        $('#dvMyProfile').hide();
        $('#dvUploadPicture').hide();
        $('#dvChangePassword').hide();
        $('#dvNotifications').hide();


        if (currentTab == "My Profile") {
            $('#dvMyProfile').show('slow');
        }
        else if (currentTab == "Upload Picture") {
            $('#dvUploadPicture').show('slow');
        }
        else if (currentTab == "Change Password") {
            $('#dvChangePassword').show('slow');
        }
        else if (currentTab == "Notifications") {
            $('#dvNotifications').show('slow');
        }
    });

    //----------------------------------------------- Choose View -------------------------------------------------//
















    //------------------------------------------------ Check if Image exsts ---------------------------------------------//


    if ($('#lblUserImage').text() == '') {
        $('.imgUserProfileImage').hide();
        $('.imgDefaultProfileImage').show();
    }


    //------------------------------------------------ Check if Image exsts ---------------------------------------------//





    //------------------------ Change Password ---------------------------//

    $(document).on("keydown", ".requredPassword", function (evt) {
        var firstChar = $(this).val()
        if (evt.keyCode == 32 && firstChar == "") {
            return false;
        }
    });

    setTimeout(function () {
        $('.requredPassword').val('');
    }, 2000);

    $('#btnSavePassword').on('click', function () {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        if (vaidateFields('requredPassword'))
            return;

        $('.myLoader').modal('show');

        var details = {
            userId: $('#lblUserID').text(),
            clientid: $('#lblCompanyID').text(),
            userType: $('#lblUserType').text(),
            system_user_username: $('#lblUsername').text(),
            oldpassword: $('#txtOldPassword').val(),
            new_password: $('#txtNewPassword').val(),
            confirmPassword: $('#txtConfirmPassword').val(),
            fullname: $('#lblFullName').text(),
            session_token: $('#lblUserToken').text(),
        };

        $.post('/as_smart_connects/changeMyPassword', details, function (data) {
            if (data.ErrorMessage) {
                $('.cleanField').val('');
                setTimeout(function () {
                    $('#myLoader').modal('hide');
                }, 1000); checkEmptyFields(data.ErrorMessage);
            }
            if (data.SuccessMessage) {
                $('.cleanField').val('');
                setTimeout(function () {
                    $('#myLoader').modal('hide');
                }, 1000);
                SuccessMessage(data.SuccessMessage);
            }
        });
    });


    //------------------------ Change Password ---------------------------//










    var contact_id;
    function getMyProfile() {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        $('#myLoader').modal('show');

        var details = {
            person_id: $('#lblPersonId').text(),
            session_token: $('#lblUserToken').text(),
            userId: $('#lblUserID').text(),
        };

        $.getJSON('/as_smart_connects/getMyProfile', details, function (data) {

            //console.log('profile', data);

            //--------------- Populate Fields --------------------//
            var id = 0;
            $('#cboTitle').val(data[id]['person']['person_title']);
            $('#txtFirstName').val(data[id]['person']['person_first_name']);
            $('#txtLastName').val(data[id]['person']['person_last_name']);
            //$('#cboCitizenship').val(data[id]['person']['person_citizenship']);
            $('#cboNationality').val(data[id]['person']['person_nationality']);
            $('#txtIDNumber').val(data[id]['person']['person_id_number']);
            $('#txtDOB').val(getDate(data[id]['person']['person_dob']));
            $('#cboMaritalStatus').val(data[id]['marital_status']);
            $('#cboGender').val(data[id]['person']['person_gender']);
            $('#cboHomeLanguage').val(data[id]['person']['person_home_language']);
            $('#editor_email_signature').html(data[id]['person']['person_signature']);



            $('#txtFacebookProfile').val(removeNull(data[id]['person']['person_social_media_facebook_profile']));
            $('#txtTwitterProfile').val(removeNull(data[id]['person']['person_social_media_twitter_profile']));
            $('#txtLinkedInProfile').val(removeNull(data[id]['person']['person_social_media_linkedin_profile']));
            $('#txtInstagramProfile').val(removeNull(data[id]['person']['person_social_media_instagram_profile']));







            //-------------------------------------------- Check boxes -------------------------------------------------------------------//



            $('#chkSendEmailToChatAssignedToMe').removeAttr('checked');
            if (data[id]['person']['person_notification_live_chat_message_to_chats_assigned_to_me'] == 1) {
                $('#chkSendEmailToChatAssignedToMe').trigger('click');
            }


            $('#chkSendEmailToAnyChat').removeAttr('checked');
            if (data[id]['person']['person_notification_live_chat_message_to_any'] == 1) {
                $('#chkSendEmailToAnyChat').trigger('click');
            }


            $('#chkSendEmailToAnyChatOnPendingAndNew').removeAttr('checked');
            if (data[id]['person']['person_notification_live_chat_message_to_chats_on_pending_or_new_status'] == 1) {
                $('#chkSendEmailToAnyChatOnPendingAndNew').trigger('click');
            }


            $('#chkSendEmailToChatTransferedToMe').removeAttr('checked');
            if (data[id]['person']['person_notification_live_chat_transfered_to_me'] == 1) {
                $('#chkSendEmailToChatTransferedToMe').trigger('click');
            }


            $('#chkSendEmailToChatTransferedToMyDivision').removeAttr('checked');
            if (data[id]['person']['person_notification_live_chat_transfered_to_my_division'] == 1) {
                $('#chkSendEmailToChatTransferedToMyDivision').trigger('click');
            }


            $('#chkSendEmailToTicketAssignedToMe').removeAttr('checked');
            if (data[id]['person']['person_notification_ticket_assigned_to_me'] == 1) {
                $('#chkSendEmailToTicketAssignedToMe').trigger('click');
            }


            $('#chkSendEmailToTicketAssignedToMyDivision').removeAttr('checked');
            if (data[id]['person']['person_notification_ticket_assigned_to_my_division'] == 1) {
                $('#chkSendEmailToTicketAssignedToMyDivision').trigger('click');
            }


            $('#chkSendEmailToResponseOfTicketAssignedToMe').removeAttr('checked');
            if (data[id]['person']['person_notification_ticket_response_to_ticket_assigned_to_me'] == 1) {
                $('#chkSendEmailToResponseOfTicketAssignedToMe').trigger('click');
            }


            $('#chkSendEmailToAnyTickets').removeAttr('checked');
            if (data[id]['person']['person_notification_ticket_response_to_any_ticket'] == 1) {
                $('#chkSendEmailToAnyTickets').trigger('click');
            }


            $('#chkSendEmailToResponseOfTicketAssignedToMyDivision').removeAttr('checked');
            if (data[id]['person']['person_notification_ticket_response_to_a_ticket_assigned_to_my_division'] == 1) {
                $('#chkSendEmailToResponseOfTicketAssignedToMyDivision').trigger('click');
            }


            $('#chkSendEmailToStatusChangesOfTicketsAssignedToMe').removeAttr('checked');
            if (data[id]['person']['person_notification_ticket_status_changes_to_ticket_assigned_to_me'] == 1) {
                $('#chkSendEmailToStatusChangesOfTicketsAssignedToMe').trigger('click');
            }


            $('#chkSendEmailToNewTickets').removeAttr('checked');
            if (data[id]['person']['person_notification_ticket_for_new_tickets'] == 1) {
                $('#chkSendEmailToNewTickets').trigger('click');
            }



            //-------------------------------------------- Check boxes -------------------------------------------------------------------//







            contact_id = data[id]['contact'][0]['contact_id'];
            $('#txtCellNumber').val(data[id]['contact'][0]['contact_cellphone']);
            $('#txtEmailAddress').val(data[id]['contact'][0]['contact_email_address']);

            $('#txtPhysicalAddressStreet1').val(data[id]['contact'][0]['contact_physical_address_street1']);
            $('#txtPhysicalAddressStreet2').val(data[id]['contact'][0]['contact_physical_address_street2']);
            $('#txtPhysicalAddressSuburb').val(data[id]['contact'][0]['contact_physical_address_suburb']);
            $('#txtPhysicalAddressCity').val(data[id]['contact'][0]['contact_physical_address_city']);
            $('#cboPhysicalProvince').val(data[id]['contact'][0]['contact_physical_address_province']);
            $('#txtPhysicalCode').val(data[id]['contact'][0]['contact_physical_address_code']);

            $('#txtPostalAddres1').val(data[id]['contact'][0]['contact_postal_address_street1']);
            $('#txtPostalAddres2').val(data[id]['contact'][0]['contact_postal_address_street2']);
            $('#txtPostalAddres3').val(data[id]['contact'][0]['contact_postal_address_suburb']);
            $('#txtPostalAddres4').val(data[id]['contact'][0]['contact_postal_address_city']);
            $('#cboPostalProvince').val(data[id]['contact'][0]['contact_postal_address_province']);
            $('#txtPostalCode').val(data[id]['contact'][0]['contact_postal_address_code']);


            //--------------- Populate Fields --------------------//


            setTimeout(function () {
                $('#myLoader').modal('hide');
            }, 1000);

        });
    }

    getMyProfile();










    //------------------------ Upload profile picture ---------------------------//

    $(document).on('click','#btnCloseAlert', function () {
        $('.showSweetAlert').hide();
    });

    $('#btnBrowseUserImage').click(function () {
        $('#btnCropImage').removeAttr('disabled', 'disabled');

        $('#upload').trigger('click').change(function () {
            readURL(this);
        });
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#imgLogo').attr('src', e.target.result);
                $('#spCompanyLogo').html(e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }


    //$("#userImageUpload").change(function (e) {
    //    readURL(this);
    //});
    //function readURL(input) {
    //    if (input.files && input.files[0]) {
    //        var reader = new FileReader();

    //        reader.onload = function (e) {
    //            $('#imgUser').attr('src', e.target.result);
    //        }
    //        reader.readAsDataURL(input.files[0]);
    //    }
    //}


    //------------------------ Upload profile picture ---------------------------//











    //---------------------------- Update My Profile ----------------------------//

    var sameAs = 'yes';

    $('#btnSameAsPhysical').click(function () {
        if (sameAs == 'yes') {
            sameAs = 'no';
            $(this).removeClass('btn-warning').addClass('btn-success').html('<i class="fa fa-check" aria-hidden="true"></i>');

            $('#txtPostalAddres1').val($('#txtPhysicalAddressStreet1').val());
            $('#txtPostalAddres2').val($('#txtPhysicalAddressStreet2').val());
            $('#txtPostalAddres3').val($('#txtPhysicalAddressSuburb').val());
            $('#txtPostalAddres4').val($('#txtPhysicalAddressCity').val());
            $('#cboPostalProvince').val($('#cboPhysicalProvince').val());
            $('#txtPostalCode').val($('#txtPhysicalCode').val());
        }
        else {
            sameAs = 'yes';
            $(this).removeClass('btn-success').addClass('btn-warning').html('<i class="fa fa-arrow-right" aria-hidden="true"></i>');

            $('#txtPostalAddres1').val('');
            $('#txtPostalAddres2').val('');
            $('#txtPostalAddres3').val('');
            $('#txtPostalAddres4').val('');
            $('#cboPostalProvince').val('');
            $('#txtPostalCode').val('');
        }
    });


    $('#liPersonalDetails').click(function () {
        $('.liProfile').removeClass('activeTab');
        $(this).addClass('activeTab');

        $('#btnSubmitSignature').hide();
        $('#btnSubmitChanges').show();

        $('#as-personalDetails').show();
        $('#as-dvContactAddress').hide();
        $('#as-dvEmailSignature').hide();
    });

    $('#liContactAddress').click(function () {
        $('.liProfile').removeClass('activeTab');
        $(this).addClass('activeTab');


        $('#btnSubmitSignature').hide();
        $('#btnSubmitChanges').show();

        $('#as-dvContactAddress').show();
        $('#as-personalDetails').hide();
        $('#as-dvEmailSignature').hide();
    });

    $('#liEmailSignature').click(function () {
        $('.liProfile').removeClass('activeTab');
        $(this).addClass('activeTab');

        $('#btnSubmitSignature').show();
        $('#btnSubmitChanges').hide();

        $('#as-dvEmailSignature').show();
        $('#as-dvContactAddress').hide();
        $('#as-personalDetails').hide();
    });



    $('#liLiveNotifications').click(function () {
        $('.liNotifications').removeClass('activeTab');
        $(this).addClass('activeTab');

        $('#as-liveChatSupport').show();
        $('#as-dvTicketsSupport').hide();
    });

    $('#liTicketsSupportNotifications').click(function () {
        $('.liNotifications').removeClass('activeTab');
        $(this).addClass('activeTab');

        $('#as-dvTicketsSupport').show();
        $('#as-liveChatSupport').hide();
    });






    //$('#cboCitizenship').change(function () {
    //    if ($(this).val() == 'South African Citizen') {
    //        $('#dvShoNationality').hide();
    //        $('#cboNationality').val('ZA');
    //    }
    //    else {
    //        $('#dvShoNationality').show();
    //        $('#cboNationality').val('');
    //    }
    //});



    $('#txtIDNumber').focusout(function () {
        validateIdNumberAsMunicipality($(this), $('#cboGender'), $('#txtDOB'), $('.age'), 15, 60);
    });






    var person_notification_live_chat_message_to_chats_assigned_to_me = 0;
    $('#chkSendEmailToChatAssignedToMe').click(function () {
        if ($(this).is(':checked')) {
            person_notification_live_chat_message_to_chats_assigned_to_me = 1;
        }
    });


    var person_notification_live_chat_message_to_any = 0;
    $('#chkSendEmailToAnyChat').click(function () {
        if ($(this).is(':checked')) {
            person_notification_live_chat_message_to_any = 1;
        }
    });


    var person_notification_live_chat_message_to_chats_on_pending_or_new_status = 0;
    $('#chkSendEmailToAnyChatOnPendingAndNew').click(function () {
        if ($(this).is(':checked')) {
            person_notification_live_chat_message_to_chats_on_pending_or_new_status = 1;
        }
    });


    var person_notification_live_chat_transfered_to_me = 0;
    $('#chkSendEmailToChatTransferedToMe').click(function () {
        if ($(this).is(':checked')) {
            person_notification_live_chat_transfered_to_me = 1;
        }
    });


    var person_notification_live_chat_transfered_to_my_division = 0;
    $('#chkSendEmailToChatTransferedToMyDivision').click(function () {
        if ($(this).is(':checked')) {
            person_notification_live_chat_transfered_to_my_division = 1;
        }
    });


    var person_notification_ticket_assigned_to_me = 0;
    $('#chkSendEmailToTicketAssignedToMe').click(function () {
        if ($(this).is(':checked')) {
            person_notification_ticket_assigned_to_me = 1;
        }
    });


    var person_notification_ticket_assigned_to_my_division = 0;
    $('#chkSendEmailToTicketAssignedToMyDivision').click(function () {
        if ($(this).is(':checked')) {
            person_notification_ticket_assigned_to_my_division = 1;
        }
    });


    var person_notification_ticket_response_to_ticket_assigned_to_me = 0;
    $('#chkSendEmailToResponseOfTicketAssignedToMe').click(function () {
        if ($(this).is(':checked')) {
            person_notification_ticket_response_to_ticket_assigned_to_me = 1;
        }
    });


    var person_notification_ticket_response_to_any_ticket = 0;
    $('#chkSendEmailToAnyTickets').click(function () {
        if ($(this).is(':checked')) {
            person_notification_ticket_response_to_any_ticket = 1;
        }
    });


    var person_notification_ticket_response_to_a_ticket_assigned_to_my_division = 0;
    $('#chkSendEmailToResponseOfTicketAssignedToMyDivision').click(function () {
        if ($(this).is(':checked')) {
            person_notification_ticket_response_to_a_ticket_assigned_to_my_division = 1;
        }
    });


    var person_notification_ticket_status_changes_to_ticket_assigned_to_me = 0;
    $('#chkSendEmailToStatusChangesOfTicketsAssignedToMe').click(function () {
        if ($(this).is(':checked')) {
            person_notification_ticket_status_changes_to_ticket_assigned_to_me = 1;
        }
    });


    var person_notification_ticket_for_new_tickets = 0;
    $('#chkSendEmailToNewTickets').click(function () {
        if ($(this).is(':checked')) {
            person_notification_ticket_for_new_tickets = 1;
        }
    });




    $('#btnSubmitChanges').on('click', function () {

        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }


        if (vaidateFields('requiredPersonal')) {
            $('#liPersonalDetails').trigger('click');
            return;
        }

        if (vaidateFields('requiredaddress')) {
            $('#liContactAddress').trigger('click');
            return;
        }

        if ($('#txtEmailAddress').val() != "") {
            if (!validateEmail($('#txtEmailAddress').val(), "Email is not valid")) {
                $('#liContactAddress').trigger('click');
                return;
            }
        }

        $('.myLoader').modal('show');

        var details = {
            userId: $('#lblUserID').text(),
            clientid: $('#lblCompanyID').text(),
            userType: $('#lblUserType').text(),
            cellphone: $('#txtCellNumber').val(),
            email: $('#txtEmailAddress').val(),
            person_title: $('#cboTitle').val(),
            person_first_name: $('#txtFirstName').val(),
            person_last_name: $('#txtLastName').val(),
            //citizenship: $('#cboCitizenship').val(),
            nationality: $('#cboNationality').val(),
            id_number: $('#txtIDNumber').val(),
            dob: $('#txtDOB').val(),
            marital_status: $('#cboMaritalStatus').val(),
            gender: $('#cboGender').val(),
            language: $('#cboHomeLanguage').val(),

            person_social_media_facebook_profile: $('#txtFacebookProfile').val(),
            person_social_media_twitter_profile: $('#txtTwitterProfile').val(),
            person_social_media_linkedin_profile: $('#txtLinkedInProfile').val(),
            person_social_media_instagram_profile: $('#txtInstagramProfile').val(),
            
            person_id: $('#lblPersonId').text(),
            contact_id: contact_id,
            contact_physical_address_street1: $('#txtPhysicalAddressStreet1').val(),
            contact_physical_address_street2: $('#txtPhysicalAddressStreet2').val(),
            contact_physical_address_suburb: $('#txtPhysicalAddressSuburb').val(),
            contact_physical_address_city: $('#txtPhysicalAddressCity').val(),
            contact_physical_address_province: $('#cboPhysicalProvince').val(),
            contact_physical_address_code: $('#txtPhysicalCode').val(),

            contact_postal_address_street1: $('#txtPostalAddres1').val(),
            contact_postal_address_street2: $('#txtPostalAddres2').val(),
            contact_postal_address_suburb: $('#txtPostalAddres3').val(),
            contact_postal_address_city: $('#txtPostalAddres4').val(),
            contact_postal_address_province: $('#cboPostalProvince').val(),
            contact_postal_address_code: $('#txtPostalCode').val(),
            session_token: $('#lblUserToken').text(),
        };

        //console.log(details);
        //return;
        $.post('/as_smart_connects/updatePerson', details, function (data) {
            if (data.ErrorMessage) {
                setTimeout(function () {
                    $('#myLoader').modal('hide');
                }, 1000);
                checkEmptyFields(data.ErrorMessage);
            }
            if (data.SuccessMessage) {
                getMyProfile();
                setTimeout(function () {
                    $('#myLoader').modal('hide');
                }, 1000);
                SuccessMessage(data.SuccessMessage);
            }
        });
    });

















    //------------------------------------------ Update person signature ------------------------------------------------------//







    //$(window).resize(function () {
    //    $('#editor_email_signature').css('width', $(window).width() * 0.8);
    //});

    //function resizeScript() {
    //    $(window).resize();
    //}
    //resizeScript();




    $(function () {
        $('#editControls_email_signature a').click(function (e) {
            switch ($(this).data('role')) {
                case 'h1':
                case 'h2':
                case 'p':
                    document.execCommand('formatBlock', false, $(this).data('role'));
                    break;
                default:
                    document.execCommand($(this).data('role'), false, null);
                    break;
            }
        });
    });









    $('#btnSubmitSignature').on('click', function () {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }


        if ($('#editor_email_signature').html() == "") {
            showErrorMessage("Email signature cannot be empty.");
            return;
        }

        $('#myLoader').modal('show');

        var details = {
            company_id: $('#lblCompanyID').text(),
            company_name: $('#lblCompanyName').text(),

            person_id: $('#lblPersonId').text(),
            person_signature: $('#editor_email_signature').html(),

            session_token: $('#lblUserToken').text(),
            system_user_id: $('#lblUserID').text(),
        };
        //console.log(details);
        //return;
        $.post(http + 'person_update', details, function (data) {
            if (data.response_type == 'success') {
                showSuccessMessage(data.description);

                setTimeout(function () {
                    $('#successModal').modal('hide');
                }, 1000);
            }
            else {
                showErrorMessage(data.description);
            }

            setTimeout(function () {
                $('#myLoader').modal('hide');
            }, 1000);

        });
    });





    //------------------------------------------ Update person signature ------------------------------------------------------//
















































    $('#btnSubmitNotificationChanges').on('click', function () {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }


        $('.myLoader').modal('show');

        var details = {
            person_notification_live_chat_message_to_chats_assigned_to_me: person_notification_live_chat_message_to_chats_assigned_to_me,
            person_notification_live_chat_message_to_any: person_notification_live_chat_message_to_any,
            person_notification_live_chat_message_to_chats_on_pending_or_new_status: person_notification_live_chat_message_to_chats_on_pending_or_new_status,
            person_notification_live_chat_transfered_to_me: person_notification_live_chat_transfered_to_me,
            person_notification_live_chat_transfered_to_my_division: person_notification_live_chat_transfered_to_my_division,
            person_notification_ticket_assigned_to_me: person_notification_ticket_assigned_to_me,
            person_notification_ticket_assigned_to_my_division: person_notification_ticket_assigned_to_my_division,
            person_notification_ticket_response_to_ticket_assigned_to_me: person_notification_ticket_response_to_ticket_assigned_to_me,
            person_notification_ticket_response_to_any_ticket: person_notification_ticket_response_to_any_ticket,
            person_notification_ticket_response_to_a_ticket_assigned_to_my_division: person_notification_ticket_response_to_a_ticket_assigned_to_my_division,
            person_notification_ticket_status_changes_to_ticket_assigned_to_me: person_notification_ticket_status_changes_to_ticket_assigned_to_me,
            person_notification_ticket_for_new_tickets: person_notification_ticket_for_new_tickets,

            person_id: $('#lblPersonId').text(),
            session_token: $('#lblUserToken').text(),
            userId: $('#lblUserID').text(),
        };

        //console.log(details);
        //return;
        $.post('/as_smart_connects/updatePersonNotification', details, function (data) {
            if (data.ErrorMessage) {
                setTimeout(function () {
                    $('#myLoader').modal('hide');
                }, 1000);
                checkEmptyFields(data.ErrorMessage);
            }
            if (data.SuccessMessage) {
                getMyProfile();
                setTimeout(function () {
                    $('#myLoader').modal('hide');
                }, 1000);
                SuccessMessage(data.SuccessMessage);
            }
        });
    });







    //---------------------------- Update My Profile ----------------------------//





















    //------------------------ post logo ---------------------------//


    var validatedFiles = Array();
    var validatedname = Array();

    //$("#companyImageUpload").change(function (e) {
    //    //$('#imagess').html('');
    //    if (e.target.files.length < 1 || validatedname.indexOf(e.target.files[0].name) >= 0) {
    //        $("#companyImageUpload").val('');
    //        return;
    //    }


    //    validatedFiles.push(e.target.files[0]);
    //    validatedname.push(e.target.files[0].name);


    //    var item = $(this).clone(true);

    //    var fileName = $(this).val();
    //    if (fileName) {
    //        $('#imgCont').append(item);
    //    }


    //    $('#imagess').html('');
    //    var temp = Array();
    //    for (var i = 0; i < validatedFiles.length; i++) {
    //        var reader = new FileReader();
    //        reader.onload = function (e) {
    //            temp.push(e.target.result);
    //        }
    //        reader.readAsDataURL(validatedFiles[i]);
    //    }

    //});


    $(document).on('click', '.confirm', function () {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        uploadImage($('#spUserPhoto').text());
    });


    $(document).on('click', '#btnSavePicture', function () {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        uploadImage($('#spTakePicture').text());
        $('.adminCamera-modal-lg').modal('hide');
    });


    function b64toBlob(b64Data, contentType, sliceSize) {
        contentType = contentType || '';
        sliceSize = sliceSize || 512;

        var byteCharacters = atob(b64Data);
        var byteArrays = [];

        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);

            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
        }

        var blob = new Blob(byteArrays, { type: contentType });
        return blob;
    }



    function uploadImage(imageurl) {
        //var fd = new FormData();
        //var img = new Image();
        //img.src = $('#spUserPhoto').text();
        //fd.append("image", img);



        //for (var i = 0; i < $('#imgCont').children().length; i++) {
        //    var doc = $('#imgCont').children().get(i);
        //    var file = doc.files[0];
        //    fd.append(doc.name + i, file);
        //}
        //console.log(fd);

        var ImageURL = imageurl;

        // Split the base64 string in data and contentType
        var block = ImageURL.split(";");
        // Get the content type
        var contentType = block[0].split(":")[1];// In this case "image/gif"
        // get the real base64 content of the file
        var realData = block[1].split(",")[1];// In this case "iVBORw0KGg...."

        // Convert to blob
        var blob = b64toBlob(realData, contentType);

        // Create a FormData and append the file
        var fd = new FormData();
        fd.append("image", blob);
        send_image_to_server(fd);
    }


    function send_image_to_server(fd) {

        //var fileSelect = document.getElementById('companyImageUpload');
        var details = {
            person_id: $('#lblPersonId').text(),
            folder_name: $('#lblCompanyFolder').text()
        };
        //console.log(details);
        //return;
        var url = http + 'uploadProfilePic?person_id=' + details.person_id + '&folder_name=' + details.folder_name + '';
        //console.log(url);

        var xhr;
        if (window.XMLHttpRequest)
            xhr = new XMLHttpRequest();
        else if (window.ActiveXObject)
            xhr = new ActiveXObject("Microsoft.XMLHTTP");

        xhr.open('POST', url, true);

        xhr.upload.onprogress = function (e) {

        };

        xhr.send(fd);
        xhr.onload = function (dataa) {

            var data = JSON.parse(xhr.response);
            //console.log(data);
            if (data.Message == 'Success') {
                showProfilePic();
            }
            else if (data.error) {
                showErrorMessage(data.error);
            }
            else if (data.Message) {
                showErrorMessage(data.Message);
            }
            else {
                showErrorMessage(data);
            }
        };
        //xhr.onload = function () {
        //    setTimeout(function () {
        //        showProfilePic();
        //    }, 4000);
        //    var data = xhr.response;
        //};
    }




    //------------------------ post logo ---------------------------//







    function showProfilePic() {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        var details = {
            person_id: $('#lblPersonId').text(),
            session_token: $('#lblUserToken').text(),
            system_user_id: $('#lblUserID').text(),
        };

        $.getJSON(http + 'getMyProfilePic', details, function (data) {
            //console.log('getMyProfilePic', data);    

            if (data[0]['person_profile_picture'] != null && data[0]['person_profile_picture'] != '') {
                var employees_profile_pic = 'https://athandweserver.dedicated.co.za:234/' + $('#lblCompanyFolder').text() + '/images/' + data[0]['person_profile_picture'];
                $('.cls_smart_connect_my_profile_pic').attr("src", employees_profile_pic);
                $('.cr-image').attr("src", employees_profile_pic);;
            }
            else {
                $('.cls_smart_connect_my_profile_pic').attr("src", "/Content/Images/default-user.png");
                $('.cr-image').attr("src", "/Content/Images/default-user.png");
            }
        });
    }








































  




    /***************************************************************
                            Validations
    ***************************************************************/

    function validateIdNumberAsMunicipality(thiss, genderr, dob, agee, minAge, maxAge) {

        var idNumber = thiss.val();

        // assume everything is correct and if it later turns out not to be, just set this to false
        var correct = '';

        // SA ID Number have to be 13 digits, so check the length
        if (idNumber.length != 13 || !isNumber(idNumber)) {
            correct = 'ID number : ' + idNumber + ' does not appear to be authentic - <b class="text-red">Input not a valid number!</b>';
        }

        // get first 6 digits as a valid date
        var tempDate = new Date(idNumber.substring(0, 2), idNumber.substring(2, 4) - 1, idNumber.substring(4, 6));

        var id_date = tempDate.getDate();
        var id_month = tempDate.getMonth();
        var id_year = tempDate.getFullYear();

        var month = id_month + 1;
        if (month < 10)
            month = '0' + month;
        var day = id_date;
        if (day < 10)
            day = '0' + day;
        var fullDates = id_year + "-" + month + "-" + day;

        var fullDate = id_year + "-" + (id_month + 1) + "-" + id_date;

        if (!((tempDate.getYear() == idNumber.substring(0, 2)) && (id_month == idNumber.substring(2, 4) - 1) && (id_date == idNumber.substring(4, 6))) && correct == '') {
            correct = 'ID number : <b class="text-yellow">' + idNumber + '</b> does not appear to be authentic - <b class="text-red">Date part not valid!</b>';
        }

        // get the gender
        var genderCode = idNumber.substring(6, 10);
        var gender = parseInt(genderCode) < 5000 ? "Female" : "Male";

        // get country ID for citzenship
        var citzenship = parseInt(idNumber.substring(10, 11)) == 0 ? "Yes" : "No";

        // apply Luhn formula for check-digits
        var tempTotal = 0;
        var checkSum = 0;
        var multiplier = 1;
        for (var i = 0; i < 13; ++i) {
            tempTotal = parseInt(idNumber.charAt(i)) * multiplier;
            if (tempTotal > 9) {
                tempTotal = parseInt(tempTotal.toString().charAt(0)) + parseInt(tempTotal.toString().charAt(1));
            }
            checkSum = checkSum + tempTotal;
            multiplier = (multiplier % 2 == 0) ? 1 : 2;
        }
        if ((checkSum % 10) != 0 && correct == '') {
            correct = 'ID number : <b class="text-yellow">' + idNumber + '</b> does not appear to be authentic - <b class="text-red">Check digit is not valid!</b>';
        };

        if (citzenship == "No" && correct == '') {
            correct = 'ID number : <b class="text-yellow">' + idNumber + '</b> does not appear to be authentic - <b class="text-red">Only south african Id number allowed!</b>';
        };

        var age = calculateAge(tempDate);
        if ((age < minAge || age > maxAge) && correct == '') {
            ////console.log(age);
            minAge = minAge == 0.5 ? "6 Months" : minAge;
            correct = 'ID number : <b class="text-yellow">' + idNumber + '</b> does not appear to be authentic - <b class="text-red">The age must be between ' + minAge + ' and ' + maxAge + ' Years!</b>';
        };

        // if no error found, hide the error message
        if (correct == '') {
            thiss.css('border-color', 'green');

            if (genderr)
                genderr.val(gender);
            if (agee)
                agee.val(Math.abs(age));
            if (dob)
                dob.val(fullDates);

            return true;
        }
            // otherwise, show the error
        else {
            checkEmptyFields(correct);
            thiss.css('border-color', 'red');
            if (dob)
                dob.val('').removeAttr('disabled');
            if (genderr)
                genderr.val('');
            if (agee)
                agee.val('');
            thiss.val('').focus();

            return false;
        }

    }


    function vaidateFields(clas) {
        $('.' + clas).css('border-color', 'silver');
        var input;
        $('.' + clas).map(function () {
            if (!$(this).val() || $(this).css('border-color') == 'red') {
                $(this).css('border-color', 'red');
                $(this).focus();
                input = $(this);
            }
        }).get();

        if (input) {
            input.focus();
            return true;
        }
        else
            return false;
    }

    function validateEmail(sEmail, msg) {
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(sEmail)) {
            return true;
        }
        else {
            //var msg = "Please enter valid email address.";
            checkEmptyFields(msg);
            return false;
        }
    }

    function SuccessMessage(msg) {
        $('#myPleaseWait').modal('show');
        $('#txtModalLoaderAlertMessage').hide;
        $('#txtModalLoaderSuccessMessage').show();
        $('#txtModalLoaderSuccessMessage b').html(msg);

        setTimeout(function () {
            $('#myPleaseWait').hide();
            $('#txtModalLoaderSuccessMessage').hide();
            $('#myPleaseWait').modal('hide');
        }, 3000);
    }

    function checkEmptyFields(msg) {

        $('#myPleaseWait').modal('show');
        $('#txtModalLoaderAlertMessage').show();
        $('#txtModalLoaderAlertMessage b').text(msg);

        setTimeout(function () {
            $('#myPleaseWait').hide();
            $('#myPleaseWait').modal('hide');
            $('#txtModalLoaderAlertMessage').hide();
        }, 3000);
    }



    /***************************************************************
                        Validations Ends
    ***************************************************************/










});