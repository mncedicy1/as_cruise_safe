﻿$(document).ready(function () {

    var http = httpsapi + "/api/admin/";
    //var http = httpsapi_local + '/api/admin/';

    var how_is_connection = checkNetworkConnection();


    //--------------------- Today's date ------------------------//


    var today = new Date();

    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();

    if (mm < 10)
        mm = "0" + mm;
    if (dd < 10)
        dd = "0" + dd;

    var todayDate = yyyy + "-" + mm + "-" + dd;


    //--------------------- Today's date ------------------------//

















    //-------------------------------------------------- Validate Company ---------------------------------------------//

    //$('#cboCountry').change(function () {
    //    if ($(this).val() == '')
    //        $('.companyValidated').attr('disabled', 'disabled');
    //    else
    //        $('.companyValidated').removeAttr('disabled', 'disabled');


    //    $(this).css('border-color','silver');
    //    if ($('#txtCompanyName').val() != '')
    //        validateCompany($('#txtCompanyName').val(), $('#cboCountry').val());
    //});


    //var isCompanyValid = false;

    //$('#txtCompanyName').focusout(function () {
    //    if (vaidateFields('validateCompany'))
    //        return;

    //    validateCompany($('#txtCompanyName').val(), $('#cboCountry').val());
    //});


    function validateCompany(company_name, country) {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        isCompanyValid = false;
        $('#txtCompanyName').css('border-color', 'orange');
        $('#spIconValidation').find('i').css('color', 'orange').addClass('fa-spinner').addClass('fa-spin').removeClass('fa-times').removeClass('fa-check');

        var details = {
            company_name: company_name,
            country: country
        };
        $.getJSON(http + 'company_validation', details, function (data) {
            //console.log('company_validation', data);
            if (data.response_type == 'success') {
                isCompanyValid = true;
                $('#txtCompanyName').css('border-color', 'green');
                $('#spIconValidation').find('i').css('color', 'green').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-check');
            }
            else {
                $('#txtCompanyName').css('border-color', 'red');
                $('#spIconValidation').find('i').css('color', 'red').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-times');
            }
        });
    }


    //-------------------------------------------------- Validate Company ---------------------------------------------//
















    //-------------------------------------------------- Create Account ---------------------------------------------//


    $(document).on("keydown", ".signUpRequired", function (evt) {
        var firstChar = $(this).val()
        if (evt.keyCode == 32 && firstChar == "") {
            return false;
        }
    });

    
    $('#btnSuccessButton').on('click', function (evt) {
        location.href = '/home/index';
    });


    $('#btnSignUp').on('click', function (evt) {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        if (vaidateFields('signUpRequired'))
            return;

        if ($('#txtCompanyName').val().length < 3) {
            showErrorMessage('A company name cannot be less than 3 characters');
            return;
        }

        if ($('#txtSignUpPassword').val() != $('#txtConfirmPassword').val()) {
            showErrorMessage('Passwords do not match. Please try again');
            return;
        }

        if (!validateEmail($('#txtEmail').val(), "You have captured an invalid e-mail address. Please try again.")) {
            return;
        }

        //if (!isCompanyValid) {
        //    showErrorMessage('A Company already exists with this name. Please try again');
        //    $('#txtCompanyName').focus();
        //    return;
        //}


        var default_roles_arr = Array();
        var default_roles_names_arr = ['Administrator', 'Live Chat Support Agent', 'Tickets Support Agent'];
        var default_roles_desc_arr = ['Has the right to do everything', 'Supporting customers live on the website live chat', 'Supporting Customers Tickets'];
        var default_roles_landing_arr = ['home', 'live_chat', 'support_tickets'];

        for (var i = 0; i < default_roles_names_arr.length; i++) {
            var details = {
                role_name: default_roles_names_arr[i],
                role_description: default_roles_desc_arr[i],
                landing_page: default_roles_landing_arr[i]
            };
            default_roles_arr.push(details);
        }




        $('#myLoader').modal('show');

        var details = {
            country: $('#cboCountry').val(),
            company_name: $('#txtCompanyName').val(),
            company_size: $('#cboCompanySize').val(),
            person_first_name: $('#txtFirstName').val(),
            person_last_name: $('#txtLastName').val(),
            contact_email_address: $('#txtEmail').val(),
            system_user_password: $('#txtSignUpPassword').val(),
            payment_period: "Monthly",//payment_period,
            package_name: "Free Trial",//package_name,
            location: "",
            device_id: $('#lblDeviceId').text(),
            device_type: "web",
            roles: default_roles_arr
        };
        //console.log(details);
        //return;
        $.post(http + 'partialRegister', details, function (data) {
            //console.log(data);
            if (data.response_type == 'success') {
                showSuccessMessage(data.description);

                $('#cboCountry').val('');
                $('#txtCompanyName').val('');
                $('#txtFirstName').val('');
                $('#txtLastName').val('');
                $('#txtEmail').val('');
                $('#txtSignUpPassword').val('');

                $('#myLoader').modal('hide');
            }
            else {
                $('#myLoader').modal('hide');

                var htmlError = "";
                if (data.description == 'User already exists. Please go to Forgot Password to receive a password reminder.') {
                    htmlError = " <a class='resendMyActivationLink' href='#'>Send me my activation link again</a>";
                }

                showErrorMessage(data.description + htmlError);
            }
        });

        evt.preventDefault();
    });
    

    $('#btnShowMeas_smart_connects').on('click', function (evt) {
        $('#showMeas_smart_connects').modal('show');
    });


    //-------------------------------------------------- Create Account ---------------------------------------------//











    //--------------------------------- Resend My Activation Link ---------------------------------------//



    $(document).on('click', '.resendMyActivationLink', function () {
        $('#donee').trigger('click');
        resendActivationLink();
    });

    function resendActivationLink() {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        $('#myLoader').modal('show');

        var details = {
            username: $('#txtEmail').val(),
            sent_from: 'account_registration'
        };

        $.getJSON(http + 'resend_my_activation_link', details, function (data) {
            if (data.response_type == 'success') {
                showSuccessMessage(data.description);
                $('#myLoader').modal('hide');
            }
            else {
                $('#myLoader').modal('hide');
                showErrorMessage(data.description);
            }
        });
    }



    //--------------------------------- Resend My Activation Link ---------------------------------------//














    //--------------------------------- Change Backgrounds ---------------------------------------//


    function changeBackgroundTheme() {
        var theme = Math.floor(Math.random() * 25);
        var image = 'url(../Content/Images/background_images/' + theme + '.jpg)'
        $('.accountbg').css('background-image', image);
    }

    //changeBackgroundTheme();



//--------------------------------- Change Backgrounds ---------------------------------------//















    /***************************************************************
            Validations
***************************************************************/

    function validateEmail(sEmail, msg) {
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(sEmail)) {
            return true;
        }
        else {
            //var msg = "Please enter valid email address.";
            showErrorMessage(msg);
            return false;
        }
    }

    function SuccessMessage(msg) {
        $('#myPleaseWait').modal('show');
        $('#txtModalLoaderAlertMessage').hide;
        $('#txtModalLoaderSuccessMessage').show();
        $('#txtModalLoaderSuccessMessage b').html(msg);

        setTimeout(function () {
            $('#myPleaseWait').hide();
            $('#txtModalLoaderSuccessMessage').hide();
            $('#myPleaseWait').modal('hide');
        }, 3000);
    }

    function checkEmptyFields(msg) {

        $('#myPleaseWait').modal('show');
        $('#txtModalLoaderAlertMessage').show();
        $('#txtModalLoaderAlertMessage b').text(msg);

        setTimeout(function () {
            $('#myPleaseWait').hide();
            $('#myPleaseWait').modal('hide');
            $('#txtModalLoaderAlertMessage').hide();
        }, 3000);
    }



    /***************************************************************
                        Validations Ends
    ***************************************************************/



    //Removing  Messages CoOperation Services

    $(document).on('click', function () {
        if ($('#txtModalLoaderAlertMessage').is(':visible')) {
            $('#txtModalLoaderAlertMessage').hide();
        }
        if ($('#txtModalLoaderSuccessMessage').is(':visible')) {
            $('#txtModalLoaderSuccessMessage').hide();
        }
    });




});