﻿$(document).ready(function () {

    var http = httpsapi + "/api/admin/";

    var how_is_connection = checkNetworkConnection();



    //-------------------------------------------------- Reset Password ---------------------------------------------//




    var uri = window.location.toString();
    function getValuParameter(uri) {
        return uri.substring(uri.indexOf("=") + 1);
    }

    $('#btnSuccessButton').on('click', function (evt) {
        location.href = '/home/index';
    });

    $(document).on("keydown", ".resetPassRequired", function (evt) {
        var firstChar = $(this).val()
        if (evt.keyCode == 32 && firstChar == "") {
            return false;
        }
    });

    $('#btnResetPassword').on('click', function (evt) {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        if (vaidateFields('resetPassRequired'))
            return;

        if ($('#txtNewPassword').val() != $('#txtConfirmPassword').val()) {
            showErrorMessage('Passwords do not match.  Please try again');
            return;
        }

        $('#myLoader').modal('show');

        var value = getValuParameter(uri);
        var details = {
            encryptedEmail: value,
            password: $('#txtNewPassword').val()
        };

        $.getJSON(http + 'resetPassword', details, function (data) {

            if (data.response_type == 'success') {
                showSuccessMessage(data.description);

                $('#txtNewPassword').val('');
                $('#txtConfirmPassword').val('');

                setTimeout(function () {
                    $('#myLoader').modal('hide');
                }, 1000);
            }
            else {
                setTimeout(function () {
                    $('#myLoader').modal('hide');
                }, 1000);

                $('#txtNewPassword').val('');
                $('#txtConfirmPassword').val('');
                showErrorMessage(data.description);
            }
        });

        evt.preventDefault();
    });





    //-------------------------------------------------- Reset Password ---------------------------------------------//

















    //--------------------------------- Change Backgrounds ---------------------------------------//


    function changeBackgroundTheme() {
        var theme = Math.floor(Math.random() * 25);
        var image = 'url(../Content/Images/background_images/' + theme + '.jpg)'
        $('.accountbg').css('background-image', image);
    }

    //changeBackgroundTheme();



//--------------------------------- Change Backgrounds ---------------------------------------//

















    /***************************************************************
            Validations
***************************************************************/

    function validateEmail(inputText) {
        var d = false;
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(inputText)) {
            d = true;
        }

        if (d == false) {
            var msg = "Please enter valid email address.";
            showErrorMessage(msg);
        }
        return d;
    }



    /***************************************************************
                        Validations Ends
    ***************************************************************/






});