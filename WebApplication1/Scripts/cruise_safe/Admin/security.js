﻿function checkPrev(myPrivs) {

    //-------------------- Check Prev ---------------------------//

    if ($('#lblUserType').text() != 'business' && $('#lblUserRoleName').text() != "Administrator") {
        location.href = '/as_smart_connects/home';
    }

    //-------------------- Check Prev ---------------------------//
}



$(document).ready(function () {

    var http = httpsapi + "/api/admin/";
    var https = httpsapi + "/api/";
    //var http = httpsapi_local + '/api/admin/';

    var how_is_connection = checkNetworkConnection();






    $('.actionAdministrator').trigger('click');
    $('.btnSecurity').find('a').addClass('activeMenu');












    //------------------------------------------- my employees --------------------------------------------------------------//

    var httpOrg = httpsapi + "/api/organisation/";


    var my_employees;

    function getMyEmployees() {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        //$('#myLoader').modal('show');

        var details = {
            company_id: $('#lblCompanyID').text(),
            session_token: $('#lblUserToken').text(),
            system_user_id: $('#lblUserID').text(),
        };
        //console.log(details);
        $.getJSON(httpOrg + 'getMyEmployeesList', details, function (data) {
            //console.log('getMyEmployees', data);
            my_employees = data;

            if (data.length > 0) {
                $('#cboEmployeeOptions_Security').html('');
                $('#cboEmployeeOptions_Security1').html('');
                $('#cboEmployeeOptions_Security2').html('');

                $('#cboEmployee2Options_Security').html('');


                for (var i = 0; i < data.length; i++) {
                    var htmlcontent = '<option value="' + data[i].person_id + '">' + removeNull(data[i].person_first_name) + ' ' + removeNull(data[i].person_last_name) + ' "' + removeNull(data[i].contact_email_address) + '"</option>';
                    $('#cboEmployeeOptions_Security').append(htmlcontent);
                    $('#cboEmployeeOptions_Security1').append(htmlcontent);
                    $('#cboEmployeeOptions_Security2').append(htmlcontent);

                    $('#cboEmployee2Options_Security').append(htmlcontent);
                }
            }

            getSecurity();
            //setTimeout(function () {
            //    $('#myLoader').modal('hide');
            //}, 1000);
        });
    }

    getMyEmployees();






    function getEmployeeDetailsByPersonId(person_id) {
        var employee_details;
        for (var i = 0; i < my_employees.length; i++) {
            if (my_employees[i]['person_id'] == person_id) {
                employee_details = my_employees[i];
            }
        }

        return employee_details;
    }


    function getEmployeeById(person_id) {
        var employee_name = '';

        for (var i = 0; i < my_employees.length; i++) {
            if (my_employees[i].person_id == person_id) {
                employee_name = removeNull(my_employees[i].person_first_name) + ' ' + removeNull(my_employees[i].person_last_name) + ' "' + removeNull(my_employees[i].contact_email_address) + '"';
            }
        }

        return employee_name;
    }


    function getEmployeeEmailById(person_id) {
        var employee_email = '';

        for (var i = 0; i < my_employees.length; i++) {
            if (my_employees[i].person_id == person_id) {
                employee_email = removeNull(my_employees[i].contact_email_address);
            }
        }

        return employee_email;
    }







    var isoptionsecurityshown = false;
    var isoptionsecurityshown1 = false;
    var isoptionsecurityshown2 = false;

    var isoptionsecurity2shown = false;

    $('#txtSendNotificationTo').mouseup(function () {
        $('#cboEmployeeOptions_Security').fadeIn(200);

        setTimeout(function () {
            isoptionsecurityshown = true;
        }, 1000);
    });

    $('#txtSendNotificationTo1').mouseup(function () {
        $('#cboEmployeeOptions_Security1').fadeIn(200);

        setTimeout(function () {
            isoptionsecurityshown1 = true;
        }, 1000);
    });

    $('#txtSendNotificationTo2').mouseup(function () {
        $('#cboEmployeeOptions_Security2').fadeIn(200);

        setTimeout(function () {
            isoptionsecurityshown2 = true;
        }, 1000);
    });








    $('#txtSelectAdministrator').mouseup(function () {
        $('#cboEmployee2Options_Security').fadeIn(200);

        setTimeout(function () {
            isoptionsecurity2shown = true;
        }, 1000);
    });


    $(document).click(function () {
        if (isoptionsecurityshown) {
            $('#cboEmployeeOptions_Security').fadeOut(200);
            isoptionsecurityshown = false;
        }
        else if (isoptionsecurityshown1) {
            $('#cboEmployeeOptions_Security1').fadeOut(200);
            isoptionsecurityshown1 = false;
        }
        else if (isoptionsecurityshown2) {
            $('#cboEmployeeOptions_Security2').fadeOut(200);
            isoptionsecurityshown2 = false;
        }
        else if (isoptionsecurity2shown) {
            $('#cboEmployee2Options_Security').fadeOut(200);
            isoptionsecurity2shown = false;
        }
    });




    $("#txtSendNotificationTo").on("keyup", function () {
        $('#cboEmployeeOptions_Security').show();
        isoptionsecurityshown = true;

        var value = $(this).val().toLowerCase();
        $("#cboEmployeeOptions_Security option").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

    $("#txtSendNotificationTo1").on("keyup", function () {
        $('#cboEmployeeOptions_Security1').show();
        isoptionsecurityshown1 = true;

        var value = $(this).val().toLowerCase();
        $("#cboEmployeeOptions_Security1 option").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

    $("#txtSendNotificationTo2").on("keyup", function () {
        $('#cboEmployeeOptions_Security2').show();
        isoptionsecurityshown2 = true;

        var value = $(this).val().toLowerCase();
        $("#cboEmployeeOptions_Security2 option").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });








    $("#txtSelectAdministrator").on("keyup", function () {
        $('#cboEmployee2Options_Security').show();
        isoptionsecurity2shown = true;

        var value = $(this).val().toLowerCase();
        $("#cboEmployee2Options_Security option").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });






    $("#cboEmployeeOptions_Security").change(function () {
        $('#txtSendNotificationTo').attr('data-chossenemployeesecurity', $(this).val());

        $('#txtSendNotificationTo').val(getEmployeeById($(this).val()));
        $('#cboEmployeeOptions_Security').fadeOut(200);
        isoptionsecurityshown = false;
    });

    $("#cboEmployeeOptions_Security1").change(function () {
        $('#txtSendNotificationTo1').attr('data-chossenemployeesecurity', $(this).val());

        $('#txtSendNotificationTo1').val(getEmployeeById($(this).val()));
        $('#cboEmployeeOptions_Security1').fadeOut(200);
        isoptionsecurityshown1 = false;
    });

    $("#cboEmployeeOptions_Security2").change(function () {
        $('#txtSendNotificationTo2').attr('data-chossenemployeesecurity', $(this).val());

        $('#txtSendNotificationTo2').val(getEmployeeById($(this).val()));
        $('#cboEmployeeOptions_Security2').fadeOut(200);
        isoptionsecurityshown2 = false;
    });


    $("#cboEmployee2Options_Security").change(function () {
        $('#txtSelectAdministrator').attr('data-chossenemployeesecurity2', $(this).val());

        $('#txtSelectAdministrator').val(getEmployeeById($(this).val()));
        $('#cboEmployee2Options_Security').fadeOut(200);
        isoptionsecurity2shown = false;
    });




    //------------------------------------------- Pull my employees --------------------------------------------------------------//























    //---------------------------------------- Get Security Settings -----------------------------------------//

    var security;
    var id;


    function getSecurity() {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        $('#myLoader').modal('show');

        var details = {
            company_id: $('#lblCompanyID').text(),
            session_token: $('#lblUserToken').text(),
            system_user_id: $('#lblUserID').text(),
        };
        $.getJSON(http + 'getSecurity', details, function (data) {
            //console.log('getSecurity', data);
            security = data;

            if (data.length > 0) {
                security_id = 1;


                $('#cboSessionTimeout').val(data[0]['session_timeout_in']);
                $('#cboLoginAttempts').val(data[0]['login_attempts']);

                $('#txtSendNotificationTo').val(getEmployeeById(data[0]['security_administrator_one']));
                $('#txtSendNotificationTo').attr('data-chossenemployeesecurity', data[0]['security_administrator_one']);

                $('#btnAddPerson1').show();
                $('#btnAddPerson2').show();
                if (removeNull(data[0]['security_administrator_other_one']) == "" && removeNull(data[0]['security_administrator_other_two']) == "") {
                    $('#btnAddPerson1').show();
                    $('#btnAddPerson2').hide();
                }

                $('.cls_person_one').removeClass("cls_shown").hide();
                if (removeNull(data[0]['security_administrator_other_one']) != "") {
                    $('#txtSendNotificationTo1').val(getEmployeeById(data[0]['security_administrator_other_one']));
                    $('#txtSendNotificationTo1').attr('data-chossenemployeesecurity', data[0]['security_administrator_other_one']);
                    $('.cls_person_one').addClass("cls_shown").show();
                    $('#btnAddPerson1').hide();
                }



                $('.cls_person_two').removeClass("cls_shown").hide();
                if (removeNull(data[0]['security_administrator_other_two']) != "") {
                    $('#txtSendNotificationTo2').val(getEmployeeById(data[0]['security_administrator_other_two']));
                    $('#txtSendNotificationTo2').attr('data-chossenemployeesecurity', data[0]['security_administrator_other_two']);
                    $('.cls_person_two').addClass("cls_shown").show();
                    $('#btnAddPerson2').hide();
                }



                $('#cboMinimumCharacters').val(data[0]['minimum_password_characters']);
                $('#cboPasswordCannot_be_same_as_last').val(data[0]['password_cannot_be_same_as_last_password']);
                $('#cboPasswordExpires').val(data[0]['password_expires_in']);

                $('#txtSelectAdministrator').val(getEmployeeById(data[0]['security_administrator_two']));
                $('#txtSelectAdministrator').attr('data-chossenemployeesecurity2', data[0]['security_administrator_two']);
                //security_administrator_two_email: (send_notification_to_administrator == 0) ? null : getEmployeeEmailById($('#txtSelectAdministrator').attr('data-chossenemployeesecurity2')),


                $('#chkBlock_user_profile').removeAttr('checked');
                if (data[0]['block_user_profile'] == 1) {
                    $('#chkBlock_user_profile').trigger('click');
                }


                $('#chkSend_notification_to_the_profile_owner').removeAttr('checked');
                if (data[0]['send_notification_to_profile_owner'] == 1) {
                    $('#chkSend_notification_to_the_profile_owner').trigger('click');
                }


                $('#chkSend_notification_to_administrator').removeAttr('checked');
                if (data[0]['send_notification_to_administrator'] == 1) {
                    $('#chkSend_notification_to_administrator').trigger('click');
                }


                $('#chkCannot_contain_username').removeAttr('checked');
                if (data[0]['cannot_contain_username'] == 1) {
                    $('#chkCannot_contain_username').trigger('click');
                }


                $('#chkHave_alphabet_and_number').removeAttr('checked');
                if (data[0]['have_alphabet_and_number'] == 1) {
                    $('#chkHave_alphabet_and_number').trigger('click');
                }


                $('#chkHave_special_character').removeAttr('checked');
                if (data[0]['have_special_character'] == 1) {
                    $('#chkHave_special_character').trigger('click');
                }



                $('#chk_do_not_allow_login_during_non_business_working_hours').removeAttr('checked');
                if (data[0]['do_not_allow_login_during_non_business_working_hours'] == 1) {
                    $('#chk_do_not_allow_login_during_non_business_working_hours').trigger('click');
                }


                $('#chk_do_not_allow_login_when_schedule_is_non_working_day').removeAttr('checked');
                if (data[0]['do_not_allow_login_when_schedule_is_non_working_day'] == 1) {
                    $('#chk_do_not_allow_login_when_schedule_is_non_working_day').trigger('click');
                }


                $('#chk_do_not_allow_login_when_attendance_is_not_present').removeAttr('checked');
                if (data[0]['do_not_allow_login_when_attendance_is_not_present'] == 1) {
                    $('#chk_do_not_allow_login_when_attendance_is_not_present').trigger('click');
                }



            }

            setTimeout(function () {
                $('#myLoader').modal('hide');
            }, 1000);
        });
    }

    

    //---------------------------------------- Get Security Settings -----------------------------------------//












    $('.btnGoHome').click(function () {
        location.href = "/as_smart_connects/home";
    });






    //------------------------------------------ Save Security Settings ------------------------------------------------------//






    $('#btnAddPerson1').click(function () {
        $('.cls_person_one').addClass("cls_shown").show();

        $('#btnAddPerson1').hide();
        $('#btnAddPerson2').show();

        if ($('.cls_shown').length == 2) {
            $('.cls_hide_all_add_btn').hide();
        }
    });

    $('#btnAddPerson2').click(function () {
        $('.cls_person_two').addClass("cls_shown").show();

        $('#btnAddPerson1').show();
        $('#btnAddPerson2').hide();

        if ($('.cls_shown').length == 2) {
            $('.cls_hide_all_add_btn').hide();
        }
    });






    $('#btnRemovePerson1').click(function () {
        $('.cls_person_one').removeClass("cls_shown").hide();

        $('#btnAddPerson1').show();
        $('#btnAddPerson2').hide();

        $('#txtSendNotificationTo1').val("");
        $('#txtSendNotificationTo1').attr("data-chossenemployeesecurity", 0);

        if ($('.cls_shown').length == 2) {
            $('.cls_hide_all_add_btn').hide();
        }
    });

    $('#btnRemovePerson2').click(function () {
        $('.cls_person_two').removeClass("cls_shown").hide();

        $('#btnAddPerson1').hide();
        $('#btnAddPerson2').show();

        $('#txtSendNotificationTo2').val("");
        $('#txtSendNotificationTo2').attr("data-chossenemployeesecurity",0);

        if ($('.cls_shown').length == 2) {
            $('.cls_hide_all_add_btn').hide();
        }
    });









    var block_user_profile = 0;
    var send_notification_to_profile_owner = 0;
    var send_notification_to_administrator = 0;
    var cannot_contain_username = 0;
    var have_alphabet_and_number = 0;
    var have_special_character = 0;
    var security_id = 0;

    var do_not_allow_login_during_non_business_working_hours = 0;
    var do_not_allow_login_when_schedule_is_non_working_day = 0;
    var do_not_allow_login_when_attendance_is_not_present = 0;

    $('#chkBlock_user_profile').click(function () {
        if ($(this).is(':checked')) {
            block_user_profile = 1;
        }
        else {
            block_user_profile = 0;
        }
    });




    $('#chkSend_notification_to_the_profile_owner').click(function () {
        if ($(this).is(':checked')) {
            send_notification_to_profile_owner = 1;
        }
        else {
            send_notification_to_profile_owner = 0;
        }
    });




    $('#chkSend_notification_to_administrator').click(function () {
        if ($(this).is(':checked')) {
            send_notification_to_administrator = 1;
            $('#txtSelectAdministrator').show();
        }
        else {
            send_notification_to_administrator = 0;
            $('#txtSelectAdministrator').hide();
        }
    });



    $('#chkCannot_contain_username').click(function () {
        if ($(this).is(':checked')) {
            cannot_contain_username = 1;
        }
        else {
            cannot_contain_username = 0;
        }
    });



    $('#chkHave_alphabet_and_number').click(function () {
        if ($(this).is(':checked')) {
            have_alphabet_and_number = 1;
        }
        else {
            have_alphabet_and_number = 0;
        }
    });



    $('#chkHave_special_character').click(function () {
        if ($(this).is(':checked')) {
            have_special_character = 1;
        }
        else {
            have_special_character = 0;
        }
    });






    $('#chk_do_not_allow_login_during_non_business_working_hours').click(function () {
        if ($(this).is(':checked')) {
            do_not_allow_login_during_non_business_working_hours = 1;
        }
        else {
            do_not_allow_login_during_non_business_working_hours = 0;
        }
    });


    $('#chk_do_not_allow_login_when_schedule_is_non_working_day').click(function () {
        if ($(this).is(':checked')) {
            do_not_allow_login_when_schedule_is_non_working_day = 1;
        }
        else {
            do_not_allow_login_when_schedule_is_non_working_day = 0;
        }
    });

    $('#chk_do_not_allow_login_when_attendance_is_not_present').click(function () {
        if ($(this).is(':checked')) {
            do_not_allow_login_when_attendance_is_not_present = 1;
        }
        else {
            do_not_allow_login_when_attendance_is_not_present = 0;
        }
    });



    $('#btnSaveSecuritySettings').on('click', function () {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        $('#myLoader').modal('show');

        var details = {
            security_id: security_id,
            security_administrator_one: ($('#txtSendNotificationTo').val() == "") ? null : $('#txtSendNotificationTo').attr('data-chossenemployeesecurity'),
            security_administrator_one_email: ($('#txtSendNotificationTo').val() == "") ? null : getEmployeeEmailById($('#txtSendNotificationTo').attr('data-chossenemployeesecurity')),

            security_administrator_other_one: ($('#txtSendNotificationTo1').val() == "") ? null: $('#txtSendNotificationTo1').attr('data-chossenemployeesecurity'),
            security_administrator_other_one_email: ($('#txtSendNotificationTo1').val() == "") ? null : getEmployeeEmailById($('#txtSendNotificationTo1').attr('data-chossenemployeesecurity')),

            security_administrator_other_two: ($('#txtSendNotificationTo2').val() == "") ? null : $('#txtSendNotificationTo2').attr('data-chossenemployeesecurity'),
            security_administrator_other_two_email: ($('#txtSendNotificationTo2').val() == "") ? null : getEmployeeEmailById($('#txtSendNotificationTo2').attr('data-chossenemployeesecurity')),

            session_timeout_in: $('#cboSessionTimeout').val(),
            login_attempts: $('#cboLoginAttempts').val(),
            block_user_profile: block_user_profile,
            send_notification_to_profile_owner: send_notification_to_profile_owner,

            send_notification_to_administrator: send_notification_to_administrator,
            security_administrator_two: (send_notification_to_administrator == 0) ? null : $('#txtSelectAdministrator').attr('data-chossenemployeesecurity2'),
            security_administrator_two_email: (send_notification_to_administrator == 0) ? null : getEmployeeEmailById($('#txtSelectAdministrator').attr('data-chossenemployeesecurity2')),

            minimum_password_characters: $('#cboMinimumCharacters').val(),
            password_cannot_be_same_as_last_password: $('#cboPasswordCannot_be_same_as_last').val(),
            password_expires_in: $('#cboPasswordExpires').val(),

            cannot_contain_username: cannot_contain_username,
            have_alphabet_and_number: have_alphabet_and_number,
            have_special_character: have_special_character,

            security_company_id: $('#lblCompanyID').text(),
            security_last_updated_by: $('#lblPersonId').text(),
            security_system_user_id: $('#lblUserID').text(),
            session_token: $('#lblUserToken').text(),

            do_not_allow_login_during_non_business_working_hours: do_not_allow_login_during_non_business_working_hours,
            do_not_allow_login_when_schedule_is_non_working_day: do_not_allow_login_when_schedule_is_non_working_day,
            do_not_allow_login_when_attendance_is_not_present: do_not_allow_login_when_attendance_is_not_present
        };

        console.log(details);
        //return;
        $.post(http + 'save_security_settings', details, function (data) {
            if (data.response_type == 'success') {
                showSuccessMessage(data.description);


                setTimeout(function () {
                    $('#successModal').modal('hide');
                }, 1000);
            }
            else {
                showErrorMessage(data.description);
            }

            setTimeout(function () {
                $('#myLoader').modal('hide');
            }, 1000);

        });
    });




    //------------------------------------------ Save Security Settings ------------------------------------------------------//










});