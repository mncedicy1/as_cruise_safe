﻿$(document).ready(function () {

    var http = httpsapi + "/api/admin/";


    //--------------------- Today's date ------------------------//



    var parms = {};

    //var session = "user_session_counrty, user_session_province, user_session_city, user_session_ip, user_session_local_ip,
    //    user_session_counrty_code, user_session_province_code, user_session_continent_code, user_session_continent



    getLocalIP().then((ipAddr) => {
        parms.user_session_local_ip = ipAddr;
        $('#lblDeviceId').html(ipAddr);
        $.getJSON('https://api.ipify.org?format=json', function (data) {
            parms.user_session_ip = data.ip;
            $.getJSON('https://www.geoplugin.net/json.gp?jsoncallback=?', function (data) {
                parms.user_session_counrty = data.geoplugin_countryName;
                parms.user_session_province = data.geoplugin_region;
                parms.user_session_city = data.geoplugin_city;
                parms.user_session_counrty_code = data.geoplugin_currencyCode;
                parms.user_session_province_code = data.geoplugin_regionCode;
                parms.user_session_continent_code = data.geoplugin_continentCode;
                parms.user_session_continent = data.geoplugin_continentName;
                parms.user_session_latitude = data.geoplugin_latitude;
                parms.user_session_longitude = data.geoplugin_longitude;
                console.log(parms); 
            });
        });
    });
  
   

    



    var today = new Date();

    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();

    if (mm < 10)
        mm = "0" + mm;
    if (dd < 10)
        dd = "0" + dd;

    var todayDate = yyyy + "-" + mm + "-" + dd;


    //--------------------- Today's date ------------------------//


    var loginThemes = ['btnTheme1', 'btnTheme2', 'btnTheme3', 'btnTheme4', 'btnTheme5', 'btnTheme6'];
    var countInterval;
    var secAvail = 0;
    function startAvailable() {
        secAvail = 0;
        countInterval = setInterval(function () {
            secAvail++;
            if (secAvail == 3) {
                changeLoginTheme()
            }
        }, 3000);
    }


    function changeLoginTheme() {
        var theme = loginThemes[Math.floor(Math.random() * loginThemes.length)];
        startAvailable();
        $('#' + theme).trigger('click');
    }

    changeLoginTheme();




    $('#btnSignIn').on('click', function (evt) {

        doneLogin(evt);
        evt.preventDefault();
    });



    $(document).on('keypress', function (evt) {

        if (evt.which == 13) {
            doneLogin(evt);
        }
        //evt.preventDefault();
    });



    function doneLogin(evt) {

        $('.myLoader').modal('show');

        parms.user_email = $('#txtUsername').val();
        parms.user_password = $('#txtPassword').val();
        parms.user_device_id = $('#lblDeviceId').text();

        $.getJSON('/admin/login', parms, function (data) {
            //console.log(data);

            if (data.results == 'success') {
               // subscribeTopic(data.system_user.system_user_web_device_id, data.municipality.municipality_id);
                location.href = "/Home/Menu"
            }
            else {
                showErrorMessage(data.results);
            }

                $('#myLoader').modal('hide');
        });
    }





    function subscribeTopic(deviceid, municipalityid) {

        var res = httpsapi.substring(8, httpsapi.indexOf('.'));
        var details = {
            "to": "/topics/municipality" + res + municipalityid,
            "registration_tokens": [deviceid]
        }

        var serverKey = "AIzaSyCJDJNvIItrK0JX2LunYo4J67DBlEohkTg";

        //console.log(details);

        $.ajax({
            url: "https://iid.googleapis.com/iid/v1:batchAdd",
            type: "POST",
            data: JSON.stringify(details),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", "key=" + serverKey);
            },
            error: function (response) {
                console.log(response);
            },
            success: function (response) {
                console.log(response, details.to);
            }
        });
    }








    //$('#btnSignIn').on('click', function (evt) {

    //    location.href = "/AS/home"; 0784884519
    //    evt.preventDefault();
    //});



    /***************************************************************
            Validations
***************************************************************/

    function validateEmail(inputText) {
        var d = false;
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(inputText)) {
            d = true;
        }

        if (d == false) {
            var msg = "Please enter valid email address.";
            checkEmptyFields(msg);
        }
        return d;
    }

    function SuccessMessage(msg) {
        $('#myPleaseWait').modal('show');
        $('#txtModalLoaderAlertMessage').hide;
        $('#txtModalLoaderSuccessMessage').show();
        $('#txtModalLoaderSuccessMessage b').html(msg);

        setTimeout(function () {
            $('#myPleaseWait').hide();
            $('#txtModalLoaderSuccessMessage').hide();
            $('#myPleaseWait').modal('hide');
        }, 3000);
    }

    function checkEmptyFields(msg) {

        $('#myPleaseWait').modal('show');
        $('#txtModalLoaderAlertMessage').show();
        $('#txtModalLoaderAlertMessage b').text(msg);

        setTimeout(function () {
            $('#myPleaseWait').hide();
            $('#myPleaseWait').modal('hide');
            $('#txtModalLoaderAlertMessage').hide();
        }, 3000);
    }



    /***************************************************************
                        Validations Ends
    ***************************************************************/



    //Removing  Messages CoOperation Services

    $(document).on('click', function () {
        if ($('#txtModalLoaderAlertMessage').is(':visible')) {
            $('#txtModalLoaderAlertMessage').hide();
        }
        if ($('#txtModalLoaderSuccessMessage').is(':visible')) {
            $('#txtModalLoaderSuccessMessage').hide();
        }
    });




});