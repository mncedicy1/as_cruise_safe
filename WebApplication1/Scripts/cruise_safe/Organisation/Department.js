﻿function checkPrev(myPrivs) {

    //-------------------- Check Prev ---------------------------//

    if ($('#lblDepartmentType').text() != 'business') {
        if (myPrivs.indexOf(1002) >= 0) {

        } else {
            location.href = '/Home/menu';
        }
    }

    //-------------------- Check Prev ---------------------------//
}

$(document).ready(function () {



    var http = httpsapi + "/api/Organisation/";
    var https = httpsapi + "/api/";
    // http = httpsapi_local + '/api/Organisation/';

    var how_is_connection = '';//checkNetworkConnection();




    $('.actionAdministrator').trigger('click');
    $('.btnDepartmentManagement').find('a').addClass('activeMenu');



    //------------------------------------------------------------------ Get all Departments ----------------------------------------------------------------------------------------//




    var isSendingg = false;
    var scrollHeight = 0;
    var Departments = Array();

    var users = Array();

    var start_fromm = 0;
    var user_id = null;
    var department_id = null;


    function getDepartments() {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        $('#myLoader').modal('show');

        var details = {
            client_id: $('#lblClientID').text()
        };
        console.log(details);
        $.getJSON(http+'get_departments', details, function (data) {
            console.log('getDepartments', data);
            Departments = data;
            displayDepartments(data);
         

                $('#myLoader').modal('hide');
        });
    }




    getUsers(true);


    function displayDepartments(data) {

        $('#zeroStatsDepartments').hide();
        if (data.length > 0) {
            var str = "";
            for (i = 0; i < data.length; i++) {
                var userStatus = "";
                var statusButton = "";

                if (data[i]['department_status'] == 'active') {
                    userStatus = "label-success badge badge-success";
                    statusButton = '<button id="v' + i + '"  style="width:90px" type="button" class="btn btn-danger removeDept btn-flat btn-xs"> Deactivate</button>';
                }
                else {
                    userStatus = "label-danger badge badge-danger";
                    statusButton = '<button id="t' + i + '"  style="width:90px" type="button" class="btn btn-success activateDept btn-flat btn-xs"> Activate</button>';
                }

                str += '<tr class="btnChooseMember" style="cursor:pointer;" id="b' + i + '"><td>' + (i + 1) + '</td>';
                str += '<td>' + data[i]['department_name'] + '</td>';
                str += '<td>' + data[i]['department_abbr'] + '</td>';
                str += '<td>' + getName(data[i]['department_director']) + '</td>';
                str += '<td>' + data[i]['department_region_name'] + '</td>';
                str += '<td>' + data[i]['department_teams'] + '</td>';
                str += '<td><span class="' + userStatus + '">' + data[i]['department_status'] + '</span></td>';
                str += '<td>       <button id="b' + i + '" style="width:90px" type="button" class="btn btn-warning btn-flat btn-xs btnView">View</button>  ' + statusButton + ' </td>';
            }
            $('#tblDepartments').html(str);
        }


        if (start_fromm == 0 && data.length == 0) {
            $('#zeroStatsDepartments').show();
        }




        //------------ view -------------------//


        $('.btnView').click(function () {

            id = this.id.substring(1);
            $('#btnAddDepartment').html('Save Changes');

            //--------------- Populate Fields ------------------//
            department_id = Departments[id]['department_id'];

            $('#btnDirector').attr('data-id', data[id]['department_director']);
            $('#btnDirector').html(getName(data[id]['department_director'])).css('border-color', 'rgba(42, 50, 60, 0.2)');

            $('#txtShortName').val(Departments[id]['department_abbr']);
            $('#cboRegions').val(Departments[id]['department_region_id']);
            $('#cboDirector').val(Departments[id]['department_director']);
            $('#txtPhone').val(Departments[id]['department_phone']);
            $('#txtEmail').val(Departments[id]['department_email']);
            $('#txtName').val(Departments[id]['department_name']);
         

            //--------------- Populate Fields ------------------//

            $('.adminDepartments-modal-lg').modal('show');
        });

        //------------ view -------------------//







        //------------ activate -------------------//

        $('.activateDept').click(function () {
            id = this.id.substring(1);

            $('#btnConfirm').hide();
            $('#btnConfirmActive').show();


            $('#confirmModal').modal('show');
            $('#lblConfirmMessage').html("Are you sure, you want to activate " + Departments[id]['department_name']+' Department');
        });

        //------------ activate -------------------//






        //------------ deactivate  -------------------//

        $('.removeDept').click(function () {
            id = this.id.substring(1);

            $('#btnConfirm').show();
            $('#btnConfirmActive').hide();


            $('#confirmModal').modal('show');
            $('#lblConfirmMessage').html("Are you sure, you want to remove " + Departments[id]['department_name'] + ' Department');
        });

        //------------ deactivate   -------------------//






    }




    function getUsers(loadMore) {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }


        var details = {
            clientid: $('#lblClientID').text()
        };

        $.getJSON('/admin/getUsers', details, function (data) {
            isSendingg = false;
            users = data;
            console.log(users);
            $('#cboDirector').html('<option value="">-- Choose User --</option>');
            for (i = 0; i < data.length; i++) {
                $('#cboDirector').append('<option value="' + data[i]['user_id']+'">' + data[i]['user_title'] + ' ' + data[i]['user_firstname'] + ' ' + data[i]['user_surname'] + '</option>');
            }
            $('#cboDirector').append('<option value="0" style="font-weight:bold;color:green">New User</option>');

           
            if (loadMore) {
                getDepartments();
                getRoles();
            }

        });
    }


    function getName(user_id) {
        var name = '';
        for (var l = 0; l < users.length; l++) {
            if (users[l]['user_id'] == user_id)
                name = users[l]['user_title'] + ' ' + users[l]['user_firstname'] + ' ' + users[l]['user_surname'];
        }
        return name;
    }



    $('#btnDirector').click(function () {
        listUsersAll(Array());
    });





    function listUsersAll(notArr) {
        var str = '';
        $("#txtSearchh").val('');
        for (var m = 0; m < users.length; m++) {
            if (notArr.indexOf(parseInt(users[m].user_id)) < 0) {
                var name = users[m]['user_title'] + ' ' + users[m]['user_firstname'] + ' ' + users[m]['user_surname'];
                str += '<div class="col-sm-6 col-lg-6 memberr"><div class="panel text-center btnWidget2 userList" id = "user' + users[m]['user_id'] + '">' +
                    '<div class="panel-body p-t-10" style="padding: 20px;"><h4 class="m-t-0 m-b-15">' +
                    '<i class="fas fa-user text-primary m-r-10"></i><b>' + name + '</b></h4>' +
                    '<p class="text-muted m-b-0 m-t-20"> ' + users[m].user_email + '</p></div></div></div>';
            }
        }
        $('#listUsersAll').html(str);
        $('#btnPickOne').hide();
        $('.usersmodallg').modal('show');

        $("#txtSearchh").on("keyup", function () {
            var value = $(this).val().toLowerCase();
            $("#listUsersAll div.memberr").filter(function () {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
            });
            $('#rows').html($("#listUsersAll div.memberr:visible").length);
        });

        $('#btnAddNewUser').click(function () {
            user_id = null;
            $('.clsPassword').show();
            $('.clsPasswords').removeAttr('disabled').addClass('requiredUser');
            $('#btnAddUser').html('Save');
            $('.cleanUsertField').val('');
            $('#txtPassword').val('');
            $('.adminUsers-modal-lg').modal('show');
        });

        $('#btnPickOne').on('click', function () {
       
                $('#btnDirector').attr('data-id', $(this).attr('data-id'));
                $('#btnDirector').html($(this).attr('data-name')).css('border-color', 'rgba(42, 50, 60, 0.2)');
           
            $('.usersmodallg').modal('hide');
        });

        $(document).on('click', ".btnWidget2", function () {
            $(document).find(".btnWidget2").removeClass('active');
            $(this).addClass('active');
                $('#btnPickOne').show('slow').html('Select ' + getName(this.id.substring(4))).attr('data-id', this.id.substring(4));
                $('#btnPickOne').attr('data-name', getName(this.id.substring(4)));
        });


    }




    function getRegions() {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        roles = Array();

        var details = {
            region_client_id: $('#lblClientID').text(),
            //  userId: $('#lblUserID').text(),
        };
        $.getJSON(http + 'getRegions', details, function (data) {
            console.log('getRegions', data);
            if (data) {
                regions = data;
                $('#cboRegions').html('<option value="">-- Choose Region --</option>');
                for (var i = 0; i < data.length; i++) {
                    var htmlcontent = '<option value="' + data[i].OBJECTID + '">' + data[i].REGIONNAME + '</option>';
                    $('#cboRegions').append(htmlcontent);
                }
            }

        });

    }

    getRegions();







    function getRoles() {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        roles = Array();

        var details = {
            clientid: $('#lblClientID').text(),
            //  userId: $('#lblUserID').text(),
        };
        $.getJSON('/admin/getUserRoles', details, function (data) {
            //console.log('Roles', data);
            if (data) {
                roles = data['roles'];
                $('#cboRole').html('<option value="">-- Choose Role --</option>');
                for (var i = 0; i < data['roles'].length; i++) {
                    var htmlcontent = '<option value="' + data['roles'][i].role_id + '">' + data['roles'][i].role_name + '</option>';
                    $('#cboRole').append(htmlcontent);
                }
            }
        });

    }


    $('#btnAddUser').on('click', function () {
        saveUser(user_id);
    });




    function saveUser(user_id) {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        if (vaidateFields('requiredUser'))
            return;
        if (user_id == null) {
            if ($('#txtPassword').val() != $('#txtConfirmPassword').val()) {
                showConnectionMessage("Passwords do not match");
                return;
            }
        }
        isSendingg = true;

        $('.myLoader').modal('show');

        var details = {
            user_id: user_id,
            user_client_id: $('#lblClientID').text(),
            user_id_number: $('#txtIDNumber').val(),
            user_title: $('#cboTitle').val(),
            user_firstname: $('#txtFirstName').val(),
            user_surname: $('#txtLastName').val(),
            user_gender: $('#cboGender').val(),
            user_cellphone: $('#txtCellNumber').val(),
            user_email: $('#txtEmailAddress').val(),
            user_role_id: $('#cboRole').val(),
            user_password: $('#txtPassword').val(),
            user_saved_by: $('#lblUserID').text(),
            user_updated_by: $('#lblUserID').text()

        };
        console.log(details);
        $.post(https + 'admin/addUser', details, function (data) {
            console.log(data);
            isSendingg = false;
            if (data.indexOf('success') >= 0) {
                showSuccessMessageClose(data);
                getUsers(false);
                $('#myLoader').modal('hide');

                $('.adminUsers-modal-lg').modal('hide');
            }
            else {


                $('#myLoader').modal('hide');

                showErrorMessage(data);
            }
        });
    }







    //----------------------------- Search Departments ----------------------------------------//

    $(".search-bar").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#tblDepartments tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

    //----------------------------- Search Departments ----------------------------------------//






    //------------------------------------------------------------------ Get all Departments ----------------------------------------------------------------------------------------//












    //------------------------ Add new Departments ---------------------------//







    $('#btnAddNew').click(function () {
        user_id = null;
        $('#btnDirector').attr('data-id', '');
        $('#btnDirector').html('Add User').css('border-color', 'rgba(42, 50, 60, 0.2)');
        $('#btnAddDepartment').html('Save');
        $('.cleanDepartmentField').val('');
        $('.adminDepartments-modal-lg').modal('show');

    });











    $('#btnAddDepartment').on('click', function () {
        saveDepartment(department_id);
    });




    function saveDepartment(department_id) {
       
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

 
        if (vaidateFields('requiredDepartment'))
            return;

        if ($('#btnDirector').attr('data-id').length == 0) {
            $('#btnDirector').css('border-color', 'red');
            return;
        }

        isSendingg = true;
        console.log(department_id);

        $('.myLoader').modal('show');

        var details = {
            department_id: department_id,
            department_client_id: $('#lblClientID').text(),
            department_abbr: $('#txtShortName').val(),
            department_status: 'active',
            department_region_id: $('#cboRegions').val(),
            department_director: $('#btnDirector').attr('data-id'),
            department_phone: $('#txtPhone').val(),
            department_email: $('#txtEmail').val(),
            department_name: $('#txtName').val(),
            department_created_by: $('#lblUserID').text(),
            department_updated_by: $('#lblUserID').text()
        };
        console.log(details);
        $.post(http + 'save_department', details, function (data) {
            console.log(data);
            isSendingg = false;
            if (data.indexOf('success') >= 0) {
                showSuccessMessage(data);
                getDepartments();
                $('#myLoader').modal('hide');
      
                $('.adminDepartments-modal-lg').modal('hide');
            }
            else {

       
                    $('#myLoader').modal('hide');
               
                showErrorMessage(data);
            }
        });
    }




    //------------------------ Add new Departments ---------------------------//
















    //---------------------- activate user --------------------------//


    $('#btnConfirmActive').on('click', function () {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        $('#confirmModal').modal('hide');
        $('.myLoader').modal('show');

        var details = {
            userId: $('#lblUserID').text(),
            clientid: $('#lblClientID').text(),
            department_id: Departments[id]['department_id'],
            fullname: Departments[id]['department_name']
        };

        $.post('/admin/activateDept', details, function (data) {
          
            if (data.message.indexOf('Error')<0) {
                getDepartments();
                    $('#myLoader').modal('hide');
              

                showSuccessMessageClose(data.message);
                    $('#successModal').modal('hide');
           
            }
            else {
                    $('#myLoader').modal('hide');
                showErrorMessage(data.message);
            }
        });
    });


    //---------------------- activate user ends --------------------------//








    //---------------------- deactivate user --------------------------//


    $('#btnConfirm').on('click', function () {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        $('#confirmModal').modal('hide');
        $('.myLoader').modal('show');

        var details = {
            userId: $('#lblUserID').text(),
            clientid: $('#lblClientID').text(),
            department_id: Departments[id]['department_id'],
            fullname: Departments[id]['department_name']
        };
        //console.log(details);
        //return;
        $.post('/admin/deactivateDept', details, function (data) {
            if (data.message.indexOf('Error') < 0) {
                getDepartments();
                    $('#myLoader').modal('hide');

                showSuccessMessageClose(data.message);
                    $('#successModal').modal('hide');
            }
            else {
                    $('#myLoader').modal('hide');
                showErrorMessage(data.message);
            }
        });
    });


    //---------------------- deactivate user ends --------------------------//



































































    function vaidateFields(clas) {
        $('.' + clas).css('border-color', 'silver');
        var input;
        $('.' + clas).map(function () {
            if (!$(this).val() || $(this).css('border-color') == 'red') {
                $(this).css('border-color', 'red');
                $(this).focus();
                input = $(this);
            }
        }).get();

        if (input) {
            input.focus();
            return true;
        }
        else
            return false;
    }

    function validateEmail(sEmail, msg) {
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(sEmail)) {
            return true;
        }
        else {
            //var msg = "Please enter valid email address.";
            checkEmptyFields(msg);
            return false;
        }
    }

    function SuccessMessage(msg) {
        $('#myPleaseWait').modal('show');
        $('#txtModalLoaderAlertMessage').hide;
        $('#txtModalLoaderSuccessMessage').show();
        $('#txtModalLoaderSuccessMessage b').html(msg);

        setTimeout(function () {
            $('#myPleaseWait').hide();
            $('#txtModalLoaderSuccessMessage').hide();
            $('#myPleaseWait').modal('hide');
        }, 3000);
    }

    function checkEmptyFields(msg) {

        $('#myPleaseWait').modal('show');
        $('#txtModalLoaderAlertMessage').show();
        $('#txtModalLoaderAlertMessage b').text(msg);

        setTimeout(function () {
            $('#myPleaseWait').hide();
            $('#myPleaseWait').modal('hide');
            $('#txtModalLoaderAlertMessage').hide();
        }, 3000);
    }



    /***************************************************************
                        Validations Ends
    ***************************************************************/



    //Removing  Messages CoOperation Services

    $(document).on('click', function () {
        if ($('#txtModalLoaderAlertMessage').is(':visible')) {
            $('#txtModalLoaderAlertMessage').hide();
        }
        if ($('#txtModalLoaderSuccessMessage').is(':visible')) {
            $('#txtModalLoaderSuccessMessage').hide();
        }

    });






});