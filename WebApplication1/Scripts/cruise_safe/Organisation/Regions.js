﻿function checkPrev(myPrivs) {

    //-------------------- Check Prev ---------------------------//

    if ($('#lblRegionType').text() != 'business') {
        if (myPrivs.indexOf(1002) >= 0) {

        } else {
            location.href = '/Home/menu';
        }
    }

    //-------------------- Check Prev ---------------------------//
}

$(document).ready(function () {



    var http = httpsapi + "/api/Organisation/";
    var https = httpsapi + "/api/";
    // http = httpsapi_local + '/api/Organisation/';

    var how_is_connection = '';//checkNetworkConnection();




    $('.actionAdministrator').trigger('click');
    $('.btnRegionManagement').find('a').addClass('activeMenu');



    //------------------------------------------------------------------ Get all Regions ----------------------------------------------------------------------------------------//

    $('.carousel').carousel({
        interval: false,
    });


    var isSendingg = false;
    var scrollHeight = 0;
    var Regions = Array();

    var users = Array();
    var members = Array();
    var membersArr = Array();

    var membersOffice = Array();
    var membersArrOffice = Array();

    var start_fromm = 0;
    var user_id = null;
    var region_id = null;
    var isLeader = 'member';
    var idOffice = 0;




    var regions = Array();
    getRegionsAll();


    function getRegionsAll() {

        var details = {
            region_client_id: $('#lblClientID').text(),
            //  userId: $('#lblUserID').text(),
        };
        $.getJSON(http + 'getRegionsAll', details, function (data) {
            console.log('getRegionsAll', data);
            if (data) {
                Regions = data;
                displayRegions(data);
            }

            setTimeout(function () {
                $('#myLoader').modal('hide');
            }, 1000);

        });

    }


 





    getUsers(true);


    function displayRegions(data) {

        $('#zeroStatsRegions').hide();
        if (data.length > 0) {
            var str = "";
            for (i = 0; i < data.length; i++) {
                var userStatus = "";
                var statusButton = "";

                if (data[i]['region']['region_status'] == 'active') {
                    userStatus = "label-success badge badge-success";
                }
                else {
                    userStatus = "label-danger badge badge-danger";
                }

                str += '<tr class="btnChooseMember" style="cursor:pointer;" id="b' + i + '"><td>' + (i + 1) + '</td>';
                str += '<td>' + data[i]['region']['REGIONNAME'] + '</td>';
                str += '<td>' + removeNulls(getName(data[i]['region']['region_director_id'])) + '</td>';
                str += '<td>' + removeNulls(data[i]['region']['region_phone']) + '</td>';
                str += '<td>' + removeNulls(data[i]['region']['region_email']) + '</td>';
                str += '<td>' + data[i]['offices'].length + '</td>';
                str += '<td>' + data[i]['towns'].length + '</td>';
                str += '<td><span class="' + userStatus + '">' + data[i]['region']['region_status'] + '</span></td>';
                str += '<td>       <button id="b' + i + '" style="width:100px" type="button" class="btn btn-warning btn-flat btn-xs btnView">View</button>  ' + statusButton + ' </td>';
            }
            $('#tblRegions').html(str);
        }


        if (start_fromm == 0 && data.length == 0) {
            $('#zeroStatsRegions').show();
        }








        //------------ view -------------------//


        $('.btnView').click(function () {

            id = this.id.substring(1);
            $('#btnAddRegion').html('Save Changes');
            members.push(Regions[id]['region']['region_director_id']);
            loadLeaders();
            //--------------- Populate Fields ------------------//
            region_id = Regions[id]['region']['region_id'];

            $('#txtRegionName').val(Regions[id]['region']['REGIONNAME']);
            $('#txtPhone').val(Regions[id]['region']['region_phone']);
            $('#cboPerson').val(Regions[id]['region']['region_director_id']);
            $('#txtEmail').val(Regions[id]['region']['region_email']);
          //  members = Array();
         //   membersArr = Array();

            var str = "";
            for (var i = 0; i < Regions[id]['offices'].length; i++) {
                str += '<tr class="btnChooseMember" style="cursor:pointer;" id="h' + i + '"><td>' + (i + 1) + '</td>';
                str += '<td>' + Regions[id]['offices'][i].region_office_name + '</td>';
                str += '<td>' + removeNulls(getName(Regions[id]['offices'][i].region_office_director_id)) + '</td>';
                str += '<td>' + removeNulls(Regions[id]['offices'][i].region_office_phone) + '</td>';
                str += '<td>' + removeNulls(Regions[id]['offices'][i].region_office_email) + '</td>';
                str += '<td>' + subText(Regions[id]['offices'][i].region_office_address, 20, '...')  + '</td>';
                str += '<td>       <button id="o' + i + '" style="width:100px" type="button" class="btn btn-warning btn-flat btn-xs btnViewRegion pull-right">View</button>  ' + statusButton + ' </td>';
            }
            $('#tblOffices').html(str);
       

            str = "";
            for (var i = 0; i < Regions[id]['towns'].length; i++) {
                str += '<tr class="btnChooseMember" style="cursor:pointer;" id="f' + i + '"><td>' + (i + 1) + '</td>';
                str += '<td>' + Regions[id]['towns'][i].region_town_region_name + '</td>';
                str += '<td>' + Regions[id]['towns'][i].region_town_name + '</td>';
                str += '<td>' + Regions[id]['towns'][i].region_town_status + '</td>';
                str += '<td>' + getDatee(Regions[id]['towns'][i].region_town_timestamp) + '</td></tr>';
            }
            $('#tblTowns').html(str);


            $('.adminRegions-modal-lg').modal('show');




            $('.btnViewRegion').click(function () {

                idOffice = this.id.substring(1);
                $('#btnAddRegion').html('Save Changes');
                membersOffice.push(Regions[id]['offices'][idOffice].region_office_director_id);
                loadLeadersOffice();
                //--------------- Populate Fields ------------------//
                region_id = Regions[id]['region']['region_id'];

                $('.lblOfficeName').html(Regions[id]['offices'][idOffice].region_office_name);
                $('#txtOfficeName').val(Regions[id]['offices'][idOffice].region_office_name);
                $('#txtOfficePhone').val(Regions[id]['offices'][idOffice].region_office_phone);
                $('#cboOfficePerson').val(Regions[id]['offices'][idOffice].region_office_director_id);
                $('#txtOfficeEmail').val(Regions[id]['offices'][idOffice].region_office_email);
                $('#txtOfficeCoordinates').val(Regions[id]['offices'][idOffice].region_office_address_location);
                $('#txtOfficeAddress').val(Regions[id]['offices'][idOffice].region_office_address);

                $('#lblTitle').html(Regions[id]['region']['REGIONNAME']);
                $('.adminOffices-modal-lg').modal('show');
                $('.adminRegions-modal-lg').modal('hide');
            });

        });
        //------------ view -------------------//
    }



    $('#btnAddOffice').click(function () {
        $('.cleanOfficenField').val('');
        $('#lblTitle').html(Regions[id]['region']['REGIONNAME']);
        $('.adminOffices-modal-lg').modal('show');
        $('.adminRegions-modal-lg').modal('hide');
    });



    $('#btnCloseOffice').click(function () {

        $('.adminOffices-modal-lg').modal('hide');
        $('.adminRegions-modal-lg').modal('show');
    });

    $('.btnCloseMap').click(function () {
        $('#viewMapModal').modal('hide');
    });



    var styleControl1 = document.getElementById('style-selector-control1');
    var styleSelector1 = document.getElementById('style-selector1');
    var center = new google.maps.LatLng(-26.2566038, 27.9848897);

    var map;
    $('#btnAMap').click(function () {

        $('#viewMapModal').modal('show');

        $('.map-control').css('position', 'absolute').css('z-index', '1').css('margin', '25px');


        map = new google.maps.Map(document.getElementById('dvMap1'), {
            zoom: 12,
            center: center,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: false,
            options: {
                gestureHandling: 'greedy'
            }
        });


        $.getJSON('/Content/files/geo.json', function (data) {
            mapData = data;
            features = map.data.addGeoJson(mapData);

            //features.forEach(function (feature, index) {
            //    map.data.setStyle(function (feature) {
            //        var bounds = new google.maps.LatLngBounds();
            //        feature.h.h[0].h.forEach(function (coord, index) {
            //            bounds.extend(coord);
            //        });
            //        center = bounds.getCenter();
            //        map.setCenter(center);
            //    });
            //});



        });

        map.data.setStyle({
            icon: '//example.com/path/to/image.png',
            fillColor: 'lime',
            strokeWeight: 1
        });

        map.controls[google.maps.ControlPosition.TOP_LEFT].push(styleControl1);

        map.data.addListener('click', function (event) {
            var bounds = new google.maps.LatLngBounds();
            event.feature.h.h[0].h.forEach(function (coord, index) {
                bounds.extend(coord);
            });
            //  map.setZoom(11);

            map.setCenter(bounds.getCenter());
            map.fitBounds(bounds);

            //   selected(event.latLng, map, null);
        });





        google.maps.event.addListenerOnce(map, 'bounds_changed', function () {
            map.setZoom(map.getZoom() - 1);
        });


        map.data.addListener('mouseover', function (event) {
            map.data.overrideStyle(event.feature, { fillColor: 'red' });
        });
        map.data.addListener('mouseout', function (event) {
            map.data.overrideStyle(event.feature, { fillColor: 'lime' });
        });

        map.data.addListener('mouseover', mouseOverDataItem1);
        map.data.addListener('mouseout', mouseOutOfDataItem1);




        map.setOptions({ styles: styles[styleSelector1.value] });
        map.setTilt(45);
        styleSelector1.addEventListener('change', function () {
            if (styleSelector1.value == 'satellite') {
                map.setMapTypeId(google.maps.MapTypeId.HYBRID);
                map.setOptions({ styles: null });
            }
            else {
                map.setMapTypeId(google.maps.MapTypeId.ROADMAP);
                map.setOptions({ styles: styles[styleSelector1.value] });
            }
        });

       
        for (var i = 0; i < Regions.length; i++) {
         
         
            for (var r = 0;r < Regions[i]['offices'].length; r++) {


                var latLng = new google.maps.LatLng(Regions[i]['offices'][r].region_office_address_latitude
                    , Regions[i]['offices'][r].region_office_address_longtude);

                var div = document.createElement('div');
                div.className = 'ripple-red';

                var zindex = '12';
                selected(latLng, map, null);

                var marker = new RichMarker({
                    position: latLng,
                    map: map,
                    id: i,
                    zIndex: zindex,
                    flat: true,
                    content: div,
                    draggable: false,
                    animation: google.maps.Animation.DROP,
                    title: Regions[i]['offices'][r].region_office_name
                });
                markers.push(marker);
                console.log(marker, map);


            }
        

        }


        function mouseOverDataItem1(mouseEvent) {
            const titleText = mouseEvent.feature.j.REGIONNAME;
            if (titleText) {
                map.getDiv().setAttribute('title', titleText);
            }
        }

        function mouseOutOfDataItem1(mouseEvent) {
            map.getDiv().removeAttribute('title');
        }



        google.maps.event.addListener(map, 'click', function (event) {
            //    selected(event.latLng, map, null);
        });



    });











    var markers = Array();
    var mapData;
    var features = Array();
    var marker;
    var geocoder = new google.maps.Geocoder();
    var infowindow = new google.maps.InfoWindow;
    var mapp;
    var flightPath;
  

    $('.clsMap').click(function () {
        $('.adminMap-modal-lg').modal('show');

    
  
        $('.cleanUserField').val('');
        mapp = new google.maps.Map(document.getElementById('dvMap11'), {
            zoom: 12,
            center: center,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: false,
            options: {
                gestureHandling: 'greedy'
            }
        });
       

        $.getJSON('/Content/files/geo.json', function (data) {
            mapData = data;
            features = mapp.data.addGeoJson(mapData);

            console.log('mmm', features);
            features.forEach(function (feature, index) {
                mapp.data.setStyle(function (feature) {
                    console.log('mmm', feature.j.REGIONNAME);
                    if (feature.j.REGIONNAME != Regions[id]['region']['REGIONNAME']) {
                      
                        return { visible: false, strokeWeight: 1 };
                    } else {
                        var bounds = new google.maps.LatLngBounds();
                        feature.h.h[0].h.forEach(function (coord, index) {
                            bounds.extend(coord);
                        });
                        center = bounds.getCenter();
                        mapp.setCenter(center);
                     //   selected(center, mapp, null);
               
                        return null;  
                    }
                });
            });



        });

        mapp.data.setStyle({
            icon: '//example.com/path/to/image.png',
            fillColor: 'lime',
            strokeWeight: 1
        });

        mapp.controls[google.maps.ControlPosition.TOP_LEFT].push(styleControl1);

        mapp.data.addListener('click', function (event) {
            var bounds = new google.maps.LatLngBounds();
            event.feature.h.h[0].h.forEach(function (coord, index) {
                bounds.extend(coord);
            });
            //  map.setZoom(11);

            mapp.setCenter(bounds.getCenter());
            mapp.fitBounds(bounds);

         //   selected(event.latLng, mapp, null);
        });

  



        google.maps.event.addListenerOnce(mapp, 'bounds_changed', function () {
            mapp.setZoom(mapp.getZoom() - 1);
        });


        mapp.data.addListener('mouseover', function (event) {
            mapp.data.overrideStyle(event.feature, { fillColor: 'red' });
        });
        mapp.data.addListener('mouseout', function (event) {
            mapp.data.overrideStyle(event.feature, { fillColor: 'lime' });
        });

        mapp.data.addListener('mouseover', mouseOverDataItem1);
        mapp.data.addListener('mouseout', mouseOutOfDataItem1);




        mapp.setOptions({ styles: styles[styleSelector1.value] });
        mapp.setTilt(45);
        styleSelector1.addEventListener('change', function () {
            if (styleSelector1.value == 'satellite') {
                mapp.setMapTypeId(google.maps.MapTypeId.HYBRID);
                mapp.setOptions({ styles: null });
            }
            else {
                mapp.setMapTypeId(google.maps.MapTypeId.ROADMAP);
                mapp.setOptions({ styles: styles[styleSelector1.value] });
            }
        });

        var count = 0;
        for (var i = 0; i < Regions[id]['offices'].length; i++) {
            count++;
            if (Regions[id]['offices'][i].region_office_name == Regions[id]['offices'][idOffice].region_office_name) {
                var latLng = new google.maps.LatLng(Regions[id]['offices'][idOffice].region_office_address_latitude
                    , Regions[id]['offices'][idOffice].region_office_address_longtude);

                var div = document.createElement('div');
                div.className = 'ripple-red';

                var zindex = '12';
                selected(latLng, mapp, null);

                var marker = new RichMarker({
                    position: latLng,
                    map: mapp,
                    id: i,
                    zIndex: zindex,
                    flat: true,
                    content: div,
                    draggable: false,
                    animation: google.maps.Animation.DROP,
                    title: Regions[id]['offices'][i].region_office_name
                });
                markers.push(marker);
                console.log(marker, mapp);
            }

        }


        function mouseOverDataItem1(mouseEvent) {
            const titleText = mouseEvent.feature.j.REGIONNAME;
            if (titleText) {
                mapp.getDiv().setAttribute('title', titleText);
            }
        }

        function mouseOutOfDataItem1(mouseEvent) {
            mapp.getDiv().removeAttribute('title');
        }



        google.maps.event.addListener(mapp, 'click', function (event) {
       //    selected(event.latLng, mapp, null);
        });

        $('#btnSearchCoordinates').click(function () {
            var latlngStr = $('#searchCoordinates').val().split(',', 2);
            var latLng = { lat: parseFloat(latlngStr[0]), lng: parseFloat(latlngStr[1]) };
            selected(latLng, mapp, null);
        });

        $('#btnAddress').click(function () {
            selected(null, mapp, $('#searchAddress').val());
        });


        var input = document.getElementById('searchAddress');
        var searchBox = new google.maps.places.SearchBox(input);
        // mapp.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        searchBox.addListener('place_changed', function () {
            var place = searchBox.getPlace();
            if (place.geometry) {
                selected(place.geometry.location, mapp, null);
            }
        });
    });



    function getImageMarker() {
        return {
            url: '/Content/Images/traffic_light.png',
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(30, 30)
        };
    }



    function getMeters(lat1, lon1, lat2, lon2) {
        var R = 6371e3; // metres
        var φ1 = lat1 * Math.PI / 180; // φ, λ in radians
        var φ2 = lat2 * Math.PI / 180;
        var Δφ = (lat2 - lat1) * Math.PI / 180;
        var Δλ = (lon2 - lon1) * Math.PI / 180;

        var a = Math.sin(Δφ / 2) * Math.sin(Δφ / 2) +
            Math.cos(φ1) * Math.cos(φ2) *
            Math.sin(Δλ / 2) * Math.sin(Δλ / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        var d = R * c; // in metres
        return d;
    }



    function selected(latLng, map, address) {
        var data = (latLng) ? { 'latLng': latLng } : { 'address': address };
        console.log(data);
        geocoder.geocode(data, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    getLocation(results[0], results[0].geometry.location);
                    if (marker)
                        marker.setMap(null);

                    marker = new google.maps.Marker({
                     //   map: map,
                        position: results[0].geometry.location,
                        icon: getImageMarker(),
                        draggable: true,
                        // animation: google.maps.Animation.DROP,
                        title: results[0].formatted_address

                    });

                    (function (marker) {
                        google.maps.event.addListener(marker, "dragend", function (e) {

                            geocoder.geocode({
                                'latLng': e.latLng
                            }, function (results, status) {
                                if (status == google.maps.GeocoderStatus.OK) {
                                    if (results[0]) {

                                        getLocation(results[0], e.latLng);
                                    }
                                }
                            });
                        });
                    })(marker);

                    infowindow.setContent(results[0].formatted_address);
                    infowindow.open(map, marker);
                }
            }
        });





    }


    function isWithinPoly(latLng) {
        // console.log(mapData, features[0].h.h[0].h);
        var obj = { region_name: '', region_name_near: '', region_id: 0, region_name_near_dist: 0, region_within: false, region_lat: '', region_lng: '' };
        for (var f = 0; f < features.length; f++) {
            var paths = features[f].h.h[0].h;
            var polygon = new google.maps.Polygon({
                paths: paths,
                id: mapData.features[f].properties.OBJECTID,
                name: mapData.features[f].properties.REGIONNAME,
                visible: false
            });

            var isWithinPolygon = google.maps.geometry.poly.containsLocation(latLng, polygon);
            if (isWithinPolygon) {
                obj.region_id = polygon.id;
                obj.region_name = polygon.name;
                obj.region_name_near = polygon.name;
                obj.region_name_near_dist = 0;
                obj.region_within = true;
                obj.region_lat = '';
                obj.region_lng = '';
                f = features.length;
            }
            else {
                for (var i = 0; i < paths.length; i++) {
                    var dist = getMeters(latLng.lat(), latLng.lng(), paths[i].lat(), paths[i].lng());
                    if (obj.region_name_near_dist == 0 || obj.region_name_near_dist > dist / 1000) {
                        obj.region_name_near_dist = dist / 1000;
                        obj.region_id = polygon.id;
                        obj.region_name = 'Out of Bounds';
                        obj.region_name_near = polygon.name;
                        obj.region_within = false;
                        obj.region_lat = paths[i].lat();
                        obj.region_lng = paths[i].lng();
                    }
                }
            }
        }
        console.log(obj);
        return obj;
    }


    var details, region;
    function getLocation(results, latLng) {

        region = isWithinPoly(latLng);


        console.log(results, latLng);
        console.log("Latitude: " + latLng.lat() + " " + ", longitude: " + latLng.lng());
        $('#txtCoordinates').val(latLng.lat() + ',' + latLng.lng());
        $('#txtTown').val(results.address_components[3].long_name);
        $('#txtSuburb').val(results.address_components[2].long_name);
        $('#txtStreet').val(results.address_components[0].long_name + " " + results.address_components[1].long_name);
        $('#txtRegion').val(region.region_name);

        details = {
            traffic_light_created_by: $('#lblUserID').text(),
            traffic_light_client_id: $('#lblClientID').text(),
            traffic_light_title: results.formatted_address,
            traffic_light_latitude: latLng.lat(),
            traffic_light_longitude: latLng.lng(),
            traffic_light: results.address_components[0].long_name + " " + results.address_components[1].long_name,
            traffic_light_address: (results.address_components[2]) ? results.address_components[2].long_name : "",
            traffic_light_town: (results.address_components[3]) ? results.address_components[3].long_name : "",
            traffic_light_metropolitan: (results.address_components[4]) ? results.address_components[4].long_name : "",
            traffic_light_province: (results.address_components[5]) ? results.address_components[5].long_name : "",
            traffic_light_other: results.formatted_address,
            traffic_light_postal_code: (results.address_components[7]) ? results.address_components[7].long_name : (results.address_components[6]) ? results.address_components[6].long_name : "",

            traffic_light_region_name: region.region_name,
            traffic_light_region_name_near: region.region_name_near,
            traffic_light_region_name_near_distance_km: region.region_name_near_dist,
            traffic_light_region_id: region.region_id
        };

        if (flightPath)
            flightPath.setMap(null);



        if (region.region_name == 'Out of Bounds') {
            var flightPlanCoordinates = [
                { lat: parseFloat(details.traffic_light_latitude), lng: parseFloat(details.traffic_light_longitude) }];
            var coord = {}; var title = '';
            if (region.region_name_near_dist > 2) {


                msg = parseInt(region.region_name_near_dist) + ' Km away from ' + region.region_name_near + ' (Out of Bounds),  The distance between traffic light and region must be less than 2 Km.';
                title = parseInt(region.region_name_near_dist) + ' Km';
                coord = { lat: parseFloat(region.region_lat), lng: parseFloat(region.region_lng) };
                flightPlanCoordinates.push(coord);
                console.log(flightPlanCoordinates);
                //  showErrorMessageClose(msg);
                flightPath = new google.maps.Polyline({
                    path: flightPlanCoordinates,
                    geodesic: true, strokeColor: '#FF0000',
                    strokeOpacity: 1.0, strokeWeight: 4, title: title
                });
                flightPath.setMap(mapp);
                var latLng = new google.maps.LatLng(coord.lat, coord.lng);
                var latLng1 = new google.maps.LatLng(flightPlanCoordinates[0].lat, flightPlanCoordinates[0].lng);
                var bounds = new google.maps.LatLngBounds();
                bounds.extend(latLng);
                bounds.extend(latLng1);
                mapp.setCenter(bounds.getCenter());
                mapp.fitBounds(bounds, { top: 100, right: 100, left: 100 });
                marker.title = msg;

            }
        }



    }
















    function getUsers(loadMore) {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }


        $('#myLoader').modal('show');
        var details = {
            clientid: $('#lblClientID').text()
        };

        $.getJSON('/admin/getUsers', details, function (data) {
            isSendingg = false;
            users = data;
            console.log(users);

            getRegionsAll();
            if (loadMore) {
               
                getRoles();
            }
            else {
                listUsersAll(members);
            }

        });
    }




    function getName(user_id) {
        var name = '';
        for (var l = 0; l < users.length; l++) {
            if (users[l]['user_id'] == user_id)
                name = users[l]['user_title'] + ' ' + users[l]['user_firstname'] + ' ' + users[l]['user_surname'];
        }
        return name;
    }

    function getPhone(user_id) {
        var name = '';
        for (var p = 0; p < users.length; p++) {
            if (users[p]['user_id'] == user_id)
                name = users[p]['user_cellphone'];
        }
        return name;
    }
    function getEmail(user_id) {
        var name = '';
        for (var e = 0; e < users.length; e++) {
            if (users[e]['user_id'] == user_id)
                name = users[e]['user_email'];
        }
        return name;
    }

    $('#cboType').change(function () {
        console.log($(this).val());
        if ($(this).val() == '0') {
            $('#btnDirectord').attr('data-id', '');
            $('#btnDirectord').html('Add User').css('border-color', 'rgba(42, 50, 60, 0.2)');
            $('#btnAddDepartmentd').html('Save');
            $('.cleanDepartmentField').val('');
            $('#btnDirectord').attr('data-id', '').html('Add User');
            $('.adminDepartments-modal-lg').modal('show');
            $(this).val('').trigger('change');
        }
    });





    $('#btnDirectord').click(function () {
        isLeader = 'director';
        listUsersAll(Array());
    });

    $(document).on('click', ".btnWidget", function () {
        $(document).find(".btnWidget").removeClass('active');
        $(this).addClass('active');
     
    });


    $('#cboPerson').change(function () {
        console.log($(this).val());
        if ($(this).val() == '0') {
            $('#btnAddNewUser').trigger('click');
            $(this).val('').trigger('change');
        }
        else if ($(this).val() == '00') {
            $(this).val('').trigger('change');
            isLeader = 'leader';
            listUsersAll(members);
        }
        //else {
        //    loadMembers();
        //}
        
    });


    $('#cboOfficePerson').change(function () {
        console.log($(this).val());
        if ($(this).val() == '0') {
            $('#btnAddNewUser').trigger('click');
            $(this).val('').trigger('change');
        }
        else if ($(this).val() == '00') {
            $(this).val('').trigger('change');
            isLeader = 'directorOffice';
            listUsersAll(membersOffice);
        }
        //else {
        //    loadMembers();
        //}

    });



    function loadMembers() {
        var str = '';
        membersArr = Array();

        for (var m = 0; m < members.length; m++) {
            var type = (parseInt(members[m]) == parseInt($('#cboPerson').val())) ? '<b><i class="fas fa-dot-circle"></i> Contact Person</b>' : 'Region Member';
            str += '<div class="col-sm-6 col-lg-6"><div class="panel text-center btnWidget2" id = "usee' + members[m] +'">'+
                '<div class="panel-body p-t-10" style="padding: 20px;"><h4 class="m-t-0 m-b-15">' +
                '<i class="fas fa-user text-primary m-r-10"></i><b>' + getName(members[m]) +'</b></h4>'+
                '<p class="text-muted m-b-0 m-t-20"> ' + type + '</p></div></div></div>';
            membersArr.push({
                member_user_id: members[m],
                member_type: type
            });
        }
        $('#listMembers').html(str);
        $('#btnRemoveMember').hide('slow');
        $('#btnRegionLeader').hide('slow');
    }

    function loadLeaders() {
        $('#cboPerson').html('<option value="">-- Choose Contact Person --</option>');
        for (var m = 0; m < members.length; m++) {
            $('#cboPerson').append('<option value="' + members[m] + '">' + getName(members[m]) + '</option>');
        }
        $('#cboPerson').append('<option value="00" style="font-weight:bold;color:green">Select From Users</option>');
        $('#cboPerson').append('<option value="0" style="font-weight:bold;color:green">Add New User</option>');
    }






    function loadLeadersOffice() {
        var val = '';
        $('#cboOfficePerson').html('<option value="">-- Choose Contact Person --</option>');
        for (var m = 0; m < membersOffice.length; m++) {
            $('#cboOfficePerson').append('<option selected="selected" value="' + membersOffice[m] + '">' + getName(membersOffice[m]) + '</option>');
            val = membersOffice[m];
        }
        $('#cboOfficePerson').append('<option value="00" style="font-weight:bold;color:green">Select From Users</option>');
        $('#cboOfficePerson').append('<option value="0" style="font-weight:bold;color:green">Add New User</option>');
        $('#cboPersonOffice').val(val).trigger('change');
    }






    function listUsersAll(notArr) {
        var str = '';
        $("#txtSearchh").val('');
        for (var m = 0; m < users.length; m++) {
            if (notArr.indexOf(parseInt(users[m].user_id)) < 0){
                var name = users[m]['user_title'] + ' ' + users[m]['user_firstname'] + ' ' + users[m]['user_surname'];
                str += '<div class="col-sm-6 col-lg-6 memberr"><div class="panel text-center btnWidget2 userList" id = "user' + users[m]['user_id']+'">' +
                    '<div class="panel-body p-t-10" style="padding: 20px;"><h4 class="m-t-0 m-b-15">' +
                    '<i class="fas fa-user text-primary m-r-10"></i><b>' + name + '</b></h4>' +
                    '<p class="text-muted m-b-0 m-t-20"> ' + users[m].user_email + '</p></div></div></div>';
            }
        }
        $('#listUsersAll').html(str);
        $('#btnPickOne').hide();
        $('.usersmodallg').modal('show');

        $("#txtSearchh").on("keyup", function () {
            var value = $(this).val().toLowerCase();
            $("#listUsersAll div.memberr").filter(function () {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
            });
            $('#rows').html($("#listUsersAll div.memberr:visible").length);
        });

        $('#btnAddNewUser').click(function () {
            user_id = null;
            $('.clsPassword').show();
            $('.clsPasswords').removeAttr('disabled').addClass('requiredUser');
            $('#btnAddUser').html('Save');
            $('.cleanUsertField').val('');
            $('#txtPassword').val('');
            $('.adminUsers-modal-lg').modal('show');
        });

        $('#btnPickOne').on('click', function () {
            if (isLeader == 'director') {
                $('#btnDirectord').attr('data-id', $(this).attr('data-id'));
                $('#btnDirectord').html($(this).attr('data-name')).css('border-color', 'rgba(42, 50, 60, 0.2)');
            }
            else if (isLeader == 'directorOffice'){
                membersOffice = Array();
                membersOffice.push(parseInt($(this).attr('data-id')));
                loadLeadersOffice();
            }
            else {
               // if (members.indexOf(parseInt($(this).attr('data-id'))) < 0) {
                members = Array();
                    members.push(parseInt($(this).attr('data-id')));
              //  }
                var leadVal = $('#cboPerson').val();
                loadLeaders();
                if (isLeader == 'leader') {
                    leadVal = parseInt($(this).attr('data-id'));
                }
                $('#cboPerson').val(leadVal).trigger('change');
                loadMembers();
            }
            $('.usersmodallg').modal('hide');
        });


    }




    $('#btnAddMember').on('click', function () {
        isLeader = 'member';
        listUsersAll(members);
    });



    function getRoles() {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        roles = Array();

        var details = {
            clientid: $('#lblClientID').text(),
            //  userId: $('#lblUserID').text(),
        };
        $.getJSON('/admin/getUserRoles', details, function (data) {
            //console.log('Roles', data);
            if (data) {
                roles = data['roles'];
                $('#cboRole').html('<option value="">-- Choose Role --</option>');
                for (var i = 0; i < data['roles'].length; i++) {
                    var htmlcontent = '<option value="' + data['roles'][i].role_id + '">' + data['roles'][i].role_name + '</option>';
                    $('#cboRole').append(htmlcontent);
                }
            }
        });

    }


    $('#btnAddUser').on('click', function () {
        saveUser(user_id);
    });



    $('#btnRemoveMember').on('click', function () {
        members.remove(parseInt($(this).attr('data-id')));
        if (parseInt($(this).attr('data-id')) == parseInt($('#cboPerson').val()))
            $('#cboPerson').val('');
        var leadVal = $('#cboPerson').val();
        loadLeaders();
        $('#cboPerson').val(leadVal).trigger('change');

        loadMembers();
    });


    $('#btnRegionLeader').on('click', function () {
        $('#cboPerson').val(parseInt($(this).attr('data-id'))).trigger('change');
        loadMembers();
    });



    $(document).on('click', ".btnWidget2", function () {
        $(document).find(".btnWidget2").removeClass('active');
        $(this).addClass('active');
        if ($(this).hasClass('userList')) {
            $('#btnPickOne').show('slow').html('Select ' + getName(this.id.substring(4))).attr('data-id', this.id.substring(4));
            $('#btnPickOne').attr('data-name', getName(this.id.substring(4)));
        }
        else {
            $('#btnRemoveMember').show('slow').html('Remove ' + getName(this.id.substring(4))).attr('data-id', this.id.substring(4));
            if (parseInt(this.id.substring(4)) != parseInt($('#cboPerson').val()))
                $('#btnRegionLeader').show('slow').attr('data-id', this.id.substring(4));
            else
                $('#btnRegionLeader').hide('slow').attr('data-id', '');
        }
    });



    function saveUser(user_id) {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        if (vaidateFields('requiredUser'))
            return;
        if (user_id == null) {
            if ($('#txtPassword').val() != $('#txtConfirmPassword').val()) {
                showConnectionMessage("Passwords do not match");
                return;
            }
        }
        isSendingg = true;

        $('.myLoader').modal('show');

        var details = {
            user_id: user_id,
            user_client_id: $('#lblClientID').text(),
            user_id_number: $('#txtIDNumber').val(),
            user_title: $('#cboTitle').val(),
            user_firstname: $('#txtFirstName').val(),
            user_surname: $('#txtLastName').val(),
            user_gender: $('#cboGender').val(),
            user_cellphone: $('#txtCellNumber').val(),
            user_email: $('#txtEmailAddress').val(),
            user_role_id: $('#cboRole').val(),
            user_password: $('#txtPassword').val(),
            user_saved_by: $('#lblUserID').text(),
            user_updated_by: $('#lblUserID').text()

        };
        console.log(details);
        $.post(https + 'admin/addUser', details, function (data) {
            console.log(data);
            isSendingg = false;
            if (data.indexOf('success') >= 0) {
                showSuccessMessageClose(data);
                getUsers(false);
                $('#myLoader').modal('hide');

                $('.adminUsers-modal-lg').modal('hide');
            }
            else {


                $('#myLoader').modal('hide');

                showErrorMessage(data);
            }
        });
    }







    //----------------------------- Search Regions ----------------------------------------//

    $(".search-bar").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#tblRegions tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

    //----------------------------- Search Regions ----------------------------------------//






    //------------------------------------------------------------------ Get all Regions ----------------------------------------------------------------------------------------//












    //------------------------ Add new Regions ---------------------------//







    $('#btnAddNew').click(function () {
        region_id = null;
        members = Array();
        membersArr = Array();
        loadLeaders();
        $('#btnAddRegion').html('Save');
        $('.cleanRegionField').val('');
        $('#listMembers').html('');
        $('#tblOffices').html('');
        $('#tblTowns').html('');
        $('.adminRegions-modal-lg').modal('show');

    });






    $('#btnAddRegion').on('click', function () {
        saveRegion(region_id);
    });






    function saveRegion(region_id) {
       
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

 
        if (vaidateFields('requiredRegion'))
            return;

        isSendingg = true;
        console.log(region_id);


        $('.myLoader').modal('show');

        var details = {
            cruise_safe_organo_region: {
                region_id: region_id,
                region_client_id: $('#lblClientID').text(),
                region_type: $('#cboType').val(),
                region_status: $('#cboStatust').val(),
                region_contact_person: $('#cboPerson').val(),
                region_company_name: $('#txtNamet').val(),
                region_created_by: $('#lblUserID').text(),
                region_updated_by: $('#lblUserID').text()
            },
            members: membersArr
        };

        setTimeout(function () { $('.myLoader').modal('hide'); $('.adminRegions-modal-lg').modal('hide');}, 1000);


        return;
        console.log(details);
        $.post(http + 'save_region', details, function (data) {
            console.log(data);
            isSendingg = false;
            if (data.indexOf('success') >= 0) {
                showSuccessMessage(data);
                getRegions();
                $('#myLoader').modal('hide');
      
                $('.adminRegions-modal-lg').modal('hide');
            }
            else {

       
                    $('#myLoader').modal('hide');
               
                showErrorMessage(data);
            }
        });
    }




    //------------------------ Add new Regions ---------------------------//
















    //---------------------- activate user --------------------------//


    $('#btnConfirmActive').on('click', function () {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        $('#confirmModal').modal('hide');
        $('.myLoader').modal('show');

        var details = {
            userId: $('#lblRegionID').text(),
            clientid: $('#lblClientID').text(),
            system_user_id: Regions[id]['user_id'],
            fullname: Regions[id]['user_firstname'] + ' ' + Regions[id]['user_surname']
        };

        $.post('/admin/activate', details, function (data) {
          
            if (data.message.indexOf('Error')<0) {
                getRegions();
                    $('#myLoader').modal('hide');
              

                showSuccessMessageClose(data.message);
                    $('#successModal').modal('hide');
           
            }
            else {
                    $('#myLoader').modal('hide');
                showErrorMessage(data.message);
            }
        });
    });


    //---------------------- activate user ends --------------------------//








    //---------------------- deactivate user --------------------------//


    $('#btnConfirm').on('click', function () {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        $('#confirmModal').modal('hide');
        $('.myLoader').modal('show');

        var details = {
            userId: $('#lblRegionID').text(),
            clientid: $('#lblClientID').text(),
            system_user_id: Regions[id]['user_id'],
            fullname: Regions[id]['user_firstname'] + ' ' + Regions[id]['user_surname']
        };
        //console.log(details);
        //return;
        $.post('/admin/deactivate', details, function (data) {
            if (data.message.indexOf('Error') < 0) {
                getRegions();
                    $('#myLoader').modal('hide');

                showSuccessMessageClose(data.message);
                    $('#successModal').modal('hide');
            }
            else {
                    $('#myLoader').modal('hide');
                showErrorMessage(data.message);
            }
        });
    });


    //---------------------- deactivate user ends --------------------------//










    $('#btnAddDepartmentd').on('click', function () {
        saveDepartment();
    });




    function saveDepartment() {

        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }


        if (vaidateFields('requiredDepartment'))
            return;

        if ($('#btnDirectord').attr('data-id').length == 0) {
            $('#btnDirectord').css('border-color', 'red');
            return;
        }

        isSendingg = true;
        console.log();


        $('.myLoader').modal('show');

        var details = {
            department_id: null,
            department_client_id: $('#lblClientID').text(),
            department_abbr: $('#txtShortNamed').val(),
            department_status: $('#cboStatusd').val(),
            department_director: $('#btnDirectord').attr('data-id'),
            department_phone: $('#txtPhoned').val(),
            department_email: $('#txtEmaild').val(),
            department_name: $('#txtNamed').val(),
            department_created_by: $('#lblUserID').text(),
            department_updated_by: $('#lblUserID').text()
        };
        console.log(details);
        $.post(http + 'save_department', details, function (data) {
            console.log(data);
            isSendingg = false;
            if (data.indexOf('success') >= 0) {
                showSuccessMessageClose(data);
            
                $('#myLoader').modal('hide');

                $('.adminDepartments-modal-lg').modal('hide');
            }
            else {


                $('#myLoader').modal('hide');

                showErrorMessage(data);
            }
        });
    }




























































    function vaidateFields(clas) {
        $('.' + clas).css('border-color', 'silver');
        var input;
        $('.' + clas).map(function () {
            if (!$(this).val() || $(this).css('border-color') == 'red') {
                $(this).css('border-color', 'red');
                $(this).focus();
                input = $(this);
            }
        }).get();

        if (input) {
            input.focus();
            return true;
        }
        else
            return false;
    }

    function validateEmail(sEmail, msg) {
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(sEmail)) {
            return true;
        }
        else {
            //var msg = "Please enter valid email address.";
            checkEmptyFields(msg);
            return false;
        }
    }

    function SuccessMessage(msg) {
        $('#myPleaseWait').modal('show');
        $('#txtModalLoaderAlertMessage').hide;
        $('#txtModalLoaderSuccessMessage').show();
        $('#txtModalLoaderSuccessMessage b').html(msg);

        setTimeout(function () {
            $('#myPleaseWait').hide();
            $('#txtModalLoaderSuccessMessage').hide();
            $('#myPleaseWait').modal('hide');
        }, 3000);
    }

    function checkEmptyFields(msg) {

        $('#myPleaseWait').modal('show');
        $('#txtModalLoaderAlertMessage').show();
        $('#txtModalLoaderAlertMessage b').text(msg);

        setTimeout(function () {
            $('#myPleaseWait').hide();
            $('#myPleaseWait').modal('hide');
            $('#txtModalLoaderAlertMessage').hide();
        }, 3000);
    }

    Array.prototype.remove = function () {
        var what, a = arguments, L = a.length, ax;
        while (L && this.length) {
            what = a[--L];
            while ((ax = this.indexOf(what)) !== -1) {
                this.splice(ax, 1);
            }
        }
        return this;
    };



    /***************************************************************
                        Validations Ends
    ***************************************************************/



    //Removing  Messages CoOperation Services

    $(document).on('click', function () {
        if ($('#txtModalLoaderAlertMessage').is(':visible')) {
            $('#txtModalLoaderAlertMessage').hide();
        }
        if ($('#txtModalLoaderSuccessMessage').is(':visible')) {
            $('#txtModalLoaderSuccessMessage').hide();
        }

    });






});