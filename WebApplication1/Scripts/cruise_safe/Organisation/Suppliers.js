﻿function checkPrev(myPrivs) {

    //-------------------- Check Prev ---------------------------//

    if ($('#lblSupplierType').text() != 'business') {
        if (myPrivs.indexOf(1002) >= 0) {

        } else {
            location.href = '/Home/menu';
        }
    }

    //-------------------- Check Prev ---------------------------//
}

$(document).ready(function () {



    var http = httpsapi + "/api/Organisation/";
    var https = httpsapi + "/api/";
    // http = httpsapi_local + '/api/Organisation/';

    var how_is_connection = '';//checkNetworkConnection();




    $('.actionAdministrator').trigger('click');
    $('.btnSupplierManagement').find('a').addClass('activeMenu');



    //------------------------------------------------------------------ Get all Suppliers ----------------------------------------------------------------------------------------//




    var isSendingg = false;
    var scrollHeight = 0;
    var Suppliers = Array();

    var users = Array();
    var members = Array();
    var membersArr = Array();

    var start_fromm = 0;
    var user_id = null;
    var supplier_id = null;
    var isLeader = 'member';

    function getSuppliers() {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        $('#myLoader').modal('show');

        var details = {
            client_id: $('#lblClientID').text()
        };
        console.log(details);
        $.getJSON(http +'get_suppliers', details, function (data) {
            console.log('getSuppliers', data);
            Suppliers = data;
            displaySuppliers(data);
         

                $('#myLoader').modal('hide');
        });
    }




    getUsers(true);


    function displaySuppliers(data) {

        $('#zeroStatsSuppliers').hide();
        if (data.length > 0) {
            var str = "";
            for (i = 0; i < data.length; i++) {
                var userStatus = "";
                var statusButton = "";

                if (data[i]['cruise_safe_organo_supplier']['supplier_status'] == 'active') {
                    userStatus = "label-success badge badge-success";
                    statusButton = '<button id="v' + i + '"  style="width:90px" type="button" class="btn btn-danger removeDept btn-flat btn-xs"> Deactivate</button>';
                }
                else {
                    userStatus = "label-danger badge badge-danger";
                    statusButton = '<button id="t' + i + '"  style="width:90px" type="button" class="btn btn-success activateDept btn-flat btn-xs"> Activate</button>';
                }

                str += '<tr class="btnChooseMember" style="cursor:pointer;" id="b' + i + '"><td>' + (i + 1) + '</td>';
                str += '<td>' + data[i]['cruise_safe_organo_supplier']['supplier_company_name'] + '</td>';
                str += '<td>' + getName(data[i]['cruise_safe_organo_supplier']['supplier_contact_person']) + '</td>';
                str += '<td>' + getPhone(data[i]['cruise_safe_organo_supplier']['supplier_contact_person']) + '</td>';
                str += '<td>' + getEmail(data[i]['cruise_safe_organo_supplier']['supplier_contact_person']) + '</td>';
                str += '<td>' + data[i]['cruise_safe_organo_supplier']['supplier_type'] + '</td>';
                str += '<td>' + data[i]['members'].length + '</td>';
                str += '<td><span class="' + userStatus + '">' + data[i]['cruise_safe_organo_supplier']['supplier_status'] + '</span></td>';
                str += '<td>       <button id="b' + i + '" style="width:90px" type="button" class="btn btn-warning btn-flat btn-xs btnView">View</button>  ' + statusButton + ' </td>';
            }
            $('#tblSuppliers').html(str);
        }


        if (start_fromm == 0 && data.length == 0) {
            $('#zeroStatsSuppliers').show();
        }








        //------------ view -------------------//


        $('.btnView').click(function () {

            id = this.id.substring(1);
            $('#btnAddSupplier').html('Save Changes');

            //--------------- Populate Fields ------------------//
            supplier_id = Suppliers[id]['cruise_safe_organo_supplier']['supplier_id'];

            $('#cboType').val(Suppliers[id]['cruise_safe_organo_supplier']['supplier_type']);
            $('#cboStatust').val(Suppliers[id]['cruise_safe_organo_supplier']['supplier_status']);
            $('#cboPerson').val(Suppliers[id]['cruise_safe_organo_supplier']['supplier_contact_person']);
            $('#txtNamet').val(Suppliers[id]['cruise_safe_organo_supplier']['supplier_company_name']);
            members = Array();
            membersArr = Array();

            for (var d = 0; d < data[id]['members'].length; d++) {
                members.push(Suppliers[id]['members'][d].member_user_id);
            }
            loadLeaders();
            $('#cboPerson').val(data[id]['cruise_safe_organo_supplier']['supplier_contact_person']).trigger('change');

           

            //--------------- Populate Fields ------------------//

            $('.adminSuppliers-modal-lg').modal('show');
        });

        //------------ view -------------------//




        //------------ activate -------------------//

        $('.activateDept').click(function () {
            id = this.id.substring(1);

            $('#btnConfirm').hide();
            $('#btnConfirmActive').show();


            $('#confirmModal').modal('show');
            $('#lblConfirmMessage').html("Are you sure, you want to activate " + Suppliers[id]['cruise_safe_organo_supplier']['supplier_company_name'] + ' Supplier');
        });

        //------------ activate -------------------//






        //------------ deactivate  -------------------//

        $('.removeDept').click(function () {
            id = this.id.substring(1);

            $('#btnConfirm').show();
            $('#btnConfirmActive').hide();


            $('#confirmModal').modal('show');
            $('#lblConfirmMessage').html("Are you sure, you want to remove " + Suppliers[id]['cruise_safe_organo_supplier']['supplier_company_name'] + ' Supplier');
        });

        //------------ deactivate   -------------------//






    }




    function getUsers(loadMore) {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }


        var details = {
            clientid: $('#lblClientID').text()
        };

        $.getJSON('/admin/getUsers', details, function (data) {
            isSendingg = false;
            users = data;
            console.log(users);

           
            if (loadMore) {
                getSuppliers();
                getRoles();
            }
            else {
                listUsersAll(members);
            }

        });
    }




    function getName(user_id) {
        var name = '';
        for (var l = 0; l < users.length; l++) {
            if (users[l]['user_id'] == user_id)
                name = users[l]['user_title'] + ' ' + users[l]['user_firstname'] + ' ' + users[l]['user_surname'];
        }
        return name;
    }

    function getPhone(user_id) {
        var name = '';
        for (var p = 0; p < users.length; p++) {
            if (users[p]['user_id'] == user_id)
                name = users[p]['user_cellphone'];
        }
        return name;
    }
    function getEmail(user_id) {
        var name = '';
        for (var e = 0; e < users.length; e++) {
            if (users[e]['user_id'] == user_id)
                name = users[e]['user_email'];
        }
        return name;
    }

    $('#cboType').change(function () {
        console.log($(this).val());
        if ($(this).val() == '0') {
            $('#btnDirectord').attr('data-id', '');
            $('#btnDirectord').html('Add User').css('border-color', 'rgba(42, 50, 60, 0.2)');
            $('#btnAddDepartmentd').html('Save');
            $('.cleanDepartmentField').val('');
            $('#btnDirectord').attr('data-id', '').html('Add User');
            $('.adminDepartments-modal-lg').modal('show');
            $(this).val('').trigger('change');
        }
    });





    $('#btnDirectord').click(function () {
        isLeader = 'director';
        listUsersAll(Array());
    });




    $('#cboPerson').change(function () {
        console.log($(this).val());
        if ($(this).val() == '0') {
            $('#btnAddNewUser').trigger('click');
            $(this).val('').trigger('change');
        }
        else if ($(this).val() == '00') {
            $(this).val('').trigger('change');
            isLeader = 'leader';
            listUsersAll(members);
        }
        else {
            loadMembers();
        }
        
    });



    function loadMembers() {
        var str = '';
        membersArr = Array();

        for (var m = 0; m < members.length; m++) {
            var type = (parseInt(members[m]) == parseInt($('#cboPerson').val())) ? '<b><i class="fas fa-dot-circle"></i> Contact Person</b>' : 'Supplier Member';
            str += '<div class="col-sm-6 col-lg-6"><div class="panel text-center btnWidget2" id = "usee' + members[m] +'">'+
                '<div class="panel-body p-t-10" style="padding: 20px;"><h4 class="m-t-0 m-b-15">' +
                '<i class="fas fa-user text-primary m-r-10"></i><b>' + getName(members[m]) +'</b></h4>'+
                '<p class="text-muted m-b-0 m-t-20"> ' + type + '</p></div></div></div>';
            membersArr.push({
                member_user_id: members[m],
                member_type: type
            });
        }
        $('#listMembers').html(str);
        $('#btnRemoveMember').hide('slow');
        $('#btnSupplierLeader').hide('slow');
    }

    function loadLeaders() {
        $('#cboPerson').html('<option value="">-- Choose Contact Person --</option>');
        for (var m = 0; m < members.length; m++) {
            $('#cboPerson').append('<option value="' + members[m] + '">' + getName(members[m]) + '</option>');
        }
        $('#cboPerson').append('<option value="00" style="font-weight:bold;color:green">Select From Users</option>');
        $('#cboPerson').append('<option value="0" style="font-weight:bold;color:green">Add New User</option>');
    }



    function listUsersAll(notArr) {
        var str = '';
        $("#txtSearchh").val('');
        for (var m = 0; m < users.length; m++) {
            if (notArr.indexOf(parseInt(users[m].user_id)) < 0){
                var name = users[m]['user_title'] + ' ' + users[m]['user_firstname'] + ' ' + users[m]['user_surname'];
                str += '<div class="col-sm-6 col-lg-6 memberr"><div class="panel text-center btnWidget2 userList" id = "user' + users[m]['user_id']+'">' +
                    '<div class="panel-body p-t-10" style="padding: 20px;"><h4 class="m-t-0 m-b-15">' +
                    '<i class="fas fa-user text-primary m-r-10"></i><b>' + name + '</b></h4>' +
                    '<p class="text-muted m-b-0 m-t-20"> ' + users[m].user_email + '</p></div></div></div>';
            }
        }
        $('#listUsersAll').html(str);
        $('#btnPickOne').hide();
        $('.usersmodallg').modal('show');

        $("#txtSearchh").on("keyup", function () {
            var value = $(this).val().toLowerCase();
            $("#listUsersAll div.memberr").filter(function () {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
            });
            $('#rows').html($("#listUsersAll div.memberr:visible").length);
        });

        $('#btnAddNewUser').click(function () {
            user_id = null;
            $('.clsPassword').show();
            $('.clsPasswords').removeAttr('disabled').addClass('requiredUser');
            $('#btnAddUser').html('Save');
            $('.cleanUsertField').val('');
            $('#txtPassword').val('');
            $('.adminUsers-modal-lg').modal('show');
        });

        $('#btnPickOne').on('click', function () {
            if (isLeader == 'director') {
                $('#btnDirectord').attr('data-id', $(this).attr('data-id'));
                $('#btnDirectord').html($(this).attr('data-name')).css('border-color', 'rgba(42, 50, 60, 0.2)');
            }
            else {
                if (members.indexOf(parseInt($(this).attr('data-id'))) < 0) {
                    members.push(parseInt($(this).attr('data-id')));
                }
                var leadVal = $('#cboPerson').val();
                loadLeaders();
                if (isLeader == 'leader') {
                    leadVal = parseInt($(this).attr('data-id'));
                }
                $('#cboPerson').val(leadVal).trigger('change');
                loadMembers();
            }
            $('.usersmodallg').modal('hide');
        });


    }




    $('#btnAddMember').on('click', function () {
        isLeader = 'member';
        listUsersAll(members);
    });



    function getRoles() {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        roles = Array();

        var details = {
            clientid: $('#lblClientID').text(),
            //  userId: $('#lblUserID').text(),
        };
        $.getJSON('/admin/getUserRoles', details, function (data) {
            //console.log('Roles', data);
            if (data) {
                roles = data['roles'];
                $('#cboRole').html('<option value="">-- Choose Role --</option>');
                for (var i = 0; i < data['roles'].length; i++) {
                    var htmlcontent = '<option value="' + data['roles'][i].role_id + '">' + data['roles'][i].role_name + '</option>';
                    $('#cboRole').append(htmlcontent);
                }
            }
        });

    }


    $('#btnAddUser').on('click', function () {
        saveUser(user_id);
    });



    $('#btnRemoveMember').on('click', function () {
        members.remove(parseInt($(this).attr('data-id')));
        if (parseInt($(this).attr('data-id')) == parseInt($('#cboPerson').val()))
            $('#cboPerson').val('');
        var leadVal = $('#cboPerson').val();
        loadLeaders();
        $('#cboPerson').val(leadVal).trigger('change');

        loadMembers();
    });


    $('#btnSupplierLeader').on('click', function () {
        $('#cboPerson').val(parseInt($(this).attr('data-id'))).trigger('change');
        loadMembers();
    });



    $(document).on('click', ".btnWidget2", function () {
        $(document).find(".btnWidget2").removeClass('active');
        $(this).addClass('active');
        if ($(this).hasClass('userList')) {
            $('#btnPickOne').show('slow').html('Select ' + getName(this.id.substring(4))).attr('data-id', this.id.substring(4));
            $('#btnPickOne').attr('data-name', getName(this.id.substring(4)));
        }
        else {
            $('#btnRemoveMember').show('slow').html('Remove ' + getName(this.id.substring(4))).attr('data-id', this.id.substring(4));
            if (parseInt(this.id.substring(4)) != parseInt($('#cboPerson').val()))
                $('#btnSupplierLeader').show('slow').attr('data-id', this.id.substring(4));
            else
                $('#btnSupplierLeader').hide('slow').attr('data-id', '');
        }
    });



    function saveUser(user_id) {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        if (vaidateFields('requiredUser'))
            return;
        if (user_id == null) {
            if ($('#txtPassword').val() != $('#txtConfirmPassword').val()) {
                showConnectionMessage("Passwords do not match");
                return;
            }
        }
        isSendingg = true;

        $('.myLoader').modal('show');

        var details = {
            user_id: user_id,
            user_client_id: $('#lblClientID').text(),
            user_id_number: $('#txtIDNumber').val(),
            user_title: $('#cboTitle').val(),
            user_firstname: $('#txtFirstName').val(),
            user_surname: $('#txtLastName').val(),
            user_gender: $('#cboGender').val(),
            user_cellphone: $('#txtCellNumber').val(),
            user_email: $('#txtEmailAddress').val(),
            user_role_id: $('#cboRole').val(),
            user_password: $('#txtPassword').val(),
            user_saved_by: $('#lblUserID').text(),
            user_updated_by: $('#lblUserID').text()

        };
        console.log(details);
        $.post(https + 'admin/addUser', details, function (data) {
            console.log(data);
            isSendingg = false;
            if (data.indexOf('success') >= 0) {
                showSuccessMessageClose(data);
                getUsers(false);
                $('#myLoader').modal('hide');

                $('.adminUsers-modal-lg').modal('hide');
            }
            else {


                $('#myLoader').modal('hide');

                showErrorMessage(data);
            }
        });
    }







    //----------------------------- Search Suppliers ----------------------------------------//

    $(".search-bar").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#tblSuppliers tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

    //----------------------------- Search Suppliers ----------------------------------------//






    //------------------------------------------------------------------ Get all Suppliers ----------------------------------------------------------------------------------------//












    //------------------------ Add new Suppliers ---------------------------//







    $('#btnAddNew').click(function () {
        supplier_id = null;
        members = Array();
        membersArr = Array();
        loadLeaders();
        $('#btnAddSupplier').html('Save');
        $('.cleanSupplierField').val('');
        $('#listMembers').html('');

        $('.adminSuppliers-modal-lg').modal('show');

    });











    $('#btnAddSupplier').on('click', function () {
        saveSupplier(supplier_id);
    });






    function saveSupplier(supplier_id) {
       
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

 
        if (vaidateFields('requiredSupplier'))
            return;

        isSendingg = true;
        console.log(supplier_id);


        $('.myLoader').modal('show');

        var details = {
            cruise_safe_organo_supplier: {
                supplier_id: supplier_id,
                supplier_client_id: $('#lblClientID').text(),
                supplier_type: $('#cboType').val(),
                supplier_status: $('#cboStatust').val(),
                supplier_contact_person: $('#cboPerson').val(),
                supplier_company_name: $('#txtNamet').val(),
                supplier_created_by: $('#lblUserID').text(),
                supplier_updated_by: $('#lblUserID').text()
            },
            members: membersArr
        };



        console.log(details);
        $.post(http + 'save_supplier', details, function (data) {
            console.log(data);
            isSendingg = false;
            if (data.indexOf('success') >= 0) {
                showSuccessMessage(data);
                getSuppliers();
                $('#myLoader').modal('hide');
      
                $('.adminSuppliers-modal-lg').modal('hide');
            }
            else {

       
                    $('#myLoader').modal('hide');
               
                showErrorMessage(data);
            }
        });
    }




    //------------------------ Add new Suppliers ---------------------------//
















    //---------------------- activate user --------------------------//


    $('#btnConfirmActive').on('click', function () {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        $('#confirmModal').modal('hide');
        $('.myLoader').modal('show');

        var details = {
            userId: $('#lblUserID').text(),
            clientid: $('#lblClientID').text(),
            supplier_id: Suppliers[id]['cruise_safe_organo_supplier']['supplier_id'],
            fullname: Suppliers[id]['cruise_safe_organo_supplier']['supplier_company_name']
        };

        $.post('/admin/activateSupp', details, function (data) {
          
            if (data.message.indexOf('Error')<0) {
                getSuppliers();
                    $('#myLoader').modal('hide');
              

                showSuccessMessageClose(data.message);
                    $('#successModal').modal('hide');
           
            }
            else {
                    $('#myLoader').modal('hide');
                showErrorMessage(data.message);
            }
        });
    });


    //---------------------- activate user ends --------------------------//








    //---------------------- deactivate user --------------------------//


    $('#btnConfirm').on('click', function () {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        $('#confirmModal').modal('hide');
        $('.myLoader').modal('show');

        var details = {
            userId: $('#lblUserID').text(),
            clientid: $('#lblClientID').text(),
            supplier_id: Suppliers[id]['cruise_safe_organo_supplier']['supplier_id'],
            fullname: Suppliers[id]['cruise_safe_organo_supplier']['supplier_company_name']
        };
        //console.log(details);
        //return;
        $.post('/admin/deactivateSupp', details, function (data) {
            if (data.message.indexOf('Error') < 0) {
                getSuppliers();
                    $('#myLoader').modal('hide');

                showSuccessMessageClose(data.message);
                    $('#successModal').modal('hide');
            }
            else {
                    $('#myLoader').modal('hide');
                showErrorMessage(data.message);
            }
        });
    });


    //---------------------- deactivate user ends --------------------------//










    $('#btnAddDepartmentd').on('click', function () {
        saveDepartment();
    });




    function saveDepartment() {

        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }


        if (vaidateFields('requiredDepartment'))
            return;

        if ($('#btnDirectord').attr('data-id').length == 0) {
            $('#btnDirectord').css('border-color', 'red');
            return;
        }

        isSendingg = true;
        console.log();


        $('.myLoader').modal('show');

        var details = {
            department_id: null,
            department_client_id: $('#lblClientID').text(),
            department_abbr: $('#txtShortNamed').val(),
            department_status: $('#cboStatusd').val(),
            department_director: $('#btnDirectord').attr('data-id'),
            department_phone: $('#txtPhoned').val(),
            department_email: $('#txtEmaild').val(),
            department_name: $('#txtNamed').val(),
            department_created_by: $('#lblUserID').text(),
            department_updated_by: $('#lblUserID').text()
        };
        console.log(details);
        $.post(http + 'save_department', details, function (data) {
            console.log(data);
            isSendingg = false;
            if (data.indexOf('success') >= 0) {
                showSuccessMessageClose(data);
            
                $('#myLoader').modal('hide');

                $('.adminDepartments-modal-lg').modal('hide');
            }
            else {


                $('#myLoader').modal('hide');

                showErrorMessage(data);
            }
        });
    }




























































    function vaidateFields(clas) {
        $('.' + clas).css('border-color', 'silver');
        var input;
        $('.' + clas).map(function () {
            if (!$(this).val() || $(this).css('border-color') == 'red') {
                $(this).css('border-color', 'red');
                $(this).focus();
                input = $(this);
            }
        }).get();

        if (input) {
            input.focus();
            return true;
        }
        else
            return false;
    }

    function validateEmail(sEmail, msg) {
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(sEmail)) {
            return true;
        }
        else {
            //var msg = "Please enter valid email address.";
            checkEmptyFields(msg);
            return false;
        }
    }

    function SuccessMessage(msg) {
        $('#myPleaseWait').modal('show');
        $('#txtModalLoaderAlertMessage').hide;
        $('#txtModalLoaderSuccessMessage').show();
        $('#txtModalLoaderSuccessMessage b').html(msg);

        setTimeout(function () {
            $('#myPleaseWait').hide();
            $('#txtModalLoaderSuccessMessage').hide();
            $('#myPleaseWait').modal('hide');
        }, 3000);
    }

    function checkEmptyFields(msg) {

        $('#myPleaseWait').modal('show');
        $('#txtModalLoaderAlertMessage').show();
        $('#txtModalLoaderAlertMessage b').text(msg);

        setTimeout(function () {
            $('#myPleaseWait').hide();
            $('#myPleaseWait').modal('hide');
            $('#txtModalLoaderAlertMessage').hide();
        }, 3000);
    }

    Array.prototype.remove = function () {
        var what, a = arguments, L = a.length, ax;
        while (L && this.length) {
            what = a[--L];
            while ((ax = this.indexOf(what)) !== -1) {
                this.splice(ax, 1);
            }
        }
        return this;
    };



    /***************************************************************
                        Validations Ends
    ***************************************************************/



    //Removing  Messages CoOperation Services

    $(document).on('click', function () {
        if ($('#txtModalLoaderAlertMessage').is(':visible')) {
            $('#txtModalLoaderAlertMessage').hide();
        }
        if ($('#txtModalLoaderSuccessMessage').is(':visible')) {
            $('#txtModalLoaderSuccessMessage').hide();
        }

    });






});