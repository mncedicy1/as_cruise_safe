﻿function checkPrev(myPrivs) {

    //-------------------- Check Prev ---------------------------//

    if ($('#lblTeamType').text() != 'business') {
        if (myPrivs.indexOf(1002) >= 0) {

        } else {
            location.href = '/Home/menu';
        }
    }

    //-------------------- Check Prev ---------------------------//
}

$(document).ready(function () {



    var http = httpsapi + "/api/Organisation/";
    var https = httpsapi + "/api/";
   // http = httpsapi_local + '/api/Organisation/';

    var how_is_connection = '';//checkNetworkConnection();




    $('.actionAdministrator').trigger('click');
    $('.btnTeamManagement').find('a').addClass('activeMenu');



    //------------------------------------------------------------------ Get all Teams ----------------------------------------------------------------------------------------//




    var isSendingg = false;
    var scrollHeight = 0;
    var Teams = Array();

    var users = Array();
    var members = Array();
    var membersArr = Array();

    var start_fromm = 0;
    var user_id = null;
    var team_id = null;
    var isLeader = 'member';

    function getTeams() {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        $('#myLoader').modal('show');

        var details = {
            client_id: $('#lblClientID').text()
        };
        console.log(details);
        $.getJSON(http +'get_teams', details, function (data) {
            console.log('getTeams', data);
            Teams = data;
            displayTeams(data);
         

                $('#myLoader').modal('hide');
        });
    }




    getUsers(true);
    getDepartments();

    function displayTeams(data) {

        $('#zeroStatsTeams').hide();
        if (data.length > 0) {
            var str = "";
            for (i = 0; i < data.length; i++) {
                var userStatus = "";
                var statusButton = "";

                if (data[i]['cruise_safe_organo_team']['team_status'] == 'active') {
                    userStatus = "label-success badge badge-success";
                    statusButton = '<button id="v' + i + '"  style="width:90px" type="button" class="btn btn-danger removeDept btn-flat btn-xs"> Deactivate</button>';
                }
                else {
                    userStatus = "label-danger badge badge-danger";
                    statusButton = '<button id="t' + i + '"  style="width:90px" type="button" class="btn btn-success activateDept btn-flat btn-xs"> Activate</button>';
                }

                str += '<tr class="btnChooseMember" style="cursor:pointer;" id="b' + i + '"><td>' + (i + 1) + '</td>';
                str += '<td>' + data[i]['cruise_safe_organo_team']['team_name'] + '</td>';
                str += '<td>' + getDepartmentName(data[i]['cruise_safe_organo_team']['team_department_id']) + '</td>';
                str += '<td>' + getName(data[i]['cruise_safe_organo_team']['team_leader']) + '</td>';
                str += '<td>' + getPhone(data[i]['cruise_safe_organo_team']['team_leader']) + '</td>';
                str += '<td>' + getEmail(data[i]['cruise_safe_organo_team']['team_leader']) + '</td>';
                str += '<td>' + data[i]['members'].length + '</td>';
                str += '<td><span class="' + userStatus + '">' + data[i]['cruise_safe_organo_team']['team_status'] + '</span></td>';
                str += '<td>       <button id="b' + i + '" style="width:90px" type="button" class="btn btn-warning btn-flat btn-xs btnView">View</button>  ' + statusButton + ' </td>';
            }
            $('#tblTeams').html(str);
        }


        if (start_fromm == 0 && data.length == 0) {
            $('#zeroStatsTeams').show();
        }






        //------------ view -------------------//


        $('.btnView').click(function () {

            id = this.id.substring(1);
            $('#btnAddTeam').html('Save Changes');

            //--------------- Populate Fields ------------------//
            team_id = Teams[id]['cruise_safe_organo_team']['team_id'];

            $('#cboDepartment').val(Teams[id]['cruise_safe_organo_team']['team_department_id']);
            $('#cboStatust').val(Teams[id]['cruise_safe_organo_team']['team_status']);
            $('#cboLeader').val(Teams[id]['cruise_safe_organo_team']['team_leader']);
            $('#txtNamet').val(Teams[id]['cruise_safe_organo_team']['team_name']);
            members = Array();
            membersArr = Array();

            for (var d = 0; d < data[id]['members'].length; d++) {
                members.push(Teams[id]['members'][d].member_user_id);
            }
            loadLeaders();
            $('#cboLeader').val(data[id]['cruise_safe_organo_team']['team_leader']).trigger('change');

           

            //--------------- Populate Fields ------------------//

            $('.adminTeams-modal-lg').modal('show');
        });

        //------------ view -------------------//




        //------------ activate -------------------//

        $('.activateDept').click(function () {
            id = this.id.substring(1);

            $('#btnConfirm').hide();
            $('#btnConfirmActive').show();


            $('#confirmModal').modal('show');
            $('#lblConfirmMessage').html("Are you sure, you want to activate " + Teams[id]['cruise_safe_organo_team']['team_name'] + ' Team');
        });

        //------------ activate -------------------//






        //------------ deactivate  -------------------//

        $('.removeDept').click(function () {
            id = this.id.substring(1);

            $('#btnConfirm').show();
            $('#btnConfirmActive').hide();


            $('#confirmModal').modal('show');
            $('#lblConfirmMessage').html("Are you sure, you want to remove " + Teams[id]['cruise_safe_organo_team']['team_name'] + ' Team');
        });

        //------------ deactivate   -------------------//






    }




    function getUsers(loadMore) {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }


        var details = {
            clientid: $('#lblClientID').text()
        };

        $.getJSON('/admin/getUsers', details, function (data) {
            isSendingg = false;
            users = data;
            console.log(users);

            $('#cboDirectord').html('<option value="">-- Choose User --</option>');
            for (i = 0; i < data.length; i++) {
                $('#cboDirectord').append('<option value="' + data[i]['user_id'] + '">' + data[i]['user_title'] + ' ' + data[i]['user_firstname'] + ' ' + data[i]['user_surname'] + '</option>');
            }
            $('#cboDirectord').append('<option value="0" style="font-weight:bold;color:green">New User</option>');

           
            if (loadMore) {
                getTeams();
                getRoles();
            }
            else {
                listUsersAll(members);
            }

        });
    }



    function getDepartments() {

        var details = {
            client_id: $('#lblClientID').text()
        };
        console.log(details);
        $.getJSON(http + 'get_departments', details, function (data) {
            console.log('getDepartments', data);
            Departments = data;
            console.log(data);
            $('#cboDepartment').html('<option value="">-- Choose Department --</option>');
            for (var d = 0; d < data.length; d++) {
                $('#cboDepartment').append('<option value="' + data[d]['department_id'] + '">' + data[d]['department_name'] + '</option>');
            }
            $('#cboDepartment').append('<option value="0" style="font-weight:bold;color:green">New Department</option>');

        });
    }


    function getDepartmentName(department_id) {
        var name = '';
        for (var l = 0; l < Departments.length; l++) {
            if (Departments[l]['department_id'] == department_id)
                name = Departments[l]['department_name'] + ' (' + Departments[l]['department_abbr'] + ')';
        }
        return name;
    }



    function getName(user_id) {
        var name = '';
        for (var l = 0; l < users.length; l++) {
            if (users[l]['user_id'] == user_id)
                name = users[l]['user_title'] + ' ' + users[l]['user_firstname'] + ' ' + users[l]['user_surname'];
        }
        return name;
    }



    function getPhone(user_id) {
        var name = '';
        for (var p = 0; p < users.length; p++) {
            if (users[p]['user_id'] == user_id)
                name = users[p]['user_cellphone'];
        }
        return name;
    }
    function getEmail(user_id) {
        var name = '';
        for (var e = 0; e < users.length; e++) {
            if (users[e]['user_id'] == user_id)
                name = users[e]['user_email'];
        }
        return name;
    }


    $('#cboDepartment').change(function () {
        console.log($(this).val());
        if ($(this).val() == '0') {
            $('#btnDirectord').attr('data-id', '');
            $('#btnDirectord').html('Add User').css('border-color', 'rgba(42, 50, 60, 0.2)');
            $('#btnAddDepartmentd').html('Save');
            $('.cleanDepartmentField').val('');
            $('#btnDirectord').attr('data-id', '').html('Add User');
            $('.adminDepartments-modal-lg').modal('show');
            $(this).val('').trigger('change');
        }
    });





    $('#btnDirectord').click(function () {
        isLeader = 'director';
        listUsersAll(Array());
    });




    $('#cboLeader').change(function () {
        console.log($(this).val());
        if ($(this).val() == '0') {
            $('#btnAddNewUser').trigger('click');
            $(this).val('').trigger('change');
        }
        else if ($(this).val() == '00') {
            $(this).val('').trigger('change');
            isLeader = 'leader';
            listUsersAll(members);
        }
        else {
            loadMembers();
        }
        
    });



    function loadMembers() {
        var str = '';
        membersArr = Array();

        for (var m = 0; m < members.length; m++) {
            var type = (parseInt(members[m]) == parseInt($('#cboLeader').val())) ? '<b><i class="fas fa-dot-circle"></i> Team Leader</b>' : 'Team Member';
            str += '<div class="col-sm-6 col-lg-6"><div class="panel text-center btnWidget2" id = "usee' + members[m] +'">'+
                '<div class="panel-body p-t-10" style="padding: 20px;"><h4 class="m-t-0 m-b-15">' +
                '<i class="fas fa-user text-primary m-r-10"></i><b>' + getName(members[m]) +'</b></h4>'+
                '<p class="text-muted m-b-0 m-t-20"> ' + type + '</p></div></div></div>';
            membersArr.push({
                member_user_id: members[m],
                member_type: type
            });
        }
        $('#listMembers').html(str);
        $('#btnRemoveMember').hide('slow');
        $('#btnTeamLeader').hide('slow');
    }

    function loadLeaders() {
        $('#cboLeader').html('<option value="">-- Choose Team Leader --</option>');
        for (var m = 0; m < members.length; m++) {
            $('#cboLeader').append('<option value="' + members[m] + '">' + getName(members[m]) + '</option>');
        }
        $('#cboLeader').append('<option value="00" style="font-weight:bold;color:green">Select From Users</option>');
        $('#cboLeader').append('<option value="0" style="font-weight:bold;color:green">Add New User</option>');
    }



    function listUsersAll(notArr) {
        var str = '';
        $("#txtSearchh").val('');
        for (var m = 0; m < users.length; m++) {
            if (notArr.indexOf(parseInt(users[m].user_id)) < 0){
                var name = users[m]['user_title'] + ' ' + users[m]['user_firstname'] + ' ' + users[m]['user_surname'];
                str += '<div class="col-sm-6 col-lg-6 memberr"><div class="panel text-center btnWidget2 userList" id = "user' + users[m]['user_id']+'">' +
                    '<div class="panel-body p-t-10" style="padding: 20px;"><h4 class="m-t-0 m-b-15">' +
                    '<i class="fas fa-user text-primary m-r-10"></i><b>' + name + '</b></h4>' +
                    '<p class="text-muted m-b-0 m-t-20"> ' + users[m].user_email + '</p></div></div></div>';
            }
        }
        $('#listUsersAll').html(str);
        $('#btnPickOne').hide();
        $('.usersmodallg').modal('show');

        $("#txtSearchh").on("keyup", function () {
            var value = $(this).val().toLowerCase();
            $("#listUsersAll div.memberr").filter(function () {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
            });
            $('#rows').html($("#listUsersAll div.memberr:visible").length);
        });

        $('#btnAddNewUser').click(function () {
            user_id = null;
            $('.clsPassword').show();
            $('.clsPasswords').removeAttr('disabled').addClass('requiredUser');
            $('#btnAddUser').html('Save');
            $('.cleanUsertField').val('');
            $('#txtPassword').val('');
            $('.adminUsers-modal-lg').modal('show');
        });

        $('#btnPickOne').on('click', function () {
            if (isLeader == 'director') {
                $('#btnDirectord').attr('data-id', $(this).attr('data-id'));
                $('#btnDirectord').html($(this).attr('data-name')).css('border-color', 'rgba(42, 50, 60, 0.2)');
            }
            else {
                if (members.indexOf(parseInt($(this).attr('data-id'))) < 0) {
                    members.push(parseInt($(this).attr('data-id')));
                }
                var leadVal = $('#cboLeader').val();
                loadLeaders();
                if (isLeader == 'leader') {
                    leadVal = parseInt($(this).attr('data-id'));
                }
                $('#cboLeader').val(leadVal).trigger('change');
                loadMembers();
            }
            $('.usersmodallg').modal('hide');
        });


    }




    $('#btnAddMember').on('click', function () {
        isLeader = 'member';
        listUsersAll(members);
    });



    function getRoles() {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        roles = Array();

        var details = {
            clientid: $('#lblClientID').text(),
            //  userId: $('#lblUserID').text(),
        };
        $.getJSON('/admin/getUserRoles', details, function (data) {
            //console.log('Roles', data);
            if (data) {
                roles = data['roles'];
                $('#cboRole').html('<option value="">-- Choose Role --</option>');
                for (var i = 0; i < data['roles'].length; i++) {
                    var htmlcontent = '<option value="' + data['roles'][i].role_id + '">' + data['roles'][i].role_name + '</option>';
                    $('#cboRole').append(htmlcontent);
                }
            }
        });

    }


    $('#btnAddUser').on('click', function () {
        saveUser(user_id);
    });



    $('#btnRemoveMember').on('click', function () {
        members.remove(parseInt($(this).attr('data-id')));
        if (parseInt($(this).attr('data-id')) == parseInt($('#cboLeader').val()))
            $('#cboLeader').val('');
        var leadVal = $('#cboLeader').val();
        loadLeaders();
        $('#cboLeader').val(leadVal).trigger('change');

        loadMembers();
    });


    $('#btnTeamLeader').on('click', function () {
        $('#cboLeader').val(parseInt($(this).attr('data-id'))).trigger('change');
        loadMembers();
    });



    $(document).on('click', ".btnWidget2", function () {
        $(document).find(".btnWidget2").removeClass('active');
        $(this).addClass('active');
        if ($(this).hasClass('userList')) {
            $('#btnPickOne').show('slow').html('Select ' + getName(this.id.substring(4))).attr('data-id', this.id.substring(4));
            $('#btnPickOne').attr('data-name', getName(this.id.substring(4)));
        }
        else {
            $('#btnRemoveMember').show('slow').html('Remove ' + getName(this.id.substring(4))).attr('data-id', this.id.substring(4));
            if (parseInt(this.id.substring(4)) != parseInt($('#cboLeader').val()))
                $('#btnTeamLeader').show('slow').attr('data-id', this.id.substring(4));
            else
                $('#btnTeamLeader').hide('slow').attr('data-id', '');
        }
    });



    function saveUser(user_id) {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        if (vaidateFields('requiredUser'))
            return;
        if (user_id == null) {
            if ($('#txtPassword').val() != $('#txtConfirmPassword').val()) {
                showConnectionMessage("Passwords do not match");
                return;
            }
        }
        isSendingg = true;

        $('.myLoader').modal('show');

        var details = {
            user_id: user_id,
            user_client_id: $('#lblClientID').text(),
            user_id_number: $('#txtIDNumber').val(),
            user_title: $('#cboTitle').val(),
            user_firstname: $('#txtFirstName').val(),
            user_surname: $('#txtLastName').val(),
            user_gender: $('#cboGender').val(),
            user_cellphone: $('#txtCellNumber').val(),
            user_email: $('#txtEmailAddress').val(),
            user_role_id: $('#cboRole').val(),
            user_password: $('#txtPassword').val(),
            user_saved_by: $('#lblUserID').text(),
            user_updated_by: $('#lblUserID').text()

        };
        console.log(details);
        $.post(https + 'admin/addUser', details, function (data) {
            console.log(data);
            isSendingg = false;
            if (data.indexOf('success') >= 0) {
                showSuccessMessageClose(data);
                getUsers(false);
                $('#myLoader').modal('hide');

                $('.adminUsers-modal-lg').modal('hide');
            }
            else {


                $('#myLoader').modal('hide');

                showErrorMessage(data);
            }
        });
    }







    //----------------------------- Search Teams ----------------------------------------//

    $(".search-bar").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#tblTeams tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

    //----------------------------- Search Teams ----------------------------------------//






    //------------------------------------------------------------------ Get all Teams ----------------------------------------------------------------------------------------//












    //------------------------ Add new Teams ---------------------------//







    $('#btnAddNew').click(function () {
        team_id = null;
        members = Array();
        membersArr = Array();
        loadLeaders();
        $('#btnAddTeam').html('Save');
        $('.cleanTeamField').val('');
        $('#listMembers').html('');

        $('.adminTeams-modal-lg').modal('show');

    });











    $('#btnAddTeam').on('click', function () {
        saveTeam(team_id);
    });






    function saveTeam(team_id) {
       
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

 
        if (vaidateFields('requiredTeam'))
            return;

        isSendingg = true;
        console.log(team_id);


        $('.myLoader').modal('show');

        var details = {
            cruise_safe_organo_team: {
                team_id: team_id,
                team_client_id: $('#lblClientID').text(),
                team_department_id: $('#cboDepartment').val(),
                team_status: $('#cboStatust').val(),
                team_leader: $('#cboLeader').val(),
                team_name: $('#txtNamet').val(),
                team_created_by: $('#lblUserID').text(),
                team_updated_by: $('#lblUserID').text()
            },
            members: membersArr
        };



        console.log(details);
        $.post(http + 'save_team', details, function (data) {
            console.log(data);
            isSendingg = false;
            if (data.indexOf('success') >= 0) {
                showSuccessMessage(data);
                getTeams();
                $('#myLoader').modal('hide');
      
                $('.adminTeams-modal-lg').modal('hide');
            }
            else {

       
                    $('#myLoader').modal('hide');
               
                showErrorMessage(data);
            }
        });
    }




    //------------------------ Add new Teams ---------------------------//


















    //---------------------- activate user --------------------------//


    $('#btnConfirmActive').on('click', function () {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        $('#confirmModal').modal('hide');
        $('.myLoader').modal('show');

        var details = {
            userId: $('#lblUserID').text(),
            clientid: $('#lblClientID').text(),
            team_id: Teams[id]['cruise_safe_organo_team']['team_id'],
            fullname: Teams[id]['cruise_safe_organo_team']['team_name']
        };

        $.post('/admin/activateTeam', details, function (data) {
          
            if (data.message.indexOf('Error')<0) {
                getTeams();
                    $('#myLoader').modal('hide');
              

                showSuccessMessageClose(data.message);
                    $('#successModal').modal('hide');
           
            }
            else {
                    $('#myLoader').modal('hide');
                showErrorMessage(data.message);
            }
        });
    });


    //---------------------- activate user ends --------------------------//








    //---------------------- deactivate user --------------------------//


    $('#btnConfirm').on('click', function () {
        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }

        $('#confirmModal').modal('hide');
        $('.myLoader').modal('show');

        var details = {
            userId: $('#lblUserID').text(),
            clientid: $('#lblClientID').text(),
            team_id: Teams[id]['cruise_safe_organo_team']['team_id'],
            fullname: Teams[id]['cruise_safe_organo_team']['team_name']
        };
        console.log(details);
        //return;
        $.post('/admin/deactivateTeam', details, function (data) {
            if (data.message.indexOf('Error') < 0) {
                getTeams();
                    $('#myLoader').modal('hide');

                showSuccessMessageClose(data.message);
                    $('#successModal').modal('hide');
            }
            else {
                    $('#myLoader').modal('hide');
                showErrorMessage(data.message);
            }
        });
    });


    //---------------------- deactivate user ends --------------------------//










    $('#btnAddDepartmentd').on('click', function () {
        saveDepartment();
    });




    function saveDepartment() {

        if (how_is_connection == "offline") {
            showConnectionMessage("Looks like you are offline. Please check your internet connection.");
            return;
        }


        if (vaidateFields('requiredDepartment'))
            return;

        if ($('#btnDirectord').attr('data-id').length == 0) {
            $('#btnDirectord').css('border-color', 'red');
            return;
        }

        isSendingg = true;
        console.log();


        $('.myLoader').modal('show');

        var details = {
            department_id: null,
            department_client_id: $('#lblClientID').text(),
            department_abbr: $('#txtShortNamed').val(),
            department_status: $('#cboStatusd').val(),
            department_director: $('#btnDirectord').attr('data-id'),
            department_phone: $('#txtPhoned').val(),
            department_email: $('#txtEmaild').val(),
            department_name: $('#txtNamed').val(),
            department_created_by: $('#lblUserID').text(),
            department_updated_by: $('#lblUserID').text()
        };
        console.log(details);
        $.post(http + 'save_department', details, function (data) {
            console.log(data);
            isSendingg = false;
            if (data.indexOf('success') >= 0) {
                showSuccessMessageClose(data);
                getDepartments();
                $('#myLoader').modal('hide');

                $('.adminDepartments-modal-lg').modal('hide');
            }
            else {


                $('#myLoader').modal('hide');

                showErrorMessage(data);
            }
        });
    }




























































    function vaidateFields(clas) {
        $('.' + clas).css('border-color', 'silver');
        var input;
        $('.' + clas).map(function () {
            if (!$(this).val() || $(this).css('border-color') == 'red') {
                $(this).css('border-color', 'red');
                $(this).focus();
                input = $(this);
            }
        }).get();

        if (input) {
            input.focus();
            return true;
        }
        else
            return false;
    }

    function validateEmail(sEmail, msg) {
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(sEmail)) {
            return true;
        }
        else {
            //var msg = "Please enter valid email address.";
            checkEmptyFields(msg);
            return false;
        }
    }

    function SuccessMessage(msg) {
        $('#myPleaseWait').modal('show');
        $('#txtModalLoaderAlertMessage').hide;
        $('#txtModalLoaderSuccessMessage').show();
        $('#txtModalLoaderSuccessMessage b').html(msg);

        setTimeout(function () {
            $('#myPleaseWait').hide();
            $('#txtModalLoaderSuccessMessage').hide();
            $('#myPleaseWait').modal('hide');
        }, 3000);
    }

    function checkEmptyFields(msg) {

        $('#myPleaseWait').modal('show');
        $('#txtModalLoaderAlertMessage').show();
        $('#txtModalLoaderAlertMessage b').text(msg);

        setTimeout(function () {
            $('#myPleaseWait').hide();
            $('#myPleaseWait').modal('hide');
            $('#txtModalLoaderAlertMessage').hide();
        }, 3000);
    }

    Array.prototype.remove = function () {
        var what, a = arguments, L = a.length, ax;
        while (L && this.length) {
            what = a[--L];
            while ((ax = this.indexOf(what)) !== -1) {
                this.splice(ax, 1);
            }
        }
        return this;
    };



    /***************************************************************
                        Validations Ends
    ***************************************************************/



    //Removing  Messages CoOperation Services

    $(document).on('click', function () {
        if ($('#txtModalLoaderAlertMessage').is(':visible')) {
            $('#txtModalLoaderAlertMessage').hide();
        }
        if ($('#txtModalLoaderSuccessMessage').is(':visible')) {
            $('#txtModalLoaderSuccessMessage').hide();
        }

    });






});