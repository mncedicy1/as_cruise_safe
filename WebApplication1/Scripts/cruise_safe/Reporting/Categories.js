﻿function checkPrev(myPrivs) {

    //-------------------- Check Prev ---------------------------//

    if ($('#lblUserType').text() != 'business') {
        if (myPrivs.indexOf(1002) >= 0) {

        } else {
            //   location.href = '/as_smart_connects/home';
        }
    }

    //-------------------- Check Prev ---------------------------//
}

$(document).ready(function () {



   var http = httpsapi + "/api/reporting/";
    var https = httpsapi + "/api/";
    //http = httpsapi_local + '/api/reporting/';




    $('.actionAdministrator').trigger('click');
    $('.btnUserManagement').find('a').addClass('activeMenu');




    var destination;
    var source;
    var travelMode = 'DRIVING';
    var latMap = '';
    var lngMap = '';
    var town = '';
    var seletedTop = 'all';
    var allData = Array();



    function getCategories() {


        $('#myLoader').modal('show');


        var details = {
            client_id: $('#lblClientID').text(),
        };

        $.getJSON(http + 'getCategoriesWeb', details, function (data) {
            console.log('getCategories', data);
            allData = data;

            loadData();

            setTimeout(function () {
                $('#myLoader').modal('hide');
            }, 1000);

        });
    }

    getCategories();





   
    $("#btnSave").click(function () {

  

        $('#myLoader').modal('show');
        $.post(http + 'saveCategories', details, function (data) {
            if (data.indexOf('success') >= 0) {
                showSuccessMessageClose("Traffic Light Saved");
                $('.adminUsers-modal-lg').modal('hide');
                getCategories();
            }
            else {
                showErrorMessage(data);
                $('#myLoader').modal('hide');
            }

           
        });
    });






    function getNumber(number,i) {
        if (number == 0)
            return '<span id="y' + i + '" class="badge badge-danger btnView" style="width:50px;cursor:pointer">' + number + '</span>';
        else
            return '<span id="q' + i + '" class="badge badge-warning btnView" style="width:50px;cursor:pointer">' + number + '</span>';
    }


    function getText(text, maxlength) {
        text = text + '';
        if (text.length > maxlength)
            return '<span title="' + text + '">' + text.substring(0, maxlength) +'...</span>';
        else
            return '<span title="' + text + '">' + text+'</span>';
    }


    function getQuick(text) {
        text = text + '';
        if (text == "quick reporting")
            return '<span class="badge badge-success" title="quick reporting">Yes</span>';
        else
            return '<span class="badge badge-warning" title="detailed reporting">No</span>';
    }

    function getStatus(status) {
        if (status == 'inactive')
            return '<span class="badge badge-warning" style="width:105px">' + status + '</span>';
        if (status == 'active')
            return '<span class="badge badge-success" style="width:105px">' + status + '</span>';
        else
            return '<span class="badge badge-danger" style="width:105px">' + status + '</span>';
    }


    function getButtons(status,i) {
        if (status != 'deleted') {
            return '<button id="b' + i + '" style="width:100px" ' +
                'type="button" class="btn btn-primary btn-flat btn-xs btnView pull-right">View</button>';
        }
        else {
            return '<button id="u' + i + '" style="width:100px" ' +
                'type="button" class="btn btn-primary btn-flat btn-xs btnUndo pull-right">Undo Delete</button>';
        }
    }
    
    

    var selectedIdAns;
    var selectedId;
    function loadData() {
        if (allData) {
            var str = "";
            for (i = 0; i < allData.length; i++) {
                str += '<tr><td>' + (i + 1) + '</td>';
                str += '<td>' + getText(allData[i]['category']['category_name'],90) + '</td>';
                str += '<td>' + getNumber(allData[i]['category_subs'].length, i) + '</td>';
                str += '<td>' + getStatus(allData[i]['category']['category_status']) + '</td>';
                str += '<td>' + getDatee(allData[i]['category']['category_timestamp']) + '</td>';
                str += '<td>' + getButtons(allData[i]['category']['category_status'],i) + '</td>';
            }
            $('#tblCategories').html(str);
        }



        $('.btnView').click(function () {
            var i = this.id.substring(1);
            loadSelected(i);
        });

        $('.btnUndo').click(function () {
            selectedId = this.id.substring(1);
            $('#clsComment').val(allData[selectedId]['category']['category_name']);
            category_status = "active";
            category_name = allData[selectedId]['category']['category_name'];
            $('.clsTitle').html('Undo Delete');
            $('.clsMsg').html('Are you sure you want to <b style="color:green">recover</b> this item?');
            $('.confirmModal').modal('show');
        });
    }



    function loadSelected(i) {
        selectedId = i;
        
        $('#viewModal').modal('show');
        $('.clsBtnStatus').hide();
        $('.footer').hide();

        $('#txtCategory').val(allData[i]['category']['category_name']);
        $('.clsStatus').html(getStatus(allData[i]['category']['category_status']));

        if (allData[i]['category']['category_status'] == 'active')
            $('.btnInactive').show();
        else
            $('.btnActive').show();

        
        loadItems();
    
    }



    function getButtonsAns(status, i) {
        if (status == 'deleted') {
            return '<button id="f' + i + '" style="width:160px" ' +
                'type="button" class="btn btn-primary btn-flat btn-xs btnUndoItem pull-right">Undo Delete</button>';
        }
        else {
            return '<button id="g' + i + '" style="width:80px" ' +
                'type="button" class="btn btn-danger btn-flat btn-xs btnRemoveItem pull-right">Remove</button>' +
                '<button id="q' + i + '" style="width:80px" ' +
                'type="button" class="btn btn-primary btn-flat btn-xs btnViewItem pull-right">View</button>';
        }
    }


    function loadItems() {
        var str = "";
        for (i = 0; i < allData[selectedId]['category_subs'].length; i++) {
            str += '<tr><td>' + (i + 1) + '</td>';
            str += '<td>' + getText(allData[selectedId]['category_subs'][i]['category_sub_name'], 130) + '</td>';
            str += '<td>' + getQuick(allData[selectedId]['category_subs'][i]['category_sub_type']) + '</td>';
            str += '<td>' + getStatus(allData[selectedId]['category_subs'][i]['category_sub_status']) + '</td>';
            str += '<td>' + getDatee(allData[selectedId]['category_subs'][i]['category_sub_timestamp']) + '</td>';
            str += '<td>' + getButtonsAns(allData[selectedId]['category_subs'][i]['category_sub_status'],i) + '</td>';
        }
        $('#tblFAQItems').html(str);

        $('.btnViewItem').click(function () {
            var i = this.id.substring(1);
            loadSelectedAns(i,'view');
        });

        $('.btnRemoveItem').click(function () {
            var i = this.id.substring(1);
            loadSelectedAns(i, 'remove');
        });

        $('.btnUndoItem').click(function () {
            var i = this.id.substring(1);
            loadSelectedAns(i, 'recover');
        });

    }




    function loadSelectedAns(i,type) {
        selectedIdAns = i;
        $('.answerModal').modal('show');
   
        if (type == 'view') {
            $('#clsCategoryType').val(allData[selectedId]['category_subs'][selectedIdAns]['category_sub_type']).removeAttr('disabled');
            $('#clsItem1').val(allData[selectedId]['category_subs'][selectedIdAns]['category_sub_name']).removeAttr('disabled');
            $('.clsCategory1').html(getText(allData[selectedId]['category']['category_name'], 80));
            $('#clsCategoryTitle').html('Category');
            $('#btnYesConfirm1').show();
            $('#btnYesConfirm1Remove').hide();
            $('#btnYesConfirm1Recover').hide();
            $('#btnYesConfirm1New').hide();
        }
        else if (type == 'remove') {
            $('#clsCategoryType').val(allData[selectedId]['category_subs'][selectedIdAns]['category_sub_type']).attr('disabled', 'disabled');
            $('#clsItem1').val(allData[selectedId]['category_subs'][selectedIdAns]['category_sub_name']).attr('disabled', 'disabled');
            $('.clsCategory1').html('');
            $('#clsCategoryTitle').html('Are you sure you want to <b style="color:red">delete</b> this sub-category?');
            $('#btnYesConfirm1').hide();
            $('#btnYesConfirm1Remove').show();
            $('#btnYesConfirm1Recover').hide();
            $('#btnYesConfirm1New').hide();
        }
        else if (type == 'recover') {
            $('#clsCategoryType').val(allData[selectedId]['category_subs'][selectedIdAns]['category_sub_type']).attr('disabled', 'disabled');
            $('#clsItem1').val(allData[selectedId]['category_subs'][selectedIdAns]['category_sub_name']).attr('disabled', 'disabled');
            $('.clsCategory1').html('');
            $('#clsCategoryTitle').html('Are you sure you want to <b style="color:green">recover</b> this sub-category?');
            $('#btnYesConfirm1').hide();
            $('#btnYesConfirm1Remove').hide();
            $('#btnYesConfirm1Recover').show();
            $('#btnYesConfirm1New').hide();
        }
        else if (type == 'new') {
            $('#clsCategoryType').val('').removeAttr('disabled');
            $('#clsItem1').val('').removeAttr('disabled');
            $('.clsCategory1').html(getText(allData[selectedId]['category']['category_name'], 80));
            $('#clsCategoryTitle').html('Category');
            $('#btnYesConfirm1').hide();
            $('#btnYesConfirm1Remove').hide();
            $('#btnYesConfirm1Recover').hide();
            $('#btnYesConfirm1New').show();
        }
    }



    $('#btnAddNewItem').click(function () {
        loadSelectedAns(0, 'new');
    });



    $('#btnAddNew').click(function () {
        $('.questionModal').modal('show');
        $('#clsCategory1').val('');
    });




    $('.btnClose').click(function () {
        $('#viewModal').modal('hide');
        $('.footer').show('slow');
       
    });


    $('#txtCategory').keyup(function () {
        if ($(this).val().trim() != allData[selectedId]['category']['category_name'])
            $('.divSaveCategory').slideDown('fast');
        else
            $('.divSaveCategory').slideUp('fast');
    });




    var category_status = '';
    var category_name = '';

    $('.btnRemove').click(function () {
        $('#clsComment').val(allData[selectedId]['category']['category_name']);
        category_status = "deleted";
        category_name = allData[selectedId]['category']['category_name'];
        $('.clsTitle').html('Delete category');
        $('.clsMsg').html('Are you sure you want to <b style="color:red">delete</b> this category?');
        $('.confirmModal').modal('show');
    });

    $('.btnInactive').click(function () {
        $('#clsComment').val(allData[selectedId]['category']['category_name']);
        category_status = "inactive";
        category_name = allData[selectedId]['category']['category_name'];
        $('.clsTitle').html('Update Status');
        $('.clsMsg').html('The status will change to <b style="color:red">Inactive</b><br><b>Are you sure?</b>');
        $('.confirmModal').modal('show');
    });

    $('.btnActive').click(function () {
        $('#clsComment').val(allData[selectedId]['category']['category_name']);
        category_status = "active";
        category_name = allData[selectedId]['category']['category_name'];
        $('.clsTitle').html('Update Status');
        $('.clsMsg').html('The status will change to <b style="color:green">Active</b><br><b>Are you sure?</b>');
        $('.confirmModal').modal('show');
    });

    $('.btnSaveCategory').click(function () {
        $('#clsComment').val($('#txtCategory').val());
        category_status = allData[selectedId]['category']['category_status'];
        category_name = $('#txtCategory').val().trim();
        $('.clsTitle').html('Update Category');
        $('.clsMsg').html('Are you sure you want to <b style="color:green">save</b> this category?');
        $('.confirmModal').modal('show');
    });
    
    $('#btnYesConfirm').click(function () {
    
        var obj = JSON.parse(JSON.stringify(allData[selectedId]['category']));
        obj.category_status = category_status;
        obj.category_name = category_name;
        $('.confirmModal').modal('hide');
        updateCategories(obj); 
    });

    function updateCategories(obj) {

        $('#myLoader').modal('show');
        $.post(http + 'updateCategories', obj, function (data) {
            console.log(data);
            if (data.indexOf('success') >= 0) {
                allData[selectedId]['category']['category_status'] = category_status;
                allData[selectedId]['category']['category_name'] = category_name;
                showSuccessMessageClose('Success');
                $('#viewModal').modal('hide');
                $('#viewMapModal').modal('hide');
                $('.footer').show('slow');
                $('.divSaveCategory').slideUp('fast');
                loadData();
            } else {
                showErrorMessageClose(data);
            }
            $('#myLoader').modal('hide');
        });
    }



    var category_sub_status = '';
    var category_sub_name = '';
    var category_sub_quick = '';

    $('#btnYesConfirm1').click(function () {
        category_sub_quick = $('#clsCategoryType').val();
        category_sub_status = allData[selectedId]['category_subs'][selectedIdAns]['category_sub_status'];
        category_sub_name = $('#clsItem1').val().trim();
        $('.confirmModal').modal('hide');
        updateCategoriesItem();
    });

    $('#btnYesConfirm1Remove').click(function () {
        category_sub_status = 'deleted';
        category_sub_quick = allData[selectedId]['category_subs'][selectedIdAns]['category_sub_type'];
        category_sub_name = allData[selectedId]['category_subs'][selectedIdAns]['category_sub_name'];
        $('.confirmModal').modal('hide');
        updateCategoriesItem();
    });

    $('#btnYesConfirm1Recover').click(function () {
        category_sub_status = 'active';
        category_sub_quick = allData[selectedId]['category_subs'][selectedIdAns]['category_sub_type'];
        category_sub_name = allData[selectedId]['category_subs'][selectedIdAns]['category_sub_name'];
        $('.confirmModal').modal('hide');
        updateCategoriesItem();
    });

    $('#btnYesConfirm1New').click(function () {
        if (!vaidateFields('requiredComment1')) {
            saveCategoriesItem();
        }
    });


    $('#btnYesConfirm1Category').click(function () {
        if (!vaidateFields('requiredComment2')) {
            saveCategories();
        }
    });


    function updateCategoriesItem() {

        var obj = JSON.parse(JSON.stringify(allData[selectedId]['category_subs'][selectedIdAns]));
        obj.category_sub_status = category_sub_status;
        obj.category_sub_name = category_sub_name;
        obj.category_sub_type = category_sub_quick;  

        $('#myLoader').modal('show');
        $.post(http + 'updateCategoriesItem', obj, function (data) {
            console.log(data);
            if (data.indexOf('success') >= 0) {
                allData[selectedId]['category_subs'][selectedIdAns]['category_sub_status'] = category_sub_status;
                allData[selectedId]['category_subs'][selectedIdAns]['category_sub_name'] = category_sub_name;
                allData[selectedId]['category_subs'][selectedIdAns]['category_sub_type'] = category_sub_quick;
                showSuccessMessageClose('Success');
                $('.answerModal').modal('hide');
                loadItems();
                loadData();
            } else {
                showErrorMessageClose(data);
            }
            $('#myLoader').modal('hide');
        });
    }


    function saveCategoriesItem() {

        var obj = {
            'category_sub_name': $('#clsItem1').val().trim(),
            'category_sub_ref_category_id': allData[selectedId]['category']['category_id'],
            'category_sub_client_id': $('#lblClientID').text(),
            'category_sub_ref_category': allData[selectedId]['category']['category_name'],
            'category_sub_type': $('#clsCategoryType').val(),
            'category_sub_status': 'active'
        }

        $('#myLoader').modal('show');
        $.post(http + 'saveCategoriesItem', obj, function (data) {
            console.log(data);
            if (data.indexOf('category_sub_id') >= 0) {
                allData[selectedId]['category_subs'].push(JSON.parse(data));
                showSuccessMessageClose('Success');
                $('.answerModal').modal('hide');
                loadItems();
                loadData();
            } else {
                showErrorMessageClose(data);
            }
            $('#myLoader').modal('hide');
        });
    }



    function saveCategories() {

        var obj = {
            'category_name': $('#clsCategory1').val().trim(),
            'category_client_id': $('#lblClientID').text(),
            'category_status': 'active'
        }

        $('#myLoader').modal('show');
        $.post(http + 'saveCategories', obj, function (data) {
            console.log(data);
            if (data.indexOf('category_id') >= 0) {
                var dataObj = { 'category': JSON.parse(data), 'category_subs': [] };
                allData.push(dataObj);
                showSuccessMessageClose('Success');
                $('.questionModal').modal('hide');
                loadData();
                setTimeout(function () {
                    loadSelected(allData.length - 1);
                    setTimeout(function () {
                        loadSelectedAns(0, 'new');
                    }, 500);
                }, 500);
            } else {
                showErrorMessageClose(data);
            }
            $('#myLoader').modal('hide');
        });
    }













 




 





















    function vaidateFields(clas) {
        $('.' + clas).css('border-color', 'silver');
        var input;
        $('.' + clas).map(function () {
            if (!$(this).val() || $(this).css('border-color') == 'red') {
                $(this).css('border-color', 'red');
                $(this).focus();
                input = $(this);

                $(this).keyup(function () {
                    if ($(this).val())
                        $(this).css('border-color', 'silver');
                    else
                        $(this).css('border-color', 'red');
                });
            }
        }).get();

        if (input) {
            input.focus();
            return true;
        }
        else
            return false;
    }

    function validateEmail(sEmail, msg) {
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(sEmail)) {
            return true;
        }
        else {
            //var msg = "Please enter valid email address.";
            checkEmptyFields(msg);
            return false;
        }
    }

    function SuccessMessage(msg) {
        $('#myPleaseWait').modal('show');
        $('#txtModalLoaderAlertMessage').hide;
        $('#txtModalLoaderSuccessMessage').show();
        $('#txtModalLoaderSuccessMessage b').html(msg);

        setTimeout(function () {
            $('#myPleaseWait').hide();
            $('#txtModalLoaderSuccessMessage').hide();
            $('#myPleaseWait').modal('hide');
        }, 3000);
    }

    function checkEmptyFields(msg) {

        $('#myPleaseWait').modal('show');
        $('#txtModalLoaderAlertMessage').show();
        $('#txtModalLoaderAlertMessage b').text(msg);

        setTimeout(function () {
            $('#myPleaseWait').hide();
            $('#myPleaseWait').modal('hide');
            $('#txtModalLoaderAlertMessage').hide();
        }, 3000);
    }



    /***************************************************************
                        Validations Ends
    ***************************************************************/



    //Removing  Messages CoOperation Services

    $(document).on('click', function () {
        if ($('#txtModalLoaderAlertMessage').is(':visible')) {
            $('#txtModalLoaderAlertMessage').hide();
        }
        if ($('#txtModalLoaderSuccessMessage').is(':visible')) {
            $('#txtModalLoaderSuccessMessage').hide();
        }

    });






});

