﻿function checkPrev(myPrivs) {

    //-------------------- Check Prev ---------------------------//

    if ($('#lblUserType').text() != 'business') {
        if (myPrivs.indexOf(1002) >= 0) {

        } else {
            //   location.href = '/as_smart_connects/home';
        }
    }

    //-------------------- Check Prev ---------------------------//
}

$(document).ready(function () {



    var http = httpsapi + "/api/reporting/";
    var https = httpsapi + "/api/";
    //var http = httpsapi_local + '/api/reporting/';




    $('.actionAdministrator').trigger('click');
    $('.btnUserManagement').find('a').addClass('activeMenu');



    var id;
    var start_from = 0;
    var last_row = false;
    var isSending = false;
    var fullname = "";
    var person_id;
    var system_user_id;

    var selectedSugg;

    var seletedTop = 'Question';
    var allData = Array();

    var arr = ['Question', 'Suggestion', 'Complement'];
    var arrCount = [0, 0, 0, 0];
    var arrToday = [0, 0, 0, 0];


    function getTraces() {


        $('#myLoader').modal('show');

        var details = {
            client_id: $('#lblClientID').text(),
        };

        $.getJSON(http + 'getSuggestionsAll', details, function (data) {

            allData = data;
            arrCount = [0, 0, 0, 0];
            $('.Trace').find('span').html(allData.length);

            for (i = 0; i < allData.length; i++) {
                var indx = arr.indexOf(allData[i]['suggestions_type']);
                arrCount[indx]++;
                var date = new Date().getHours() - new Date(allData[i]['suggestions_timestamp']).getHours();
                arrToday[indx] += arrToday[indx] + (date < 24) ? 1 : 0;
            }
            for (var c = 0; c < arr.length; c++) {
                $('#' + arr[c].replace(' ', '_')).find('h2').find('b').html(arrCount[c]);
                $('#' + arr[c].replace(' ', '_')).find('p').find('b').html(arrToday[c]);
            }

            loadData();

                $('#myLoader').modal('hide');

        });
    }

    getTraces();


    var selectedData;
    var selectedId;
    var selectedDataComments = Array();

    function loadData() {
        if (allData) {

            var str = "";
            for (i = 0; i < allData.length; i++) {
                if (allData[i]['suggestions_type'] == seletedTop) {
                    str += '<tr><td>' + (i + 1) + '</td>';
                    str += '<td>' + allData[i]['suggestions_cellphone'] + '</td>';
                    str += '<td>' + allData[i]['suggestions_type'] + '</td>';
                    str += '<td>' + allData[i]['suggestions_subject'] + '</td>';
                    str += '<td>' + allData[i]['suggestions_status'] + '</td>';
                    str += '<td>' + getDateTime(allData[i]['suggestions_timestamp']) + '</td>';
                    str += '<td><button id="b' + i + '"    style="width:100px" type="button" class="btn btn-warning btn-flat btn-xs btnView">View</button></td>';
                }
            }
            $('#tblReports').html(str);
        }
        $('.btnView').click(function () {
            selectedId = this.id.substring(1);
            $('#viewModal').modal('show');
            selectedData = allData[selectedId];
            showItem();
            selectedSugg = selectedData['suggestions_id'];
            getIdentified(selectedData['suggestions_registered_id'], selectedData['suggestions_type']);
        });
    }

    function showItem() {
        $('.clsReference').html(selectedData['suggestions_cellphone']);
        $('.clsDistance').html(selectedData['suggestions_type']);
        $('.clsSubject').html(selectedData['suggestions_subject']);
        $('.clsStatus').html(selectedData['suggestions_status']);
        $('.clsDate').html(getDateTime(selectedData['suggestions_timestamp']));
        $('.clsBody').html(selectedData['suggestions_body']);
        loadComment();
    }




    function loadComment() {
        var str = '';
        for (i = 0; i < selectedDataComments.length; i++) {
            str += '<div class="cd-timeline-block" style="margin-bottom:20px;margin-top:0px">' +
                '<div class="cd-timeline-content" style="width:98%;padding-bottom:5px;padding-top:5px">' +
                '<h5 class=""><span class="">' + selectedDataComments[i].comments_cellphone + '</span>' +
                '<small class="pull-right">' + getDatee(selectedDataComments[i].comments_timestamp) + '</small><br>' +
                '<small class="pull-right">' + getTime(selectedDataComments[i].comments_timestamp) + '</small>' +
                '</small></h5><p class="clsBody" style="font-size:12px">' + selectedDataComments[i].comments_body + '</p ></div></div>';
        }
        $('.cd-timelinee').html(str);
    }




    $(".btnClose").click(function () {
        $('#viewModal').modal('hide');
    });




    $(".btnWidget").click(function () {
        seletedTop = this.id.replace('_', ' ');
        $('.btnWidget').removeClass('active');
        $(this).addClass('active');
        $('.selectedTitle').html($(this).find('h4').text());
        loadData();

    });




    var identifiedData;

    function getIdentified(registered_id, suggestions_type) {

        $('#myLoader').modal('show');
        var details = {
            registered_id: registered_id,
            suggestions_type: suggestions_type
        };
        $.getJSON(http + 'getSuggestions', details, function (data) {
            identifiedData = data;
            var clickk = 0;

            var str = "";
            for (i = 0; i < identifiedData.length; i++) {
                if (selectedSugg == identifiedData[i]['suggestions']['comments_suggestion_id'])
                    clickk = i;
                str += '<tr><td>' + (i + 1) + '</td>';
                str += '<td>' + identifiedData[i]['suggestions']['suggestions_type'] + '</td>';
                str += '<td>' + identifiedData[i]['suggestions']['suggestions_subject'] + '</td>';
                str += '<td>' + getDatee(identifiedData[i]['suggestions']['suggestions_timestamp']) + '</td>';
                str += '<td><button id="p' + i + '"    style="width:100px" type="button" class="btn btn-primary btn-flat btn-xs btnViewr">View</button></td>';
            }
            $('#tblReportsHistory').html(str);

            setTimeout(function () {
                $(document).find('#p' + clickk).trigger('click');
                $('#myLoader').modal('hide');
            }, 100);


           
            $('.btnViewr').click(function () {
                var id = this.id.substring(1);
                selectedData = identifiedData[id]['suggestions'];
                selectedDataComments = identifiedData[id]['comments'];
                showItem();
            });

          


        });
    }








    function vaidateFields(clas) {
        $('.' + clas).css('border-color', 'silver');
        var input;
        $('.' + clas).map(function () {
            if (!$(this).val() || $(this).css('border-color') == 'red') {
                $(this).css('border-color', 'red');
                $(this).focus();
                input = $(this);
            }
        }).get();

        if (input) {
            input.focus();
            return true;
        }
        else
            return false;
    }

    function validateEmail(sEmail, msg) {
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(sEmail)) {
            return true;
        }
        else {
            //var msg = "Please enter valid email address.";
            checkEmptyFields(msg);
            return false;
        }
    }

    function SuccessMessage(msg) {
        $('#myPleaseWait').modal('show');
        $('#txtModalLoaderAlertMessage').hide;
        $('#txtModalLoaderSuccessMessage').show();
        $('#txtModalLoaderSuccessMessage b').html(msg);

        setTimeout(function () {
            $('#myPleaseWait').hide();
            $('#txtModalLoaderSuccessMessage').hide();
            $('#myPleaseWait').modal('hide');
        }, 3000);
    }

    function checkEmptyFields(msg) {

        $('#myPleaseWait').modal('show');
        $('#txtModalLoaderAlertMessage').show();
        $('#txtModalLoaderAlertMessage b').text(msg);

        setTimeout(function () {
            $('#myPleaseWait').hide();
            $('#myPleaseWait').modal('hide');
            $('#txtModalLoaderAlertMessage').hide();
        }, 3000);
    }



    /***************************************************************
                        Validations Ends
    ***************************************************************/



    //Removing  Messages CoOperation Services

    $(document).on('click', function () {
        if ($('#txtModalLoaderAlertMessage').is(':visible')) {
            $('#txtModalLoaderAlertMessage').hide();
        }
        if ($('#txtModalLoaderSuccessMessage').is(':visible')) {
            $('#txtModalLoaderSuccessMessage').hide();
        }

    });






});

