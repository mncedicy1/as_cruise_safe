﻿function checkPrev(myPrivs) {

    //-------------------- Check Prev ---------------------------//

    if ($('#lblUserType').text() != 'business') {
        if (myPrivs.indexOf(1002) >= 0) {

        } else {
            //   location.href = '/as_smart_connects/home';
        }
    }

    //-------------------- Check Prev ---------------------------//
}

$(document).ready(function () {


    var http = httpsapi + "/api/reporting/";
    var https = httpsapi + "/api/";
    //http = httpsapi_local + '/api/reporting/';


    var users = Array();
    var Teams = Array();
    var Suppliers = Array();
    var roles = Array();

    $('.open-left').trigger('click');
    $('#dashTop').show('slow');
    $('.noDashTop').hide('slow');
    $('#divFull').removeClass('pull-right').addClass('pull-left').css('margin-left', '-50px');
    $('.carousel').carousel({
        interval: false,
    });
    var chart_colors5 = [am5.color("#ff3102"),
    am5.color("#ff6b02"),
    am5.color("#ffa602"),
    am5.color("#ffd402"),
    am5.color("#fbff02"),
    am5.color("#b5ff02")
    ];



    var seletedTop = 'btnMap';

    var allData = Array();
    var Categories = Array();
    var arr = Array();
    var startDateMoment = moment().subtract(29, 'days');
    var endDateMoment = moment();

    var startDate = '';
    var endDate = '';

    cb(startDateMoment, endDateMoment);


    var arrCount = Array();
    var arrToday = Array();


    function createDataMix(type, type1) {
        var category = Array();
        var dataArray = Array();
        var series = Array(); 

        for (var i = 0; i < allData.length; i++) {
            var column = allData[i]['reporting_ticket'][type];
            column = type.indexOf('created_date') < 0 ? column : getDatee(column);
            var indx = category.indexOf(column);

            if (allData[i]['reporting_ticket']['ticket_category_id'] == Categories[seletedTop].category.category_id || seletedTop == '0') {

                if (indx < 0) {
                    category.push(column);
                    var obj = { category: column };
                    var arr = sortDesc(createDataWhereXY(type1, type, column));
                    for (var c = 0; c < arr.length; c++) {
                        var obj1 = {};
                        obj1[arr[c].column] = arr[c].value;
                        obj = { ...obj, ...obj1 };
                        if (series.indexOf(arr[c].column) < 0)
                            series.push(arr[c].column);
                    }
                    dataArray.push(obj);
                }
            }
           
        }
        console.log({ data: dataArray, series: series });
        return { data: dataArray, series: series };
    }



    function createDataWhere(type, whereType, whereValue) {
        var category = Array();
        var dataArray = Array();

        for (var i = 0; i < allData.length; i++) {
            var value = allData[i]['reporting_ticket'][whereType];
            value = whereType.indexOf('created_date') < 0 ? value : getDatee(value);

            if (value == whereValue) {
                var column = allData[i]['reporting_ticket'][type];
                column = type.indexOf('created_date') < 0 ? column : getDatee(column);

                var indx = category.indexOf(column);
                if (indx < 0) {
                    category.push(column);
                    dataArray.push({ column: column, value: 1 });
                }
                else {
                    dataArray[indx].value++;
                }
            }
        }
        return sortDesc(dataArray);
    }




    function createDataWhereXY(type, whereType, whereValue) {
        var category = Array();
        var dataArray = Array();

        for (var i = 0; i < allData.length; i++) {
            var value = allData[i]['reporting_ticket'][whereType];
            value = whereType.indexOf('created_date') < 0 ? value : getDatee(value);
            if (value == whereValue) {
                var column = allData[i]['reporting_ticket'][type];
                column = type.indexOf('created_date') < 0 ? column : getDatee(column);
                var indx = category.indexOf(column);
                if (indx < 0) {
                    category.push(column);
                    dataArray.push({ column: column, value: 1 });
                }
                else {
                    dataArray[indx].value++;
                }
            }
        }
        return sortDesc(dataArray);
    }



    function createData(type) {
        var category = Array();
        var dataArray = Array();

        for (var i = 0; i < allData.length; i++) {
            var column = allData[i]['reporting_ticket'][type];
            column = type.indexOf('created_date') < 0 ? column : getDatee(column);
            var indx = category.indexOf(column);
            if (indx < 0) {
                category.push(column);
                dataArray.push({ column: column, value: 1 });
            }
            else{
                dataArray[indx].value++;
            }
        }
        return sortDesc(dataArray);
    }



    function sortDesc(arr) {
        arr.sort(function (a, b) {
            return b.value - a.value;
        });
        return arr;
    }

    function sortStepDesc(arr) {
        arr.sort(function (a, b) {
            return b.steps - a.steps;
        });
        return arr;
    }


    var seriesCategory;
    var xAxisCategory;
    var seriesCategorySide;
    var dataCategory;
    var levelCategory = Array(['', '', '']);
    var legendCategorySide;

    var seriesRegion;
    var xAxisRegion;
    var legendRegionSide;
    var seriesRegionSide;
    var dataRegion;
    var levelRegion = Array(['', '', '']);

    var seriesUsers;
    var xAxisUsers;

    var dataUsers;


    function getReporting() {

        $('.btnBack').hide('fast');
        levelCategory = Array(['', '', '']);
        levelRegion = Array(['', '', '']);

        $('#myLoader').modal('show');


        var details = {
            reporting_client_id: $('#lblClientID').text(),
            reporting_start_date: startDate,
            reporting_end_date: endDate
        };

        $.getJSON(http + 'getReportingAllTicketsWithClosed', details, function (data) {
            console.log('getReportingAllTicketsWithClosed', data);
            allData = data;

            loadDataCharts();
            loadMap();
          
            setTimeout(function () {
                $('#myLoader').modal('hide');
            }, 1000);

        });
    }

    getUsers(true);
    getCategories();
  




    function loadDataCharts() {
        dataCategory = createData('ticket_category');
        xAxisCategory.data.setAll(dataCategory);
        seriesCategory.data.setAll(dataCategory);
        seriesCategorySide.data.setAll(dataCategory);
        legendCategorySide.data.setAll(seriesCategorySide.dataItems);

        dataRegion = createData('ticket_region_name');
        xAxisRegion.data.setAll(dataRegion);
        seriesRegion.data.setAll(dataRegion);
        seriesRegionSide.data.setAll(dataRegion.reverse());
        legendRegionSide.data.setAll(seriesRegionSide.dataItems);

        loadDataChartsUsers();
    }


    function loadDataChartsUsers() {

        var category = Array();
        dataUsers = Array();

        for (var i = 0; i < allData.length; i++) {
            for (var r = 0; r < allData[i]['reporters'].length; r++) {
                var reporter = allData[i]['reporters'][r].reporter;
                var column = reporter.reporter_registered_id;
                var indx = category.indexOf(column);
                if (indx < 0) {
                    category.push(column);
                    var pic = reporter.reporter_image_url ? reporter.reporter_image_url : "/Content/Images/default-user.png";
                    dataUsers.push({ id: column, name: phonenumber(reporter.reporter_name), steps: 1, pictureSettings: { src: pic }});
                }
                else {
                    dataUsers[indx].steps++;
                }
            }
        }
        dataUsers = sortStepDesc(dataUsers);
        seriesUsers.data.setAll(dataUsers);
        xAxisUsers.data.setAll(dataUsers);

        console.log('dataUsers', dataUsers);
    }









    //init charts
    chartdivCategory([]);
    chartdivRegion([]);
    chartdivUsers([]);






    function getCategories() {
        var details = {
            client_id: $('#lblClientID').text()
        };
        $.getJSON(http + 'getCategories', details, function (data) {
            console.log('getCategories', data);
            Categories = data;
            loadCategory();
        });
    }



    function loadCategory() {
        var str = '';

        for (var r = -1; r < Categories.length; r++) {
            var name = r < 0 ? 'All' : Categories[r].category.category_name;
            var id = r < 0 ? 0 : Categories[r].category.category_id;
            var isActive = r < 0 ? 'active' : '';
            arr.push(id); arrCount.push(0); arrToday.push(0);
            str += '<div class="col-sm-6 col-lg-4"><div class="panel text-center btnWidget ' + isActive + '" id="cat' + id + '">' +
                '<div class="panel-heading" style="padding-bottom: 0px;"><h4 class="panel-title text-cornsilk font-light">' + name + '</h4>' +
                '</div><div class="panel-body p-t-0" style="padding:10px;"><h2 class="m-t-0 m-b-0" >' +
                '<i class="fas fa-chart-line text-primary m-r-10"></i> <b class="text-white">0</b></h2>' +
                '<small class="text-cornsilk m-b-0 m-t-0"><b style="color:orange">0</b>  Older Then 5 Days</small></div></div></div>';
        }
        $('#listCategory').html(str);
    }

    
    function getStatus(status, date) {
        date = date / 24;

        if (status == 'resolved' || status == 'completed')
            return '<span class="badge badge-success" style="width:105px" data-toggle="tooltip" data-placement="top" title="Completed">' + status + '</span>';
        else if (date < 5)
            return '<span class="badge badge-warning" style="width:105px" data-toggle="tooltip" data-placement="top" title="Was updated more than Five days ago, Please update urgently">' + status + '</span>';
        else
            return '<span class="badge badge-danger" style="width:105px" data-toggle="tooltip" data-placement="top" title="Was updated more than ' + date + ' day(s) ago, Please update">' + status + '</span>';
    }


    


    function getAssigned(data) {
        var str = '';
        if (data.ticket_other == 'acknowledged')
            str = getName(data.ticket_created_by);
        else if (data.ticket_other == 'inspection' || data.ticket_other == 'inspected')
            str = getName(data.ticket_inspector_id);
        else if (data.ticket_other == 'fixing' || data.ticket_other == 'fixed')
            str = getName(data.ticket_fixer_id);
        else if (data.ticket_other == 'completed' || data.ticket_other == 'resolved')
            str = getName(data.ticket_action_by);

        return str;
    }






    $('#btnEmployee').click(function () {
        listUsersAll();
    });
    function listUsersAll() {
        var str = '';
        $('#btnAddNewUser').html('New User');
        $(".clsTitle").html('Select Employee');
        for (var m = 0; m < users.length; m++) {
            var name = users[m]['user_title'] + ' ' + users[m]['user_firstname'] + ' ' + users[m]['user_surname'];
            str += '<div class="col-sm-6 col-lg-6 memberr"><div class="panel text-center btnWidget3 userList" data-id="' + users[m]['user_id'] + '"  data-name="' + name + '">' +
                '<div class="panel-body p-t-10" style="padding: 20px;"><h4 class="m-t-0 m-b-15">' +
                '<i class="fas fa-user text-primary m-r-10"></i><b>' + name + '</b></h4>' +
                '<p class="text-muted m-b-0 m-t-20"> ' + users[m].user_email + '</p></div></div></div>';
        }
        $('#listAllDiv').html(str);
        listAll('btnEmployee');


    }
    function getUsers(loadMore) {

        var details = {
            clientid: $('#lblClientID').text()
        };

        $.getJSON('/admin/getUsers', details, function (data) {
            users = data;
            if (loadMore)
                getTeams();
            else
                listUsersAll();
        });
    }
    function getName(user_id) {
        var name = '';
        for (var l = 0; l < users.length; l++) {
            if (users[l]['user_id'] == user_id)
                name = users[l]['user_firstname'] + ' ' + users[l]['user_surname'];
        }
        return name;
    }




    $('#btnTeam').click(function () {
        listTeamsAll();
    });
    function getTeams() {

        var details = {
            client_id: $('#lblClientID').text()
        };
        console.log(details);
        $.getJSON(https + 'Organisation/get_teams', details, function (data) {
            console.log('getTeams', data);
            Teams = data;
            getSuppliers();
        });
    }
    function listTeamsAll() {
        var str = '';
        $('#btnAddNewUser').html('Team Management');
        $(".clsTitle").html('Select Team');
        for (var m = 0; m < Teams.length; m++) {
            str += '<div class="col-sm-6 col-lg-6 memberr"><div class="panel text-center btnWidget3 userList"  data-id="' +
                Teams[m]['cruise_safe_organo_team']['team_id'] + '" data-name="' + Teams[m]['cruise_safe_organo_team']['team_name'] + '">' +
                '<div class="panel-body p-t-10" style="padding: 20px;"><h4 class="m-t-0 m-b-15">' +
                '<i class="fas fa-users text-primary m-r-10"></i><b>' + Teams[m]['cruise_safe_organo_team']['team_name'] + '</b></h4>' +
                '<p class="text-muted m-b-0 m-t-20"> Team Leader: ' + getName(Teams[m]['cruise_safe_organo_team'].team_leader) + '</p></div></div></div>';
        }
        $('#listAllDiv').html(str);
        listAll('btnTeam');


    }




    $('#btnSupplier').click(function () {
        listSuppliersAll();
    });
    function getSuppliers() {
        var details = {
            client_id: $('#lblClientID').text()
        };

        $.getJSON(https + 'Organisation/get_suppliers', details, function (data) {
            console.log('getSuppliers', data);
            Suppliers = data;

            getRoles();
        });
    }
    function listSuppliersAll() {
        var str = '';
        $('#btnAddNewUser').html('Supplier Management');
        $(".clsTitle").html('Select Supplier');
        for (var m = 0; m < Suppliers.length; m++) {
            str += '<div class="col-sm-6 col-lg-6 memberr"><div class="panel text-center btnWidget3 userList" data-id="' +
                Suppliers[m]['cruise_safe_organo_supplier']['supplier_id'] + '" data-name="' + Suppliers[m]['cruise_safe_organo_supplier']['supplier_company_name'] + '">' +
                '<div class="panel-body p-t-10" style="padding: 20px;"><h4 class="m-t-0 m-b-15">' +
                '<i class="fas fa-users text-primary m-r-10"></i><b>' + Suppliers[m]['cruise_safe_organo_supplier']['supplier_company_name'] + '</b></h4>' +
                '<p class="text-muted m-b-0 m-t-20"> Contact Person: ' + getName(Suppliers[m]['cruise_safe_organo_supplier']['supplier_contact_person']) + '</p></div></div></div>';
        }
        $('#listAllDiv').html(str);
        listAll('btnSupplier');
    }




    function listAll(btn) {
        $("#txtSearch").val('');
        $('.btnPickOne').hide();
        $('.usersmodallg').modal('show');

        $("#txtSearch").on("keyup", function () {
            var value = $(this).val().toLowerCase();
            $("#listAllDiv div.memberr").filter(function () {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
            });
            $('#rows').html($("#listAllDiv div.memberr:visible").length);
        });


        $('#btnPickOne').on('click', function () {
            action2 = $(this).attr('data-id');
            $('#' + btn).attr('data-id', $(this).attr('data-id'));
            $('#' + btn).html($(this).attr('data-name')).css('border-color', 'rgba(42, 50, 60, 0.2)');

            $('.usersmodallg').modal('hide');
        });

        $(document).on('click', ".btnWidget3", function () {
            $(document).find(".btnWidget3").removeClass('active');
            $(this).addClass('active');
            $('#btnPickOne').show('slow').html('Select ' + $(this).attr('data-name')).attr('data-id', $(this).attr('data-id'));;
            $('#btnPickOne').attr('data-name', $(this).attr('data-name'));
        });


    }


    function getRoles() {

        roles = Array();

        var details = {
            clientid: $('#lblClientID').text(),
            //  userId: $('#lblUserID').text(),
        };
        $.getJSON('/admin/getUserRoles', details, function (data) {
            console.log('Roles', data);
            if (data) {
                roles = data['roles'];
                $('#cboRole').html('<option value="">-- Choose Role --</option>');
                for (var i = 0; i < data['roles'].length; i++) {
                    if (data['roles'][i].role_category == 'supplier') {
                        var htmlcontent = '<option value="' + data['roles'][i].role_id + '">' + data['roles'][i].role_name + '</option>';
                        $('#cboRole').append(htmlcontent);
                    }
                }
            }
        });

    }











    $(document).on('click', ".btnWidget", function () {
        $(document).find(".btnWidget").removeClass('active');
        $(this).addClass('active');
        seletedTop = this.id;
    });


    $('.btnMap').on('click', function () {
        if (seletedTop != 'btnMap')
            $('#divDate').animate({ 'margin-right': '80' }, 'slow').animate({ 'margin-top': '109' }, 'slow');
    });
    $('.btnChart').on('click', function () {
        if (seletedTop == 'btnMap')
            $('#divDate').animate({ 'margin-right': '20' }, 'slow').animate({ 'margin-top': '85' }, 'slow');
    });


    $(document).ready(function () {
        $(window).on('resize', function () {
            $('.divContainer').animate({ 'height': window.innerHeight - 160 }, 'slow');
            $('.divContainer1').animate({ 'height': window.innerHeight - 120 }, 'slow');
        });
    });



    $('html').css('overflow', 'hidden');

    
  



    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        startDate = start.format('YYYY-MM-DD'); 
        endDate = end.format('YYYY-MM-DD');
        $('.clsDateRage').html(start.format('MMMM D, YYYY') + '&nbsp;&nbsp;to&nbsp;&nbsp;' + end.format('MMMM D, YYYY'));
        if (heatmap)
            heatmap.setMap(null);
        getReporting();
    }

    $('#reportrange').daterangepicker({
        startDate: startDateMoment,
        endDate: endDateMoment,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

 














    

    
    /***************************************************************
                        Map Starts
    ***************************************************************/

    var selector2 = 'all';

    var mapData;
    var features = Array();
    var markers = Array();
    var heatmapData = Array();
    var heatmap;

    var map = new google.maps.Map(document.getElementById('dvMap1'), {
        zoom: 11,
        center: new google.maps.LatLng(-26.229813471359144, 27.983688070361314),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false,
        options: {
            gestureHandling: 'greedy'
        }
    });



    // Add a style-selector control to the map.
    var styleControl = document.getElementById('style-selector-control');
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(styleControl);

    var styleSelector = document.getElementById('style-selector');
    map.setOptions({ styles: styles[styleSelector.value] });
    map.setTilt(45);
    styleSelector.addEventListener('change', function () {
        if (styleSelector.value == 'satellite') {
            map.setMapTypeId(google.maps.MapTypeId.HYBRID);
            map.setOptions({ styles: null });
        }
        else {
            map.setMapTypeId(google.maps.MapTypeId.ROADMAP);
            map.setOptions({ styles: styles[styleSelector.value] });
        }
    });



    $.getJSON('/Content/files/geo.json', function (data) {
        mapData = data;
        features = map.data.addGeoJson(mapData);
    }); 

    map.data.setStyle({
        icon: '//example.com/path/to/image.png',
        fillColor: 'lime',
        strokeWeight: 1
    });

    var styleSelector1 = document.getElementById('style-selector1');
    styleSelector1.addEventListener('change', function () {
        for (var i = 0; i < features.length; i++)
            map.data.remove(features[i]);
        heatmap.setMap(null);
        for (var i = 0; i < markers.length; i++) 
            markers[i].setMap(null);

        if (styleSelector1.value == 'All') {
            features = map.data.addGeoJson(mapData);
            heatmap.setMap(map);
            for (var i = 0; i < markers.length; i++)
                markers[i].setMap(map);
        }
        else if (styleSelector1.value == 'Reports') {
            for (var i = 0; i < markers.length; i++)
                markers[i].setMap(map);
        }
        else if (styleSelector1.value == 'Heat') {
            heatmap.setMap(map);
        }
        else if (styleSelector1.value == 'Shape') {
            features = map.data.addGeoJson(mapData);
        }
    });





    var styleSelector2 = document.getElementById('style-selector2');
    styleSelector2.addEventListener('change', function () {
        selector2 = styleSelector2.value;
        if (heatmap)
            heatmap.setMap(null);
        loadMap();
    });





    map.data.addListener('click', function (event) {
        var bounds = new google.maps.LatLngBounds();
        event.feature.h.h[0].h.forEach(function (coord, index) {
            bounds.extend(coord);
        });
      //  map.setZoom(11);
      
        map.setCenter(bounds.getCenter());
        map.fitBounds(bounds);
    });
    google.maps.event.addListenerOnce(map, 'bounds_changed', function () {
        map.setZoom(map.getZoom() - 1);
    });


    map.data.addListener('mouseover', function (event) {
        map.data.overrideStyle(event.feature, { fillColor: 'red' });
    });
    map.data.addListener('mouseout', function (event) {
        map.data.overrideStyle(event.feature, { fillColor: 'lime' });
    });

    map.data.addListener('mouseover', mouseOverDataItem);
    map.data.addListener('mouseout', mouseOutOfDataItem);

    function mouseOverDataItem(mouseEvent) {
        const titleText = mouseEvent.feature.j.REGIONNAME;
        if (titleText) {
            map.getDiv().setAttribute('title', titleText);
        }
    }

    function mouseOutOfDataItem(mouseEvent) {
        map.getDiv().removeAttribute('title');
    }





    function loadMap() {
 
        clearMarkers();
        heatmapData = Array();


        var latlngbounds = new google.maps.LatLngBounds();
        for (var i = 0; i < allData.length; i++) {

            if (selector2 == 'all' || selector2 == allData[i]['reporting_ticket'].ticket_status) {

                var latLng = new google.maps.LatLng(parseFloat(allData[i]['reporting_ticket'].ticket_latitude)
                    , parseFloat(allData[i]['reporting_ticket'].ticket_longitude));


                var date = getHours(allData[i]['reporting_ticket']['ticket_created_date']);

                heatmapData.push({ location: latLng, weight: 0.2 })

                var div = document.createElement('div');
                div.className = (allData[i]['reporting_ticket']['ticket_status'] == 'new') ? 'ripple-redblue' :
                    (allData[i]['reporting_ticket']['ticket_status'] == 'acknowledged') ? 'ripple-red2' :
                        (allData[i]['reporting_ticket']['ticket_status'] == 'processing') ? 'ripple-redpep' :
                        (allData[i]['reporting_ticket']['ticket_status'] == 'paused') ? 'ripple-red1' :
                                (allData[i]['reporting_ticket']['ticket_status'] == 'out of bounds') ? 'ripple-red' :
                                    (allData[i]['reporting_ticket']['ticket_status'] == 'resolved') ? 'ripple-red3' :'ripple-redsky';


                var marker = new RichMarker({
                    position: latLng,
                    map: map,
                    id: i,
                    flat: true,
                    content: div,
                    draggable: false,
                    animation: google.maps.Animation.DROP,
                    title: allData[i]['reporting_ticket'].ticket_category + ' - ' + allData[i]['reporting_ticket'].ticket_sub_category
                });
                markers.push(marker);

                (function (marker) {
                    google.maps.event.addListener(marker, "click", function (e) {
                        if (map.getZoom() != 18) {
                            map.setZoom(18);
                            map.setCenter(marker.getPosition());
                        } else {
                            loadSelected(marker.id);
                        }
                    });

                    google.maps.event.addListener(marker, "dblclick", function (e) {
                        loadSelected(marker.id);
                    });

                })(marker);
                latlngbounds.extend(marker.position);
            }
        }





       
        heatmap = new google.maps.visualization.HeatmapLayer({
            data: heatmapData
        });

        heatmap.setMap(map);
        heatmap.set("radius", heatmap.get("radius") ? null : 50);

        const trafficLayer = new google.maps.TrafficLayer();

        trafficLayer.setMap(map);

        //if (allData.length > 0)
        //    map.setCenter(latlngbounds.getCenter());
        //else
        //    map.setCenter(new google.maps.LatLng(-25.7307893, 28.007999));
        //map.setZoom(10);


    }








    function loadMapStatus() {

        clearMarkers();
        heatmapData = Array();


        var latlngbounds = new google.maps.LatLngBounds();
        for (var i = 0; i < allDataStatus.length; i++) {


                var latLng = new google.maps.LatLng(parseFloat(allDataStatus[i].reporting_latitude)
                    , parseFloat(allDataStatus[i].reporting_longitude));


                var date = getHours(allDataStatus[i]['reporting_timestamp']);

                heatmapData.push({ location: latLng, weight: date / 100 })

                var div = document.createElement('div');
                div.className = 'ripple-red1';


                var marker = new RichMarker({
                    position: latLng,
                    map: map,
                    id: i,
                    flat: true,
                    content: div,
                    draggable: false,
                    animation: google.maps.Animation.DROP,
                    title: allDataStatus[i].reporting_category + ' - ' + allDataStatus[i].reporting_sub_category
                });
                markers.push(marker);

                (function (marker) {
                    google.maps.event.addListener(marker, "click", function (e) {
                        if (map.getZoom() != 18) {
                            map.setZoom(18);
                            map.setCenter(marker.getPosition());
                        } else {
                            loadSelected(marker.id);
                        }
                    });

                    google.maps.event.addListener(marker, "dblclick", function (e) {
                        loadSelected(marker.id);
                    });

                })(marker);
                latlngbounds.extend(marker.position);
            
        }


        heatmap = new google.maps.visualization.HeatmapLayer({
            data: heatmapData
        });

        heatmap.setMap(map);
        heatmap.set("radius", heatmap.get("radius") ? null : 50);

        const trafficLayer = new google.maps.TrafficLayer();

        trafficLayer.setMap(map);

        //if (allDataStatus.length > 0)
        //    map.setCenter(latlngbounds.getCenter());
        //else
        //    map.setCenter(new google.maps.LatLng(-25.7307893, 28.007999));
        //map.setZoom(10);


    }



    function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
        markers = Array();
    }

    function clearMarkers() {
        setMapOnAll(null);
    }

    function showMarkers() {
        setMapOnAll(map);
    }

    function deleteMarkers() {
        clearMarkers();
        markers = [];
    }


  /***************************************************************
                        Map Ends
    ***************************************************************/
































       /***************************************************************
                        Category Starts
    ***************************************************************/


 

    function chartdivCategory(data) {

        var root = am5.Root.new("chartdivCategory");
        root.setThemes([
            am5themes_Animated.new(root)
        ]);

        var chart = root.container.children.push(am5xy.XYChart.new(root, {
            panX: true,
            panY: true,
            wheelX: "panX",
            wheelY: "zoomX"
        }));

        var cursor = chart.set("cursor", am5xy.XYCursor.new(root, {}));
        cursor.lineY.set("visible", true);

        chart.get("colors").set("colors", chart_colors5);

        // Create axes
        var xRenderer = am5xy.AxisRendererX.new(root, { minGridDistance: 30 });
        xRenderer.labels.template.setAll({
            rotation: -20,
            centerY: am5.p50,
            centerX: am5.p100,
            paddingRight: 15
        });

        xAxisCategory = chart.xAxes.push(am5xy.CategoryAxis.new(root, {
            maxDeviation: 0.3,
            categoryField: "column",
            renderer: xRenderer,
            tooltip: am5.Tooltip.new(root, {})
        }));

        var yAxis = chart.yAxes.push(am5xy.ValueAxis.new(root, {
            maxDeviation: 0.3,
            renderer: am5xy.AxisRendererY.new(root, {})
        }));


        // Create series
        seriesCategory = chart.series.push(am5xy.ColumnSeries.new(root, {
            name: "Series 1",
            xAxis: xAxisCategory,
            yAxis: yAxis,
            valueYField: "value",
            sequencedInterpolation: true,
            categoryXField: "column",
            tooltip: am5.Tooltip.new(root, {
                labelText: "{valueY}"
            })
        }));
        seriesCategory.columns.template.setAll({ cornerRadiusTL: 5, cornerRadiusTR: 5 });
        seriesCategory.columns.template.adapters.add("fill", (fill, target) => {
            return chart.get("colors").getIndex(seriesCategory.columns.indexOf(target));
        });

        seriesCategory.columns.template.adapters.add("stroke", (stroke, target) => {
            return chart.get("colors").getIndex(seriesCategory.columns.indexOf(target));
        });


        // Set data

        xAxisCategory.data.setAll(data);
        seriesCategory.data.setAll(data);



        // Make stuff animate on load
        // https://www.amcharts.com/docs/v5/concepts/animations/
        seriesCategory.appear(1000);
        chart.appear(1000, 100);

        seriesCategory.columns.template.events.once("click", function (ev) {
            console.log("Clicked on a column", ev.target._dataItem.dataContext);

            if (levelCategory.length == 1) {
                data = createDataWhere('ticket_sub_category', 'ticket_category', ev.target._dataItem.dataContext.column);
                levelCategory.push(['ticket_sub_category', 'ticket_category', ev.target._dataItem.dataContext.column]);
                $('#btnBackCategory').show('fast');
            }
            else if (levelCategory.length == 2) {
                data = createDataWhere('ticket_created_date', 'ticket_sub_category', ev.target._dataItem.dataContext.column);
                levelCategory.push(['ticket_created_date', 'ticket_sub_category', ev.target._dataItem.dataContext.column]);
            }
            else if (levelCategory.length == 3) {
                data = createDataWhere('ticket_title', 'ticket_created_date', ev.target._dataItem.dataContext.column);
                levelCategory.push(['ticket_title', 'ticket_created_date', ev.target._dataItem.dataContext.column]);
            }
            else {
                return;
            }

           

            console.log("Clicked on a column", levelCategory);

            xAxisCategory.data.setAll(data);
            seriesCategory.data.setAll(data);
            seriesCategorySide.data.setAll(data);
        });

        $('#btnBackCategory').click(function () {

       

            var indx = levelCategory.length - 1;
            if (indx > 1) {
                levelCategory.splice(indx, 1);
                data = createDataWhere(levelCategory[indx-1][0] + '', levelCategory[indx-1][1] + '', levelCategory[indx-1][2] + '');
            }
            else if (indx == 1) {
                data = createData('ticket_category');
                $('#btnBackCategory').hide('fast');
                levelCategory.splice(indx, 1);
            }
            else {
                return;
            }

           

            console.log("Clicked on a column", levelCategory,data);

            xAxisCategory.data.setAll(data);
            seriesCategory.data.setAll(data);
            seriesCategorySide.data.setAll(data.reverse());

            

        });


        chartdivCategorySide(data);
    }
   
    function chartdivCategorySide(data) {



            // Create root element
            // https://www.amcharts.com/docs/v5/getting-started/#Root_element
        var root = am5.Root.new("chartdivCategorySide");

            // Set themes
            // https://www.amcharts.com/docs/v5/concepts/themes/
            root.setThemes([
                am5themes_Animated.new(root)
            ]);

            // Create chart
            // https://www.amcharts.com/docs/v5/charts/percent-charts/pie-chart/
            var chart = root.container.children.push(
                am5percent.PieChart.new(root, {
                    endAngle: 270,
                    layout: root.horizontalLayout

                })
            );

            // Create series
            // https://www.amcharts.com/docs/v5/charts/percent-charts/pie-chart/#Series
            seriesCategorySide = chart.series.push(
                am5percent.PieSeries.new(root, {
                    valueField: "value",
                    categoryField: "column",
                    endAngle: 270
                })
            );
        seriesCategorySide.get("colors").set("colors", chart_colors5);

        // Disabling labels and ticks
        seriesCategorySide.labels.template.set("visible", false);
        seriesCategorySide.ticks.template.set("visible", false);

        // Adding gradients
        seriesCategorySide.slices.template.set("strokeOpacity", 0);
        seriesCategorySide.slices.template.set("fillGradient", am5.RadialGradient.new(root, {
            stops: [{
                brighten: 1.5
            }, {
                brighten: 1
            }, {
                brighten: 0.5
            }, {
                brighten: 0
            }, {
                brighten:-0.1
            }]
        }));


        // Create legend
        // https://www.amcharts.com/docs/v5/charts/percent-charts/legend-percent-series/
        legendCategorySide = chart.children.push(am5.Legend.new(root, {
            centerY: am5.percent(50),
            y: am5.percent(50),
            marginTop: 15,
            marginBottom: 15,
            layout: root.horizontalLayout
        }));

        legendCategorySide.data.setAll(seriesCategorySide.dataItems);




            seriesCategorySide.states.create("hidden", {
                endAngle: -90
            });

            // Set data
            // https://www.amcharts.com/docs/v5/charts/percent-charts/pie-chart/#Setting_data
            seriesCategorySide.data.setAll(data);

            seriesCategorySide.appear(1000, 100);





    }

 



           /***************************************************************
                        Category ends
    ***************************************************************/
















    /***************************************************************
                     Region Starts
 ***************************************************************/


    

    function chartdivRegion(data) {

        var root = am5.Root.new("chartdivRegion");
        root.setThemes([
            am5themes_Animated.new(root)
        ]);

        var chart = root.container.children.push(am5xy.XYChart.new(root, {
            panX: true,
            panY: true,
            wheelX: "panX",
            wheelY: "zoomX"
        }));

        var cursor = chart.set("cursor", am5xy.XYCursor.new(root, {}));
        cursor.lineY.set("visible", false);

        chart.get("colors").set("colors", chart_colors5);
        // Create axes
        var xRenderer = am5xy.AxisRendererX.new(root, { minGridDistance: 30 });
        xRenderer.labels.template.setAll({
            rotation: -30,
            centerY: am5.p50,
            centerX: am5.p100,
            paddingRight: 15
        });

        xAxisRegion = chart.xAxes.push(am5xy.CategoryAxis.new(root, {
            maxDeviation: 0.3,
            categoryField: "column",
            renderer: xRenderer,
            tooltip: am5.Tooltip.new(root, {})
        }));

        var yAxis = chart.yAxes.push(am5xy.ValueAxis.new(root, {
            maxDeviation: 0.3,
            renderer: am5xy.AxisRendererY.new(root, {})
        }));


        // Create series
        seriesRegion = chart.series.push(am5xy.ColumnSeries.new(root, {
            name: "Series 1",
            xAxis: xAxisRegion,
            yAxis: yAxis,
            valueYField: "value",
            sequencedInterpolation: true,
            categoryXField: "column",
            tooltip: am5.Tooltip.new(root, {
                labelText: "{valueY}"
            })
        }));
        seriesRegion.columns.template.setAll({ cornerRadiusTL: 5, cornerRadiusTR: 5 });
        seriesRegion.columns.template.adapters.add("fill", (fill, target) => {
            return chart.get("colors").getIndex(seriesRegion.columns.indexOf(target));
        });

        seriesRegion.columns.template.adapters.add("stroke", (stroke, target) => {
            return chart.get("colors").getIndex(seriesRegion.columns.indexOf(target));
        });


        xAxisRegion.data.setAll(data);
        seriesRegion.data.setAll(data);



        // Make stuff animate on load
        // https://www.amcharts.com/docs/v5/concepts/animations/
        seriesRegion.appear(1000);
        chart.appear(1000, 100);

        console.log("data on a column", data);

        seriesRegion.columns.template.events.once("click", function (ev) {
            console.log("Clicked on a column", ev.target._dataItem.dataContext);

            if (levelRegion.length == 1) {
                data = createDataWhere('ticket_town', 'ticket_region_name', ev.target._dataItem.dataContext.column);
                levelRegion.push(['ticket_town', 'ticket_region_name', ev.target._dataItem.dataContext.column]);
                $('#btnBackRegion').show('fast');
            }
            else if (levelRegion.length == 2) {
                data = createDataWhere('ticket_created_date', 'ticket_town', ev.target._dataItem.dataContext.column);
                levelRegion.push(['ticket_created_date', 'ticket_town', ev.target._dataItem.dataContext.column]);
            }
            else if (levelRegion.length == 3) {
                data = createDataWhere('ticket_title', 'ticket_created_date', ev.target._dataItem.dataContext.column);
                levelRegion.push(['ticket_title', 'ticket_created_date', ev.target._dataItem.dataContext.column]);
            }
            else {
                return;
            }



            console.log("Clicked on a column", levelRegion);

            xAxisRegion.data.setAll(data);
            seriesRegion.data.setAll(data);
            seriesRegionSide.data.setAll(data.reverse());
            legendRegionSide.data.setAll(seriesRegionSide.dataItems);
        });


        chartdivRegionSide(data);


        $('#btnBackRegion').click(function () {



            var indx = levelRegion.length - 1;
            if (indx > 1) {
                levelRegion.splice(indx, 1);
                data = createDataWhere(levelRegion[indx - 1][0] + '', levelRegion[indx - 1][1] + '', levelRegion[indx - 1][2] + '');
            }
            else if (indx == 1) {
                data = createData('ticket_region_name');
                $('#btnBackRegion').hide('fast');
                levelRegion.splice(indx, 1);
            }
            else {
                return;
            }



            console.log("Clicked on a column", levelRegion, data);

            xAxisRegion.data.setAll(data);
            seriesRegion.data.setAll(data);
            seriesRegionSide.data.setAll(data.reverse());
            legendRegionSide.data.setAll(seriesRegionSide.dataItems);


        });

    }


    function chartdivRegionSide(data) {



        // Create root
        var root = am5.Root.new("chartdivRegionSide");

        // Set themes
        // https://www.amcharts.com/docs/v5/concepts/themes/
        root.setThemes([
            am5themes_Animated.new(root)
        ]);

        // Create chart
        // https://www.amcharts.com/docs/v5/charts/percent-charts/pie-chart/
        var chart = root.container.children.push(am5percent.PieChart.new(root, {
            radius: am5.percent(90),
            innerRadius: am5.percent(50),
            layout: root.horizontalLayout
        }));

        // Create series
        // https://www.amcharts.com/docs/v5/charts/percent-charts/pie-chart/#Series
        seriesRegionSide = chart.series.push(am5percent.PieSeries.new(root, {
            name: "Series",
            valueField: "value",
            categoryField: "column"
        }));
        seriesRegionSide.get("colors").set("colors", chart_colors5);
        // Set data
        // https://www.amcharts.com/docs/v5/charts/percent-charts/pie-chart/#Setting_data
        seriesRegionSide.data.setAll(data);

        // Disabling labels and ticks
        seriesRegionSide.labels.template.set("visible", false);
        seriesRegionSide.ticks.template.set("visible", false);

        // Adding gradients
        seriesRegionSide.slices.template.set("strokeOpacity", 0);
        seriesRegionSide.slices.template.set("fillGradient", am5.RadialGradient.new(root, {
            stops: [{
                brighten: -0.8
            }, {
                brighten: -0.8
            }, {
                brighten: -0.5
            }, {
                brighten: 0
            }, {
                brighten: -0.5
            }]
        }));

        // Create legend
        // https://www.amcharts.com/docs/v5/charts/percent-charts/legend-percent-series/
        legendRegionSide = chart.children.push(am5.Legend.new(root, {
            centerY: am5.percent(50),
            y: am5.percent(50),
            marginTop: 15,
            marginBottom: 15,
            layout: root.verticalLayout
        }));

        legendRegionSide.data.setAll(seriesRegionSide.dataItems);


        // Play initial series animation
        // https://www.amcharts.com/docs/v5/concepts/animations/#Animation_of_series
        seriesRegionSide.appear(1000, 100);

    }

  



           /***************************************************************
                        Region ends
    ***************************************************************/


















           /***************************************************************
                        Users starts
    ***************************************************************/

 

    function chartdivUsers(data) {

 


        // Create root element
        // https://www.amcharts.com/docs/v5/getting-started/#Root_element
        var root = am5.Root.new("chartdivUsers");

        // Set themes
        // https://www.amcharts.com/docs/v5/concepts/themes/
        root.setThemes([
            am5themes_Animated.new(root)
        ]);

        

        // Create chart
        // https://www.amcharts.com/docs/v5/charts/xy-chart/
        var chart = root.container.children.push(
            am5xy.XYChart.new(root, {
                panX: false,
                panY: false,
                wheelX: "none",
                wheelY: "none",
                paddingBottom: 50,
                paddingTop: 40
            })
        );

        // Create axes
        // https://www.amcharts.com/docs/v5/charts/xy-chart/axes/

        var xRenderer = am5xy.AxisRendererX.new(root, {});
        xRenderer.grid.template.set("visible", false);
        xRenderer.labels.template.setAll({
            rotation: -25,
            centerY: am5.p50,
            centerX: am5.p100,
            paddingRight: 15
        });

        xAxisUsers = chart.xAxes.push(
            am5xy.CategoryAxis.new(root, {
                paddingTop: 40,
                categoryField: "name",
                renderer: xRenderer
            })
        );






        var yRenderer = am5xy.AxisRendererY.new(root, {});
        yRenderer.grid.template.set("strokeDasharray", [3]);

        var yAxis = chart.yAxes.push(
            am5xy.ValueAxis.new(root, {
                min: 0,
                renderer: yRenderer
            })
        );

        // Add series
        // https://www.amcharts.com/docs/v5/charts/xy-chart/series/
        seriesUsers = chart.series.push(
            am5xy.ColumnSeries.new(root, {
                name: "Reporter",
                xAxis: xAxisUsers,
                yAxis: yAxis,
                valueYField: "steps",
                categoryXField: "name",
                sequencedInterpolation: true,
                calculateAggregates: true,
                maskBullets: false,
                tooltip: am5.Tooltip.new(root, {
                    dy: -30,
                    pointerOrientation: "vertical",
                    labelText: "{categoryX} : {valueY}"
                })
            })
        );

        seriesUsers.columns.template.setAll({
            strokeOpacity: 0,
            cornerRadiusBR: 10,
            cornerRadiusTR: 10,
            cornerRadiusBL: 10,
            cornerRadiusTL: 10,
            maxWidth: 50,
            fillOpacity: 0.8
        });

        var currentlyHovered;


        seriesUsers.columns.template.events.once("click", function (ev) {
            console.log("Clicked on a column", ev.target._dataItem.dataContext);
            modalShow('.appUserModal');
        });

        seriesUsers.columns.template.events.on("pointerover", function (e) {
            handleHover(e.target.dataItem);
        });

        seriesUsers.columns.template.events.on("pointerout", function (e) {
            handleOut();
        });

        function handleHover(dataItem) {
            if (dataItem && currentlyHovered != dataItem) {
                handleOut();
                currentlyHovered = dataItem;
                var bullet = dataItem.bullets[0];
                bullet.animate({
                    key: "locationY",
                    to: 1,
                    duration: 600,
                    easing: am5.ease.out(am5.ease.cubic)
                });
            }
        }

        function handleOut() {
            if (currentlyHovered) {
                var bullet = currentlyHovered.bullets[0];
                bullet.animate({
                    key: "locationY",
                    to: 0,
                    duration: 600,
                    easing: am5.ease.out(am5.ease.cubic)
                });
            }
        }

        var circleTemplate = am5.Template.new({});

        seriesUsers.bullets.push(function (root, seriesUsers, dataItem) {
            var bulletContainer = am5.Container.new(root, {});
            var circle = bulletContainer.children.push(
                am5.Circle.new(
                    root,
                    {
                        radius: 34
                    },
                    circleTemplate
                )
            );

            var maskCircle = bulletContainer.children.push(
                am5.Circle.new(root, { radius: 27 })
            );

            // only containers can be masked, so we add image to another container
            var imageContainer = bulletContainer.children.push(
                am5.Container.new(root, {
                    mask: maskCircle
                })
            );

            var image = imageContainer.children.push(
                am5.Picture.new(root, {
                    templateField: "pictureSettings",
                    centerX: am5.p50,
                    centerY: am5.p50,
                    width: 60,
                    height: 60
                })
            );

            return am5.Bullet.new(root, {
                locationY: 0,
                sprite: bulletContainer
            });
        });

        // heatrule
        seriesUsers.set("heatRules", [
            {
                dataField: "valueY",
                min: am5.color("#fbff02"),
                max: am5.color("#ff5f02"),
                target: seriesUsers.columns.template,
                key: "fill"
            },
            {
                dataField: "valueY",
                min: am5.color("#fbff02"),
                max: am5.color("#ff5f02"),
                target: circleTemplate,
                key: "fill"
            }
        ]);

        seriesUsers.data.setAll(data);
        xAxisUsers.data.setAll(data);


        var cursor = chart.set("cursor", am5xy.XYCursor.new(root, {}));
        cursor.lineX.set("visible", true);
        cursor.lineY.set("visible", true);

        cursor.events.on("cursormoved", function () {
            var dataItem = seriesUsers.get("tooltip").dataItem;
            if (dataItem) {
                handleHover(dataItem);
            } else {
                handleOut();
            }
        });

        // Make stuff animate on load
        // https://www.amcharts.com/docs/v5/concepts/animations/
        seriesUsers.appear();
        chart.appear(1000, 100);


    }





           /***************************************************************
                        Users ends
    ***************************************************************/







});



















