﻿function checkPrev(myPrivs) {

    //-------------------- Check Prev ---------------------------//

    if ($('#lblUserType').text() != 'business') {
        if (myPrivs.indexOf(1002) >= 0) {

        } else {
            //   location.href = '/as_smart_connects/home';
        }
    }

    //-------------------- Check Prev ---------------------------//
}

$(document).ready(function () {


    var http = httpsapi + "/api/reporting/";
    var https = httpsapi + "/api/";
    //http = httpsapi_local + '/api/reporting/';


    var users = Array();
    var Teams = Array();
    var Suppliers = Array();
    var roles = Array();

    $('.open-left').trigger('click');
    $('#dashTop').show('slow');
    $('.noDashTop').hide('slow');
    $('#divFull').removeClass('pull-right').addClass('pull-left').css('margin-left', '-50px');
    $('.carousel').carousel({
        interval: false,
    });
 

    var seletedTop = 'btnMap';

    var allDataAction = Array();
    var allData = Array();
    var Categories = Array();
    var arr = Array();
    var startDateMoment = moment().subtract(29, 'days');
    var endDateMoment = moment();

    var startDate = '';
    var endDate = '';





    cb(startDateMoment, endDateMoment);


    var arrCount = Array();
    var arrToday = Array();


    var table = $('#example').DataTable({
        "processing": true,
        data: [],
        scrollY: '50vh',
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        scrollCollapse: true,
        "columns": [
            { "data": "reporting_ticket.ticket_reference", title: "Reference" },
            { "data": "reporting_ticket.ticket_region_name", title: "Region" },
            { "data": "reporting_ticket.ticket_other", title: "Status" },
            { "data": "reporting_ticket.ticket_category", title: "Category" },
            { "data": "reporting_ticket.ticket_sub_category", title: "Sub-Category" },
            {
                "data": "reporting_ticket.ticket_created_date", title: "Date", render: function (data, type, row) {
                    return getDatee(data);
                }
            }
        ],
        dom: 'Bfrtip',
        buttons: [
            'colvis',
            'excel',
            'pdf',
            {
                className: 'btnDateRage',
                text: $('.clsDateRage').html(),
                action: function (e, dt, node, config) {
                    $('#reportrange').trigger('click'); 
                }
            }
        ]
    });
    $('.dt-button').addClass('btn-warning').addClass('btn-sm');



    function getReporting() {

        $('.btnBack').hide('fast');
        levelCategory = Array(['', '', '']);
        levelRegion = Array(['', '', '']);

        $('#myLoader').modal('show');


        var details = {
            reporting_client_id: $('#lblClientID').text(),
            reporting_start_date: startDate,
            reporting_end_date: endDate
        };

        console.log('getReportingAllTicketsWithClosed', details);

        $.getJSON(http + 'getReportingAllTicketsWithClosed', details, function (data) {
            console.log('getReportingAllTicketsWithClosed', data, table);
            allData = data;

            setTimeout(function () {
                $('#myLoader').modal('hide');
            }, 1000);

            table.clear();
            var rowNode = table.rows.add(data).draw().nodes()
                .to$()
                .addClass('new');
        
           
        });

        getTicketAction();
    }














    var tableAction = $('#exampleAction').DataTable({
        "processing": true,
        data: [],
        scrollY: '50vh',
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        scrollCollapse: true,
        "columns": [
            { "data": "action_ticket_reference", title: "Reference" },
            { "data": "action_assigned_by_name", title: "User" },
            { "data": "action_other", title: "Progress" },
            { "data": "action_status", title: "Main Status" },
            { "data": "action_name", title: "New Status" },
            { "data": "action_prev_status", title: "Prev Status" },
            {
                "data": "action_timestamp", title: "Date", render: function (data, type, row) {
                    return getDatee(data);
                }
            }
        ],
        dom: 'Bfrtip',
        buttons: [
            'colvis',
            'excel',
            'pdf',
            {
                className: 'btnDateRage',
                text: $('.clsDateRage').html(),
                action: function (e, dt, node, config) {
                    $('#reportrange').trigger('click');
                }
            }
        ]
    });
    $('.dt-button').addClass('btn-warning').addClass('btn-sm');



    function getTicketAction() {

        var details = {
            action_client_id: $('#lblClientID').text(),
            action_start_date: startDate,
            action_end_date: endDate
        };

        $.getJSON(http + 'getTicketAction', details, function (data) {
            console.log('getTicketAction', data);
            allDataAction = data;
            setTimeout(function () {
                $('#myLoader').modal('hide');
            }, 1000);


            tableAction.clear();
            var rowNode = tableAction.rows.add(allDataAction).draw().nodes()
                .to$()
                .addClass('new');


        });
    }



 









    $('#cmbFilter').change(function () {
        table
            .search($(this).val())
            .draw();
    });










    function createDataMix(type, type1) {
        var category = Array();
        var dataArray = Array();
        var series = Array(); 

        for (var i = 0; i < allData.length; i++) {
            var column = allData[i]['reporting_ticket'][type];
            column = type.indexOf('created_date') < 0 ? column : getDatee(column);
            var indx = category.indexOf(column);

            if (allData[i]['reporting_ticket']['ticket_category_id'] == Categories[seletedTop].category.category_id || seletedTop == '0') {

                if (indx < 0) {
                    category.push(column);
                    var obj = { category: column };
                    var arr = sortDesc(createDataWhereXY(type1, type, column));
                    for (var c = 0; c < arr.length; c++) {
                        var obj1 = {};
                        obj1[arr[c].column] = arr[c].value;
                        obj = { ...obj, ...obj1 };
                        if (series.indexOf(arr[c].column) < 0)
                            series.push(arr[c].column);
                    }
                    dataArray.push(obj);
                }
            }
           
        }
        console.log({ data: dataArray, series: series });
        return { data: dataArray, series: series };
    }



    function createDataWhere(type, whereType, whereValue) {
        var category = Array();
        var dataArray = Array();

        for (var i = 0; i < allData.length; i++) {
            var value = allData[i]['reporting_ticket'][whereType];
            value = whereType.indexOf('created_date') < 0 ? value : getDatee(value);

            if (value == whereValue) {
                var column = allData[i]['reporting_ticket'][type];
                column = type.indexOf('created_date') < 0 ? column : getDatee(column);

                var indx = category.indexOf(column);
                if (indx < 0) {
                    category.push(column);
                    dataArray.push({ column: column, value: 1 });
                }
                else {
                    dataArray[indx].value++;
                }
            }
        }
        return sortDesc(dataArray);
    }




    function createDataWhereXY(type, whereType, whereValue) {
        var category = Array();
        var dataArray = Array();

        for (var i = 0; i < allData.length; i++) {
            var value = allData[i]['reporting_ticket'][whereType];
            value = whereType.indexOf('created_date') < 0 ? value : getDatee(value);
            if (value == whereValue) {
                var column = allData[i]['reporting_ticket'][type];
                column = type.indexOf('created_date') < 0 ? column : getDatee(column);
                var indx = category.indexOf(column);
                if (indx < 0) {
                    category.push(column);
                    dataArray.push({ column: column, value: 1 });
                }
                else {
                    dataArray[indx].value++;
                }
            }
        }
        return sortDesc(dataArray);
    }



    function createData(type) {
        var category = Array();
        var dataArray = Array();

        for (var i = 0; i < allData.length; i++) {
            var column = allData[i]['reporting_ticket'][type];
            column = type.indexOf('created_date') < 0 ? column : getDatee(column);
            var indx = category.indexOf(column);
            if (indx < 0) {
                category.push(column);
                dataArray.push({ column: column, value: 1 });
            }
            else{
                dataArray[indx].value++;
            }
        }
        return sortDesc(dataArray);
    }



    function sortDesc(arr) {
        arr.sort(function (a, b) {
            return b.value - a.value;
        });
        return arr;
    }

    function sortStepDesc(arr) {
        arr.sort(function (a, b) {
            return b.steps - a.steps;
        });
        return arr;
    }


    var seriesCategory;
    var xAxisCategory;
    var seriesCategorySide;
    var dataCategory;
    var levelCategory = Array(['', '', '']);

    var seriesRegion;
    var xAxisRegion;
    var legendRegionSide;
    var seriesRegionSide;
    var dataRegion;
    var levelRegion = Array(['', '', '']);

    var seriesUsers;
    var xAxisUsers;

    var dataUsers;
  


    getUsers(true);
   // getCategories();


    function loadDataCharts() {
        dataCategory = createData('ticket_category');
        xAxisCategory.data.setAll(dataCategory);
        seriesCategory.data.setAll(dataCategory);
        seriesCategorySide.data.setAll(dataCategory.reverse());

        dataRegion = createData('ticket_region_name');
        xAxisRegion.data.setAll(dataRegion);
        seriesRegion.data.setAll(dataRegion);
        seriesRegionSide.data.setAll(dataRegion.reverse());
        legendRegionSide.data.setAll(seriesRegionSide.dataItems);

        loadDataChartsUsers();
    }


    function loadDataChartsUsers() {

        var category = Array();
        dataUsers = Array();

        for (var i = 0; i < allData.length; i++) {
            for (var r = 0; r < allData[i]['reporters'].length; r++) {
                var reporter = allData[i]['reporters'][r].reporter;
                var column = reporter.reporter_registered_id;
                var indx = category.indexOf(column);
                if (indx < 0) {
                    category.push(column);
                    var pic = reporter.reporter_image_url ? reporter.reporter_image_url : "/Content/Images/default-user.png";
                    dataUsers.push({ id: column,name: reporter.reporter_name, steps: 1, pictureSettings: { src: pic }});
                }
                else {
                    dataUsers[indx].steps++;
                }
            }
        }
        dataUsers = sortStepDesc(dataUsers);
        seriesUsers.data.setAll(dataUsers);
        xAxisUsers.data.setAll(dataUsers);

        console.log('dataUsers', dataUsers);
    }





























    function getCategories() {
        var details = {
            client_id: $('#lblClientID').text()
        };
        $.getJSON(http + 'getCategories', details, function (data) {
            console.log('getCategories', data);
            Categories = data;
            loadCategory();
        });
    }



    function loadCategory() {
        var str = '';

        for (var r = -1; r < Categories.length; r++) {
            var name = r < 0 ? 'All' : Categories[r].category.category_name;
            var id = r < 0 ? 0 : Categories[r].category.category_id;
            var isActive = r < 0 ? 'active' : '';
            arr.push(id); arrCount.push(0); arrToday.push(0);
            str += '<div class="col-sm-6 col-lg-4"><div class="panel text-center btnWidget ' + isActive + '" id="cat' + id + '">' +
                '<div class="panel-heading" style="padding-bottom: 0px;"><h4 class="panel-title text-cornsilk font-light">' + name + '</h4>' +
                '</div><div class="panel-body p-t-0" style="padding:10px;"><h2 class="m-t-0 m-b-0" >' +
                '<i class="fas fa-chart-line text-primary m-r-10"></i> <b class="text-white">0</b></h2>' +
                '<small class="text-cornsilk m-b-0 m-t-0"><b style="color:orange">0</b>  Older Then 5 Days</small></div></div></div>';
        }
        $('#listCategory').html(str);
    }

    
    function getStatus(status, date) {
        date = date / 24;

        if (status == 'resolved' || status == 'completed')
            return '<span class="badge badge-success" style="width:105px" data-toggle="tooltip" data-placement="top" title="Completed">' + status + '</span>';
        else if (date < 5)
            return '<span class="badge badge-warning" style="width:105px" data-toggle="tooltip" data-placement="top" title="Was updated more than Five days ago, Please update urgently">' + status + '</span>';
        else
            return '<span class="badge badge-danger" style="width:105px" data-toggle="tooltip" data-placement="top" title="Was updated more than ' + date + ' day(s) ago, Please update">' + status + '</span>';
    }


    


    function getAssigned(data) {
        var str = '';
        if (data.ticket_other == 'acknowledged')
            str = getName(data.ticket_created_by);
        else if (data.ticket_other == 'inspection' || data.ticket_other == 'inspected')
            str = getName(data.ticket_inspector_id);
        else if (data.ticket_other == 'fixing' || data.ticket_other == 'fixed')
            str = getName(data.ticket_fixer_id);
        else if (data.ticket_other == 'completed' || data.ticket_other == 'resolved')
            str = getName(data.ticket_action_by);

        return str;
    }






    $('#btnEmployee').click(function () {
        listUsersAll();
    });
    function listUsersAll() {
        var str = '';
        $('#btnAddNewUser').html('New User');
        $(".clsTitle").html('Select Employee');
        for (var m = 0; m < users.length; m++) {
            var name = users[m]['user_title'] + ' ' + users[m]['user_firstname'] + ' ' + users[m]['user_surname'];
            str += '<div class="col-sm-6 col-lg-6 memberr"><div class="panel text-center btnWidget3 userList" data-id="' + users[m]['user_id'] + '"  data-name="' + name + '">' +
                '<div class="panel-body p-t-10" style="padding: 20px;"><h4 class="m-t-0 m-b-15">' +
                '<i class="fas fa-user text-primary m-r-10"></i><b>' + name + '</b></h4>' +
                '<p class="text-muted m-b-0 m-t-20"> ' + users[m].user_email + '</p></div></div></div>';
        }
        $('#listAllDiv').html(str);
        listAll('btnEmployee');


    }
    function getUsers(loadMore) {

        var details = {
            clientid: $('#lblClientID').text()
        };

        $.getJSON('/admin/getUsers', details, function (data) {
            users = data;
            if (loadMore)
                getTeams();
            else
                listUsersAll();
        });
    }
    function getName(user_id) {
        var name = '';
        for (var l = 0; l < users.length; l++) {
            if (users[l]['user_id'] == user_id)
                name = users[l]['user_firstname'] + ' ' + users[l]['user_surname'];
        }
        return name;
    }




    $('#btnTeam').click(function () {
        listTeamsAll();
    });
    function getTeams() {

        var details = {
            client_id: $('#lblClientID').text()
        };
        console.log(details);
        $.getJSON(https + 'Organisation/get_teams', details, function (data) {
            console.log('getTeams', data);
            Teams = data;
            getSuppliers();
        });
    }
    function listTeamsAll() {
        var str = '';
        $('#btnAddNewUser').html('Team Management');
        $(".clsTitle").html('Select Team');
        for (var m = 0; m < Teams.length; m++) {
            str += '<div class="col-sm-6 col-lg-6 memberr"><div class="panel text-center btnWidget3 userList"  data-id="' +
                Teams[m]['cruise_safe_organo_team']['team_id'] + '" data-name="' + Teams[m]['cruise_safe_organo_team']['team_name'] + '">' +
                '<div class="panel-body p-t-10" style="padding: 20px;"><h4 class="m-t-0 m-b-15">' +
                '<i class="fas fa-users text-primary m-r-10"></i><b>' + Teams[m]['cruise_safe_organo_team']['team_name'] + '</b></h4>' +
                '<p class="text-muted m-b-0 m-t-20"> Team Leader: ' + getName(Teams[m]['cruise_safe_organo_team'].team_leader) + '</p></div></div></div>';
        }
        $('#listAllDiv').html(str);
        listAll('btnTeam');


    }




    $('#btnSupplier').click(function () {
        listSuppliersAll();
    });
    function getSuppliers() {
        var details = {
            client_id: $('#lblClientID').text()
        };

        $.getJSON(https + 'Organisation/get_suppliers', details, function (data) {
            console.log('getSuppliers', data);
            Suppliers = data;

            getRoles();
        });
    }
    function listSuppliersAll() {
        var str = '';
        $('#btnAddNewUser').html('Supplier Management');
        $(".clsTitle").html('Select Supplier');
        for (var m = 0; m < Suppliers.length; m++) {
            str += '<div class="col-sm-6 col-lg-6 memberr"><div class="panel text-center btnWidget3 userList" data-id="' +
                Suppliers[m]['cruise_safe_organo_supplier']['supplier_id'] + '" data-name="' + Suppliers[m]['cruise_safe_organo_supplier']['supplier_company_name'] + '">' +
                '<div class="panel-body p-t-10" style="padding: 20px;"><h4 class="m-t-0 m-b-15">' +
                '<i class="fas fa-users text-primary m-r-10"></i><b>' + Suppliers[m]['cruise_safe_organo_supplier']['supplier_company_name'] + '</b></h4>' +
                '<p class="text-muted m-b-0 m-t-20"> Contact Person: ' + getName(Suppliers[m]['cruise_safe_organo_supplier']['supplier_contact_person']) + '</p></div></div></div>';
        }
        $('#listAllDiv').html(str);
        listAll('btnSupplier');
    }




    function listAll(btn) {
        $("#txtSearch").val('');
        $('.btnPickOne').hide();
        $('.usersmodallg').modal('show');

        $("#txtSearch").on("keyup", function () {
            var value = $(this).val().toLowerCase();
            $("#listAllDiv div.memberr").filter(function () {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
            });
            $('#rows').html($("#listAllDiv div.memberr:visible").length);
        });


        $('#btnPickOne').on('click', function () {
            action2 = $(this).attr('data-id');
            $('#' + btn).attr('data-id', $(this).attr('data-id'));
            $('#' + btn).html($(this).attr('data-name')).css('border-color', 'rgba(42, 50, 60, 0.2)');

            $('.usersmodallg').modal('hide');
        });

        $(document).on('click', ".btnWidget3", function () {
            $(document).find(".btnWidget3").removeClass('active');
            $(this).addClass('active');
            $('#btnPickOne').show('slow').html('Select ' + $(this).attr('data-name')).attr('data-id', $(this).attr('data-id'));;
            $('#btnPickOne').attr('data-name', $(this).attr('data-name'));

         
        });


    }


    function getRoles() {

        roles = Array();

        var details = {
            clientid: $('#lblClientID').text(),
            //  userId: $('#lblUserID').text(),
        };
        $.getJSON('/admin/getUserRoles', details, function (data) {
            console.log('Roles', data);
            if (data) {
                roles = data['roles'];
                $('#cboRole').html('<option value="">-- Choose Role --</option>');
                for (var i = 0; i < data['roles'].length; i++) {
                    if (data['roles'][i].role_category == 'supplier') {
                        var htmlcontent = '<option value="' + data['roles'][i].role_id + '">' + data['roles'][i].role_name + '</option>';
                        $('#cboRole').append(htmlcontent);
                    }
                }
            }
        });

    }











    $(document).on('click', ".btnWidget", function () {
        $(document).find(".btnWidget").removeClass('active');
        $(this).addClass('active');
        seletedTop = this.id;
        tableAction.clear();
        tableAction.rows.add(allDataAction).draw();
        table.clear();
        table.rows.add(allData).draw();

    });



    $(document).ready(function () {
        $(window).on('resize', function () {
            $('.divContainer').animate({ 'height': window.innerHeight - 150 }, 'slow');
            $('.divContainer1').animate({ 'height': window.innerHeight - 120 }, 'slow');
        });
        $('.divContainer').animate({ 'height': window.innerHeight - 150 }, 'slow');
        $('.divContainer1').animate({ 'height': window.innerHeight - 120 }, 'slow');

    });



    $('html').css('overflow', 'hidden');

    
  



    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        startDate = start.format('YYYY-MM-DD'); 
        endDate = end.format('YYYY-MM-DD');
        $('.clsDateRage').html(start.format('MMMM D, YYYY') + '&nbsp;&nbsp;to&nbsp;&nbsp;' + end.format('MMMM D, YYYY'));
        $(document).find('.btnDateRage').find('span').html($('.clsDateRage').html());
        getReporting();
    }

    $('#reportrange').daterangepicker({
        startDate: startDateMoment,
        endDate: endDateMoment,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

 














    







    




});



















