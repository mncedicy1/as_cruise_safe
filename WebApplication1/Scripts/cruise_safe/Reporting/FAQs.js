﻿function checkPrev(myPrivs) {

    //-------------------- Check Prev ---------------------------//

    if ($('#lblUserType').text() != 'business') {
        if (myPrivs.indexOf(1002) >= 0) {

        } else {
            //   location.href = '/as_smart_connects/home';
        }
    }

    //-------------------- Check Prev ---------------------------//
}

$(document).ready(function () {



   var http = httpsapi + "/api/reporting/";
    var https = httpsapi + "/api/";
    //http = httpsapi_local + '/api/reporting/';




    $('.actionAdministrator').trigger('click');
    $('.btnUserManagement').find('a').addClass('activeMenu');




    var destination;
    var source;
    var travelMode = 'DRIVING';
    var latMap = '';
    var lngMap = '';
    var town = '';
    var seletedTop = 'all';
    var allData = Array();



    function getFAQs() {


        $('#myLoader').modal('show');


        var details = {
            faq_client_id: $('#lblClientID').text(),
        };

        $.getJSON(http + 'getFAQs', details, function (data) {
            console.log('getFAQs', data);
            allData = data;

            loadData();

            setTimeout(function () {
                $('#myLoader').modal('hide');
            }, 1000);

        });
    }

    getFAQs();





   
    $("#btnSave").click(function () {

  

        $('#myLoader').modal('show');
        $.post(http + 'saveFAQ', details, function (data) {
            if (data.indexOf('success') >= 0) {
                showSuccessMessageClose("Traffic Light Saved");
                $('.adminUsers-modal-lg').modal('hide');
                getFAQs();
            }
            else {
                showErrorMessage(data);
                $('#myLoader').modal('hide');
            }

           
        });
    });






    function getNumber(number) {
        if (number == 0)
            return '<span class="badge badge-danger" style="width:40px">' + number + '</span>';
        else
            return '<span class="badge badge-warning" style="width:40px">' + number + '</span>';
    }


    function getText(text, maxlength) {
        text = text + '';
        if (text.length > maxlength)
            return '<span title="' + text + '">' + text.substring(0, maxlength) +'...</span>';
        else
            return '<span title="' + text + '">' + text+'</span>';
    }


    function getStatus(status) {
        if (status == 'inactive')
            return '<span class="badge badge-warning" style="width:105px">' + status + '</span>';
        if (status == 'active')
            return '<span class="badge badge-success" style="width:105px">' + status + '</span>';
        else
            return '<span class="badge badge-danger" style="width:105px">' + status + '</span>';
    }


    function getButtons(status,i) {
        if (status != 'deleted') {
            return '<button id="b' + i + '" style="width:100px" ' +
                'type="button" class="btn btn-primary btn-flat btn-xs btnView pull-right">View</button>';
        }
        else {
            return '<button id="u' + i + '" style="width:100px" ' +
                'type="button" class="btn btn-primary btn-flat btn-xs btnUndo pull-right">Undo Delete</button>';
        }
    }
    
    

    var selectedIdAns;
    var selectedId;
    function loadData() {
        if (allData) {
            var str = "";
            for (i = 0; i < allData.length; i++) {
                str += '<tr><td>' + (i + 1) + '</td>';
                str += '<td>' + getText(allData[i]['faq']['faq_question'],90) + '</td>';
                str += '<td>' + getNumber(allData[i]['faq_answers'].length) + '</td>';
                str += '<td>' + getStatus(allData[i]['faq']['faq_status']) + '</td>';
                str += '<td>' + getDatee(allData[i]['faq']['faq_timestamp']) + '</td>';
                str += '<td>' + getButtons(allData[i]['faq']['faq_status'],i) + '</td>';
            }
            $('#tblFAQs').html(str);
        }



        $('.btnView').click(function () {
            var i = this.id.substring(1);
            loadSelected(i);
        });

        $('.btnUndo').click(function () {
            selectedId = this.id.substring(1);
            $('#clsComment').val(allData[selectedId]['faq']['faq_question']);
            faq_status = "active";
            faq_question = allData[selectedId]['faq']['faq_question'];
            $('.clsTitle').html('Undo Delete');
            $('.clsMsg').html('Are you sure you want to <b style="color:green">recover</b> this faq question?');
            $('.confirmModal').modal('show');
        });
    }



    function loadSelected(i) {
        selectedId = i;
        
        $('#viewModal').modal('show');
        $('.clsBtnStatus').hide();
        $('.footer').hide();

        $('#txtQuestion').val(allData[i]['faq']['faq_question']);
        $('.clsStatus').html(getStatus(allData[i]['faq']['faq_status']));

        if (allData[i]['faq']['faq_status'] == 'active')
            $('.btnInactive').show();
        else
            $('.btnActive').show();

        
        loadAnswers();
    
    }



    function getButtonsAns(status, i) {
        if (status == 'deleted') {
            return '<button id="f' + i + '" style="width:160px" ' +
                'type="button" class="btn btn-primary btn-flat btn-xs btnUndoAnswer pull-right">Undo Delete</button>';
        }
        else {
            return '<button id="g' + i + '" style="width:80px" ' +
                'type="button" class="btn btn-danger btn-flat btn-xs btnRemoveAnswer pull-right">Remove</button>' +
                '<button id="q' + i + '" style="width:80px" ' +
                'type="button" class="btn btn-primary btn-flat btn-xs btnViewAnswer pull-right">View</button>';
        }
    }


    function loadAnswers() {
        var str = "";
        for (i = 0; i < allData[selectedId]['faq_answers'].length; i++) {
            str += '<tr><td>' + (i + 1) + '</td>';
            str += '<td>' + getText(allData[selectedId]['faq_answers'][i]['faq_answer'], 130) + '</td>';
            str += '<td>' + getStatus(allData[selectedId]['faq_answers'][i]['faq_answer_status']) + '</td>';
            str += '<td>' + getDatee(allData[selectedId]['faq_answers'][i]['faq_answer_timestamp']) + '</td>';
            str += '<td>' + getButtonsAns(allData[selectedId]['faq_answers'][i]['faq_answer_status'],i) + '</td>';
        }
        $('#tblFAQAnswers').html(str);

        $('.btnViewAnswer').click(function () {
            var i = this.id.substring(1);
            loadSelectedAns(i,'view');
        });

        $('.btnRemoveAnswer').click(function () {
            var i = this.id.substring(1);
            loadSelectedAns(i, 'remove');
        });

        $('.btnUndoAnswer').click(function () {
            var i = this.id.substring(1);
            loadSelectedAns(i, 'recover');
        });

    }


    


    function loadSelectedAns(i,type) {
        selectedIdAns = i;
        $('.answerModal').modal('show');
   
        if (type == 'view') {
            $('#clsAnswer1').val(allData[selectedId]['faq_answers'][selectedIdAns]['faq_answer']).removeAttr('disabled');
            $('.clsQuestion1').html(getText(allData[selectedId]['faq']['faq_question'], 80));
            $('#clsQuestionTitle').html('Question');
            $('#btnYesConfirm1').show();
            $('#btnYesConfirm1Remove').hide();
            $('#btnYesConfirm1Recover').hide();
            $('#btnYesConfirm1New').hide();
        }
        else if (type == 'remove') {
            $('#clsAnswer1').val(allData[selectedId]['faq_answers'][selectedIdAns]['faq_answer']).attr('disabled', 'disabled');
            $('.clsQuestion1').html('');
            $('#clsQuestionTitle').html('Are you sure you want to <b style="color:red">delete</b> this faq answer?');
            $('#btnYesConfirm1').hide();
            $('#btnYesConfirm1Remove').show();
            $('#btnYesConfirm1Recover').hide();
            $('#btnYesConfirm1New').hide();
        }
        else if (type == 'recover') {
            $('#clsAnswer1').val(allData[selectedId]['faq_answers'][selectedIdAns]['faq_answer']).attr('disabled', 'disabled');
            $('.clsQuestion1').html('');
            $('#clsQuestionTitle').html('Are you sure you want to <b style="color:green">recover</b> this faq answer?');
            $('#btnYesConfirm1').hide();
            $('#btnYesConfirm1Remove').hide();
            $('#btnYesConfirm1Recover').show();
            $('#btnYesConfirm1New').hide();
        }
        else if (type == 'new') {
            $('#clsAnswer1').val('').removeAttr('disabled');
            $('.clsQuestion1').html(getText(allData[selectedId]['faq']['faq_question'], 80));
            $('#clsQuestionTitle').html('Question');
            $('#btnYesConfirm1').hide();
            $('#btnYesConfirm1Remove').hide();
            $('#btnYesConfirm1Recover').hide();
            $('#btnYesConfirm1New').show();
        }
    }



    $('#btnAddNewAnswer').click(function () {
        loadSelectedAns(0, 'new');
    });



    $('#btnAddNew').click(function () {
        $('.questionModal').modal('show');
        $('#clsQuestion1').val('');
    });




    $('.btnClose').click(function () {
        $('#viewModal').modal('hide');
        $('.footer').show('slow');
       
    });


    $('#txtQuestion').keyup(function () {
        if ($(this).val().trim() != allData[selectedId]['faq']['faq_question'])
            $('.divSaveQuestion').slideDown('fast');
        else
            $('.divSaveQuestion').slideUp('fast');
    });




    var faq_status = '';
    var faq_question = '';

    $('.btnRemove').click(function () {
        $('#clsComment').val(allData[selectedId]['faq']['faq_question']);
        faq_status = "deleted";
        faq_question = allData[selectedId]['faq']['faq_question'];
        $('.clsTitle').html('Delete question');
        $('.clsMsg').html('Are you sure you want to <b style="color:red">delete</b> this question?');
        $('.confirmModal').modal('show');
    });

    $('.btnInactive').click(function () {
        $('#clsComment').val(allData[selectedId]['faq']['faq_question']);
        faq_status = "inactive";
        faq_question = allData[selectedId]['faq']['faq_question'];
        $('.clsTitle').html('Update Status');
        $('.clsMsg').html('The status will change to <b style="color:red">Inactive</b><br><b>Are you sure?</b>');
        $('.confirmModal').modal('show');
    });

    $('.btnActive').click(function () {
        $('#clsComment').val(allData[selectedId]['faq']['faq_question']);
        faq_status = "active";
        faq_question = allData[selectedId]['faq']['faq_question'];
        $('.clsTitle').html('Update Status');
        $('.clsMsg').html('The status will change to <b style="color:green">Active</b><br><b>Are you sure?</b>');
        $('.confirmModal').modal('show');
    });

    $('.btnSaveQuestion').click(function () {
        $('#clsComment').val($('#txtQuestion').val());
        faq_status = allData[selectedId]['faq']['faq_status'];
        faq_question = $('#txtQuestion').val().trim();
        $('.clsTitle').html('Update Question');
        $('.clsMsg').html('Are you sure you want to <b style="color:green">save</b> this question?');
        $('.confirmModal').modal('show');
    });
    
    $('#btnYesConfirm').click(function () {
    
        var obj = JSON.parse(JSON.stringify(allData[selectedId]['faq']));
        obj.faq_status = faq_status;
        obj.faq_question = faq_question;
        $('.confirmModal').modal('hide');
        updateFAQ(obj); 
    });

    function updateFAQ(obj) {

        $('#myLoader').modal('show');
        $.post(http + 'updateFAQ', obj, function (data) {
            console.log(data);
            if (data.indexOf('success') >= 0) {
                allData[selectedId]['faq']['faq_status'] = faq_status;
                allData[selectedId]['faq']['faq_question'] = faq_question;
                showSuccessMessageClose('Success');
                $('#viewModal').modal('hide');
                $('#viewMapModal').modal('hide');
                $('.footer').show('slow');
                $('.divSaveQuestion').slideUp('fast');
                loadData();
            } else {
                showErrorMessageClose(data);
            }
            $('#myLoader').modal('hide');
        });
    }



    var faq_status_ans = '';
    var faq_question_ans = '';

    $('#btnYesConfirm1').click(function () {
        faq_status_ans = allData[selectedId]['faq_answers'][selectedIdAns]['faq_answer_status'];
        faq_question_ans = $('#clsAnswer1').val().trim();
        $('.confirmModal').modal('hide');
        updateFAQAns();
    });

    $('#btnYesConfirm1Remove').click(function () {
        faq_status_ans = 'deleted';
        faq_question_ans = allData[selectedId]['faq_answers'][selectedIdAns]['faq_answer'];
        $('.confirmModal').modal('hide');
        updateFAQAns();
    });

    $('#btnYesConfirm1Recover').click(function () {
        faq_status_ans = 'active';
        faq_question_ans = allData[selectedId]['faq_answers'][selectedIdAns]['faq_answer'];
        $('.confirmModal').modal('hide');
        updateFAQAns();
    });

    $('#btnYesConfirm1New').click(function () {
        if (!vaidateFields('requiredComment1')) {
            saveFAQAns();
        }
    });


    $('#btnYesConfirm1Question').click(function () {
        if (!vaidateFields('requiredComment2')) {
            saveFAQ();
        }
    });


    function updateFAQAns() {

        var obj = JSON.parse(JSON.stringify(allData[selectedId]['faq_answers'][selectedIdAns]));
        obj.faq_answer_status = faq_status_ans;
        obj.faq_answer = faq_question_ans;

        $('#myLoader').modal('show');
        $.post(http + 'updateFAQAns', obj, function (data) {
            console.log(data);
            if (data.indexOf('success') >= 0) {
                allData[selectedId]['faq_answers'][selectedIdAns]['faq_answer_status'] = faq_status_ans;
                allData[selectedId]['faq_answers'][selectedIdAns]['faq_answer'] = faq_question_ans;
                showSuccessMessageClose('Success');
                $('.answerModal').modal('hide');
                loadAnswers();
                loadData();
            } else {
                showErrorMessageClose(data);
            }
            $('#myLoader').modal('hide');
        });
    }


    function saveFAQAns() {

        var obj = {
            'faq_answer': $('#clsAnswer1').val().trim(),
            'faq_answer_faq_id': allData[selectedId]['faq']['faq_id'],
            'faq_answer_client_id': $('#lblClientID').text(),
            'faq_answer_type': 'faq'
        }

        $('#myLoader').modal('show');
        $.post(http + 'saveFAQAns', obj, function (data) {
            console.log(data);
            if (data.indexOf('faq_answer_id') >= 0) {
                allData[selectedId]['faq_answers'].push(JSON.parse(data));
                showSuccessMessageClose('Success');
                $('.answerModal').modal('hide');
                loadAnswers();
                loadData();
            } else {
                showErrorMessageClose(data);
            }
            $('#myLoader').modal('hide');
        });
    }



    function saveFAQ() {

        var obj = {
            'faq_question': $('#clsQuestion1').val().trim(),
            'faq_client_id': $('#lblClientID').text(),
            'faq_type': 'faq'
        }

        $('#myLoader').modal('show');
        $.post(http + 'saveFAQ', obj, function (data) {
            console.log(data);
            if (data.indexOf('faq_id') >= 0) {
                var dataObj = { 'faq': JSON.parse(data), 'faq_answers': [] };
                allData.push(dataObj);
                showSuccessMessageClose('Success');
                $('.questionModal').modal('hide');
                loadData();
                setTimeout(function () {
                    loadSelected(allData.length - 1);
                    setTimeout(function () {
                        loadSelectedAns(0, 'new');
                    }, 500);
                }, 500);
            } else {
                showErrorMessageClose(data);
            }
            $('#myLoader').modal('hide');
        });
    }













 




 





















    function vaidateFields(clas) {
        $('.' + clas).css('border-color', 'silver');
        var input;
        $('.' + clas).map(function () {
            if (!$(this).val() || $(this).css('border-color') == 'red') {
                $(this).css('border-color', 'red');
                $(this).focus();
                input = $(this);

                $(this).keyup(function () {
                    if ($(this).val())
                        $(this).css('border-color', 'silver');
                    else
                        $(this).css('border-color', 'red');
                });
            }
        }).get();

        if (input) {
            input.focus();
            return true;
        }
        else
            return false;
    }

    function validateEmail(sEmail, msg) {
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(sEmail)) {
            return true;
        }
        else {
            //var msg = "Please enter valid email address.";
            checkEmptyFields(msg);
            return false;
        }
    }

    function SuccessMessage(msg) {
        $('#myPleaseWait').modal('show');
        $('#txtModalLoaderAlertMessage').hide;
        $('#txtModalLoaderSuccessMessage').show();
        $('#txtModalLoaderSuccessMessage b').html(msg);

        setTimeout(function () {
            $('#myPleaseWait').hide();
            $('#txtModalLoaderSuccessMessage').hide();
            $('#myPleaseWait').modal('hide');
        }, 3000);
    }

    function checkEmptyFields(msg) {

        $('#myPleaseWait').modal('show');
        $('#txtModalLoaderAlertMessage').show();
        $('#txtModalLoaderAlertMessage b').text(msg);

        setTimeout(function () {
            $('#myPleaseWait').hide();
            $('#myPleaseWait').modal('hide');
            $('#txtModalLoaderAlertMessage').hide();
        }, 3000);
    }



    /***************************************************************
                        Validations Ends
    ***************************************************************/



    //Removing  Messages CoOperation Services

    $(document).on('click', function () {
        if ($('#txtModalLoaderAlertMessage').is(':visible')) {
            $('#txtModalLoaderAlertMessage').hide();
        }
        if ($('#txtModalLoaderSuccessMessage').is(':visible')) {
            $('#txtModalLoaderSuccessMessage').hide();
        }

    });






});

