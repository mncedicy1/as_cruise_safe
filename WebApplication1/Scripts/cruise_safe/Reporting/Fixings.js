﻿function checkPrev(myPrivs) {

    //-------------------- Check Prev ---------------------------//

    if ($('#lblUserType').text() != 'business') {
        if (myPrivs.indexOf(1002) >= 0) {

        } else {
            //   location.href = '/as_smart_connects/home';
        }
    }

    //-------------------- Check Prev ---------------------------//
}

$(document).ready(function () {



    var http = httpsapi + "/api/reporting/";
    var https = httpsapi + "/api/";
    //http = httpsapi_local + '/api/reporting/';


    var mapData;
    var users = Array();
    var Teams = Array();
    var Suppliers = Array();
    var roles = Array();

    $('.actionAdministrator').trigger('click');
    $('.btnUserManagement').find('a').addClass('activeMenu');




    var destination;
    var source;
    var travelMode = 'DRIVING';
    var latMap = '';
    var lngMap = '';
    var town = '';
    var seletedTop1 = '';
    var seletedTop = '0';

    var allData = Array();
    var Categories = Array();
    var arr = Array();


    var arrCount = Array();
    var arrToday = Array();

    function getReporting() {

        loadRegions();

        $('#myLoader').modal('show');


        var details = {
            reporting_client_id: $('#lblClientID').text(),
            ticket_created_by: $('#lblUserID').text(),
            ticket_other: "fixing",
            region_name: $('#region_name').text()
        };

        $.getJSON(http + 'getReportingMyTickets', details, function (data) {
            console.log('getReportingMyTickets', data);
            allData = data;


            loadData(true);



            setTimeout(function () {
                $('#myLoader').modal('hide');
            }, 1000);

        });
    }

getUsers(true);
    getCategories();
    
   








    var regions = Array();
    getRegionsAll();


    function getRegionsAll() {

        var details = {
            region_client_id: $('#lblClientID').text(),
            //  userId: $('#lblUserID').text(),
        };
        $.getJSON(http + 'getRegionsAll', details, function (data) {
            console.log('getRegionsAll', data);
            if (data) {
                regions = data;

                $('#cboRegions').html('<option value="">-- Choose Region --</option>');
                for (var i = 0; i < data.length; i++) {
                    var htmlcontent = '<option value="' + data[i].region.OBJECTID + '">' + data[i].region.REGIONNAME + '</option>';
                    $('#cboRegions').append(htmlcontent);
                }

                var htmlcontent = '';
                for (var i = 0; i < data.length; i++) {
                    for (var c = 0; c < data[i].offices.length; c++) {
                        htmlcontent += '<option class="region region' + data[i].region.OBJECTID + '" value="' + data[i].offices[c].region_office_address_location + '">Regional Office (' + data[i].offices[c].region_office_name + ')</option>';
                    }
                }
                htmlcontent += '<option value="-26.2024661,28.0367126">Head Office (75 Helen Joseph St)</option>';
                htmlcontent += '<option value="Current">Current Location</option>';
                $('#cboStartFrom').html(htmlcontent);
            }
            getTrafficLight();
        });

    }


    function loadRegions() {
        var str = '';
        arr = Array();
        arrCount = Array();
        arrToday = Array();
        arrTodayBoundary = Array();

        for (var r = -1; r < regions.length; r++) {
            var name = r < 0 ? 'All' : regions[r].region.REGIONNAME;
            var id = r < 0 ? 0 : regions[r].region.OBJECTID;
            var isActive = r < 0 ? 'active' : '';
            arr.push(id); arrCount.push(0); arrToday.push(0); arrTodayBoundary.push(0);
            str += '<div class="col-sm-6 col-lg-3"><div class="panel text-center btnWidget ' + isActive + '" id="cat' + id + '">' +
                '<div class="panel-heading" style="padding-bottom: 0px;"><h6 class="panel-title text-cornsilk font-light">' + name + '</h6>' +
                '</div><div class="panel-body p-t-0" style="padding:10px;"><h4 class="m-t-5 m-b-5" >' +
                '<i class="fas fa-chart-line text-primary m-r-10"></i> <b class="text-white">0</b></h4>' +
                '<small style="font-size:12px" class="text-cornsilk m-b-0 m-t-0"><b style="color:red" class="broken">0</b> ' +
                'Broken&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Outside <b style="color:orange" class="boundary">0</b> </small></div></div></div>';
        }
        $('#listRegions').html(str);
    }





















    function getCategories() {
        var details = {
            client_id: $('#lblClientID').text()
        };
        $.getJSON(http + 'getCategories', details, function (data) {
            console.log('getCategories', data);
            Categories = data;
            loadCategory();
            getReporting();
        });
    }



    function loadCategory() {
        var str = '';

        for (var r = -1; r < Categories.length; r++) {
            var name = r < 0 ? 'All' : Categories[r].category.category_name;
            var id = r < 0 ? 0 : Categories[r].category.category_id;
            var isActive = r < 0 ? 'active' : '';
            arr.push(id); arrCount.push(0); arrToday.push(0);
            str += '<div class="col-sm-6 col-lg-4"><div class="panel text-center btnWidget ' + isActive + '" id="cat' + id + '">' +
                '<div class="panel-heading" style="padding-bottom: 0px;"><h5 class="panel-title text-cornsilk font-light">' + name + '</h5>' +
                '</div><div class="panel-body p-t-0" style="padding:10px;"><h3 class="m-t-0 m-b-0" >' +
                '<i class="fas fa-chart-line text-primary m-r-10"></i> <b class="text-white">0</b></h3>' +
                '<small style="font-size:12px" class="text-cornsilk m-b-0 m-t-0"><b style="color:orange">0</b>  Older Then 5 Days</small></div></div></div>';
        }
        $('#listCategory').html(str);
    }





    function updateSeenStatus(reporting) {
        $.post(http + 'updateViewStatus', reporting, function (data) {
            console.log(data);
            if (data.indexOf('success') < 0) {
                showErrorMessage(data);
            }
        });
    }

    function getStatus(status) {
        if (status == 'fixing')
            return '<span class="badge badge-warning" style="width:105px">' + status + '</span>';
        else
            return '<span class="badge badge-success" style="width:105px">' + status + '</span>';
    }


    function getInspector(typee, id) {
        if (typee == 'Team')
            return getTeamName(id);
        else if (typee == 'Supplier')
            return getSupplierName(id);
        else
            return getName(id);
    }



    function getImage(reporting) {
        var str = '';
        for (var r = 0; r < reporting.length; r++) {
            for (var ii = 0; ii < reporting[r].image.length; ii++) {
                str += '<a data-magnify="gallery" data-src="" data-caption="" data-group="a" href="' + reporting[r].image[ii].image_location + '">' +
                    '<i class="fas fa-image" style="color:green; margin-right:5px"></i></a>';
            }
        }
        return str;
    }



    var selectedRep = 0;
    var imgPosition = 0;
    var selectedId;
    var selectedReportId = 0;
    function loadData(loadTop) {


        if (loadTop) {
            for (i = 0; i < allData.length; i++) {

                var indx = arr.indexOf(allData[i]['reporting_ticket']['ticket_category_id']);
                arrCount[indx]++;
                arrCount[0]++;
                var date = getHours(allData[i]['reporting_ticket']['ticket_created_date']);
                arrToday[indx] += (date > (24 * 5)) ? 1 : 0;
                arrToday[0] += (date > (24 * 5)) ? 1 : 0;

            }
            for (var c = 0; c < arr.length; c++) {
                $('#cat' + arr[c]).find('h3').find('b').html(arrCount[c]);
                $('#cat' + arr[c]).find('small').find('b').html(arrToday[c]);

                console.log('cat', arr[c], arrCount[c], arrToday[c]);
            }
        }


        if (allData) {

            var str = "";
            for (var w = 0; w < allData.length; w++) {
                if (allData[w]['reporting_ticket']['ticket_category_id'] == seletedTop || seletedTop == '0') {
                    str += '<tr><td>' + (w + 1) + '</td>';
                    str += '<td>' + allData[w]['reporting_ticket']['ticket_reference'] + getVip(allData[w]['reporting_ticket']['ticket_vip_level']) + '</td>';
                    str += '<td>' + allData[w]['reporting_ticket']['ticket_town'] + '</td>';
                    str += '<td>' + allData[w]['reporting_ticket']['ticket_fixer_type'] + '</td>';
                    str += '<td>' + getInspector(allData[w]['reporting_ticket']['ticket_fixer_type'], allData[w]['reporting_ticket']['ticket_fixer_id']) + '</td>';
                    str += '<td>' + allData[w]['reporting_ticket']['ticket_category'] + '</td>';
                    str += '<td>' + allData[w]['reporting_ticket']['ticket_sub_category'] + '</td>';
                    str += '<td>' + getStatus(allData[w]['reporting_ticket']['ticket_other']) + '</td>';
                    str += '<td>' + getDatee(allData[w]['reporting_ticket']['ticket_fixer_assigned_date']) + '</td>';
                    str += '<td><button id="b' + w + '" style="width:100px" ' +
                        'type="button" class="btn btn-primary btn-flat btn-xs btnView pull-right">View</button></td>';
                }
            }
            $('#tblReports').html(str);
        }



        $('.btnView').click(function () {
            selectedId = parseInt(this.id.substring(1));
            if (allData[selectedId]['report_reportings'].length > 1)
                $('.isMany').show();
            else
                $('.isMany').hide();

            console.log(allData[selectedId]);

            loadSelected(selectedId);
        });
    }

    function getVip(vip_level) {
        if (vip_level < 99)
            return ' <span class="badge badge-danger" title="Important Ticket">VIP</span>';
        else
            return '';
    }



    function loadSelected(selectedId) {

        //if (allData[selectedId]['reporting_ticket'].reporting_status == "new") {
        //    allData[selectedId]['reporting_ticket'].reporting_status = "acknowledged";
        //    updateSeenStatus(allData[selectedId]['reporting_ticket']);
        //}
        $('.divHeaders').hide().slideDown('slow');


        if (allData[selectedId]['reporting_ticket'].reporting_status == "resolve")
            $('.btnResolve').hide();
        else
            $('.btnResolve').show();


        modalShow1('#viewModal');
        $('.footer').hide('slow');
        latMap = allData[selectedId]['reporting_ticket'].ticket_latitude;
        lngMap = allData[selectedId]['reporting_ticket'].ticket_longitude;
        destination = { lat: parseFloat(latMap), lng: parseFloat(lngMap) };



        $('.region').hide();
        $('.region' + allData[selectedId]['reporting_ticket'].ticket_region_id).show();


        $('#cboStartFrom option').each(function () {
            if ($(this).css('display') != 'none') {
                $(this).prop("selected", true);
                return false;
            }
        });
        $('#cboStartFrom').trigger('change');




        $('.clsClean').html(''); $('.clsMessage').val(''); 
        $('.clsReferenceMain').html(allData[selectedId]['reporting_ticket']['ticket_reference']);
        $('.clsProvince').html(allData[selectedId]['reporting_ticket']['ticket_metropolitan'] + ' - ' + allData[selectedId]['reporting_ticket']['ticket_province']);
        $('.clsReference').html(allData[selectedId]['reporting_ticket']['ticket_reference']);
        $('.clsName').html(allData[selectedId]['reporting_ticket']['ticket_person_name']);
        $('.clsLocationType').html(allData[selectedId]['reporting_ticket']['ticket_type']);
        $('.clsType').html(allData[selectedId]['reporting_ticket']['ticket_title']);
        $('.clsCategory').html(allData[selectedId]['reporting_ticket']['ticket_category']);
        $('.clsDate').html(getDateTime(allData[selectedId]['reporting_ticket']['ticket_fixer_assigned_date']));
        $('.clsAddress').html(allData[selectedId]['reporting_ticket']['ticket_address']);
        $('.clsStatus').html(getStatus(allData[selectedId]['reporting_ticket']['ticket_other']));
        $('.clsCoordinates').html(allData[selectedId]['reporting_ticket']['ticket_longitude'] + ',' + allData[selectedId]['reporting_ticket']['ticket_latitude']);
        $('.clsSub').html(allData[selectedId]['reporting_ticket']['ticket_sub_category']);

        $('.clsReqFixName').html(getInspector(allData[selectedId]['reporting_ticket']['ticket_fixer_type'], allData[selectedId]['reporting_ticket']['ticket_fixer_id']));
        $('.clsReqFixType').html(allData[selectedId]['reporting_ticket']['ticket_fixer_type']);
        $('.clsReqFixStatus').html(allData[selectedId]['reporting_ticket']['ticket_fixing_status']);
        $('.clsReqFixDate').html(getDateTime(allData[selectedId]['reporting_ticket']['ticket_fixer_assigned_date']));
        $('.clsReqFixBy').html(getName(allData[selectedId]['reporting_ticket']['ticket_fixer_assigned_by']));
        var com = removeNulls(allData[selectedId]['reporting_ticket']['ticket_fixer_assigned_comment']+'');
        $('.clsReqFixComment').html(com.length>24 ? (com.substring(0, 24) + '...') : com).attr('title', com);

        $('.clsReqInspName').html(getInspector(allData[selectedId]['reporting_ticket']['ticket_inspector_type'], allData[selectedId]['reporting_ticket']['ticket_inspector_id']));
        $('.clsReqInspType').html(allData[selectedId]['reporting_ticket']['ticket_inspector_type']);
        $('.clsReqInspStatus').html(allData[selectedId]['reporting_ticket']['ticket_inspection_status']);
        $('.clsReqInspDate').html(getDateTime(allData[selectedId]['reporting_ticket']['ticket_inspector_assigned_date']));
        $('.clsReqInspBy').html(getName(allData[selectedId]['reporting_ticket']['ticket_inspector_assigned_by']));
        com = removeNulls(allData[selectedId]['reporting_ticket']['ticket_inspector_assigned_comment']+'');
        $('.clsReqInspComment').html(com.length > 24 ? (com.substring(0, 24) + '...') : com).attr('title', com);


        if (allData[selectedId]['reporting_ticket']['ticket_inspector_feedback_status'] == 'inspected') {
            $('.clsFeedBack').show();
            $('.clsFeedStatus').html(allData[selectedId]['reporting_ticket']['ticket_inspector_feedback_status']);
            $('.clsFeedSubmittedBy').html(getName(allData[selectedId]['reporting_ticket']['ticket_inspector_feedback_by']));
            $('.clsFeedDate').html(getDateTime(allData[selectedId]['reporting_ticket']['ticket_inspector_feedback_date']));
            $('.clsFeedComment').html(allData[selectedId]['reporting_ticket']['ticket_inspector_feedback_comment']);


            $('.clsFeedImages').html('');
            for (var ii = 0; ii < allData[selectedId]['report_reportings']['image'].length; ii++) {
                var img = allData[selectedId]['report_reportings']['image'][ii];
                if (img.image_type == 'inspection') {
                    var str = '<a  style="margin-right: 5px;" data-magnify="gallery" data-src=""' +
                        ' data-caption="Inpection Feedback Image" data-group="report_images" href="' + img.image_location + '">' +
                        '<img src="' + img.image_location + '"  style="margin-bottom: 3px;" height="95" class="imga" width="95"></i></a>';
                    $('.clsFeedImages').append(str);
                }
            }
        }
        else
            $('.clsFeedBack').hide();


        if (allData[selectedId]['reporting_ticket']['ticket_fixer_feedback_status'] == 'fixed') {
            $('.clsFeedFixBack').show();
            $('.clsFeedFixStatus').html(allData[selectedId]['reporting_ticket']['ticket_fixer_feedback_status']);
            $('.clsFeedFixSubmittedBy').html(getName(allData[selectedId]['reporting_ticket']['ticket_fixer_feedback_by']));
            $('.clsFeedFixDate').html(getDateTime(allData[selectedId]['reporting_ticket']['ticket_fixer_feedback_date']));
            $('.clsFeedFixComment').html(allData[selectedId]['reporting_ticket']['ticket_fixer_feedback_comment']);

            $('.clsFeedFixImages').html('');
            for (var ii = 0; ii < allData[selectedId]['report_reportings']['image'].length; ii++) {
                var img = allData[selectedId]['report_reportings']['image'][ii];
                if (img.image_type == 'fixing') {
                    var str = '<a  style="margin-right: 5px;" data-magnify="gallery" data-src=""' +
                        ' data-caption="Fixing Feedback Image" data-group="report_images" href="' + img.image_location + '">' +
                        '<img src="' + img.image_location + '"  style="margin-bottom: 3px;" height="95" class="imga" width="95"></i></a>';
                    $('.clsFeedFixImages').append(str);
                }
            }
        }
        else
            $('.clsFeedFixBack').hide();


    






        var strr1 = '';
        for (var r = 0; r < allData[selectedId]['reporters'].length; r++) {
            var rep = allData[selectedId]['reporters'][r]['reporter'];
            var img = (rep.reporter_image_url) ? rep.reporter_image_url : '/Content/Images/default-user.png';
            strr1 += '<div class="panel text-center btnWidget2 btnReport" id="k' + r + '"><div class="panel-heading row">' +
                '<a class="col-md-3" data-magnify="gallery" ' +
                'data-src="" data-caption="' + phonenumber(rep.reporter_name) + '" data-group="reporter' + r +'" href="' + img + '">' +
                '<img src="' + img + '"  class="imga" height="60"></i></a>' +
                '<h5 class="panel-title text-muted font-light col-md-8" style="top: 11px;">' +
                '<b>' + phonenumber(rep.reporter_name) + '</b><br><small><b>' + rep.reporter_report_reference +
                '</b></small>&nbsp;&nbsp;-&nbsp;&nbsp;<small>' + getDatee(rep.reporter_date) + '</small>' +
                '</h5><span class="col-md-1" style="top: 20px;right:12px">' +
                getVip(rep.reporter_vip_level) + '</span></div></div>';
        }
        $('.clsReporters').html(strr1);

        setTimeout(function () { $(document).find('#k0').trigger('click'); }, 200);


        $('.btnReport').click(function () {
            selectedRep = parseInt(this.id.substring(1));

            $('.clsMessage').val(allData[selectedId]['reporters'][selectedRep]['reporter']['reporter_comment']);
            $('.clsReportDate').html(getDateTime(allData[selectedId]['reporters'][selectedRep]['reporter']['reporter_datetime']));
            $('.clsReportType').html(allData[selectedId]['reporters'][selectedRep]['reporter']['reporter_type']);
            $('.clsDeviceType').html(allData[selectedId]['reporters'][selectedRep]['reporter']['reporter_reference_type']);


            $('.clsImages').html('');
            for (var ii = 0; ii < allData[selectedId]['reporters'][selectedRep]['image'].length; ii++) {
                var img = allData[selectedId]['reporters'][selectedRep]['image'][ii];
                var rep = allData[selectedId]['reporters'][selectedRep]['reporter'];
                var str = '<a  style="margin-right:10px" data-magnify="gallery"' +
                    ' data-src="" data-caption="User report image by ' + phonenumber(rep.reporter_name) +
                    '" data-group="reporter_images" href="' + img.image_location + '">' +
                    '<img src="' + img.image_location + '" height="95" class="imga" width="95"></i></a>';
                $('.clsImages').append(str);
            }



        });















        modalHide('#viewMapModal');

        var type = allData[selectedId]['reporting_ticket']['reporting_title'];
        //if (allData[selectedId]['reporting_ticket']['ticket_body'] || allData[selectedId]['report_reportings']['image'].length > 0
        //    || allData[selectedId]['report_reportings']['voice'].length > 0)
        //    $('#divMsgImg').show('slow');
        //else
        //    $('#divMsgImg').hide('slow');

    }


    var action = '';
    var action1 = '';
    var action2 = '';

    $('.btnActionType').click(function () {
        action1 = '';
        action2 = '';
        $('#cboReason').val('');
        $('.cboType').val('');
        $('.cboTypee').hide();
        $('.btnActionTypeDiv').hide();
        $('.' + this.id).show('slow');
        $('#titleEmployee').html('Employee: *');
        action = $(this).attr('data-type');
        if (action == 'Complete') {
            action1 = 'Complete';
            action2 = '0';
        }
        if (this.id != 'btnCloseTicket') {
            $('#clsComment').val(getMessages());
            if (this.id == 'btnFeedback') {
                $('#cboFeedbackType').val('Employee').trigger('change');
                $('#titleEmployee').html('Submitted By: *');
            }
        }
        else
            $('#clsComment').val('');
    });

    $('.cboType').change(function () {
        action2 = '';
        $('.btnAdd').html(' Select ');
        $('.cboTypee').hide();
        $('#' + $(this).val()).show('slow');
        action1 = $(this).val();
    });


    $('#cboReason').change(function () {
        action2 = '';
        action1 = $(this).val();
    });


    $('#btnSubmit').click(function () {
        if (action) {
            if (action1) {
                if (action == 'Close' || action2) {
                    allData[selectedId]['reporting_ticket']['ticket_action'] = action;
                    allData[selectedId]['reporting_ticket']['ticket_action1'] = action1;
                    allData[selectedId]['reporting_ticket']['ticket_action2'] = action2;
                    allData[selectedId]['reporting_ticket']['ticket_action3'] = $('#clsComment').val();
                    allData[selectedId]['reporting_ticket']['ticket_action_by'] = $('#lblUserID').text();
                    console.log(allData[selectedId]['reporting_ticket'], JSON.stringify(allData[selectedId]['reporting_ticket']));

                    updateTicket();
                }
                else {
                    if (action1 == 'Employee')
                        showWarningMessageClose("Select an employee");
                    else if (action1 == 'Team')
                        showWarningMessageClose("Select a team");
                    else if (action1 == 'Supplier')
                        showWarningMessageClose("Select a supplier");
                }
            }
            else {
                if (action == 'Close')
                    showWarningMessageClose("Select a reason");
                else if (action == 'Fix')
                    showWarningMessageClose("Select a fixer");
            }
        }
        else
            showWarningMessageClose("Select an action");
    });

    function updateTicket() {

        $('#myLoader').modal('show');
        $.post(http + 'updateTicket', allData[selectedId]['reporting_ticket'], function (data) {
            console.log(data);
            if (data.indexOf('success') >= 0) {
                showSuccessMessage('Success');
                $('.acionModal').modal('hide');
                modalHide1('#viewModal');
                modalHide('#viewMapModal');
                if (document.getElementById("myAudio")) {
                    document.getElementById("myAudio").pause();
                    $('#myAudio').remove();
                }
                if (action == 'Fix') {
                    allData[selectedId]['reporting_ticket']['ticket_other'] = 'fixing';
                    allData[selectedId]['reporting_ticket']['ticket_fixer_type'] = action1;
                    allData[selectedId]['reporting_ticket']['ticket_fixer_id'] = action2;
                    allData[selectedId]['reporting_ticket']['ticket_fixer_assigned_date'] = getToday();
                    $('#myLoader').modal('hide');
                    showSuccessMessage('Success');
                    loadData(false);
                }
                else if (action == 'Fix_Feedback') {
                    allData[selectedId]['reporting_ticket']['ticket_other'] = 'fixed';
                    allData[selectedId]['reporting_ticket']['ticket_fixer_feedback_status'] = 'fixed';
                    allData[selectedId]['reporting_ticket']['ticket_fixer_feedback_by'] = $('#lblUserID').text();;
                    allData[selectedId]['reporting_ticket']['ticket_fixer_feedback_date'] = getToday();


                    if (files.length > 0) {
                        uploadFile(allData[selectedId]['reporting_ticket']['ticket_id']);
                    }
                    else {
                        $('#myLoader').modal('hide');
                        showSuccessMessage('Success');
                        loadData(false);
                    }
                }
                else {
                    allData.splice(selectedId, 1);
                    $('#myLoader').modal('hide');
                    showSuccessMessage('Success');
                    loadData(false);
                }
               
            } else {
                showErrorMessageClose(data);
            }
        });
    }




    function uploadFile(ticket_id) {

        var details = '?ticket_id=' + ticket_id +
            '&client_id=' + $('#lblClientID').text() +
            '&user_id=' + $('#lblUserID').text() +
            '&image_type=fixing';

        var fd = new FormData();
        for (var i = 0; i < files.length; i++)
            fd.append("images" + i, files[i]);

        var xhr;
        if (window.XMLHttpRequest)
            xhr = new XMLHttpRequest();
        else if (window.ActiveXObject)
            xhr = new ActiveXObject("Microsoft.XMLHTTP");

        xhr.open('POST', http + "PostImageTicket" + details, true);
        xhr.upload.onprogress = function (e) {
            if (e.lengthComputable) {
                var percentComplete = parseInt((e.loaded / e.total) * 100);
                showloader(percentComplete + ' % uploaded');
            }
        };
        xhr.send(fd);
        xhr.onload = function () {
            var data = JSON.parse(xhr.response);
            hideloader();

            console.log(data);

            loadData(false);

            if (data.message == 'success') {
                showSuccessMessage(data.value);
            }
            else {
                showErrorMessage(data.value);
            }
            $('#myLoader').modal('hide');
        };

    }






    $('#btnEmployee').click(function () {
        listUsersAll();
    });
    function listUsersAll() {
        var str = '';
        $('#btnAddNewUser').html('New User');
        $(".clsTitle").html('Select Employee');
        for (var m = 0; m < users.length; m++) {
            if (users[m]['user_region_name'] == $('#region_name').text()) {
                var name = users[m]['user_title'] + ' ' + users[m]['user_firstname'] + ' ' + users[m]['user_surname'];
                str += '<div class="col-sm-6 col-lg-6 memberr"><div class="panel text-center btnWidget3 userList" data-id="' + users[m]['user_id'] + '"  data-name="' + name + '">' +
                    '<div class="panel-body p-t-10" style="padding: 20px;"><h4 class="m-t-0 m-b-15">' +
                    '<i class="fas fa-user text-primary m-r-10"></i><b>' + name + '</b></h4>' +
                    '<p class="text-muted m-b-0 m-t-20"> ' + users[m].user_email + '</p></div></div></div>';
            }
        }
        $('#listAllDiv').html(str);
        listAll('btnEmployee');


    }
    function getUsers(loadMore) {

        var details = {
            clientid: $('#lblClientID').text()
        };

        $.getJSON('/admin/getUsers', details, function (data) {
            users = data;
            if (loadMore)
                getTeams();
            else
                listUsersAll();
        });
    }
    function getName(id) {
        var name = '';
        for (var l = 0; l < users.length; l++) {
            if (users[l]['user_id'] == id)
                name = users[l]['user_firstname'] + ' ' + users[l]['user_surname'];
        }
        return name;
    }




    $('#btnTeam').click(function () {
        listTeamsAll();
    });
    function getTeams() {

        var details = {
            client_id: $('#lblClientID').text()
        };
        console.log(details);
        $.getJSON(https + 'Organisation/get_teams', details, function (data) {
            console.log('getTeams', data);
            Teams = data;
            getSuppliers();
        });
    }
    function listTeamsAll() {
        var str = '';
        $('#btnAddNewUser').html('Team Management');
        $(".clsTitle").html('Select Team');
        for (var m = 0; m < Teams.length; m++) {
            if (Teams[m]['cruise_safe_organo_team']['team_region_name'] == $('#region_name').text()) {
                str += '<div class="col-sm-6 col-lg-6 memberr"><div class="panel text-center btnWidget3 userList"  data-id="' +
                    Teams[m]['cruise_safe_organo_team']['team_id'] + '" data-name="' + Teams[m]['cruise_safe_organo_team']['team_name'] + '">' +
                    '<div class="panel-body p-t-10" style="padding: 20px;"><h4 class="m-t-0 m-b-15">' +
                    '<i class="fas fa-users text-primary m-r-10"></i><b>' + Teams[m]['cruise_safe_organo_team']['team_name'] + '</b></h4>' +
                    '<p class="text-muted m-b-0 m-t-20"> Team Leader: ' + getName(Teams[m]['cruise_safe_organo_team'].team_leader) + '</p></div></div></div>';
            }
        }
        $('#listAllDiv').html(str);
        listAll('btnTeam');


    }
    function getTeamName(id) {
        var name = '';
        for (var t = 0; t < Teams.length; t++) {
            if (Teams[t]['cruise_safe_organo_team']['team_id'] == id)
                name = Teams[t]['cruise_safe_organo_team']['team_name'];
        }
        return name;
    }



    $('#btnSupplier').click(function () {
        listSuppliersAll();
    });
    function getSuppliers() {
        var details = {
            client_id: $('#lblClientID').text()
        };

        $.getJSON(https + 'Organisation/get_suppliers', details, function (data) {
            console.log('getSuppliers', data);
            Suppliers = data;

            getRoles();
        });
    }
    function listSuppliersAll() {
        var str = '';
        $('#btnAddNewUser').html('Supplier Management');
        $(".clsTitle").html('Select Supplier');
        for (var m = 0; m < Suppliers.length; m++) {
            str += '<div class="col-sm-6 col-lg-6 memberr"><div class="panel text-center btnWidget3 userList" data-id="' +
                Suppliers[m]['cruise_safe_organo_supplier']['supplier_id'] + '" data-name="' + Suppliers[m]['cruise_safe_organo_supplier']['supplier_company_name'] + '">' +
                '<div class="panel-body p-t-10" style="padding: 20px;"><h4 class="m-t-0 m-b-15">' +
                '<i class="fas fa-users text-primary m-r-10"></i><b>' + Suppliers[m]['cruise_safe_organo_supplier']['supplier_company_name'] + '</b></h4>' +
                '<p class="text-muted m-b-0 m-t-20"> Contact Person: ' + getName(Suppliers[m]['cruise_safe_organo_supplier']['supplier_contact_person']) + '</p></div></div></div>';
        }
        $('#listAllDiv').html(str);
        listAll('btnSupplier');
    }
    function getSupplierName(id) {
        var name = '';
        for (var s = 0; s < Suppliers.length; s++) {
            if (Suppliers[s]['cruise_safe_organo_supplier']['supplier_id'] == id)
                name = Suppliers[s]['cruise_safe_organo_supplier']['supplier_company_name'];
        }
        return name;
    }



    function listAll(btn) {
        $("#txtSearch").val('');
        $('.btnPickOne').hide();
        $('.usersmodallg').modal('show');

        $("#txtSearch").on("keyup", function () {
            var value = $(this).val().toLowerCase();
            $("#listAllDiv div.memberr").filter(function () {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
            });
            $('#rows').html($("#listAllDiv div.memberr:visible").length);
        });


        $('#btnPickOne').on('click', function () {
            action2 = $(this).attr('data-id');
            $('#' + btn).attr('data-id', $(this).attr('data-id'));
            $('#' + btn).html($(this).attr('data-name')).css('border-color', 'rgba(42, 50, 60, 0.2)');

            $('.usersmodallg').modal('hide');
        });

        $(document).on('click', ".btnWidget3", function () {
            $(document).find(".btnWidget3").removeClass('active');
            $(this).addClass('active');
            $('#btnPickOne').show('slow').html('Select ' + $(this).attr('data-name')).attr('data-id', $(this).attr('data-id'));;
            $('#btnPickOne').attr('data-name', $(this).attr('data-name'));
        });


    }


    function getRoles() {

        roles = Array();

        var details = {
            clientid: $('#lblClientID').text(),
            //  userId: $('#lblUserID').text(),
        };
        $.getJSON('/admin/getUserRoles', details, function (data) {
            console.log('Roles', data);
            if (data) {
                roles = data['roles'];
                $('#cboRole').html('<option value="">-- Choose Role --</option>');
                for (var i = 0; i < data['roles'].length; i++) {
                    if (data['roles'][i].role_category == 'supplier') {
                        var htmlcontent = '<option value="' + data['roles'][i].role_id + '">' + data['roles'][i].role_name + '</option>';
                        $('#cboRole').append(htmlcontent);
                    }
                }
            }
        });

    }


    $('#btnAddUser').on('click', function () {
        saveUser();
    });

    $('#btnAddNewUser').click(function () {
        if ($('#btnAddNewUser').text() == 'Team Management')
            location.href = '/Organisation/Team';
        else if ($('#btnAddNewUser').text() == 'Supplier Management')
            location.href = '/Organisation/Suppliers';
        else {
            $('.clsPassword').show();
            $('.clsPasswords').removeAttr('disabled').addClass('requiredUser');
            $('#btnAddUser').html('Save');
            $('.cleanUsertField').val('');
            $('#txtPassword').val('');
            $('.adminUsers-modal-lg').modal('show');
        }
    });


    function saveUser() {

        if (vaidateFields('requiredUser'))
            return;

        if ($('#txtPassword').val() != $('#txtConfirmPassword').val()) {
            showConnectionMessage("Passwords do not match");
            return;
        }


        $('.myLoader').modal('show');

        var details = {
            user_id: null,
            user_client_id: $('#lblClientID').text(),
            user_id_number: $('#txtIDNumber').val(),
            user_title: $('#cboTitle').val(),
            user_firstname: $('#txtFirstName').val(),
            user_surname: $('#txtLastName').val(),
            user_gender: $('#cboGender').val(),
            user_cellphone: $('#txtCellNumber').val(),
            user_email: $('#txtEmailAddress').val(),
            user_role_id: $('#cboRole').val(),
            user_password: $('#txtPassword').val(),
            user_saved_by: $('#lblUserID').text(),
            user_updated_by: $('#lblUserID').text()

        };
        console.log(details);
        $.post(https + 'admin/addUser', details, function (data) {
            console.log(data);
            if (data.indexOf('success') >= 0) {
                showSuccessMessageClose(data);
                getUsers(false);
                $('#myLoader').modal('hide');

                $('.adminUsers-modal-lg').modal('hide');
            }
            else {


                $('#myLoader').modal('hide');

                showErrorMessage(data);
            }
        });
    }















    function getMessages() {
        var str = '';
        for (var a = 0; a < allData[selectedId]['report_reportings'].length; a++) {
            var report = allData[selectedId]['report_reportings'][a]['report_itt'];
            if (report.reporting_title == 'User Reporting' && report.reporting_body) {
                str += report.reporting_person_name + ' : ' + report.reporting_body + '\r\n';
            }
            console.log(report.reporting_title, report.reporting_body);
        }
        return str;
    }



    $('.btnAction').click(function () {
        $('.btnWidget2').removeClass('active');
        $('#imagesDiv').html('');
        $('.cleanAction').val('');
        $('.cboType').val('');
        $('.cboTypee').hide();
        $('.btnActionTypeDiv').hide();
        action = '';
        $('.acionModal').modal('show');
    });


    $('#btnPrev').click(function () {
        if (selectedReportId > 0)
            selectedReportId--;
        else
            selectedReportId = allData[selectedId]['report_reportings'].length - 1;
        loadSelected(selectedId);
    });
    $('#btnNext').click(function () {
        if (selectedReportId < (allData[selectedId]['report_reportings'].length - 1))
            selectedReportId++;
        else
            selectedReportId = 0;
        loadSelected(selectedId);
    });


    $('.clsRight').click(function () {
        imgPosition++;
        if (imgPosition >= allData[selectedId]['image'].length)
            imgPosition = 0;

        $('.clsImage').attr('src', allData[selectedId]['image'][imgPosition].image_location);
    });


    $('.clsLeft').click(function () {
        imgPosition--;
        if (imgPosition < 0)
            imgPosition = allData[selectedId]['image'].length - 1;

        $('.clsImage').attr('src', allData[selectedId]['image'][imgPosition].image_location);
    });


    $('.btnClose').click(function () {
        modalHide1('#viewModal');
        $('.footer').show('slow');
        if (document.getElementById("myAudio"))
            document.getElementById("myAudio").pause();
        $('#myAudio').remove();
    });


    $('.btnCloseMap').click(function () {
        modalHide('#viewMapModal');
    });


    $('.btnResolve').click(function () {
        if (allData[selectedId]['report_itt'].reporting_status == "acknowledged") {
            allData[selectedId]['report_itt'].reporting_status = "resolved";
            updateResolveStatus(allData[selectedId]['report_itt']);
        }

    });

    function updateResolveStatus(reporting) {
        $.post(http + 'updateResolveStatus', reporting, function (data) {
            console.log(data);
            if (data.indexOf('success') >= 0) {
                showSuccessMessage('Success');
                modalHide1('#viewModal');
                modalHide('#viewMapModal');
                if (document.getElementById("myAudio")) {
                    document.getElementById("myAudio").pause();
                    $('#myAudio').remove();
                }
            } else {
                showErrorMessageClose(data);
            }
        });
    }



    $(document).on('click', ".btnWidget", function () {
        $(document).find(".btnWidget").removeClass('active');
        $(this).addClass('active');

        if (!$(this).hasClass('btnActionType')) {
            seletedTop = this.id.replace('cat', '');
            loadData(false);
            $('.selectedTitle').html($('#cat' + seletedTop).find('h4').html());
         
        }
    });


    $(document).on('click', ".btnWidget2", function () {
        $(document).find(".btnWidget2").removeClass('active');
        $(this).addClass('active');

    });









    $('#cboTravelMode').change(function () {
        travelMode = $(this).val();
        loadMap();
    });






    $('#cboStartFrom').change(function () {
        if ($(this).val() == 'Current') {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    source = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                    loadMap();
                });
            } else {
                showErrorMessageClose("Browser doesn't support Geolocation");
            }
        }
        else {
            source = {
                lat: parseFloat($(this).val().split(',')[0]),
                lng: parseFloat($(this).val().split(',')[1])
            };
            loadMap();
        }

    });


    var opened = false;
    $('#btnMap').click(function () {
        $('.clsTop').show();
        $('#style-selector2').show();
        modalShow('#viewMapModal');
        opened = true;
        loadMap1(0);

    });


    $('.btnLocate').click(function () {
        $('.clsTop').show();
        $('#style-selector2').hide();
        modalShow('#viewMapModal');

        if (heatmap)
            heatmap.setMap(null);

        loadMap1(allData[selectedId]['reporting_ticket']['ticket_id']);
    });



    function loadMap() {



        document.getElementById("clsDistance").innerHTML = '';
        $('#dvPanel').html('');

        var map = new google.maps.Map(document.getElementById('dvMap'), {
            zoom: 15,
            center: new google.maps.LatLng(-26.2566038, 27.9848897),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: false,
            options: {
                gestureHandling: 'greedy'
            }
        });


        map.data.addGeoJson(mapData);
        map.data.setStyle({
            icon: '//example.com/path/to/image.png',
            fillColor: 'lime',
            strokeWeight: 1
        });

        map.data.addListener('click', function (event) {
            var bounds = new google.maps.LatLngBounds();
            event.feature.h.h[0].h.forEach(function (coord, index) {
                bounds.extend(coord);
            });
            map.setZoom(11);
            map.setCenter(bounds.getCenter());
        });

        map.data.addListener('mouseover', function (event) {
            map.data.overrideStyle(event.feature, { fillColor: 'red' });
        });
        map.data.addListener('mouseout', function (event) {
            map.data.overrideStyle(event.feature, { fillColor: 'lime' });
        });

        map.data.addListener('mouseover', mouseOverDataItem);
        map.data.addListener('mouseout', mouseOutOfDataItem);

        function mouseOverDataItem(mouseEvent) {
            const titleText = mouseEvent.feature.j.REGIONNAME;
            if (titleText) {
                map.getDiv().setAttribute('title', titleText);
            }
        }

        function mouseOutOfDataItem(mouseEvent) {
            map.getDiv().removeAttribute('title');
        }




        var directionsService = new google.maps.DirectionsService();
        var directionsDisplay = new google.maps.DirectionsRenderer({ 'draggable': true });
        directionsDisplay.setMap(map);
        directionsDisplay.setPanel(document.getElementById('dvPanel'));
        google.maps.event.addListener(directionsDisplay, 'directions_changed', function () {
            //  destination = directionsDisplay.directions.request.destination;
            // GetRoute();
            var latlng = directionsDisplay.directions.request.destination;

            console.log(latlng);
            var panorama = new google.maps.StreetViewPanorama(
                document.getElementById('pano'), {
                    position: latlng,
                    pov: {
                        heading: 34,
                        pitch: 10
                    }
                });
            map.setStreetView(panorama);
        });

        google.maps.event.addListener(directionsDisplay, 'click', function (event) {
            console.log(event);

        });


        //*********DIRECTIONS AND ROUTE**********************//

        var request = {
            origin: source,
            destination: destination,
            travelMode: google.maps.TravelMode[travelMode],
            unitSystem: google.maps.UnitSystem.METRIC,
            drivingOptions: {
                departureTime: new Date(Date.now()),
                trafficModel: 'optimistic'
            }

        };

        directionsService.route(request, function (response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(response);
                var latlng = response.routes[0].legs[0].end_location;
                var panorama = new google.maps.StreetViewPanorama(
                    document.getElementById('pano'), {
                        position: latlng,
                        pov: {
                            heading: 34,
                            pitch: 10
                        }
                    });
                map.setStreetView(panorama);
                console.log(latlng);

            }
            else {
                showErrorMessageClose("Directions for '" + travelMode + "' travel mode are not available");
            }
        });



        //*********DISTANCE AND DURATION**********************//
        var service = new google.maps.DistanceMatrixService();
        service.getDistanceMatrix({
            origins: [source],
            destinations: [destination],
            travelMode: google.maps.TravelMode[travelMode],
            unitSystem: google.maps.UnitSystem.METRIC,
            avoidHighways: false,
            avoidTolls: false
        }, function (response, status) {
            if (status == google.maps.DistanceMatrixStatus.OK && response.rows[0].elements[0].status != "ZERO_RESULTS") {
                var distance = response.rows[0].elements[0].distance.text;
                var duration = response.rows[0].elements[0].duration.text;
                document.getElementById("clsDistance").innerHTML = distance + ' <span style="color:green">in</span> ' + duration;
                console.log(response);

            } else {
                //   alert("Unable to find the distance via road.");
            }
        });









    }


    /***************************************************************
                          Map Starts
      ***************************************************************/

    var selector2 = 'all';
    var idMap = 0;
    var mapData;
    var features = Array();
    var markers = Array();
    var heatmapData = Array();
    var heatmap;

    var map = new google.maps.Map(document.getElementById('dvMap1'), {
        zoom: 10,
        center: new google.maps.LatLng(-26.2566038, 27.9848897),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false,
        options: {
            gestureHandling: 'greedy'
        }
    });



    // Add a style-selector control to the map.
    var styleControl = document.getElementById('style-selector-control');
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(styleControl);

    var styleSelector = document.getElementById('style-selector');
    map.setOptions({ styles: styles[styleSelector.value] });
    map.setTilt(45);
    styleSelector.addEventListener('change', function () {
        if (styleSelector.value == 'satellite') {
            map.setMapTypeId(google.maps.MapTypeId.HYBRID);
            map.setOptions({ styles: null });
        }
        else {
            map.setMapTypeId(google.maps.MapTypeId.ROADMAP);
            map.setOptions({ styles: styles[styleSelector.value] });
        }
    });



    $.getJSON('/Content/files/geo.json', function (data) {
        mapData = data;
        features = map.data.addGeoJson(mapData);
    });

    map.data.setStyle({
        icon: '//example.com/path/to/image.png',
        fillColor: 'lime',
        strokeWeight: 1
    });

    var styleSelector1 = document.getElementById('style-selector1');
    styleSelector1.addEventListener('change', function () {
        for (var i = 0; i < features.length; i++)
            map.data.remove(features[i]);
        heatmap.setMap(null);
        for (var i = 0; i < markers.length; i++)
            markers[i].setMap(null);

        if (styleSelector1.value == 'All') {
            features = map.data.addGeoJson(mapData);
            heatmap.setMap(map);
            for (var i = 0; i < markers.length; i++)
                if (markers[i].id == idMap)
                    markers[i].setMap(map);
        }
        else if (styleSelector1.value == 'Reports') {
            for (var i = 0; i < markers.length; i++)
                if (markers[i].id == idMap)
                    markers[i].setMap(map);
        }
        else if (styleSelector1.value == 'Heat') {
            heatmap.setMap(map);
        }
        else if (styleSelector1.value == 'Shape') {
            features = map.data.addGeoJson(mapData);
        }
    });





    var styleSelector2 = document.getElementById('style-selector2');
    styleSelector2.addEventListener('change', function () {
        selector2 = styleSelector2.value;
        if (heatmap)
            heatmap.setMap(null);
        loadMap1(idMap);
    });





    map.data.addListener('click', function (event) {
        var bounds = new google.maps.LatLngBounds();
        event.feature.h.h[0].h.forEach(function (coord, index) {
            bounds.extend(coord);
        });
        //  map.setZoom(11);

        map.setCenter(bounds.getCenter());
        map.fitBounds(bounds);
    });
    google.maps.event.addListenerOnce(map, 'bounds_changed', function () {
        map.setZoom(map.getZoom() - 1);
    });


    map.data.addListener('mouseover', function (event) {
        map.data.overrideStyle(event.feature, { fillColor: 'red' });
    });
    map.data.addListener('mouseout', function (event) {
        map.data.overrideStyle(event.feature, { fillColor: 'lime' });
    });

    map.data.addListener('mouseover', mouseOverDataItem);
    map.data.addListener('mouseout', mouseOutOfDataItem);

    function mouseOverDataItem(mouseEvent) {
        const titleText = mouseEvent.feature.j.REGIONNAME;
        if (titleText) {
            map.getDiv().setAttribute('title', titleText);
        }
    }

    function mouseOutOfDataItem(mouseEvent) {
        map.getDiv().removeAttribute('title');
    }




    function loadMap1(id) {

        idMap = id;

        clearMarkers();
        heatmapData = Array();


        var latlngbounds = new google.maps.LatLngBounds();
        for (var i = 0; i < allData.length; i++) {

            if ((id == 0 && (selector2 == 'all' || selector2 == allData[i]['reporting_ticket'].ticket_status)) ||
                (id > 0 && allData[i]['reporting_ticket']['ticket_id'] == id)) {

                var latLng = new google.maps.LatLng(parseFloat(allData[i]['reporting_ticket'].ticket_latitude)
                    , parseFloat(allData[i]['reporting_ticket'].ticket_longitude));


                var date = getHours(allData[i]['reporting_ticket']['ticket_created_date']);

                heatmapData.push({ location: latLng, weight: 0.2 })

                var div = document.createElement('div');
                div.className = (allData[i]['reporting_ticket']['ticket_status'] == 'new') ? 'ripple-redblue' :
                    (allData[i]['reporting_ticket']['ticket_status'] == 'acknowledged') ? 'ripple-red2' :
                        (allData[i]['reporting_ticket']['ticket_status'] == 'processing') ? 'ripple-redpep' :
                            (allData[i]['reporting_ticket']['ticket_status'] == 'paused') ? 'ripple-red1' :
                                (allData[i]['reporting_ticket']['ticket_status'] == 'out of bounds') ? 'ripple-red' :
                                    (allData[i]['reporting_ticket']['ticket_status'] == 'resolved') ? 'ripple-red3' : 'ripple-redsky';


                var marker = new RichMarker({
                    position: latLng,
                    map: map,
                    id: id,
                    flat: true,
                    content: div,
                    draggable: false,
                    animation: google.maps.Animation.DROP,
                    title: allData[i]['reporting_ticket'].ticket_category + ' - ' + allData[i]['reporting_ticket'].ticket_sub_category
                });
                markers.push(marker);

                (function (marker) {
                    google.maps.event.addListener(marker, "click", function (e) {
                        if (map.getZoom() != 18) {
                            map.setZoom(18);
                            map.setCenter(marker.getPosition());
                        } else {
                            loadSelected(marker.id);
                        }
                    });

                    google.maps.event.addListener(marker, "dblclick", function (e) {
                        loadSelected(marker.id);
                    });

                })(marker);
                latlngbounds.extend(marker.position);
            }
        }






        heatmap = new google.maps.visualization.HeatmapLayer({
            data: heatmapData
        });

        heatmap.setMap(map);
        heatmap.set("radius", heatmap.get("radius") ? null : 50);

        const trafficLayer = new google.maps.TrafficLayer();

        trafficLayer.setMap(map);

        //if (allData.length > 0)
        //    map.setCenter(latlngbounds.getCenter());
        //else
        //    map.setCenter(new google.maps.LatLng(-25.7307893, 28.007999));
        //map.setZoom(10);


    }








    function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    }

    function clearMarkers() {
        setMapOnAll(null);
    }

    function showMarkers() {
        setMapOnAll(map);
    }

    function deleteMarkers() {
        clearMarkers();
        markers = [];
    }

















    function vaidateFields(clas) {
        $('.' + clas).css('border-color', 'silver');
        var input;
        $('.' + clas).map(function () {
            if (!$(this).val() || $(this).css('border-color') == 'red') {
                $(this).css('border-color', 'red');
                $(this).focus();
                input = $(this);
            }
        }).get();

        if (input) {
            input.focus();
            return true;
        }
        else
            return false;
    }

    function validateEmail(sEmail, msg) {
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(sEmail)) {
            return true;
        }
        else {
            //var msg = "Please enter valid email address.";
            checkEmptyFields(msg);
            return false;
        }
    }

    function SuccessMessage(msg) {
        $('#myPleaseWait').modal('show');
        $('#txtModalLoaderAlertMessage').hide;
        $('#txtModalLoaderSuccessMessage').show();
        $('#txtModalLoaderSuccessMessage b').html(msg);

        setTimeout(function () {
            $('#myPleaseWait').hide();
            $('#txtModalLoaderSuccessMessage').hide();
            $('#myPleaseWait').modal('hide');
        }, 3000);
    }

    function checkEmptyFields(msg) {

        $('#myPleaseWait').modal('show');
        $('#txtModalLoaderAlertMessage').show();
        $('#txtModalLoaderAlertMessage b').text(msg);

        setTimeout(function () {
            $('#myPleaseWait').hide();
            $('#myPleaseWait').modal('hide');
            $('#txtModalLoaderAlertMessage').hide();
        }, 3000);
    }



    /***************************************************************
                        Validations Ends
    ***************************************************************/



    //Removing  Messages CoOperation Services

    $(document).on('click', function () {
        if ($('#txtModalLoaderAlertMessage').is(':visible')) {
            $('#txtModalLoaderAlertMessage').hide();
        }
        if ($('#txtModalLoaderSuccessMessage').is(':visible')) {
            $('#txtModalLoaderSuccessMessage').hide();
        }

    });






});

