﻿function checkPrev(myPrivs) {

    //-------------------- Check Prev ---------------------------//

    if ($('#lblUserType').text() != 'business') {
        if (myPrivs.indexOf(1002) >= 0) {

        } else {
            //   location.href = '/as_smart_connects/home';
        }
    }

    //-------------------- Check Prev ---------------------------//
}

$(document).ready(function () {



     var http = httpsapi + "/api/reporting/";
    var https = httpsapi + "/api/";
    //var http = httpsapi_local + '/api/reporting/';




    $('.actionAdministrator').trigger('click');
    $('.btnUserManagement').find('a').addClass('activeMenu');



    var id;
    var start_from = 0;
    var last_row = false;
    var isSending = false;
    var fullname = "";
    var person_id;
    var system_user_id;

    var seletedTop = 'Very High';
    var allData = Array();

    var arr = ['Very High', 'High', 'Medium', 'Low'];
    var arrCount = [0, 0, 0, 0];
    var arrToday = [0, 0, 0, 0];

    var questionData;


    function getTraces() {


        $('#myLoader').modal('show');

        var details = {
            client_id: $('#lblClientID').text()
        };

        $.getJSON(http + 'getScreeningAll', details, function (data) {

            console.log(data);
            questionData = data;


            for (i = 0; i < data.length; i++) {
                allData.push(data[i].cruise_safe_screening);
            }



            arrCount = [0, 0, 0, 0];
            $('.Screening').find('span').html(allData.length);

            for (i = 0; i < allData.length; i++) {
                var indx = arr.indexOf(allData[i]['screening_results']);
                arrCount[indx]++;
                var date = new Date().getHours() - new Date(allData[i]['screening_timestamp']).getHours();
                arrToday[indx] += arrToday[indx] + (date < 24) ? 1 : 0;
            }
            for (var c = 0; c < arr.length; c++) {
                $('#' + arr[c].replace(' ', '_')).find('h2').find('b').html(arrCount[c]);
                $('#' + arr[c].replace(' ', '_')).find('p').find('b').html(arrToday[c]);
            }

            loadData();

                $('#myLoader').modal('hide');

        });
    }

    getTraces();


    var selectedData;
    var selectedQuestion;
    var selectedAnswers;
    var selectedId;


    function loadData() {
        if (allData) {

            var str = "";
            for (i = 0; i < allData.length; i++) {
                if (allData[i]['screening_results'] == seletedTop) {
                    str += '<tr><td>' + (i + 1) + '</td>';
                    str += '<td>' + allData[i]['screening_cellphone'] + '</td>';
                    str += '<td>' + allData[i]['screening_name'] + ' ' + allData[i]['screening_surmane'] + ' Meters</td>';
                    str += '<td>' + allData[i]['screening_results'] + '</td>';
                    str += '<td>' + allData[i]['screening_time_elapsed'] + '</td>';
                    str += '<td>' + allData[i]['screening_province'] + '</td>';
                    str += '<td>' + allData[i]['screening_status'] + '</td>';
                    str += '<td>' + getDateTime(allData[i]['screening_timestamp']) + '</td>';
                    str += '<td><button id="b' + i + '"    style="width:100px" type="button" class="btn btn-warning btn-flat btn-xs btnView">View</button></td>';
                }
            }
            $('#tblReports').html(str);
        }
        $('.btnView').click(function () {
            selectedId = this.id.substring(1);
            $('#viewModal').modal('show');
            selectedData = allData[selectedId];
            selectedQuestion = questionData[selectedId].cruise_safe_screening_questions;
            selectedAnswers = questionData[selectedId].cruise_safe_screening_questions_asked;
            showItem();
            getIdentified(selectedData['screening_cellphone']);
        });
    }

    var name = '';

        function showItem() {
            name = selectedData['screening_name'] + ' ' + selectedData['screening_surmane'];
            $('.clsName').html(name);
            $('.clsDuration').html(selectedData['screening_time_elapsed'] + ' Minutes');
            $('.clsRisk').html(selectedData['screening_results']);
            $('.clsCellphone').html(selectedData['screening_cellphone']);
            $('.clsProvince').html(selectedData['screening_province']);
            $('.clsStatus').html(selectedData['screening_status']);
            $('.clsDate').html(getDateTime(selectedData['screening_timestamp']));
            $('.clsYeses').html(selectedData['screening_results_yeses'] + ':' + selectedData['screening_results_nos']);

            if (selectedData['screening_status'] == 'new')
                $('.footerr').show('slow');
            else 
                $('.footerr').hide('slow');
                
            var str = '';
            for (i = 0; i < selectedQuestion.length; i++) {
                var answer = selectedAnswers[i].asked_answer;
                var color = (answer == 'yes') ? 'badge-danger' : 'badge-primary';
                str += '<li class="list-group-item"><span style="width:40px;margin-left:10px" class="badge ' +
                    color + '">' + answer + '</span>' + selectedQuestion[i].screening_questions + '</li>';
            }
            $('#listQuestions').html(str);
            $('#myLoader').modal('hide');

        }





    $(".btnClose").click(function () {
        $('#viewModal').modal('hide');
    });





    $(".btnWidget").click(function () {
        seletedTop = this.id.replace('_', ' ');
        $('.btnWidget').removeClass('active');
        $(this).addClass('active');
        $('.selectedTitle').html($(this).find('h4').text());
        loadData();

    });




    var identifiedData = Array();
    var identifiedDataAll = Array();

    function getIdentified(cellphone) {
        identifiedData = Array();

        $('#myLoader').modal('show');
        var details = {
            cellphone: cellphone,
        };
        $.getJSON(http + 'getScreeningByCellphone', details, function (data) {

            identifiedDataAll = data;

            for (i = 0; i < data.length; i++) {
                identifiedData.push(data[i].cruise_safe_screening);
            }

            $('.clsCount').html(data.length);

            var str = "";
            for (i = 0; i < identifiedData.length; i++) {
                str += '<tr><td>' + (i + 1) + '</td>';
                str += '<td>' + identifiedData[i]['screening_cellphone'] + '</td>';
                str += '<td>' + identifiedData[i]['screening_results'] + '</td>';
                str += '<td>' + identifiedData[i]['screening_province'] + '</td>';
                str += '<td>' + identifiedData[i]['screening_status'] + '</td>';
                str += '<td>' + getDatee(identifiedData[i]['screening_timestamp']) + '</td>';
                str += '<td><button id="r' + i + '"    style="width:100px" type="button" class="btn btn-success btn-flat btn-xs btnViewr btnViewer">View</button></td>';
            }
            $('#tblReportsHistory').html(str);

                $('#myLoader').modal('hide');

            $('.btnViewr').click(function () {
                $('#myLoader').modal('show');
                var id = this.id.substring(1);
                setTimeout(function () {
                    selectedData = identifiedData[id];
                    selectedQuestion = identifiedDataAll[id].cruise_safe_screening_questions;
                    selectedAnswers = identifiedDataAll[id].cruise_safe_screening_questions_asked;
                    showItem();
                }, 300);
            });

        });
    }



    

    $(document).on("click",".btnViewer",function () {
        $('.btnViewer').removeClass('btn-warning').addClass('btn-success');
        $(this).removeClass('btn-success').addClass('btn-warning');
    });





    $(".btnSubmit").click(function () {

        $('#myLoader').modal('show');
        selectedData.screening_status = this.id;

        $.post(http + 'changeStatusScreen', selectedData, function (data) {

            if (data.indexOf('success') >= 0) {
                showSuccessMessage("Sreening updated successfully");
                showItem();
            }
            else {
                showErrorMessage(data);
            }

            for (i = 0; i < data.length; i++) {
                identifiedData.push(data[i].cruise_safe_screening);
            }


                $('#myLoader').modal('hide');
   


        });

    });








    function vaidateFields(clas) {
        $('.' + clas).css('border-color', 'silver');
        var input;
        $('.' + clas).map(function () {
            if (!$(this).val() || $(this).css('border-color') == 'red') {
                $(this).css('border-color', 'red');
                $(this).focus();
                input = $(this);
            }
        }).get();

        if (input) {
            input.focus();
            return true;
        }
        else
            return false;
    }

    function validateEmail(sEmail, msg) {
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(sEmail)) {
            return true;
        }
        else {
            //var msg = "Please enter valid email address.";
            checkEmptyFields(msg);
            return false;
        }
    }

    function SuccessMessage(msg) {
        $('#myPleaseWait').modal('show');
        $('#txtModalLoaderAlertMessage').hide;
        $('#txtModalLoaderSuccessMessage').show();
        $('#txtModalLoaderSuccessMessage b').html(msg);

        setTimeout(function () {
            $('#myPleaseWait').hide();
            $('#txtModalLoaderSuccessMessage').hide();
            $('#myPleaseWait').modal('hide');
        }, 3000);
    }

    function checkEmptyFields(msg) {

        $('#myPleaseWait').modal('show');
        $('#txtModalLoaderAlertMessage').show();
        $('#txtModalLoaderAlertMessage b').text(msg);

        setTimeout(function () {
            $('#myPleaseWait').hide();
            $('#myPleaseWait').modal('hide');
            $('#txtModalLoaderAlertMessage').hide();
        }, 3000);
    }



    /***************************************************************
                        Validations Ends
    ***************************************************************/



    //Removing  Messages CoOperation Services

    $(document).on('click', function () {
        if ($('#txtModalLoaderAlertMessage').is(':visible')) {
            $('#txtModalLoaderAlertMessage').hide();
        }
        if ($('#txtModalLoaderSuccessMessage').is(':visible')) {
            $('#txtModalLoaderSuccessMessage').hide();
        }

    });






});