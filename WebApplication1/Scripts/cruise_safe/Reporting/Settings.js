﻿function checkPrev(myPrivs) {

    //-------------------- Check Prev ---------------------------//

    if ($('#lblUserType').text() != 'business') {
        if (myPrivs.indexOf(1002) >= 0) {

        } else {
            //   location.href = '/as_smart_connects/home';
        }
    }

    //-------------------- Check Prev ---------------------------//
}

$(document).ready(function () {

     var http = httpsapi + "/api/reporting/";
    //var http = httpsapi_local + '/api/reporting/';

    $('.actionAdministrator').trigger('click');
    $('.btnUserManagement').find('a').addClass('activeMenu');
    var allData = Array();

    function getTraces() {

        $('#myLoader').modal('show');

        var details = {
            client_id: $('#lblClientID').text(),
        };

        $.getJSON(http + 'getSettings', details, function (data) {

            allData = data;
            console.log(data);

            if (allData.cruise_safe_settings.settings_phone_verification_registration=='yes')
                $('#checkboxOTP').attr('checked', 'checked');
            if (allData.cruise_safe_settings.settings_enable_screening == 'yes')
                $('#checkboxScreening').attr('checked', 'checked');
            if (allData.cruise_safe_settings.settings_enable_comment == 'yes')
                $('#checkboxComment').attr('checked', 'checked');
            if (allData.cruise_safe_settings.settings_enable_notification == 'yes')
                $('#checkboxNotifications').attr('checked', 'checked');
            if (allData.cruise_safe_settings.settings_enable_articles == 'yes')
                $('#checkboxArticles').attr('checked', 'checked');

            if (allData.cruise_safe_settings.settings_enable_screening_for_others == 'yes')
                $('#checkboxOthersScreening').attr('checked', 'checked');
            if (allData.cruise_safe_settings.settings_phone_verification_screening == 'yes')
                $('#checkboxOPTScreening').attr('checked', 'checked');
                

            if (allData.cruise_safe_settings.settings_bluetooth == 'yes')
                $('#checkboxBluetooth').attr('checked', 'checked');
            if (allData.cruise_safe_settings.settings_location == 'yes')
                $('#checkboxLocation').attr('checked', 'checked');
            if (allData.cruise_safe_settings.settings_tracing_data_view == 'yes')
                $('#checkboxTracingData').attr('checked', 'checked');
            if (allData.cruise_safe_settings.settings_tracing_short_distance_alert == 'yes')
                $('#checkboxTracingDistanceAlert').attr('checked', 'checked');
            if (allData.cruise_safe_settings.settings_tracing_confirmed_alert == 'yes')
                $('#checkboxTracingConfirmedAlert').attr('checked', 'checked');

            $('#cboTracingDelay').val(allData.cruise_safe_settings.settings_tracing_data_sending_delay);

           // loadData();
            $('#myLoader').modal('hide');

        });
    }

    getTraces();


    var selectedData;
    var selectedId;
    var screening_questions_id;


    function loadData() {
        if (allData) {

            var str = "";
            for (i = 0; i < allData.cruise_safe_screening_questions.length; i++) {
                str += '<tr><td>' + (i + 1) + '</td>';
                str += '<td>' + allData.cruise_safe_screening_questions[i]['screening_questions'] + '</td>';
                str += '<td>' + allData.cruise_safe_screening_questions[i]['screening_questions_compulsory'] + '</td>';
                str += '<td>' + allData.cruise_safe_screening_questions[i]['screening_questions_yes_value'] + '</td>';
                str += '<td>' + allData.cruise_safe_screening_questions[i]['screening_questions_no_value'] + '</td>';
                str += '<td>' + allData.cruise_safe_screening_questions[i]['screening_questions_status'] + '</td>';
                str += '<td><button id="b' + i + '"    style="width:100px" type="button" class="btn btn-warning btn-flat btn-xs btnView">View</button></td>';
            }
            $('#listQuestions').html(str);
        }
        $('.btnView').click(function () {
            selectedId = this.id.substring(1);
            $('#viewModal').modal('show');
            selectedData = allData.cruise_safe_screening_questions[selectedId];
            showItem();
        });
    }


    function showItem() {
        $('.cleanField').val('');
        $('#btnAdd').html('Save Changes');
        $('#txtQuestion').val(selectedData['screening_questions']);
        $('#cboStatus').val(selectedData['screening_questions_status']);
        $('#cboCompulsory').val(selectedData['screening_questions_compulsory']);
        $('#txtYesValue').val(selectedData['screening_questions_yes_value']);
        $('#txtNoValue').val(selectedData['screening_questions_no_value']);
    }




    $(".btnWidget").click(function () {
        seletedTop = this.id.replace('_', ' ');
        $('.btnWidget').removeClass('active');
        $(this).addClass('active');
        $('.selectedTitle').html($(this).find('h4').text());
        $('.divSettings').hide();
        $('.div' + seletedTop).show('slow');

    });



    $(".btnClose").click(function () {
        $('#viewModal').modal('hide');
    });



    $('#btnAddNew').click(function () {
        selectedData = {};
        $('#btnAdd').html('Save');
        $('#txtQuestion').val('');
        $('#cboStatus').val('active');
        $('#cboCompulsory').val('yes');
        $('#txtYesValue').val('Yes');
        $('#txtNoValue').val('No');
        $('#viewModal').modal('show');
    });


    $('#btnAdd').on('click', function () {
        saveQuestion();
    });



    function saveQuestion() {

        if (vaidateFields('requiredField'))
            return;

        $('.myLoader').modal('show');

        selectedData.screening_questions_client_id = $('#lblClientID').text();
        selectedData.screening_questions_weight = 1;
        selectedData.screening_questions_updated_by = $('#lblUserID').text();
        selectedData.screening_questions_saved_by = $('#lblUserID').text();
        selectedData.screening_questions_status = $('#cboStatus').val();
        selectedData.screening_questions = $('#txtQuestion').val();
        selectedData.screening_questions_compulsory = $('#cboCompulsory').val();
        selectedData.screening_questions_yes_value = $('#txtYesValue').val();
        selectedData.screening_questions_no_value = $('#txtNoValue').val();

        $.post(http + 'saveQuestion', selectedData, function (data) {
            console.log(data);
            if (data.indexOf('success') >= 0) {
                showSuccessMessage(data);
                $('#viewModal').modal('hide');
                if (selectedData.screening_questions_id) {
                    $('#myLoader').modal('hide');
                    loadData();
                }
                else {
                    getTraces();
                }
            }
            else {

                $('#myLoader').modal('hide');

                showErrorMessage(data);
            }
        });
    }






    var clickCount = 0;


    $("#checkboxOTP").change(function () {
        if ($(this).is(':checked'))
            allData.cruise_safe_settings.settings_phone_verification_registration = 'yes';
        else
            allData.cruise_safe_settings.settings_phone_verification_registration = 'no';
        saveSettings('checkboxOTP');
    });
    $("#checkboxScreening").change(function () {
        if ($(this).is(':checked'))
            allData.cruise_safe_settings.settings_enable_screening = 'yes';
        else
            allData.cruise_safe_settings.settings_enable_screening = 'no';
        saveSettings('checkboxScreening');
    });
    $("#checkboxComment").change(function () {
        if ($(this).is(':checked'))
            allData.cruise_safe_settings.settings_enable_comment = 'yes';
        else
            allData.cruise_safe_settings.settings_enable_comment = 'no';
        saveSettings('checkboxComment');
    });
    $("#checkboxNotifications").change(function () {
        if ($(this).is(':checked'))
            allData.cruise_safe_settings.settings_enable_notification = 'yes';
        else
            allData.cruise_safe_settings.settings_enable_notification = 'no';
        saveSettings('checkboxNotifications');
    });
    $("#checkboxArticles").change(function () {
        if ($(this).is(':checked'))
            allData.cruise_safe_settings.settings_enable_articles = 'yes';
        else
            allData.cruise_safe_settings.settings_enable_articles = 'no';
        saveSettings('checkboxArticles');
    });




    $("#checkboxOthersScreening").change(function () {
        if ($(this).is(':checked'))
            allData.cruise_safe_settings.settings_enable_screening_for_others = 'yes';
        else
            allData.cruise_safe_settings.settings_enable_screening_for_others = 'no';
        saveSettings('checkboxOthersScreening');
    });
    $("#checkboxOPTScreening").change(function () {
        if ($(this).is(':checked'))
            allData.cruise_safe_settings.settings_phone_verification_screening = 'yes';
        else
            allData.cruise_safe_settings.settings_phone_verification_screening = 'no';
        saveSettings('checkboxOPTScreening');
    });




    $("#checkboxBluetooth").change(function () {
        if ($(this).is(':checked'))
            allData.cruise_safe_settings.settings_bluetooth = 'yes';
        else
            allData.cruise_safe_settings.settings_bluetooth = 'no';
        saveSettings('checkboxBluetooth');
    });
    $("#checkboxLocation").change(function () {
        if ($(this).is(':checked'))
            allData.cruise_safe_settings.settings_location = 'yes';
        else
            allData.cruise_safe_settings.settings_location = 'no';
        saveSettings('checkboxLocation');
    });
    $("#checkboxTracingData").change(function () {
        if ($(this).is(':checked'))
            allData.cruise_safe_settings.settings_tracing_data_view = 'yes';
        else
            allData.cruise_safe_settings.settings_tracing_data_view = 'no';
        saveSettings('checkboxTracingData');
    });

    $("#checkboxTracingDistanceAlert").change(function () {
        if ($(this).is(':checked'))
            allData.cruise_safe_settings.settings_tracing_short_distance_alert = 'yes';
        else
            allData.cruise_safe_settings.settings_tracing_short_distance_alert = 'no';
        saveSettings('checkboxTracingDistanceAlert');
    });
    $("#checkboxTracingConfirmedAlert").change(function () {
        if ($(this).is(':checked'))
            allData.cruise_safe_settings.settings_tracing_confirmed_alert = 'yes';
        else
            allData.cruise_safe_settings.settings_tracing_confirmed_alert = 'no';
        saveSettings('checkboxTracingConfirmedAlert');
    });


    $('#cboTracingDelay').change(function () {
        allData.cruise_safe_settings.settings_tracing_data_sending_delay = $(this).val();
        saveSettings('cboTracingDelay');
    });


    function saveSettings(clickedId) {

        if (clickCount == 0) {
            $('.myLoader').modal('show');

            allData.cruise_safe_settings.settings_updated_by = $('#lblUserID').text();

            $.post(http + 'saveSettings', allData.cruise_safe_settings, function (data) {
                console.log(data);
                $('.myLoader').modal('hide');
                if (data.indexOf('success') < 0) {
                    showErrorMessage(data);
                    clickCount = 1;
                    $("#" + clickedId).trigger('click');
                }
            });
        }
        else {
            clickCount = 0;
        }
    }














    function vaidateFields(clas) {
        $('.' + clas).css('border-color', 'silver');
        var input;
        $('.' + clas).map(function () {
            if (!$(this).val() || $(this).css('border-color') == 'red') {
                $(this).css('border-color', 'red');
                $(this).focus();
                input = $(this);
            }
        }).get();

        if (input) {
            input.focus();
            return true;
        }
        else
            return false;
    }

    function validateEmail(sEmail, msg) {
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(sEmail)) {
            return true;
        }
        else {
            //var msg = "Please enter valid email address.";
            checkEmptyFields(msg);
            return false;
        }
    }

    function SuccessMessage(msg) {
        $('#myPleaseWait').modal('show');
        $('#txtModalLoaderAlertMessage').hide;
        $('#txtModalLoaderSuccessMessage').show();
        $('#txtModalLoaderSuccessMessage b').html(msg);

        setTimeout(function () {
            $('#myPleaseWait').hide();
            $('#txtModalLoaderSuccessMessage').hide();
            $('#myPleaseWait').modal('hide');
        }, 3000);
    }

    function checkEmptyFields(msg) {

        $('#myPleaseWait').modal('show');
        $('#txtModalLoaderAlertMessage').show();
        $('#txtModalLoaderAlertMessage b').text(msg);

        setTimeout(function () {
            $('#myPleaseWait').hide();
            $('#myPleaseWait').modal('hide');
            $('#txtModalLoaderAlertMessage').hide();
        }, 3000);
    }



    /***************************************************************
                        Validations Ends
    ***************************************************************/



    //Removing  Messages CoOperation Services

    $(document).on('click', function () {
        if ($('#txtModalLoaderAlertMessage').is(':visible')) {
            $('#txtModalLoaderAlertMessage').hide();
        }
        if ($('#txtModalLoaderSuccessMessage').is(':visible')) {
            $('#txtModalLoaderSuccessMessage').hide();
        }

    });






});

