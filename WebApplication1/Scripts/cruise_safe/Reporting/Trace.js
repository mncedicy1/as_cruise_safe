﻿function checkPrev(myPrivs) {

    //-------------------- Check Prev ---------------------------//

    if ($('#lblUserType').text() != 'business') {
        if (myPrivs.indexOf(1002) >= 0) {

        } else {
            //   location.href = '/as_smart_connects/home';
        }
    }

    //-------------------- Check Prev ---------------------------//
}

$(document).ready(function () {



    var http = httpsapi + "/api/reporting/";
    var https = httpsapi + "/api/";
    //var http = httpsapi_local + '/api/reporting/';




    $('.actionAdministrator').trigger('click');
    $('.btnUserManagement').find('a').addClass('activeMenu');



    var id;
    var start_from = 0;
    var last_row = false;
    var isSending = false;
    var fullname = "";
    var person_id;
    var system_user_id;

    var seletedTop = 'Very High';
    var allData = Array();

    var arr = ['Very High', 'High', 'Medium', 'Low'];
    var arrCount = [0, 0, 0, 0];
    var arrToday = [0, 0, 0, 0];


    function getTraces() {


        $('#myLoader').modal('show');

        var details = {
            client_id: $('#lblClientID').text(),
        };

        $.getJSON(http + 'getTraces', details, function (data) {

            allData = data;
            console.log(data);
            arrCount = [0, 0, 0, 0];
            $('.Trace').find('span').html(allData.length);

            for (i = 0; i < allData.length; i++) {
                var indx = arr.indexOf(allData[i]['identified_description']);
                arrCount[indx]++;
                var date = new Date().getHours() - new Date(allData[i]['identified_timestamp']).getHours();
                arrToday[indx] += arrToday[indx] + (date < 24) ? 1 : 0;
            }
            for (var c = 0; c < arr.length; c++) {
                $('#' + arr[c].replace(' ', '_')).find('h2').find('b').html(arrCount[c]);
                $('#' + arr[c].replace(' ', '_')).find('p').find('b').html(arrToday[c]);
            }

            loadData();

                $('#myLoader').modal('hide');

        });
    }

    getTraces();


    var selectedData;
    var selectedId;


    function loadData() {
        if (allData) {

            var str = "";
            for (i = 0; i < allData.length; i++) {
                if (allData[i]['identified_description'] == seletedTop) {
                    str += '<tr><td>' + (i + 1) + '</td>';
                    str += '<td>' + allData[i]['identified_reference'] + '</td>';
                    str += '<td>Less Than ' + allData[i]['identified_end_distance'] + ' Meter(s)</td>';
                    str += '<td>' + allData[i]['identified_description'] + '</td>';
                    str += '<td>' + allData[i]['identified_cellphone'] + '</td>';
                    str += '<td>' + allData[i]['identified_status'] + '</td>';
                    str += '<td>' + getDateTime(allData[i]['identified_timestamp']) + '</td>';
                    str += '<td><button id="b' + i + '"    style="width:100px" type="button" class="btn btn-success btn-flat btn-xs btnView">View</button></td>';
                }
            }
            $('#tblReports').html(str);
        }
        $('.btnView').click(function () {
            selectedId = this.id.substring(1);
            $('#viewModal').modal('show');
            selectedData = allData[selectedId];
            showItem();
            getIdentified(selectedData['identified_registered_id']);
        });
    }

    function showItem() {
        $('.clsReference').html(selectedData['identified_reference']);
        $('.clsDistance').html(selectedData['identified_bluetooth_extra'] + ' Meters');
        $('.clsRisk').html(selectedData['identified_description']);
        $('.clsCellphone').html(selectedData['identified_cellphone']);
        $('.clsStatus').html(selectedData['identified_status']);
        $('.clsDate').html(getDateTime(selectedData['identified_timestamp']));
        $('#myLoader').modal('hide');

        if (selectedData['identified_status'] == 'new')
            $('.footerr').show('slow');
        else
            $('.footerr').hide('slow');
    }





    $(".btnClose").click(function () {
        $('#viewModal').modal('hide');
    });




    $(".btnWidget").click(function () {
        seletedTop = this.id.replace('_', ' ');
        $('.btnWidget').removeClass('active');
        $(this).addClass('active');
        $('.selectedTitle').html($(this).find('h4').text());
        loadData();

    });




    var identifiedData;

    function getIdentified(registered_id) {

        $('#myLoader').modal('show');
        var details = {
            registered_id: registered_id,
        };
        $.getJSON(http + 'getIdentified', details, function (data) {
            identifiedData = data;

            $('.clsCount').html(data.length);

            var str = "";
            for (i = 0; i < identifiedData.length; i++) {
                str += '<tr><td>' + (i + 1) + '</td>';
                str += '<td>' + identifiedData[i]['identified_reference'] + '</td>';
                str += '<td>Less Than ' + identifiedData[i]['identified_end_distance'] + ' Meter(s)</td>';
                str += '<td>' + identifiedData[i]['identified_description'] + '</td>';
                str += '<td>' + identifiedData[i]['identified_cellphone'] + '</td>';
                str += '<td>' + identifiedData[i]['identified_status'] + '</td>';
                str += '<td>' + getDatee(identifiedData[i]['identified_timestamp']) + '</td>';
                str += '<td><button id="r' + i + '"    style="width:100%" type="button" class="btn btn-success btnViewer btn-flat btn-xs btnViewr">View</button></td>';
            }
            $('#tblReportsHistory').html(str);

                $('#myLoader').modal('hide');

            $('.btnViewr').click(function () {
                var id = this.id.substring(1);
                setTimeout(function () {
                    $('#myLoader').modal('show');
                    selectedData = identifiedData[id];
                    showItem();
                }, 300);
            });

        });
    }


    $(document).on("click", ".btnViewer", function () {
        $('.btnViewer').removeClass('btn-warning').addClass('btn-success');
        $(this).removeClass('btn-success').addClass('btn-warning');
    });




    $(".btnSubmit").click(function () {

        $('#myLoader').modal('show');
        selectedData.identified_status = this.id;

        $.post(http + 'changeStatusTrace', selectedData, function (data) {

            if (data.indexOf('success') >= 0) {
                showSuccessMessageClose("Sreening updated successfully");
                showItem();
            }
            else {
                showErrorMessage(data);
            }

            for (i = 0; i < data.length; i++) {
                identifiedData.push(data[i].cruise_safe_screening);
            }


            $('#myLoader').modal('hide');



        });
    });


    function vaidateFields(clas) {
        $('.' + clas).css('border-color', 'silver');
        var input;
        $('.' + clas).map(function () {
            if (!$(this).val() || $(this).css('border-color') == 'red') {
                $(this).css('border-color', 'red');
                $(this).focus();
                input = $(this);
            }
        }).get();

        if (input) {
            input.focus();
            return true;
        }
        else
            return false;
    }

    function validateEmail(sEmail, msg) {
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(sEmail)) {
            return true;
        }
        else {
            //var msg = "Please enter valid email address.";
            checkEmptyFields(msg);
            return false;
        }
    }

    function SuccessMessage(msg) {
        $('#myPleaseWait').modal('show');
        $('#txtModalLoaderAlertMessage').hide;
        $('#txtModalLoaderSuccessMessage').show();
        $('#txtModalLoaderSuccessMessage b').html(msg);

        setTimeout(function () {
            $('#myPleaseWait').hide();
            $('#txtModalLoaderSuccessMessage').hide();
            $('#myPleaseWait').modal('hide');
        }, 3000);
    }

    function checkEmptyFields(msg) {

        $('#myPleaseWait').modal('show');
        $('#txtModalLoaderAlertMessage').show();
        $('#txtModalLoaderAlertMessage b').text(msg);

        setTimeout(function () {
            $('#myPleaseWait').hide();
            $('#myPleaseWait').modal('hide');
            $('#txtModalLoaderAlertMessage').hide();
        }, 3000);
    }



    /***************************************************************
                        Validations Ends
    ***************************************************************/



    //Removing  Messages CoOperation Services

    $(document).on('click', function () {
        if ($('#txtModalLoaderAlertMessage').is(':visible')) {
            $('#txtModalLoaderAlertMessage').hide();
        }
        if ($('#txtModalLoaderSuccessMessage').is(':visible')) {
            $('#txtModalLoaderSuccessMessage').hide();
        }

    });






});

