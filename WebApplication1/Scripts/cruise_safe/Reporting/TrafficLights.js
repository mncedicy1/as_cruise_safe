﻿function checkPrev(myPrivs) {

    //-------------------- Check Prev ---------------------------//

    if ($('#lblUserType').text() != 'business') {
        if (myPrivs.indexOf(1002) >= 0) {

        } else {
            //   location.href = '/as_smart_connects/home';
        }
    }

    //-------------------- Check Prev ---------------------------//
}

$(document).ready(function () {



   var http = httpsapi + "/api/reporting/";
    var https = httpsapi + "/api/";
    //http = httpsapi_local + '/api/reporting/';




    $('.actionAdministrator').trigger('click');
    $('.btnUserManagement').find('a').addClass('activeMenu');



    var regions = Array();
    var destination;
    var source;
    var travelMode = 'DRIVING';
    var latMap = '';
    var lngMap = '';
    var town = '';
    var seletedTop = '0';
    var allData = Array();

    var arr = ['','working', 'unsure', 'broken'];
    var arrCount = Array();
    var arrToday = Array();
    var arrTodayBoundary = Array();

    function getTrafficLight() {

        loadRegions();

        $('#myLoader').modal('show');


        var details = {
            traffic_light_client_id: $('#lblClientID').text(),
            traffic_light_search: $('#txtSearch').val()
        };

        $.getJSON(http + 'getTrafficLight', details, function (data) {
            console.log('getTrafficLight', data);
            allData = data;

            loadData(true);

            setTimeout(function () {
                $('#myLoader').modal('hide');
            }, 1000);

        });
    }




    getRegionsAll();
   

    function getRegionsAll() {
    
        var details = {
            region_client_id: $('#lblClientID').text(),
            //  userId: $('#lblUserID').text(),
        };
        $.getJSON(http + 'getRegionsAll', details, function (data) {
            console.log('getRegionsAll', data);
            if (data) {
                regions = data;
               
                $('#cboRegions').html('<option value="">-- Choose Region --</option>');
                for (var i = 0; i < data.length; i++) {
                    var htmlcontent = '<option value="' + data[i].region.OBJECTID + '">' + data[i].region.REGIONNAME + '</option>';
                    $('#cboRegions').append(htmlcontent);
                }

                var htmlcontent = '';
                for (var i = 0; i < data.length; i++) {
                    for (var c = 0; c < data[i].offices.length; c++) {
                        htmlcontent += '<option class="region region' + data[i].region.OBJECTID + '" value="' + data[i].offices[c].region_office_address_location + '">Regional Office (' + data[i].offices[c].region_office_name + ')</option>';
                    }
                }
                htmlcontent += '<option value="-26.2024661,28.0367126">Head Office (75 Helen Joseph St)</option>';
                htmlcontent += '<option value="Current">Current Location</option>';
                $('#cboStartFrom').html(htmlcontent);
            }
            getTrafficLight();
        });

    }


    function loadRegions() {
        var str = '';
        arr = Array();
        arrCount = Array();
        arrToday = Array();
        arrTodayBoundary = Array();

        for (var r = -1; r < regions.length; r++) {
            var name = r < 0 ? 'All' : regions[r].region.REGIONNAME;
            var id = r < 0 ? 0 : regions[r].region.OBJECTID;
            var isActive = r < 0 ? 'active' : '';
            arr.push(id); arrCount.push(0); arrToday.push(0); arrTodayBoundary.push(0);
            str += '<div class="col-sm-6 col-lg-3"><div class="panel text-center btnWidget ' + isActive + '" id="cat' + id + '">' +
                '<div class="panel-heading" style="padding-bottom: 0px;"><h6 class="panel-title text-cornsilk font-light">' + name + '</h6>' +
                '</div><div class="panel-body p-t-0" style="padding:10px;"><h4 class="m-t-5 m-b-5" >' +
                '<i class="fas fa-chart-line text-primary m-r-10"></i> <b class="text-white">0</b></h4>' +
                '<small style="font-size:12px" class="text-cornsilk m-b-0 m-t-0"><b style="color:red" class="broken">0</b> ' +
                'Broken&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Outside <b style="color:orange" class="boundary">0</b> </small></div></div></div>';
        }
        $('#listRegions').html(str);
    }



    var imgPosition = 0;
    var selectedId;
    function loadData(loadTop) {


        if (loadTop) {


            for (i = 0; i < allData.length; i++) {

                var indx = arr.indexOf(allData[i]['traffic_light_region_id']);
                arrCount[indx]++;
                arrCount[0]++;
                var status = allData[i]['traffic_light_status'];
                arrToday[indx] += (status == 'broken') ? 1 : 0;
                arrToday[0] += (status == 'broken') ? 1 : 0;
                var boundary = allData[i]['traffic_light_region_name'];
                arrTodayBoundary[indx] += (boundary == 'Out of Bounds') ? 1 : 0;
                arrTodayBoundary[0] += (boundary == 'Out of Bounds') ? 1 : 0;

            }
            for (var c = 0; c < arr.length; c++) {
                $('#cat' + arr[c]).find('h4').find('b').html(arrCount[c]);
                $('#cat' + arr[c]).find('small').find('.broken').html(arrToday[c]);
                $('#cat' + arr[c]).find('small').find('.boundary').html(arrTodayBoundary[c]);

            }
        }



        if (allData) {

            var str = "";
            for (i = 0; i < allData.length; i++) {
                if (seletedTop == 0 || allData[i]['traffic_light_region_id'] == seletedTop) {
                    str += '<tr><td>' + (i + 1) + '</td>';
                    str += '<td>' + subText(allData[i]['traffic_light'] + ', ' + allData[i]['traffic_light_address'], 35, '...') + '</td>';
                    str += '<td class="canSearch">' + subText(allData[i]['traffic_light_town'], 15, '...') + '</td>';
                    str += '<td>' + subText(allData[i]['traffic_light_latitude'], 10, '') + ', ' + subText(allData[i]['traffic_light_longitude'], 10, '') + '</td>';
                    str += '<td class="canSearch">' + allData[i]['traffic_light_region_name_near'] + '</td>';
                    str += '<td class="canSearch">' + getStatus(allData[i]['traffic_light_status']) + '</td>';
                    str += '<td><button id="b' + i + '" style="width:100px" ' +
                        'type="button" class="btn btn-primary btn-flat btn-xs btnView pull-right">View</button></td>';
                }
            }
            $('#tblTrafficLights').html(str);
        }



        $('.btnView').click(function () {
            var i = this.id.substring(1);
            loadSelected(i);
        });

        $('.canSearch').click(function () {
            $('#txtSearch').val($(this).text().trim()).trigger('change');
        });

    }





    function mySearch(text) {
        var filter, table, tr, td, i, txtValue;
        filter = text.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[0];
            if (td) {
                txtValue = tr[i].textContent || tr[i].innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
        console.log(text);
    }



    $('#txtSearch').change(function () {
        mySearch($(this).val());
    }).keyup(function () {
        mySearch($(this).val());
    });


    $('#btnSearch').click(function () {
        getTrafficLight();
    });






    function loadSelected(i) {
        selectedId = i;

        $('#viewModal').modal('show');
        $('.clsBtnStatus').hide();
        $('.footer').hide();
        latMap = allData[i].traffic_light_latitude;
        lngMap = allData[i].traffic_light_longitude;
        destination = { lat: parseFloat(latMap), lng: parseFloat(lngMap) };

        $('.region').hide();
        $('.region' + allData[i]['traffic_light_region_id']).show();


        $('#cboStartFrom option').each(function () {
            if ($(this).css('display') != 'none') {
                $(this).prop("selected", true);
                return false;
            }
        });
        $('#cboStartFrom').trigger('change');


        $('.clsProvince').html(allData[i]['traffic_light_metropolitan'] + ' - ' + allData[i]['traffic_light_province']);
        $('.clsStreet').html(allData[i]['traffic_light']);
        var reg = allData[i]['traffic_light_region_name'] != 'Out of Bounds' ? '' : ' <small style="color:#ff4a4a">(Out of Bounds)</small>';
        $('.clsLocationType').html(allData[i]['traffic_light_region_name_near'] + reg);
        $('.clsTown').html(allData[i]['traffic_light_town']);
        $('.clsSuburb').html(allData[i]['traffic_light_address']);
        $('.clsDate').html(getDateTime(allData[i]['traffic_light_timestamp']));
        $('.clsAddress').html(allData[i]['traffic_light_title']);
        $('.clsStatus').html(getStatus(allData[i]['traffic_light_status']));
        $('.clsCoordinates').html(allData[i]['traffic_light_latitude'] + ',' + allData[i]['traffic_light_longitude']);
        if (allData[i]['traffic_light_status'] == 'working')
            $('.btnBroken').show();
        else
            $('.btnWorking').show();


        $('#viewMapModal').modal('hide');
    }






    $(document).on('click', ".btnWidget", function () {
        $(document).find(".btnWidget").removeClass('active');
        $(this).addClass('active');

        if (!$(this).hasClass('btnActionType')) {
            seletedTop = this.id.replace('cat', '');
            loadData(false);

        }
    });











    var marker;
    var geocoder = new google.maps.Geocoder();
    var infowindow = new google.maps.InfoWindow;
    var mapp;
    var flightPath;
    var styleControl1 = document.getElementById('style-selector-control1');
    var styleSelector1 = document.getElementById('style-selector1');

    $('#btnAddNew').click(function () {
     
        $('.adminUsers-modal-lg').modal('show');



        $('.cleanUserField').val('');
       mapp = new google.maps.Map(document.getElementById('dvMap11'), {
           zoom: 11,
           center: new google.maps.LatLng(-26.2566038, 27.9848897),
           mapTypeId: google.maps.MapTypeId.ROADMAP,
           mapTypeControl: false,
           options: {
               gestureHandling: 'greedy'
           }
        });
        selected(mapp.center, mapp, null);

        mapp.data.addGeoJson(mapData);

        mapp.data.setStyle({
            icon: '//example.com/path/to/image.png',
            fillColor: '#000868b8',
            strokeWeight: 1
        });
      
        mapp.controls[google.maps.ControlPosition.TOP_LEFT].push(styleControl1);

        mapp.data.addListener('click', function (event) {
            var bounds = new google.maps.LatLngBounds();
            event.feature.h.h[0].h.forEach(function (coord, index) {
                bounds.extend(coord);
            });
            //  map.setZoom(11);

            mapp.setCenter(bounds.getCenter());
            mapp.fitBounds(bounds);

            selected(event.latLng, mapp, null);
        });




        google.maps.event.addListenerOnce(mapp, 'bounds_changed', function () {
            mapp.setZoom(mapp.getZoom() - 1);
        });


        mapp.data.addListener('mouseover', function (event) {
            mapp.data.overrideStyle(event.feature, { fillColor: 'red' });
        });
        mapp.data.addListener('mouseout', function (event) {
            mapp.data.overrideStyle(event.feature, { fillColor: '#000868b8' });
        });

        mapp.data.addListener('mouseover', mouseOverDataItem1);
        mapp.data.addListener('mouseout', mouseOutOfDataItem1);



       
        mapp.setOptions({ styles: styles[styleSelector1.value] });
        mapp.setTilt(45);
        styleSelector1.addEventListener('change', function () {
            if (styleSelector1.value == 'satellite') {
                mapp.setMapTypeId(google.maps.MapTypeId.HYBRID);
                mapp.setOptions({ styles: null });
            }
            else {
                mapp.setMapTypeId(google.maps.MapTypeId.ROADMAP);
                mapp.setOptions({ styles: styles[styleSelector1.value] });
            }
        });

        var count = 0;
        for (var i = 0; i < allData.length; i++) {
                count++;
                var latLng = new google.maps.LatLng(allData[i].traffic_light_latitude
                    , allData[i].traffic_light_longitude);

            var div = document.createElement('div');
            div.className = (allData[i]['traffic_light_status'] == 'working') ? 'ripple-green' : (allData[i]['traffic_light_status'] == 'unsure') ? 'ripple-#000868b8' : 'ripple-red';

            var zindex = (allData[i]['traffic_light_status'] == 'working') ? '10' : (allData[i]['traffic_light_status'] == 'unsure') ? '11' : '12';


            var marker = new RichMarker({
                position: latLng,
                map: mapp,
                id: i,
                zIndex: zindex,
                flat: true,
                content: div,
                draggable: false,
                animation: google.maps.Animation.DROP,
                title: allData[i].traffic_light_title
            });
            markers.push(marker);
            console.log(marker, mapp);
        }

        

     
        function mouseOverDataItem1(mouseEvent) {
            const titleText = mouseEvent.feature.j.REGIONNAME;
            if (titleText) {
                mapp.getDiv().setAttribute('title', titleText);
            }
        }

        function mouseOutOfDataItem1(mouseEvent) {
            mapp.getDiv().removeAttribute('title');
        }


       
        google.maps.event.addListener(mapp, 'click', function (event) {
            selected(event.latLng, mapp,null);
        });

        $('#btnSearchCoordinates').click(function () {
            var latlngStr = $('#searchCoordinates').val().split(',', 2);
            var latLng = { lat: parseFloat(latlngStr[0]), lng: parseFloat(latlngStr[1]) };
            selected(latLng, mapp,null);
        });

        $('#btnAddress').click(function () {
            selected(null, mapp, $('#searchAddress').val());
        });


        var input = document.getElementById('searchAddress');
        var searchBox = new google.maps.places.SearchBox(input);
        // mapp.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        searchBox.addListener('place_changed', function () {
            var place = searchBox.getPlace();
            if (place.geometry) {
                selected(place.geometry.location, mapp, null);
            }
        });
       
    });


    function getMeters(lat1, lon1, lat2, lon2) {
        var R = 6371e3; // metres
        var φ1 = lat1 * Math.PI / 180; // φ, λ in radians
        var φ2 = lat2 * Math.PI / 180;
        var Δφ = (lat2 - lat1) * Math.PI / 180;
        var Δλ = (lon2 - lon1) * Math.PI / 180;

        var a = Math.sin(Δφ / 2) * Math.sin(Δφ / 2) +
            Math.cos(φ1) * Math.cos(φ2) *
            Math.sin(Δλ / 2) * Math.sin(Δλ / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        var d = R * c; // in metres
        return d;
    }
    

 
    function selected(latLng, map, address) {
        var data = (latLng) ? { 'latLng': latLng } : { 'address': address };
        console.log(data);
        geocoder.geocode(data, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    getLocation(results[0], results[0].geometry.location);
                    if (marker)
                    marker.setMap(null);

                    marker = new google.maps.Marker({
                        map: map,
                        position: results[0].geometry.location,
                        icon: getImageMarker(),
                        draggable: true,
                       // animation: google.maps.Animation.DROP,
                        title: results[0].formatted_address

                    });

                    (function (marker) {
                        google.maps.event.addListener(marker, "dragend", function (e) {
                         
                            geocoder.geocode({
                                'latLng': e.latLng
                            }, function (results, status) {
                                if (status == google.maps.GeocoderStatus.OK) {
                                    if (results[0]) {
                                     
                                        getLocation(results[0], e.latLng);
                                    }
                                }
                            });
                        });
                    })(marker);

                    infowindow.setContent(results[0].formatted_address);
                    infowindow.open(map, marker);
                }
            }
        });


  


    }


    function isWithinPoly(latLng) {
         // console.log(mapData, features[0].h.h[0].h);
        var obj = { region_name: '', region_name_near: '', region_id: 0, region_name_near_dist: 0, region_within: false, region_lat: '', region_lng: '' };
        for (var f = 0; f < features.length; f++) {
            var paths = features[f].h.h[0].h;
            var polygon = new google.maps.Polygon({
                paths: paths,
                id: mapData.features[f].properties.OBJECTID,
                name: mapData.features[f].properties.REGIONNAME,
                visible: false
            });

            var isWithinPolygon = google.maps.geometry.poly.containsLocation(latLng, polygon);
            if (isWithinPolygon) {
                obj.region_id = polygon.id;
                obj.region_name = polygon.name;
                obj.region_name_near = polygon.name;
                obj.region_name_near_dist = 0;
                obj.region_within = true;
                obj.region_lat = '';
                obj.region_lng = '';
                f = features.length;
            }
            else {
                for (var i = 0; i < paths.length; i++) {
                    var dist = getMeters(latLng.lat(), latLng.lng(), paths[i].lat(), paths[i].lng());
                    if (obj.region_name_near_dist == 0 || obj.region_name_near_dist > dist / 1000) {
                        obj.region_name_near_dist = dist/1000;
                        obj.region_id = polygon.id;
                        obj.region_name = 'Out of Bounds';
                        obj.region_name_near = polygon.name;
                        obj.region_within = false;
                        obj.region_lat = paths[i].lat();
                        obj.region_lng = paths[i].lng();
                    }
                }
            }
        }
        console.log(obj);
        return obj;
    }


    var details, region;
    function getLocation(results, latLng) {

        region  = isWithinPoly(latLng);


        console.log(results,latLng);
        console.log("Latitude: " + latLng.lat() + " " + ", longitude: " + latLng.lng());
        $('#txtCoordinates').val(latLng.lat() + ',' + latLng.lng());
        $('#txtTown').val(results.address_components[3].long_name);
        $('#txtSuburb').val(results.address_components[2].long_name);
        $('#txtStreet').val(results.address_components[0].long_name + " " + results.address_components[1].long_name);
        $('#txtRegion').val(region.region_name);

        details = {
            traffic_light_created_by: $('#lblUserID').text(),
            traffic_light_client_id: $('#lblClientID').text(),
            traffic_light_title: results.formatted_address,
            traffic_light_latitude: latLng.lat(),
            traffic_light_longitude: latLng.lng(),
            traffic_light: results.address_components[0].long_name+" "+results.address_components[1].long_name,
            traffic_light_address: (results.address_components[2]) ? results.address_components[2].long_name : "",
            traffic_light_town: (results.address_components[3]) ? results.address_components[3].long_name : "",
            traffic_light_metropolitan: (results.address_components[4]) ? results.address_components[4].long_name : "",
            traffic_light_province: (results.address_components[5]) ?results.address_components[5].long_name:"",
            traffic_light_other: results.formatted_address,
            traffic_light_postal_code: (results.address_components[7]) ? results.address_components[7].long_name : (results.address_components[6]) ? results.address_components[6].long_name:"",

            traffic_light_region_name: region.region_name,
            traffic_light_region_name_near: region.region_name_near,
            traffic_light_region_name_near_distance_km: region.region_name_near_dist,
            traffic_light_region_id: region.region_id
        };

        if (flightPath)
            flightPath.setMap(null);



        if (region.region_name == 'Out of Bounds') {
            var flightPlanCoordinates = [
                { lat: parseFloat(details.traffic_light_latitude), lng: parseFloat(details.traffic_light_longitude) }];
            var coord = {}; var title = '';
            if (region.region_name_near_dist > 2) {
               

                msg = parseInt(region.region_name_near_dist) + ' Km away from ' + region.region_name_near + ' (Out of Bounds),  The distance between traffic light and region must be less than 2 Km.';
                title = parseInt(region.region_name_near_dist) + ' Km';
                coord = { lat: parseFloat(region.region_lat), lng: parseFloat(region.region_lng) };
                flightPlanCoordinates.push(coord);
                console.log(flightPlanCoordinates);
              //  showErrorMessageClose(msg);
                flightPath = new google.maps.Polyline({
                    path: flightPlanCoordinates,
                    geodesic: true, strokeColor: '#FF0000',
                    strokeOpacity: 1.0, strokeWeight: 4, title: title
                });
                flightPath.setMap(mapp);
                var latLng = new google.maps.LatLng(coord.lat, coord.lng);
                var latLng1 = new google.maps.LatLng(flightPlanCoordinates[0].lat, flightPlanCoordinates[0].lng);
                var bounds = new google.maps.LatLngBounds();
                bounds.extend(latLng);
                bounds.extend(latLng1);
                mapp.setCenter(bounds.getCenter());
                mapp.fitBounds(bounds, { top: 100, right: 100, left: 100 });
                marker.title = msg;

            }
        }



    }

   
    $("#btnSave").click(function () {
        if (flightPath)
        flightPath.setMap(null);
        var flightPlanCoordinates = [
            { lat: parseFloat(details.traffic_light_latitude), lng: parseFloat(details.traffic_light_longitude) }];

        var msg = '';
        var title = '';
        var meterslast = 9999999;
        var coord = {};
        for (var i = 0; i < allData.length; i++) {
            var meters = getMeters(allData[i].traffic_light_latitude, allData[i].traffic_light_longitude,
                details.traffic_light_latitude, details.traffic_light_longitude);
            if (meters < 100) {
                if (meters < meterslast) {
                    msg = 'Distance between two trafic light must be greater than 100 meters';
                    meterslast = meters;
                    title = parseInt(meterslast) + ' meters';
                    coord = { lat: parseFloat(allData[i].traffic_light_latitude), lng: parseFloat(allData[i].traffic_light_longitude) };
                }
            }
        }


        if (region.region_name =='Out of Bounds') {
            if (region.region_name_near_dist > 2) {
                msg = parseInt(region.region_name_near_dist) + ' Km away from ' + region.region_name_near +' (Out of Bounds),  The distance between traffic light and region must be less than 2 Km.';
                title = parseInt(region.region_name_near_dist) + ' Km';
                coord = { lat: parseFloat(region.region_lat), lng: parseFloat(region.region_lng) };
            }
        }


        console.log(meterslast);
        flightPlanCoordinates.push(coord);

         
        if (msg) {
            console.log(flightPlanCoordinates);
            showErrorMessage(msg);
            flightPath = new google.maps.Polyline({
                path: flightPlanCoordinates,
                geodesic: true, strokeColor: '#FF0000',
                strokeOpacity: 1.0, strokeWeight: 4, title: title
            });
            flightPath.setMap(mapp);
            var latLng = new google.maps.LatLng(coord.lat, coord.lng);
            var latLng1 = new google.maps.LatLng(flightPlanCoordinates[0].lat, flightPlanCoordinates[0].lng);
            var bounds = new google.maps.LatLngBounds();
            bounds.extend(latLng);
            bounds.extend(latLng1);
            mapp.setCenter(bounds.getCenter());
            mapp.fitBounds(bounds, { top: 100, right: 100, left: 100 });

 return;
           
        }
        $('#myLoader').modal('show');
        $.post(http + 'saveTrafficLight', details, function (data) {
            if (data.indexOf('success') >= 0) {
                showSuccessMessageClose("Traffic Light Saved");
                $('.adminUsers-modal-lg').modal('hide');
                getTrafficLight();
            }
            else {
                showErrorMessage(data);
                $('#myLoader').modal('hide');
            }

           
        });
    });




    function getImageMarker() {
        return {
            url: '/Content/Images/traffic_light.png',
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(30, 30)
        };
    }



    function updateSeenStatus(reporting) {
        $.post(http + 'updateViewStatus', reporting, function (data) {
            console.log(data);
            if (data.indexOf('success') < 0) {
                showErrorMessage(data);
            }
        });
    }

    function getStatus(status) {
        if (status == 'unsure')
            return '<span class="badge badge-warning" style="width:105px">' + status + '</span>';
        if (status == 'working')
            return '<span class="badge badge-success" style="width:105px">' + status + '</span>';
        else
            return '<span class="badge badge-danger" style="width:105px">' + status + '</span>';
    }

    
    
    function getImage(imagess) {
        var str = '';
        for (var ii = 0; ii < imagess.length; ii++) {
            str += '<a data-magnify="gallery" data-src="" data-caption="" data-group="a" href="' + imagess[ii].image_location + '">' +
                    '<i class="fas fa-image" style="color:green; margin-right:5px"></i></a>';
        }
        return str;
    }




    $('.clsRight').click(function () {
        imgPosition++;
        if (imgPosition >= allData[selectedId]['image'].length)
            imgPosition = 0;

        $('.clsImage').attr('src', allData[selectedId]['image'][imgPosition].image_location);
    });


    $('.clsLeft').click(function () {
        imgPosition--;
        if (imgPosition < 0)
            imgPosition = allData[selectedId]['image'].length - 1;

        $('.clsImage').attr('src', allData[selectedId]['image'][imgPosition].image_location);
    });


    $('.btnClose').click(function () {
        $('#viewModal').modal('hide');
        $('.footer').show('slow');
        if (document.getElementById("myAudio")) {
            document.getElementById("myAudio").pause();
            $('#myAudio').remove();
        }
    });


    $('.btnCloseMap').click(function () {
        $('#viewMapModal').modal('hide');
        $('.footer').show('slow');
    });




    var light_status = '';

    $('.btnRemove').click(function () {
        $('#clsComment').val('');
        light_status = "removed";
        $('.clsTitle').html('Delete traffic light');
        $('.clsMsg').html('Are you sure you want to <b style="color:red">delete</b> this traffic light?');
        $('.confirmModal').modal('show');
    });

    $('.btnBroken').click(function () {
        $('#clsComment').val('');
        light_status = "broken";
        $('.clsTitle').html('Update traffic light');
        $('.clsMsg').html('Are you sure this traffic light is <b style="color:red">broken</b>?');
        $('.confirmModal').modal('show');
    });

    $('.btnWorking').click(function () {
        $('#clsComment').val('Fixed');
        light_status = "working";
        $('.clsTitle').html('Update traffic light');
        $('.clsMsg').html('Are you sure this traffic light is <b style="color:green">working</b>?');
        $('.confirmModal').modal('show');
    });


    $('#btnYesConfirm').click(function () {
        console.log(vaidateFields('requiredComment'));
        if (!vaidateFields('requiredComment')) {
            allData[selectedId].traffic_light_status = light_status;
            allData[selectedId].traffic_light_updated_by = $('#lblUserID').text(),
                allData[selectedId].traffic_light_creator_comment = $('#clsComment').val();
            $('.confirmModal').modal('hide');
            updateTrafficLight(allData[selectedId]);
        }
    });

    function updateTrafficLight(traffic_light) {
        $('#myLoader').modal('show');
        $.post(http + 'updateTrafficLight', traffic_light, function (data) {
            console.log(data);
            if (data.indexOf('success') >= 0) {
                showSuccessMessageClose('Success');
                $('#viewModal').modal('hide');
                $('#viewMapModal').modal('hide');
                $('.footer').show('slow');
                $('#myLoader').modal('show');
                getTrafficLight();
            } else {
                showErrorMessageClose(data);
            }
        });
    }





    $(".btnWidget").click(function () {
        seletedTop = this.id.replace('_', ' ').replace('_', ' ');
        $('.btnWidget').removeClass('active');
        $('div[id="' + this.id+'"]').addClass('active');
        $('.selectedTitle').html($(this).find('h4').text());
      
            $('.selectedTitle').html($(this).find('h4').text());
            loadData(false);


    });





    $('#cboTravelMode').change(function () {
        travelMode = $(this).val();
        loadMap();
    });




    $('#cboStartFrom').change(function () {
        if ($(this).val() == 'Current') {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    source = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                    loadMap();
                });
            } else {
                showErrorMessageClose("Browser doesn't support Geolocation");
            }
        }
        else {
            source = {
                lat: parseFloat($(this).val().split(',')[0]),
                lng: parseFloat($(this).val().split(',')[1])
            };
            loadMap();
        }
       
    });


    $('#btnMap').click(function () {
        $('.clsTop').show();
        $('#viewMapModal').modal('show');
        $('.footer').hide('slow');
        loadMap1();
    });




   
    $('.btnLocate').click(function () {
        loadMap2();
    });

    function loadMap2() {


        report = allData[selectedId];
        $('.clsTop').hide();
        $('#viewMapModal').modal('show');


    

        var icons = {
            'Bad Road': {
                icon: '/Content/Images/roads.png'
            },
            'Pothole': {
                icon: '/Content/Images/spot_green.png'
            },
            'Broken Traffic Light': {
                icon: '/Content/Images/traffic_light.png'
            }
        };
        function getImageMarker() {
            return {
                url: icons[report.reporting_category].icon,
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(30, 30)
            };
        }


        var latlngbounds = new google.maps.LatLngBounds();
        var bounds = new google.maps.LatLngBounds();
       
        
                var latLng = new google.maps.LatLng(report.reporting_incident_location_latitude
            , report.reporting_incident_location_longitude);

           /*     var div = document.createElement('div');
                div.className = (report['traffic_light_status'] == 'working') ? 'ripple-green' : (report['traffic_light_status'] == 'unsure') ? 'ripple-#000868b8' : 'ripple-red';

                var marker = new RichMarker({
                    position: latLng,
                    map: map,
                    id: i,
                   // icon: getImageMarker(),
                    flat: true,
                    //   anchor: RichMarkerPosition.MIDDLE,
                    content: div,
                    draggable: false,
                    animation: google.maps.Animation.DROP,
                    title: report.reporting_category + ' - ' + report.reporting_incident_location_address
                });
                markers.push(marker);

                (function (marker) {
                    google.maps.event.addListener(marker, "click", function (e) {
                        if (map.getZoom() != 19) {
                            map.setZoom(19);
                            map.setCenter(marker.getPosition());
                        } else {
                            //loadSelected(marker.id);
                        }
                    });

                    google.maps.event.addListener(marker, "dblclick", function (e) {
                       // loadSelected(marker.id);
                    });

                })(marker);*/

        latlngbounds.extend(latLng);
        


        map.setCenter(latlngbounds.getCenter());
        map.setZoom(16);


      
    }






  

    function loadMap() {

        var place_id = "";

        console.log(source,destination);

        document.getElementById("clsDistance").innerHTML = '';
        $('#dvPanel').html('');
        var map = new google.maps.Map(document.getElementById('dvMap'), {
            zoom: 10,
            center: new google.maps.LatLng(-26.2566038, 27.9848897),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: false,
            options: {
                gestureHandling: 'greedy'
            }
        });


       map.data.addGeoJson(mapData);
      
        map.data.setStyle({
            icon: '//example.com/path/to/image.png',
            fillColor: '#000868b8',
            strokeWeight: 1
        });


        map.data.addListener('click', function (event) {
            var bounds = new google.maps.LatLngBounds();
            event.feature.h.h[0].h.forEach(function (coord, index) {
                bounds.extend(coord);
            });
            //  map.setZoom(11);

            map.setCenter(bounds.getCenter());
            map.fitBounds(bounds);
        });
        google.maps.event.addListenerOnce(map, 'bounds_changed', function () {
            map.setZoom(map.getZoom() - 1);
        });

        map.data.addListener('mouseover', function (event) {
            map.data.overrideStyle(event.feature, { fillColor: 'red' });
        });
        map.data.addListener('mouseout', function (event) {
            map.data.overrideStyle(event.feature, { fillColor: '#000868b8' });
        });

        map.data.addListener('mouseover', mouseOverDataItem2);
        map.data.addListener('mouseout', mouseOutOfDataItem2);

        function mouseOverDataItem2(mouseEvent) {
            const titleText = mouseEvent.feature.j.REGIONNAME;
            if (titleText) {
                map.getDiv().setAttribute('title', titleText);
            }
        }

        function mouseOutOfDataItem2(mouseEvent) {
            map.getDiv().removeAttribute('title');
        }


        var directionsService = new google.maps.DirectionsService();
        var directionsDisplay = new google.maps.DirectionsRenderer({ 'draggable': false });
            directionsDisplay.setMap(map);
            directionsDisplay.setPanel(document.getElementById('dvPanel'));
            google.maps.event.addListener(directionsDisplay, 'directions_changed', function () {
            var latlng = directionsDisplay.directions.request.destination;

                if (place_id && place_id != directionsDisplay.directions.geocoded_waypoints[1].place_id) {
                    
                    console.log(directionsDisplay.directions);
               
                var panorama = new google.maps.StreetViewPanorama(
                    document.getElementById('pano'), {
                        position: latlng,
                        pov: {
                            heading: 34,
                            pitch: 10
                        }
                    });
                    map.setStreetView(panorama);
                }
                place_id = directionsDisplay.directions.geocoded_waypoints[1].place_id;

                console.log(latlng);
            });

            google.maps.event.addListener(directionsDisplay, 'click', function (event) {
                console.log(event);

            });


            //*********DIRECTIONS AND ROUTE**********************//


            var request = {
                origin: source,
                destination: destination,
                travelMode: google.maps.TravelMode[travelMode],
                unitSystem: google.maps.UnitSystem.METRIC,
                drivingOptions: {
                    departureTime: new Date(Date.now()),
                    trafficModel: 'optimistic'
                }

            };

           directionsService.route(request, function (response, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    directionsDisplay.setDirections(response);
                    var latlng = response.routes[0].legs[0].end_location;
                    var panorama = new google.maps.StreetViewPanorama(
                        document.getElementById('pano'), {
                            position: latlng,
                            pov: {
                                heading: 34,
                                pitch: 10
                            }
                        });
                 //   map.setStreetView(panorama);
                    console.log(latlng);

                }
                else {
                    if (travelMode == 'DRIVING')
                        $('#cboTravelMode').val('WALKING').trigger('change');
                    else if (travelMode == 'WALKING')
                        $('#cboTravelMode').val('TRANSIT').trigger('change');
                    else if (travelMode == 'TRANSIT')
                        $('#cboTravelMode').val('BICYCLING').trigger('change');
                    else
                        showErrorMessageClose("Directions for '" + travelMode + "' travel mode are not available");
                }
            });



            //*********DISTANCE AND DURATION**********************//
           var service = new google.maps.DistanceMatrixService();
            service.getDistanceMatrix({
                origins: [source],
                destinations: [destination],
                travelMode: google.maps.TravelMode[travelMode],
                unitSystem: google.maps.UnitSystem.METRIC,
                avoidHighways: false,
                avoidTolls: false
            }, function (response, status) {
                if (status == google.maps.DistanceMatrixStatus.OK && response.rows[0].elements[0].status != "ZERO_RESULTS") {
                    var distance = response.rows[0].elements[0].distance.text;
                    var duration = response.rows[0].elements[0].duration.text;
                    document.getElementById("clsDistance").innerHTML = distance + ' <span style="color:green">in</span> ' + duration;
                    console.log(response);

                } else {
                  //   alert("Unable to find the distance via road.");
                }
            });




       
      
        

    }





    $(document).on('click', ".btnWidget2", function () {
        $(document).find(".btnWidget2").removeClass('active');
        $(this).addClass('active');

        seletedTopMap = this.id.replace('_', ' ').replace('_', ' ');
        $('.selectedTitle').html($(this).find('h4').text());
   
        loadMap1();

    });

























    /***************************************************************
                          Map Starts
      ***************************************************************/

    var seletedTopMap = "all";

    var mapData;
    var features = Array();
    var markers = Array();
    var heatmapData = Array();
    var heatmap;

    var map = new google.maps.Map(document.getElementById('dvMap1'), {
        zoom: 11,
        center: new google.maps.LatLng(-26.2566038, 27.9848897),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false,
        options: {
            gestureHandling: 'greedy'
        }
    });

    // Add a style-selector control to the map.
    var styleControl = document.getElementById('style-selector-control');
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(styleControl);

    var styleSelector = document.getElementById('style-selector');
    map.setOptions({ styles: styles[styleSelector.value] });
    map.setTilt(45);
    styleSelector.addEventListener('change', function () {
        if (styleSelector.value == 'satellite') {
            map.setMapTypeId(google.maps.MapTypeId.HYBRID);
            map.setOptions({ styles: null });
        }
        else {
            map.setMapTypeId(google.maps.MapTypeId.ROADMAP);
            map.setOptions({ styles: styles[styleSelector.value] });
        }
    });

    $.getJSON('/Content/files/geo.json', function (data) {
        mapData = data;
        features = map.data.addGeoJson(mapData);

    
    });

    map.data.setStyle({
        icon: '//example.com/path/to/image.png',
        fillColor: '#000868b8',
        strokeWeight: 1
    });

    var styleSelector1 = document.getElementById('style-selector1');
    styleSelector1.addEventListener('change', function () {
        for (var i = 0; i < features.length; i++)
            map.data.remove(features[i]);
        heatmap.setMap(null);
        for (var i = 0; i < markers.length; i++)
            markers[i].setMap(null);

        if (styleSelector1.value == 'All') {
            features = map.data.addGeoJson(mapData);
            heatmap.setMap(map);
            for (var i = 0; i < markers.length; i++)
                markers[i].setMap(map);
        }
        else if (styleSelector1.value == 'Reports') {
            for (var i = 0; i < markers.length; i++)
                markers[i].setMap(map);
        }
        else if (styleSelector1.value == 'Heat') {
            heatmap.setMap(map);
        }
        else if (styleSelector1.value == 'Shape') {
            features = map.data.addGeoJson(mapData);
        }
    });

 
    map.data.addListener('click', function (event) {
        var bounds = new google.maps.LatLngBounds();
        event.feature.h.h[0].h.forEach(function (coord, index) {
            bounds.extend(coord);
        });
        //  map.setZoom(11);

        map.setCenter(bounds.getCenter());
        map.fitBounds(bounds);
        
    });
    google.maps.event.addListenerOnce(map, 'bounds_changed', function () {
        map.setZoom(map.getZoom() - 1);
    });


    map.data.addListener('mouseover', function (event) {
        map.data.overrideStyle(event.feature, { fillColor: 'red' });
    });
    map.data.addListener('mouseout', function (event) {
        map.data.overrideStyle(event.feature, { fillColor: '#000868b8' });
    });

    map.data.addListener('mouseover', mouseOverDataItem);
    map.data.addListener('mouseout', mouseOutOfDataItem);

    function mouseOverDataItem(mouseEvent) {
        const titleText = mouseEvent.feature.j.REGIONNAME;
        if (titleText) {
            map.getDiv().setAttribute('title', titleText);
        }
    }

    function mouseOutOfDataItem(mouseEvent) {
        map.getDiv().removeAttribute('title');
    }

    function loadMap1() {

        clearMarkers();
        heatmapData = Array();


        var latlngbounds = new google.maps.LatLngBounds();
        for (var i = 0; i < allData.length; i++) {

            if (allData[i]['traffic_light_status'] == seletedTopMap || seletedTopMap == "all") {

                var latLng = new google.maps.LatLng(parseFloat(allData[i].traffic_light_latitude)
                    , parseFloat(allData[i].traffic_light_longitude));


                if (allData[i]['traffic_light_status'] == 'boroken')
                    heatmapData.push({ location: latLng, weight: 0.5 });
                else if (allData[i]['traffic_light_status'] == 'unsure')
                    heatmapData.push({ location: latLng, weight: 0.2 });



                var div = document.createElement('div');
                div.className = (allData[i]['traffic_light_status'] == 'working') ? 'ripple-green' : (allData[i]['traffic_light_status'] == 'unsure') ? 'ripple-#000868b8' : 'ripple-red';
                var zindex = (allData[i]['traffic_light_status'] == 'working') ? '10' : (allData[i]['traffic_light_status'] == 'unsure') ? '11' : '12';


                var marker = new RichMarker({
                    position: latLng,
                    map: map,
                    id: i,
                    zIndex: zindex,
                    flat: true,
                    content: div,
                    draggable: false,
                    animation: google.maps.Animation.DROP,
                    title: allData[i].traffic_light_title
                });
                markers.push(marker);

                (function (marker) {
                    google.maps.event.addListener(marker, "click", function (e) {
                        if (map.getZoom() != 18) {
                            map.setZoom(18);
                            map.setCenter(marker.getPosition());
                        } else {
                            loadSelected(marker.id);
                        }
                    });

                    google.maps.event.addListener(marker, "dblclick", function (e) {
                        loadSelected(marker.id);
                    });

                })(marker);
                latlngbounds.extend(marker.position);
            }
        }






        heatmap = new google.maps.visualization.HeatmapLayer({
            data: heatmapData
        });

        heatmap.setMap(map);
        heatmap.set("radius", heatmap.get("radius") ? null : 50);

        const trafficLayer = new google.maps.TrafficLayer();

        trafficLayer.setMap(map);

        //if (allData.length > 0)
        //    map.setCenter(latlngbounds.getCenter());
        //else
        //    map.setCenter(new google.maps.LatLng(-25.7307893, 28.007999));
        //map.setZoom(10);


    }








 


















    function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    }

    function clearMarkers() {
        setMapOnAll(null);
    }

    function showMarkers() {
        setMapOnAll(map);
    }

    function deleteMarkers() {
        clearMarkers();
        markers = [];
    }

















    function vaidateFields(clas) {
        $('.' + clas).css('border-color', 'silver');
        var input;
        $('.' + clas).map(function () {
            if (!$(this).val() || $(this).css('border-color') == 'red') {
                $(this).css('border-color', 'red');
                $(this).focus();
                input = $(this);

                $(this).keyup(function () {
                    if ($(this).val())
                        $(this).css('border-color', 'silver');
                    else
                        $(this).css('border-color', 'red');
                });
            }
        }).get();

        if (input) {
            input.focus();
            return true;
        }
        else
            return false;
    }

    function validateEmail(sEmail, msg) {
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(sEmail)) {
            return true;
        }
        else {
            //var msg = "Please enter valid email address.";
            checkEmptyFields(msg);
            return false;
        }
    }

    function SuccessMessage(msg) {
        $('#myPleaseWait').modal('show');
        $('#txtModalLoaderAlertMessage').hide;
        $('#txtModalLoaderSuccessMessage').show();
        $('#txtModalLoaderSuccessMessage b').html(msg);

        setTimeout(function () {
            $('#myPleaseWait').hide();
            $('#txtModalLoaderSuccessMessage').hide();
            $('#myPleaseWait').modal('hide');
        }, 3000);
    }

    function checkEmptyFields(msg) {

        $('#myPleaseWait').modal('show');
        $('#txtModalLoaderAlertMessage').show();
        $('#txtModalLoaderAlertMessage b').text(msg);

        setTimeout(function () {
            $('#myPleaseWait').hide();
            $('#myPleaseWait').modal('hide');
            $('#txtModalLoaderAlertMessage').hide();
        }, 3000);
    }



    /***************************************************************
                        Validations Ends
    ***************************************************************/



    //Removing  Messages CoOperation Services

    $(document).on('click', function () {
        if ($('#txtModalLoaderAlertMessage').is(':visible')) {
            $('#txtModalLoaderAlertMessage').hide();
        }
        if ($('#txtModalLoaderSuccessMessage').is(':visible')) {
            $('#txtModalLoaderSuccessMessage').hide();
        }

    });






});

