﻿function checkPrev(myPrivs) {

    //-------------------- Check Prev ---------------------------//

    if ($('#lblUserType').text() != 'business') {
        if (myPrivs.indexOf(1002) >= 0) {

        } else {
            //   location.href = '/as_smart_connects/home';
        }
    }

    //-------------------- Check Prev ---------------------------//
}

$(document).ready(function () {



   var http = httpsapi + "/api/reporting/";
    var https = httpsapi + "/api/";
    //http = httpsapi_local + '/api/reporting/';




    $('.actionAdministrator').trigger('click');
    $('.btnUserManagement').find('a').addClass('activeMenu');





    var allData = Array();



    function getVIPs() {


        $('#myLoader').modal('show');


        var details = {
            vip_client_id: $('#lblClientID').text(),
        };

        $.getJSON(http + 'getVIPs', details, function (data) {
            console.log('getVIPs', data);
            allData = data;

            loadData();

            setTimeout(function () {
                $('#myLoader').modal('hide');
            }, 1000);

        });
    }

    getVIPs();










    function getNumber(number) {
        if (number == 0)
            return '<span class="badge badge-danger" style="width:40px">' + number + '</span>';
        else
            return '<span class="badge badge-warning" style="width:40px">' + number + '</span>';
    }


    function getText(text, maxlength) {
        text = text + '';
        if (text.length > maxlength)
            return '<span title="' + text + '">' + text.substring(0, maxlength) +'...</span>';
        else
            return '<span title="' + text + '">' + text+'</span>';
    }


    function getStatus(status) {
        if (status == 'inactive')
            return '<span class="badge badge-warning" style="width:105px">' + status + '</span>';
        if (status == 'active')
            return '<span class="badge badge-success" style="width:105px">' + status + '</span>';
        else
            return '<span class="badge badge-danger" style="width:105px">' + status + '</span>';
    }


    function getButtons(status,i) {
        if (status != 'deleted') {
            return '<button id="b' + i + '" style="width:70px" ' +
                'type="button" class="btn btn-primary btn-flat btn-xs btnView pull-right">View</button>'+
                '<button id="d' + i + '" style="width:70px" ' +
                'type="button" class="btn btn-danger btn-flat btn-xs btnRemove pull-right">Delete</button>';
        }
        else {
            return '<button id="u' + i + '" style="width:140px" ' +
                'type="button" class="btn btn-primary btn-flat btn-xs btnUndo pull-right">Undo Delete</button>';
        }
    }
    
    

    var selectedIdAns;
    var selectedId;
    function loadData() {
        if (allData) {
            var str = "";
            for (i = 0; i < allData.length; i++) {
                str += '<tr><td>' + (i + 1) + '</td>';
                str += '<td>' + removeNulls(allData[i]['registered_vip_title'] + ' ' + allData[i]['registered_vip_name'] + ' ' + allData[i]['registered_vip_surname']) + '</td>';
                str += '<td>' + removeNulls(allData[i]['registered_vip_cellphone']) + '</td>';
                str += '<td>' + removeNulls(allData[i]['registered_vip_email']) + '</td>';
                str += '<td>' + allData[i]['registered_vip_status'] + '</td>';
                str += '<td>' + allData[i]['registered_vip_level'] + '</td>';
                str += '<td>' + getDatee(allData[i]['registered_vip_timestamp']) + '</td>';
                str += '<td>' + getButtons(allData[i]['registered_vip_status'],i) + '</td>';
            }
            $('#tblVIPs').html(str);
        }


      


        $('.btnView').click(function () {
            var i = this.id.substring(1);
            loadSelected(i);
        });

        $('.btnUndo').click(function () {
            selectedId = this.id.substring(1);
            $('#clsComment').val(removeNulls(allData[selectedId]['registered_vip_email']) + "   " + removeNulls(allData[selectedId]['registered_vip_cellphone'])
                + "   " + removeNulls(allData[selectedId]['registered_vip_title'] + ' ' + allData[selectedId]['registered_vip_name']
                    + ' ' + allData[selectedId]['registered_vip_surname']));
            vip_status = allData[selectedId]['registered_vip_description']+'';
            $('.clsTitle').html('Undo Delete');
            $('.clsMsg').html('Are you sure you want to <b style="color:green">recover</b> this contact?');
            $('.confirmModal').modal('show');
        });


        $('.btnRemove').click(function () {
            selectedId = this.id.substring(1);
            $('#clsComment').val(removeNulls(allData[selectedId]['registered_vip_email']) + "   " + removeNulls(allData[selectedId]['registered_vip_cellphone'])
                + "   " + removeNulls(allData[selectedId]['registered_vip_title'] + ' ' + allData[selectedId]['registered_vip_name']
                + ' ' + allData[selectedId]['registered_vip_surname']));

            vip_status = "deleted";
            allData[selectedId]['registered_vip_description'] = allData[selectedId]['registered_vip_status']+'';
            $('.clsTitle').html('Delete contact');
            $('.clsMsg').html('Are you sure you want to <b style="color:red">delete</b> this contact?');
            $('.confirmModal').modal('show');
        });

    }



    function loadSelected(i) {
        selectedId = i; 
        saveType = 'update';
        
        $('#viewModal').modal('show');
        $('.clsBtnStatus').hide();
        $('.footer').hide();

        $('#cboType').val(allData[i]['registered_vip_level']);
        $('.clsStatus').html(getStatus(allData[i]['registered_vip_status']));
        $('#txtCellNumber').val(allData[i]['registered_vip_cellphone']);
        $('#txtEmailAddress').val(allData[i]['registered_vip_email']);
        $('#cboTitle').val(allData[i]['registered_vip_title']);
        $('#txtName').val(allData[i]['registered_vip_name']);
        $('#txtSurname').val(allData[i]['registered_vip_surname']);

        if (allData[i]['registered_vip_status'] == 'active')
            $('.btnInactive').show();
        else
            $('.btnActive').show();
 
    }






    var saveType = '';


    $('#btnAddNew').click(function () {
        saveType = 'save';
        $('#viewModal').modal('show');
    });




    $('.btnClose').click(function () {
        $('#viewModal').modal('hide');
        $('.footer').show('slow');
       
    });





    var vip_status = '';


  
    $('#btnYesConfirm').click(function () {
    
        updatePrepareStatus(vip_status);
        $('.confirmModal').modal('hide');
      
    });




    $('#btnSaveVIP').click(function () {
        if (!vaidateFields('required')) {
            if (saveType == 'save')
                saveVIP();
            else
                updatePrepare();
        }
    });





    function saveVIP() {

        var obj = {
            'registered_vip_client_id': $('#lblClientID').text(),
            'registered_vip_cellphone': $('#txtCellNumber').val().trim(),
            'registered_vip_email': $('#txtEmailAddress').val().trim(),
            'registered_vip_level': $('#cboType').val().trim(),
            'registered_vip_title': $('#cboTitle').val().trim(),
            'registered_vip_name': $('#txtName').val().trim(),
            'registered_vip_surname': $('#txtSurname').val().trim()
        }

        $('#myLoader').modal('show');
        $.post(http + 'saveVIP', obj, function (data) {
            console.log(data);
            if (data.indexOf('registered_vip_id') >= 0) {
                allData.push(JSON.parse(data));
                showSuccessMessageClose('Success');
                $('#viewModal').modal('hide');
                loadData();
            } else {
                showErrorMessageClose(data);
            }
            $('#myLoader').modal('hide');
        });
    }


    function updateVIP(obj) {

        $('#myLoader').modal('show');
        $.post(http + 'updateVIP', obj, function (data) {
            console.log(data);
            if (data.indexOf('success') >= 0) {
                allData[selectedId]['registered_vip_cellphone'] = obj['registered_vip_cellphone'];
                allData[selectedId]['registered_vip_email'] = obj['registered_vip_email'];
                allData[selectedId]['registered_vip_level'] = obj['registered_vip_level'];
                allData[selectedId]['registered_vip_title'] = obj['registered_vip_title'];
                allData[selectedId]['registered_vip_name'] = obj['registered_vip_name'];
                allData[selectedId]['registered_vip_surname'] = obj['registered_vip_surname'];
                allData[selectedId]['registered_vip_status'] = obj['registered_vip_status'];


                showSuccessMessageClose('Success');
                $('#viewModal').modal('hide');
                $('.footer').show('slow');
                loadData();
            } else {
                showErrorMessageClose(data);
            }
            $('#myLoader').modal('hide');
        });
    }


    function updatePrepare() {

        var obj = JSON.parse(JSON.stringify(allData[selectedId]));
        obj['registered_vip_cellphone'] = $('#txtCellNumber').val().trim();
        obj['registered_vip_email'] = $('#txtEmailAddress').val().trim();
        obj['registered_vip_level'] = $('#cboType').val().trim();
        obj['registered_vip_title'] = $('#cboTitle').val().trim();
        obj['registered_vip_name'] = $('#txtName').val().trim();
        obj['registered_vip_surname'] = $('#txtSurname').val().trim();

        updateVIP(obj);
    }

    function updatePrepareStatus(status) {

        var obj = JSON.parse(JSON.stringify(allData[selectedId]));
        obj['registered_vip_status'] = status;

        updateVIP(obj);
    }








 




 





















    function vaidateFields(clas) {
        $('.' + clas).css('border-color', 'silver');
        var input;
        $('.' + clas).map(function () {
            if (!$(this).val() || $(this).css('border-color') == 'red') {
                $(this).css('border-color', 'red');
                $(this).focus();
                input = $(this);

                $(this).keyup(function () {
                    if ($(this).val())
                        $(this).css('border-color', 'silver');
                    else
                        $(this).css('border-color', 'red');
                });
            }
        }).get();

        if (input) {
            input.focus();
            return true;
        }
        else
            return false;
    }

    function validateEmail(sEmail, msg) {
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(sEmail)) {
            return true;
        }
        else {
            //var msg = "Please enter valid email address.";
            checkEmptyFields(msg);
            return false;
        }
    }

    function SuccessMessage(msg) {
        $('#myPleaseWait').modal('show');
        $('#txtModalLoaderAlertMessage').hide;
        $('#txtModalLoaderSuccessMessage').show();
        $('#txtModalLoaderSuccessMessage b').html(msg);

        setTimeout(function () {
            $('#myPleaseWait').hide();
            $('#txtModalLoaderSuccessMessage').hide();
            $('#myPleaseWait').modal('hide');
        }, 3000);
    }

    function checkEmptyFields(msg) {

        $('#myPleaseWait').modal('show');
        $('#txtModalLoaderAlertMessage').show();
        $('#txtModalLoaderAlertMessage b').text(msg);

        setTimeout(function () {
            $('#myPleaseWait').hide();
            $('#myPleaseWait').modal('hide');
            $('#txtModalLoaderAlertMessage').hide();
        }, 3000);
    }



    /***************************************************************
                        Validations Ends
    ***************************************************************/



    //Removing  Messages CoOperation Services

    $(document).on('click', function () {
        if ($('#txtModalLoaderAlertMessage').is(':visible')) {
            $('#txtModalLoaderAlertMessage').hide();
        }
        if ($('#txtModalLoaderSuccessMessage').is(':visible')) {
            $('#txtModalLoaderSuccessMessage').hide();
        }

    });






});

