﻿
var myPrivs;
var myModules;

$(document).ready(function () {


    $(".Users").click(function () {
        setCookie('type', 'Users', 999);
        location.href = '/jrafixit/users';
    });
    $(".Roles").click(function () {
        setCookie('type', 'Roles', 999);
        location.href = '/jrafixit/roles';
    });

    $(".TrafficLights").click(function () {
        setCookie('type', 'TrafficLights', 999);
        location.href = '/jrafixit/TrafficLights';
    });

    $(".FAQs").click(function () {
        setCookie('type', 'FAQs', 999);
        location.href = '/jrafixit/FAQs';
    });

    $(".VIPs").click(function () {
        setCookie('type', 'VIPs', 999);
        location.href = '/jrafixit/VIPs';
    });



    $(".Categories").click(function () {
        setCookie('type', 'Categories', 999);
        location.href = '/jrafixit/Categories';
    });


    $(".ReportedIssues").click(function () {
        setCookie('type', 'ReportedIssues', 999);
        location.href = '/jrafixit/ReportedIssues';
    });

    $(".Comments").click(function () {
        setCookie('type', 'Comments', 999);
        location.href = '/jrafixit/Comments';
    });

    $(".Settings").click(function () {
        setCookie('type', 'Settings', 999);
        location.href = '/jrafixit/Settings';
    });

    $(".Dashboards").click(function () {
        setCookie('type', 'Dashboards', 999);
        location.href = '/jrafixit/Dashboards';
    });
    $(".SupportDashboards").click(function () {
        setCookie('type', 'SupportDashboards', 999);
        location.href = '/jrafixit/SupportDashboards';
    });
    $(".DataReports").click(function () {
        setCookie('type', 'DataReports', 999);
        location.href = '/jrafixit/DataReports';
    });

    $(".Reporting").click(function () {
        setCookie('type', 'Reporting', 999);
        location.href = '/jrafixit/Reporting';
    });
    $(".Inspections").click(function () {
        setCookie('type', 'Assessments', 999);
        location.href = '/jrafixit/Inspections';
    });
    $(".Fixings").click(function () {
        setCookie('type', 'Fixings', 999);
        location.href = '/jrafixit/Fixings';
    });
    $(".Confirmations").click(function () {
        setCookie('type', 'Confirmations', 999);
        location.href = '/jrafixit/Confirmations';
    });






    $(".Regions").click(function () {
        setCookie('type', 'Regions', 999);
        location.href = '/Organisation/Regions';
    });
    
    $(".Department").click(function () {
        setCookie('type', 'Department', 999);
        location.href = '/Organisation/Department';
    });
    
    $(".Team").click(function () {
        setCookie('type', 'Team', 999);
        location.href = '/Organisation/Team';
    });

    $(".Suppliers").click(function () {
        setCookie('type', 'Suppliers', 999);
        location.href = '/Organisation/Suppliers';
    });







    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }



});